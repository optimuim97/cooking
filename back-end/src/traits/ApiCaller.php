<?php

namespace App\traits;

use Symfony\Contracts\HttpClient\HttpClientInterface;


trait ApiCaller
{

    public function __construct(private HttpClientInterface $client)
    {
    }

    public function getRecipes($path = "get-recipes")
    {
        $req = $this->client->request(method: 'GET', url: "http://127.0.0.1:8000/$path");
        $data = json_decode($req->getContent());

        if (isset($data->status_code)) {
            if ($data->status_code == 302) {
                return $data;
            } else {
                return null;
            }
        }
    }

    public function getRecipeTypes($path = "get-recipe_types")
    {
        $req = $this->client->request(method: 'GET', url: "http://127.0.0.1:8000/$path");
        $data = json_decode($req->getContent());

        if (isset($data->status)) {
            if ($data->status == 302 or $data->status == 200) {
                return $data;
            } else {
                return null;
            }
        }
    }

    public function getRestaurants($path = "get-restaurants")
    {
        $req = $this->client->request(method: 'GET', url: "http://127.0.0.1:8000/$path");
        $data = json_decode($req->getContent());

        if (isset($data->status)) {
            if ($data->status == 302 or $data->status == 200) {
                return $data;
            } else {
                return null;
            }
        }
    }

    public function getCookingTricks($path = "get-cooking_tricks")
    {
        $req = $this->client->request(method: 'GET', url: "http://127.0.0.1:8000/$path");
        $data = json_decode($req->getContent());

        if (isset($data->status)) {
            if ($data->status == 302 or $data->status == 200) {
                return $data;
            } else {
                return null;
            }
        }
    }

    /* 
    public function getCookingEvents($path = "get-cooking_events")
    {
        $req = $this->client->request(method: 'GET', url: "http://127.0.0.1:8000/$path");
        $data = json_decode($req->getContent());

        if (isset($data->status)) {
            if ($data->status == 302 or $data->status == 200) {
                return $data;
            } else {
                return null;
            }
        }
    } */

    public function getCookingCourse($path = "get-courses")
    {
        $req = $this->client->request(method: 'GET', url: "http://127.0.0.1:8000/$path");
        $data = json_decode($req->getContent());

        if (isset($data->status)) {
            if ($data->status == 302 or $data->status == 200) {
                return $data;
            } else {
                return null;
            }
        }
    }

    public function getCookingEvents($path){
        $path = "/api/v1/evenements";

        $annonce_url = $_ENV['ANNONCE_URL'];        
        $req = $this->client->request(method: 'GET', url: "$annonce_url/$path");
        $data = json_decode($req->getContent());

        if (isset($data->status)) {
            if ($data->status == 302 or $data->status == 200) {
                return $data;
            } else {
                return null;
            }
        }
    }
}
