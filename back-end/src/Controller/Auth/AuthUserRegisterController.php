<?php

namespace App\Controller\Auth;

use App\Services\RegisterService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthUserRegisterController extends AbstractController
{
    private $registerUser;

    public function __construct(RegisterService $registerUser)
    {
        $this->registerUser = $registerUser;
    }

    #[Route('/auth/sign-up', name: 'app_auth_user_register', methods: ['POST'])]
    public function signUp(Request $request): Response
    {
        $data = $request->request->all();
        
        if($data == []){
            $data = json_decode($request->getContent(), true);
        }

        return $this->registerUser->addUser($data);
    }

    /**
   * @param UserInterface $user
   * @param JWTTokenManagerInterface $JWTManager
   * @return JsonResponse
   */
    public function getTokenUser(UserInterface $user, JWTTokenManagerInterface $JWTManager)
    {
        return new JsonResponse(['token' => $JWTManager->create($user)]);
    }
}
