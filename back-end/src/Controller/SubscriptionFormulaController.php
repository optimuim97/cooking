<?php

namespace App\Controller;

use App\Entity\SubscriptionFormula;
use App\Helpers\SerializerHelper;
use App\Repository\SubscriptionFormulaRepository;
use App\Repository\SubscriptionTypeRepository;
use Carbon\Carbon;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


class SubscriptionFormulaController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private SubscriptionFormulaRepository $subscriptionFormulaRepo,
        private SubscriptionTypeRepository  $subscriptionTypeRepo,
        private SerializerInterface $serializer
    )
    {
    }

    #[Route('api/add-subscription_formula', name: 'app_subscription_formula', methods: ["POST"])]
    public function add(Request $request): Response
    {
        $data = $request->request->all();

        $subscriptionFormula = new SubscriptionFormula();

        $subscriptionFormula->setTitle($data["title"])
            ->setDescription($data["description"])
            ->setPrice($data["price"])
            ->setImageUrl("https://i.imgur.com/f0W33fk.png")
            ->setActif(true)
            ->setExpirationDate(Carbon::now()->add(1, 'month'))
            ->setStartDate(Carbon::now())
            ->setIsPromo(true)
            ->setPromotionPrice(0)
            ->setUnit($data["unit"] ?? "1")
            ->setDuration($data["duration"])
            ->setCurrencyCode($data["currency_code"] ==""||$data["currency_code"]==null? "XOF" : $data["currency_code"])
            ->setCurrencyName($data["currency_name"] ==""||$data["currency_name"]==null? "FCFA" : $data["currency_name"])
            ->setAvantages($data["avantages"] ?? "Non défini");

        $this->em->persist($subscriptionFormula);
        $this->em->flush();

        return $this->json([
            "data" => $subscriptionFormula,
            "message" => "Created",
            "status_code" => Response::HTTP_CREATED
        ], 200);
    }

    #[Route('get-subscription_formulas', name: 'app_subscription_formulas', methods: ["GET"])]
    public function getSubscriptions()
    {
        $subscriptionFormula = $this->subscriptionFormulaRepo->findBy(["actif"=> 1]);

        if (!empty($subscriptionFormula)) {

            $subscriptionFormulaJson = SerializerHelper::getJson($this->serializer,$subscriptionFormula, "show_subscription_formula");

            return $this->json([
                "data" => $subscriptionFormulaJson,
                "message" => "All subscriprion Formulas",
                "status_code" => Response::HTTP_OK
            ], 200);

        } else {

            return $this->json([
                "message" => "KO",
                "status_code" => Response::HTTP_NO_CONTENT
            ], 404);
            
        }
    }

    #[Route('update-subscription_formula/{id}', name: "update_subscription_formula", methods: ["PUT"])]
    public function updateSubscriptionFormula($id, Request $request)
    {

        $data = $request->request->all();
        $subscriptionFormula = $this->subscriptionFormulaRepo->find($id);

        if(isset($data["subscription_type_id"])){
            
            if(!empty($data["subscription_type_id"])){
                $subscriptionType = $this->subscriptionTypeRepo->find($data["subscription_type_id"]);
            }

        }else{
            $subscriptionType = $subscriptionFormula->getSubscriptionType();
        }

        if ($subscriptionFormula) {

            $title = $data["title"] ?? $subscriptionFormula->getTitle();
            $description = $data["description"] ?? $subscriptionFormula->getDescription();
            $price = $data["price"] ?? $subscriptionFormula->getPrice();
            $avantages = $data["avantages"] ?? $subscriptionFormula->getAvantages();
            $duration = $data["duration"] ?? $subscriptionFormula->getDuration();
            $currencyCode = $data["currency_code"] ?? $subscriptionFormula->getCurrencyCode();
            $currencyName = $data["currency_name"] ?? $subscriptionFormula->getCurrencyName();
            $unit = $data["unit"] ?? $subscriptionFormula->getUnit();
            
            $subscriptionFormula->setTitle($title)
                ->setDescription($description)
                ->setPrice($price)
                ->setImageUrl("https://i.imgur.com/f0W33fk.png")
                ->setActif(true)
                ->setExpirationDate(Carbon::now()->add(1, $duration))
                ->setStartDate(Carbon::now())
                ->setIsPromo(true)
                ->setCurrencyCode($currencyCode == "" ? "XOF" : $currencyCode)
                ->setCurrencyName($currencyName == "" ? "FCFA" : $currencyName)
                ->setDuration($duration)
                ->setPromotionPrice(0)
                ->setAvantages($avantages)
                ->setUnit($unit)
                ->setSubscriptionType($subscriptionType);                

            $this->em->persist($subscriptionFormula);
            $this->em->flush();

            $subscriptionFormulaJson = SerializerHelper::getJson($this->serializer,$subscriptionFormula, "show_subscription_formula");

            return $this->json([
                "data" => $subscriptionFormulaJson,
                "message" => "updated",
                "status_code" => Response::HTTP_OK
            ], 200);

        } else {

            return $this->json([
                "message" => "NO FOUND",
                "status_code" => Response::HTTP_INTERNAL_SERVER_ERROR
            ], 500);
            
        }
    }

    #[Route('show_subscription_formula/{slug}', name: "show_subscription_formula", methods: ["GET"])]
    public function getSubscriptionFormula($slug)
    {
        $subscriptionFormula = $this->subscriptionFormulaRepo->findOneBy(["slug"=> $slug]);

        $subscriptionFormulaJson = SerializerHelper::getJson($this->serializer,$subscriptionFormula, "show_subscription_formula");

        return $this->json([
            "data" => $subscriptionFormulaJson,
            "message" => "updated",
            "status_code" => Response::HTTP_OK
        ], 200);
    }
    
}
