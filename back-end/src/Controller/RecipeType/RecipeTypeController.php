<?php

namespace App\Controller\RecipeType;

use App\Services\RecipeTypeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecipeTypeController extends AbstractController
{
    public $recipeTypeService;

    public function __construct(RecipeTypeService $recipeTypeService)
    {
        $this->recipeTypeService = $recipeTypeService;
    }

    #[Route('add-recipe_type', name: 'app_recipe_type', methods: ["POST"])]
    public function add(Request $request): Response
    {
        $data = $request->request->all();
        
        if($data == []){
            $data = json_decode($request->getContent(), true);
        }

        $file = $request->files->get('image');

        return $this->json($file);

        return $this->recipeTypeService->add($data, $file);
    }

    #[Route('/api/delete-recipe_type/{id}', name: 'delete_recipe_type', methods: ["DELETE"])]
    public function deleteRecipe($id): Response
    {
        return $this->recipeTypeService->delete($id);
    }

    #[Route('/show-recipe_type/{slug}', name: 'show_recipe_type', methods: ["GET"])]
    public function showRecipeType($slug): Response
    {
        return $this->recipeTypeService->show($slug);
    }

    #[Route('/api/update-recipe_type/{id}', name: 'update_recipe_type', methods: ["POST"])]
    public function updateRecipeType($id, Request $request): Response
    {
        $data = $request->request->all();
        $file = $request->files->get('image_url');
        return $this->recipeTypeService->update($id, $data, $file);
    }

    #[Route('get-recipe_types', name: 'get_recipe_types', methods: ["POST"])]
    public function getAll(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if($data == null){
            $data = $request->request->all();
        }

        return $this->recipeTypeService->getAll($data);
    }

    #[Route('get_all_recipe_types', name: 'get_recipe_types_all', methods: ["GET"])]
    public function getAllRT()
    {
        return $this->recipeTypeService->getAllRT();
    }

    #[Route('get-all-recipe_types', name: 'get_all_recipe_types_for_dash', methods: ["GET"])]
    public function getAllRecipeTypeForDash()
    {
        return $this->recipeTypeService->getAllRecipeTypeForDash();
    }
}
