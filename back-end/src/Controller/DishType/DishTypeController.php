<?php

namespace App\Controller\DishType;

use App\Services\DishTypeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DishTypeController extends AbstractController
{

    public function __construct(private DishTypeService $dishTypeService)
    {
    }

    #[Route('api/add-dish_type', name: 'app_dish_type', methods: ["POST"])]
    public function add(Request $request): Response
    {

        $data = $request->request->all();

        return $this->dishTypeService->add($data);
    }

    #[Route('/api/delete-dish_type/{id}', name: 'delete_dish_type', methods: ["DELETE"])]
    public function deleteRecipe($id): Response
    {
        return $this->dishTypeService->delete($id);
    }

    #[Route('/show-dish_type/{id}', name: 'show_dish_type', methods: ["GET"])]
    public function showRecipeType($id): Response
    {
        return $this->dishTypeService->show($id);
    }

    #[Route('/api/update-dish_type/{id}', name: 'update_dish_type', methods: ["POST"])]
    public function updateRecipeType($id, Request $request): Response
    {
        $data = $request->request->all();
        $file = $request->files->get('image_url');
        return $this->dishTypeService->update($id, $data, $file);
    }

    #[Route('get-dish_types', name: 'get_dish_types', methods: ["GET"])]
    public function getAll()
    {
        return $this->dishTypeService->getAll();
    }
}
