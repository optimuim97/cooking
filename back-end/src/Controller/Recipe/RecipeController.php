<?php
namespace App\Controller\Recipe;

use App\Services\RecipeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecipeController extends AbstractController
{
    private $recipeService;

    public function __construct(RecipeService $recipeService)
    {
        $this->recipeService = $recipeService;
    }

    #[Route('/api/add-recipe', name: 'app_recipe', methods: ["POST"])]
    public function addRecipe(Request $request): Response
    {
        $data = $request->request->all();
        $files = $request->files;

        return $this->recipeService->add($data, $files);
    }

    #[Route('/api/delete-recipe/{id}', name: 'delete_recipe', methods: ["DELETE"])]
    public function deleteRecipe($id): Response
    {
        return $this->recipeService->delete($id);
    }

    #[Route('/show-recipe/{slug}', name: 'show_recipe', methods: ["GET"])]
    public function showRecipe($slug): Response
    {
        return $this->recipeService->show($slug);
    }

    #[Route('/api/update-recipe/{id}', name: 'update_recipe', methods: ["POST"])]
    public function updateRecipe(Request $request, $id): Response
    {
        $data = $request->request->all();
        $files = $request->files;
        return $this->recipeService->update($id,$data ,$files);
    }

    #[Route('/get-recipes', name: 'get_recipes', methods: ["POST"])]
    public function getRecipes(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        return $this->recipeService->getAll($data);
    }


    #[Route('/get-recipes-by-type/{slug}', name: 'get_by_cat_recipes', methods: ["POST"])]
    public function getRecipesByType($slug, Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        return $this->recipeService->getByType($slug, $data);
    }

    #[Route('api/like-recipe/{id}', name: 'app_like_recipes', methods: ["GET"])]
    public function likeRecipe($id): Response
    {
        $user = $this->getUser();
        return $this->recipeService->likeOrDislike($id, $user);
    }


    #[Route('api/comment-recipe/{slug}', name: 'app_comment_recipes', methods: ["POST"])]
    public function commentRecipe($slug, Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        // $data = $request->request->all();
        return $this->recipeService->commentRecipe($slug, $data);
    }

    #[Route('/api/update-comment/{recipe_id}/{comment_id}', name: 'app_update_comment_recipes', methods: ["POST"])]
    public function updateCommentRecipe($recipe_id, $comment_id, Request $request): Response
    {
        $data = $request->request->all();
        return $this->recipeService->updateCommentRecipe($recipe_id, $comment_id, $data);
    }

    #[Route('/api/delete-comment/{recipe_id}/{comment_id}', name: 'app_delete_comment_recipes', methods: ["PATCH"])]
    public function deleteCommentRecipe($recipe_id, $comment_id): Response
    {
        return $this->recipeService->deleteCommentRecipe($recipe_id, $comment_id);
    }

    #[Route('/get-recipe_types/spot_light', name: 'get_recipes_spot_light', methods: ["GET"])]
    public function getRecipeSpotLight(): Response
    {
        return $this->recipeService->getRecipeSpotLight();
    }

    // FILTERS
    #[Route('api/get-recipe-by-level/{id}', name: 'app_get_recipes_by_level', methods: ["GET"])]
    public function getRecipeByLevel($id): Response
    {
        return $this->recipeService->getByLevel($id);
    }

    #[Route('get-recipe-by-name/{keyword}', name: 'app_get_recipes_by_name', methods: ["GET"])]
    public function getRecipeByName($keyword): Response
    {
        return $this->recipeService->getRecipeByName($keyword);
    }

    #[Route('api/add-to-recipe-to-bookmaker/{id}', name: 'app_add_recipe_to_book_maker', methods: ["GET"])]
    public function addRecipeToBookMaker($id): Response
    {
        return $this->recipeService->addRecipeToBookMark($id);
    }

    #[Route('api/get-recipe-bookmaker', name: 'app_get_recipe_book_maker', methods: ["GET"])]
    public function getRecipeBookMark(): Response
    {
        return $this->recipeService->getRecipeBookMark();
        
    }

    #[Route('get-recipes-count', name: 'app_get_all_recipe_count', methods: ["GET"])]
    public function getRecipeAllCount(): Response
    {
        return $this->recipeService->getRecipeAll();
    }

    #[Route('get-recipe-is-spotlight', name: 'app_get_is_spot_ligth_recipes_by_name', methods: ["GET"])]
    public function getIsSpotLight()
    {
        return $this->recipeService->getIsSpotifiyRecipe();
    }

    #[Route('get-all-recipes', name: 'app_get_all_recipe', methods: ["GET"])]
    public function getAllRecipeForDash()
    {
        return $this->recipeService->getAllRecipeForDash();
    }

}
