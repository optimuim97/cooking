<?php

namespace App\Controller;

use App\Entity\CustomLogSaver;
use App\Entity\Order;
use App\Entity\OrderDelivery;
use App\Entity\OrderItems;
use App\Entity\OrderPaymentStatus;
use App\Entity\OrderStatus;
use App\Entity\WebSite;
use App\Helpers\SerializerHelper;
use App\Helpers\Validation;
use App\Repository\DishRepository;
use App\Repository\OrderRepository;
use App\Repository\RestaurantRepository;
use App\Repository\TransactionsRepository;
use App\Services\RestaurantService;
use App\Services\SendMail;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private RestaurantRepository $restoRepo,
        private SerializerInterface $serializer,
        private DishRepository $dishRepo,
        private ValidatorInterface $validator,
        private RestaurantService $restoService,
        private OrderRepository $orderRepo,
        private TransactionsRepository $transactionRepo,
        private SendMail $mailer
    )
    {}

    #[Route('/api/make-orders', name: 'app_order', methods: ["POST"])]
    public function OrderDish(Request $request)
    {

        $data = json_decode($request->getContent());

        $orderItemsFront = $data->orderItems;
        $params = $data->params;

        $user = $this->getUser();
        $currencyCode =  "XOF";
        $currencyName =  "FCFA";
        $amount = 0;

        //WAITING
        $orderStatus = 1;
        $tax = 1;
        $reference = WebSite::generateReference();
        $restaurantId =  $params->restaurant?->id;
        $isPaid = false;

        $resto = $this->restoRepo->find($restaurantId);

        //ORDER
            $order = new Order();
            $order
                ->setCommandeType(Order::ORDER_TYPE_RESTO)
                ->setUser($user)
                ->setIsWaiting(true)
                ->setIsPaid(false)
                ->setCurrencyCode($currencyCode)
                ->setCurrencyName($currencyName)
                ->setReference($reference)
                ->setIsPaid($isPaid)
                ->setOrderStatus($orderStatus)
                ->setOrderStatusStr(OrderStatus::WAITING)
                ->setResto($resto)
                ->setAmount($amount)
                ->setTax($tax)
                // ->setCommandeType()
                ->setTotalAmount($amount * $tax);

            foreach ($orderItemsFront as $item) {
                $amount += $item->prix;
                $dish = $this->dishRepo->find($item->id);
                
                //OrderItems
                $orderItem = new OrderItems();
                
                $orderItem
                    ->setUnitPrice($item->prix)
                    ->setMetaData(json_encode($item))
                    ->setQuantity($item->quantity ?? 1)
                    ->setQuantityUnit('')
                    ->setCommande($order)
                    ->setCreatedAt(new \DateTimeImmutable())
                    ->setDish($dish);
                
                $dish->setOrderItems($orderItem);

                $this->em->persist($orderItem);
                $this->em->flush();

                $order->addOrderItem($orderItem);

            }

            $order->setAmount($amount);
            $order->setTotalAmount($amount * $tax);

            $errors = $this->validator->validate($order);

            if (count($errors) > 0) {
                $hasError = Validation::check($errors);

                if ($hasError != null) {
                    return $this->json([
                        "errors" => $hasError,
                        "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                        "message" => Response::$statusTexts['422']
                    ], 422);
                }
            }

            //OrderDelivery
            $address = $params->deliveryAddressName;

            $orderDelivery = new OrderDelivery();
            $orderDelivery
                    ->setCommande($order)
                    ->setAddressName($address)
                    ->setCity('empty')
                    ->setTown('empty');

            $this->em->persist($orderDelivery);
            $this->em->flush();

            $order->setOrderDelivery($orderDelivery);
            
            $this->em->persist($order);
            $this->em->flush();
            
            $orderJson = SerializerHelper::getJson($this->serializer, $order, "show_order");

            $bodyMail = $this->mailer->createBodyMail('mails/commande.html.twig', ['user' => $user, "order"=> $orderJson]);
            $this->mailer->sendEmail(from: 'yemakarios70@gmail.com', to: $user->getEmail(), subject: 'Détails de Commande ', body: $bodyMail);

            return $this->json([
                "message" => "OK",
                "status_code" => 200,
                "data" => $orderJson
            ], Response::HTTP_OK);

    }

    #[Route('/api/cancel-orders/{id}')]
    public function cancelOrder($id){
        return $this->restoService->cancelOrder($id);    
    }

    #[Route('web-hooks',  methods: ["POST"])]
    public function callBackUrl(Request $request){
        
        try{

            $data = $request->request->all();

            if($data == []){
                $data = json_decode($request->getContent(), true);
            }

            $order_ref_apaym = $data["id_order"];
            $reference = $data["id_transaction"];

            //Get Order
            $order = $this->orderRepo->findOneBy(["reference" => $reference]);
            $transaction = $this->transactionRepo->findOneBy(["reference" => $reference]);
            
            if($data["status_order"] == "success"){
                
                if(!empty($order)){

                    if($order?->getSubscription()?->getSubscriptionFormula()?->getSlug() == "cooker"){

                        $user = $order->getUser();
                        $user->setIsCooker(true);

                        $this->em->persist($user);
                        $this->em->flush();

                    }

                    $order->setIsPaid(true);
                    $order->setIsWaiting(false);
                    $order->setOrderPaymentStatus(OrderPaymentStatus::IS_PAID);

                    $this->em->persist($order);
                    $this->em->flush();

                }

                if(!empty($transaction)){

                    $transaction->setIsPaid(true);
                    $transaction->setExternalReference($order_ref_apaym);

                    $this->em->persist($transaction);
                    $this->em->flush();

                }

            }elseif($data["status_order"] == "failed"){

                if(!empty($order)){

                    $order->setIsPaid(false);
                    $order->setOrderPaymentStatus(OrderPaymentStatus::IS_NOT_PAID);
                    $order->setIsWaiting(true);
                    $order->setReferenceApaym($order_ref_apaym);
                    $this->em->persist($order);
                    $this->em->flush();

                }

                if(!empty($transaction)){
                    
                    $transaction->setIsPaid(true);
                    $transaction->setExternalReference($order_ref_apaym);
                    $this->em->persist($transaction);
                    $this->em->flush();

                }

            }elseif($data["status_order"] == "interrupted"){

                if(!empty($order)){

                    $order->setIsPaid(false);
                    $order->setIsWaiting(true);
                    $order->setOrderPaymentStatus(OrderPaymentStatus::IS_NOT_PAID);
                    $order->setReferenceApaym($order_ref_apaym);
                    $this->em->persist($order);
                    $this->em->flush();

                }

                if(!empty($transaction)){

                    $transaction->setIsPaid(true);
                    $this->em->persist($transaction);
                    $this->em->flush();

                }

            }else{
                
                if(!empty($order)){

                    $order->setIsPaid(false);
                    $order->setIsWaiting(true);
                    $order->setOrderPaymentStatus(OrderPaymentStatus::IS_NOT_PAID);
                    $order->setReferenceApaym($order_ref_apaym);
                    $this->em->persist($order);
                    $this->em->flush();

                }

                if(!empty($transaction)){

                    $transaction->setIsPaid(true);
                    $transaction->setExternalReference($order_ref_apaym);
                    $this->em->persist($transaction);
                    $this->em->flush();

                }
            }

            $orderJSON = SerializerHelper::getJson($this->serializer, $order, "show_order");
            $transactionJSON = SerializerHelper::getJson($this->serializer, $transaction, "show_transaction");

            return $this->json([
                "order" => $orderJSON,
                "transaction" => $transactionJSON
            ]);

        }catch(Exception $e){

            $log = new CustomLogSaver();
            $log->setCode($e->getCode());
            $log->setMessage($e->getMessage());
            
            $this->em->persist($log);
            $this->em->flush();

            return $this->json([
                "message" => $e->getMessage(),
                "code" => $e->getCode(),
            ]);

        }

    }

    #[Route('api/mark-as-treated/{id}',  methods: ["GET"])]
    public function markAsTreated($id){
        
        try{

            $order = $this->orderRepo->find($id);
            $user = $order->getUser();

            $orderJson = SerializerHelper::getJson($this->serializer, $order, "show_order");

            // $bodyMail = $this->mailer->createBodyMail('mails/cours_mail.html.twig', ['user' => $user]);
            $bodyMail = $this->mailer->createBodyMail('mails/confirmation_commande.html.twig', ['user' => $user, "order"=> $orderJson]);
            $this->mailer->sendEmail(from: 'yemakarios70@gmail.com', to: $user->getEmail(), subject: 'Confirmation de la Commande', body: $bodyMail);

            $order->setOrderStatusStr(OrderStatus::BEING_PROCESSED);
            $this->em->persist($order);
            $this->em->flush();

            return $this->json([
                "message"=> "OK",
                "status_code"=> 200
            ]);

        }catch(\Exception $e){

            return $this->json([
                "message"=>$e->getMessage(),
                "code"=> $e->getCode()
            ], 500);

        }

    }

}
