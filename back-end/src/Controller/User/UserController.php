<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Entity\UserSessions;
use App\Repository\RecipeRepository;
use App\Repository\UserRepository;
use App\Repository\UserSessionsRepository;
use App\Services\SendMail;
use App\Services\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class UserController extends AbstractController
{
    public function __construct(
        private UserService $userService,
        private RecipeRepository $recipeTypeRepository,
        private UserSessionsRepository $userSessionsRepo,
        private SerializerInterface $serializer,
        private HttpClientInterface $client,
        private JWTTokenManagerInterface $jwtTokenInterface,
        private EntityManagerInterface $em,
        private UserRepository $userRepo,
        private UserPasswordHasherInterface $passHash
    ) {
    }

    #[Route('api/get-user_datas', name: 'app_user', methods: ['GET'])]
    public function index(): Response
    {
        return $this->userService->getUserDatas();
    }

    #[Route('get-user_profil', name: 'app_user_data', methods: ['POST'])]
    public function userProfil(Request $request): Response
    {
        $data = $request->request->all();
        return $this->userService->getUserProfil($data);
    }

    #[Route('api/update-user-profil', name: 'app_update_user', methods: ['POST'])]
    public function update(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        
        if($data == []){
            $data = $request->request->all();
        }

        $files = $request->files;

        return $this->userService->updateUser($data, $files);
    }

    #[Route('api/get-user-recipes', name: 'app_user_recipes', methods: ['GET'])]
    public function getUserRecipes(): Response
    {
        return $this->userService->getUserRecipes();
    }

    #[Route('api/get-user-liked', name: 'app_user_liked_recipes', methods: ['GET'])]
    public function getUserLikedRecipes(): Response
    {
        return $this->userService->getUserLikedRecipes();
    }

    #[Route('get-user-by-token/{token}', name: 'app_user_by_token', methods: ['GET'])]
    public function getUserByToken($token)
    {
        $userSession = $this->userSessionsRepo->findOneBy(["session_id" => $token]);
        $oldToken = $token;

        if(empty($userSession)){

            return $this->json(
                [
                    "message" => "KO",
                    "status_code" => 100
                ],
                status: Response::HTTP_CONTINUE
            );
            
        }

        $sessionExpiryTime = $userSession->getTime();
        $currentTime = strtotime(date('Y-m-d H:i:s'));

        if ($currentTime < $sessionExpiryTime) {

            $user = $userSession?->getUser();
            $sessionExpiryTime = $userSession->getTime();

            if (!empty($user)) {

                if($user->getUsername() == "" || $user->getUsername() == null){

                    $user->setUsername($user->getEmail());
                    $this->em->persist($user);
                    
                }

                $token = $this->jwtTokenInterface->create($user);

                return $this->json(
                    [
                        "message" => "OK",
                        "status_code" => 200,
                        "token" => $token,
                        "uuid" => $oldToken,
                        "username" => $user->getUsername()
                    ],
                    status: 200
                );
            }

        } else {

            return $this->json(
                [
                    "message" => "KO",
                    "status_code" => 100
                ],
                status: Response::HTTP_CONTINUE
            );
        }
    }

    #[Route('api/add-cookies', name: 'app_user_add_cookies', methods: ['GET'])]
    public function addCookies()
    {
        $user = $this->getUser();

        if ($user) {

            $uuid = Uuid::v4() . '-' . $user?->getId() . '-abj-cuisine';
            // setcookie("__ga__", $uuid, path: "/", domain: ".weblogy.net");
            setcookie("__ga__", $uuid, path: "/", domain: ".abidjan.net");

            $time = time() + (3600 * 24 * 7);

            $session = new UserSessions();
            $session
                ->setUser($user)
                ->setSessionId($uuid)
                ->setTime($time);

            $this->em->persist($session);
            $this->em->flush();

            return $this->json([
                "message" => "OK",
                "status_code" => 200
            ]);
        }
    }

    #[Route('reset-password-send-mail', name: 'app_reset_password', methods: ['POST'])]
    public function request(Request $request, TokenGeneratorInterface $tokenGenerator, EntityManagerInterface $manager, SendMail $mailer)
    {
        $data = $request->request->all();

        if($data ==[]){
            $data = json_decode($request->getContent(), true);
        }

        $user = $this->userRepo->findOneBy(['email' => $data['email']]);
        // $userJson = SerializerHelper::getJson($this->serializer, $user, "user_info");

        if (empty($user)) {
            return $this->json([
                "message" => "OK",
                "status_code" => Response::HTTP_NO_CONTENT
            ]);
        }

        // création du token            
        $user->setToken($tokenGenerator->generateToken());
        // enregistrement de la date de création du token            
        $user->setPasswordRequestedAt(new \Datetime());
        $manager->persist($user);
        $manager->flush();

        $bodyMail = $mailer->createBodyMail('mails/reset_email.html.twig', ['user' => $user]);
        $mailer->sendEmail(from: 'yemakarios70@gmail.com', to: $user->getEmail(), subject: 'renouvellement du mot de passe', body: $bodyMail);

        return $this->json([
            "message" => "OK",
            "status_code" => Response::HTTP_OK
        ]);
    }

    #[Route('get-password-reset-page/{id}/{token}', name: "pwd_reset_page")]
    public function resetting(User $user, $token)
    {
        try{

            if ($user->getToken() === null || $token !== $user->getToken() || !$this->isRequestInTime($user->getPasswordRequestedAt())) {
                throw new AccessDeniedHttpException();
            }

            $id = $user->getId();

            return $this->redirect("/creer-mot-de-passe/$id/$token");

        }catch(\Exception $e){

            return $this->json([
                "message"=> $e->getMessage(),
                "status_code"=> $e->getCode()
            ]);

            return $this->redirect("/");

        }
    }

    #[Route('creer-mot-de-passe/{id}/{token}', name: "create_new_password", methods:["POST"])]
    public function addNewPassword(User $user, $token, Request $request)
    {

        $data = $request->request->all();

        if( $data == []){
            $data = json_decode($request->getContent(), true);
        }

        if ($user->getToken() === null || $token !== $user->getToken() || !$this->isRequestInTime($user->getPasswordRequestedAt())) {
            throw new AccessDeniedHttpException();
        }

        if ($data['password'] === $data["password_confirmation"]) {

            $password = $this->passHash->hashPassword($user, $data['password']);

            $user->setPassword($password);
            $user->setToken(null);
            $user->setPasswordRequestedAt(null);
            $this->em->persist($user);
            $this->em->flush();

            return $this->json([
                "message"=> "OK",
                "status_code"=> Response::HTTP_OK,
            ]);

        } else {

            return $this->json([
                "message"=> "Le mot de passe et le mot de passe de confirmation ne condorent pas",
                "path"=> "Login",
                "status_code"=> Response::HTTP_NO_CONTENT 
            ]);
            
        }

    }

    private function isRequestInTime(\Datetime $passwordRequestedAt = null)
    {
        if ($passwordRequestedAt === null) {
            return false;
        }

        $now = new \DateTime(); 
        $interval = $now->getTimestamp() - $passwordRequestedAt->getTimestamp();
        $daySeconds = 60 * 10; 
        $response = $interval > $daySeconds ? false : $reponse = true; 
        return $response; 
    }
    
}
