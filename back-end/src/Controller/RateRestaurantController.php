<?php

namespace App\Controller;

use App\Entity\RateRestaurant;
use App\Repository\RestaurantRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RateRestaurantController extends AbstractController
{

    public function __construct(private RestaurantRepository $RestaurantRepo, private EntityManagerInterface $em)
    {
    }

    #[Route('api/rate-restaurant/{id}', name: 'app_rate_restaurant', methods: ["POST"])]
    public function rate($id, Request $request): Response
    {
        
        try {
            
            $user = $this->getUser();
            $data = json_decode($request->getContent(), true);

            $restaurant = $this->RestaurantRepo->find($id);
            $rates = $restaurant->getRateRestaurants();
            
            $userWhoAlreadlyRate = [];

            // return $this->json($rates);

            /* foreach($rates as $rate){
                array_push($userWhoAlreadlyRate, $rate->getUser()[0]);                
            } */

            $checkRate = in_array($user, $userWhoAlreadlyRate);

            if (!$checkRate) {

                $rate = new RateRestaurant();
                $rate->addUser($user);
                $rate->addRestaurant($restaurant);
                $rate->setNumberOfStart($data['number_of_stars']);

                $this->em->persist($rate);
                $this->em->flush();

                return $this->json([
                    "data" => $rate,
                    "message" => "OK",
                    "status_code" => 200
                ], 200);

            }else{

                return $this->json([
                    "message"=>"ALREADY_RATE",
                    "status_code"=> 200
                ], 200);
                
            }

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => 500
            ], 500);

        }
    }

    #[Route('get-rate-restaurant/{restaurantID}', name: 'app_get_rate_restaurant', methods:["GET"])]
    public function getRestaurantRate($restaurantID)
    {
        $restaurant = $this->RestaurantRepo->find($restaurantID);
        
        if (empty($restaurant)) {

            return $this->json([
                "message" => "Not Found",
                "status_code" => 404
            ], 404);

        } else {

            return $this->json([
                "data" => $restaurant->getRateRestaurants(),
                "message" => "OK",
                "status_code" => 200
            ]);

        }
    }

    #[Route('get-rate-restaurant/average/{restaurantID}', name: 'app_get_rate_restaurant_average', methods:["GET"])]
    public function getRestaurantAverage($restaurantID){
        $restaurant = $this->RestaurantRepo->find($restaurantID);
        $average = null;
        $rates = $restaurant->getRateRestaurants();
    
        $average = $this->calculateAverage($rates);

        return $this->json(["message"=> "OK", "data"=> ['average'=> $average, 'rates'=> $rates]]);
    }

    private function calculateAverage($rates){
        $rateSum = 0;

        foreach($rates as $rate){
            $rateSum += $rate->getNumberOfStart();
        }
        
        $average = round($rateSum / count($rates) , 1);
        // $average = floor($rateSum / count($rates));
        // $average = (float) ($rateSum / count($rates));

        return $average;
    }

}
