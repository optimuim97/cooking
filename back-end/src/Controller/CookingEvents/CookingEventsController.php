<?php

namespace App\Controller\CookingEvents;

use App\Services\CookingEventService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CookingEventsController extends AbstractController
{

    public function __construct(private CookingEventService $cookingEventService)
    {
    }

    #[Route('/api/add-cooking_event', name: 'add_cooking_event', methods: ['POST'])]
    public function add(Request $request): Response
    {
        $file = $request->files->get('photoCouverture');
        $data = $request->request->all();

        if ($data == []) {
            $data = json_decode($request->getContent(), true);
        }

        return $this->cookingEventService->add($data, $file);
    }

    #[Route('/api/delete-cooking_event/{id}', name: 'delete_cooking_event', methods: ['DELETE'])]
    public function delete($id): Response
    {
        return $this->cookingEventService->delete($id);
    }

    #[Route('/show-cooking_event/{id}', name: 'show_cooking_event', methods: ['GET'])]
    public function show($id): Response
    {
        return $this->cookingEventService->show($id);
    }

    #[Route('/update-cooking_event/{id}', name: 'app_cooking_event', methods: ['PATCH'])]
    public function update($id, Request $request): Response
    {
        $data = $request->request->all();
        return $this->cookingEventService->update($id, $data);
    }

    #[Route('get-cooking_events', name: 'cooking_events', methods: ["GET"])]
    public function getAll()
    {
        return $this->cookingEventService->getAll();
    }
}
