<?php

namespace App\Controller;

use App\Entity\Rate;
use App\Repository\RecipeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RateController extends AbstractController
{

    public function __construct(private RecipeRepository $recipeRepo, private EntityManagerInterface $em)
    {
    }

    #[Route('api/rate/{id}', name: 'app_rate', methods: ["POST"])]
    public function rate($id, Request $request): Response
    {

        try {

            $user = $this->getUser();
            $data = json_decode($request->getContent(), true);
            $recipe = $this->recipeRepo->find($id);
            $rates = $recipe->getRates();
            
            $userWhoAlreadlyRate = [];

            foreach($rates as $rate){
                array_push($userWhoAlreadlyRate, $rate->getUser()[0]);                
            }

            $checkRate = in_array($user, $userWhoAlreadlyRate);

            if (!$checkRate) {

                $rate = new Rate();
                $rate->addUser($user);
                $rate->addRecipe($recipe);
                $rate->setNumberOfStart($data['number_of_stars']);

                $this->em->persist($rate);
                $this->em->flush();

                return $this->json([
                    // "data" => $rate,
                    "message" => "OK",
                    "status_code" => 200
                ], 200);

            }else{

                return $this->json([
                    "message"=>"ALREADY_RATE",
                    "status_code"=> 200
                ], 200);
                
            }

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);

        }
    }

    #[Route('get-rate/rate/{recipeID}', name: 'app_get_rate', methods:["GET"])]
    public function getRecipeRate($recipeID)
    {
        $recipe = $this->recipeRepo->find($recipeID);

        if (empty($recipe)) {

            return $this->json([
                "message" => "Not Found",
                "status_code" => 404
            ], 404);

        } else {

            return $this->json([
                "data" => $recipe->getRates(),
                "message" => "OK",
                "status_code" => 200
            ]);

        }
    }

    #[Route('get-rate/average/{recipeID}', name: 'app_get_rate_average', methods:["GET"])]
    public function getRecipeAverage($recipeID){

        $recipe = $this->recipeRepo->find($recipeID);
        $average = null;

        $rates = $recipe->getRates();
        $average = $this->calculateAverage($rates);

        return $this->json(["message"=> "OK", "data"=> ['average'=> $average, 'rates'=> $rates]]);
    }

    private function calculateAverage($rates){
        $rateSum = 0;

        foreach($rates as $rate){
            $rateSum += $rate->getNumberOfStart();
        }
        

        $average = round($rateSum / count($rates) , 1);
        // $average = floor($rateSum / count($rates));
        // $average = (float) ($rateSum / count($rates));

        return $average;
    }

}
