<?php

namespace App\Controller;

use App\Entity\RecipeCount;
use App\Helpers\SerializerHelper;
use App\Repository\RecipeCountRepository;
use App\Repository\RecipeRepository;
use Detect;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class FrontController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private RecipeRepository $recipeRepo,
        private RecipeCountRepository $recipeCountRepo,
        private SerializerInterface $serializer
    ) {}
        
    #[Route('/mot-de-passe/{path}', name: 'app_front_password')]
    public function getIndexPasswordForget($path): Response
    {
        return $this->render('FrontOffice/index.html.twig', compact('path'));
    }

    #[Route('/views-counter/{slug}', name: 'views-counter')]
    public function countView($slug)
    {
        try {

            $recipe = $this->recipeRepo->findOneBy(["slug"=>$slug]);
            
            $ip = Detect::ip(); 
            $deviceType = Detect::deviceType(); 
            // $browser = Detect::browser(); 
    
            $currentUserInfo =
                [
                    "ID" => $ip,
                    "USER" => $deviceType
                ];
    
            $checkRecipeCount = $this->recipeCountRepo->findBy(
                [
                    "identifiant" => $currentUserInfo['ID'], "user" =>  $currentUserInfo['USER']
                ]
            );
    
            foreach ( $checkRecipeCount as $recipeToCheck ) {
    
                if($recipeToCheck->getRecipe()->contains($recipe)){
                    return $this->json([
                        "status_code" => Response::HTTP_CONTINUE,
                        "message" => "KO"
                    ], 200);
                }
    
            }
             
            $userRf = $currentUserInfo["ID"] . "-" . $currentUserInfo["USER"];

            $recipeCount = new RecipeCount();
            $recipeCount
                ->setUser($currentUserInfo['USER'])
                ->setIdentifiant($currentUserInfo['ID'])
                ->addRecipe($recipe)
                ->setRf($userRf)
                ->setNumber(((int) $recipeCount->getNumber() + 1));
    
            $recipeCount->addRecipe($recipe);

            $this->em->persist($recipeCount);
            $this->em->flush();

            $recipeCountJSON = SerializerHelper::getJson($this->serializer, $recipeCount, "show_recipe_count");

            return $this->json([
                "data" => $recipeCountJSON,
                "status_code" => Response::HTTP_ACCEPTED,
                "message" => "OK"
            ], 200);

        }catch(\Exception $e){

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => $e->getMessage()
            ], 200);

        }
    }

    #[Route('/api/views-counter/{slug}', name: 'views-counter-auth')]
    public function countViewAuth($slug)
    {

        $user  = $this->getUser();

        $recipe = $this->recipeRepo->findOneBy(["slug"=>$slug]);
        $checkRecipeCount = $this->recipeCountRepo->findBy(["user"=>$user->getEmail()]);

        $ip = Detect::ip(); 
        $deviceType = Detect::deviceType(); 

        $currentUserInfo =
            [
                "ID" => $ip,
                "USER" => $deviceType
            ];

        foreach ( $checkRecipeCount as $recipeToCheck ) {
            if($recipeToCheck->getRecipe()->contains($recipe)){
                return $this->json([
                    "status_code" => Response::HTTP_CONTINUE,
                    "message" => "KO"
                ], 200);
            }
        }

        // if (empty($check) || $check == null) {
        $userRf = $currentUserInfo["ID"] . "-" . $currentUserInfo["USER"];

        $recipeCount = new RecipeCount();
        $recipeCount
            ->setUser($user->getEmail())
            ->setIdentifiant($currentUserInfo['ID'])
            ->setRf($userRf)
            ->addRecipe($recipe)
            ->setNumber(((int) $recipeCount->getNumber() + 1))
            ->addVisitor($user);

        $this->em->persist($recipeCount);
        $this->em->flush();

        $recipeCountJSON = SerializerHelper::getJson($this->serializer, $recipeCount, "show_recipe_count");

        return $this->json([
            "data" => $recipeCountJSON,
            "status_code" => Response::HTTP_OK,
            "message" => "OK"
        ], 200);

        // } else {

        return $this->json([
            "status_code" => Response::HTTP_CONTINUE,
            "message" => "Not Found"
        ], 100);
        // }
    }

    #[Route('recipes-for-abj')]
    public function getRecipeForAbj(){
        $recipes = $this->recipeRepo->findBy(["isSpotLightAbj"=> true]);

        $recipesJSON = SerializerHelper::getJson($this->serializer, $recipes, "show_recipe");

        return $this->json([
            "data"=>$recipesJSON,
            "message"=> "OK"
        ]);
    }

    // #[Route('/{page}', name: 'app_front_home_front', requirements:["page"=>"^(?!.*wdt).+"])]
    // ([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i

    /**
     * @Route("/{page}", requirements={"page": "([a-z0-9-].?!.*)+(:[0-9]+).+"})
     * @Route("/{page}", requirements={"page": "([a-z0-9-].?!.*)+(:[0-9]+).+", "page1": "([a-z0-9-].?!.*)+(:[0-9]+).+"})
     */
    // #[Route('/{page}', name: 'app_front_home_front', requirements:["page"=>"^.*$"])]
    // #[Route('/{page}/{page1}', name: 'app_front_home_front', requirements:["page"=>"^(?!panel).*$", "page1"=>"^(?!panel).*$"])]
    // public function getFrontView(Request $req): Response
    // {
        // return $this->render('FrontOffice/index.html.twig', []);
    // }

    // #[Route('/{page}', name: 'app_front_home_front', requirements:["page"=>"^.*$"])]
    // #[Route('/{page}', name: 'app_front_home_front', requirements:["page"=>"^(?!api).+"])]
    // /**
    //  * @Route("/{page}", requirements={"page": "([a-z0-9-].?!.*)+(:[0-9]+).+"})
    // */

    #[Route('/', name: 'app_front_home_front')]
    public function getIndexPage(): Response
    {
        return $this->render('FrontOffice/index.html.twig');
    }

}
