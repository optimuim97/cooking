<?php

namespace App\Controller\Budget;

use App\Services\BudgetService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BudgetController extends AbstractController
{
    public function __construct(private BudgetService $budgetService)
    {
        $this->budgetService = $budgetService;
    }

    #[Route('/api/add-budget', name: 'app_add_budget', methods: ['POST'])]
    public function add(Request $request): Response
    {
        $data = $request->request->all();
        return $this->budgetService->add($data);
    }

    #[Route('/delete-budget', name: 'app_delete_budget', methods: ['DELETE'])]
    public function delete($id): Response
    {
        return $this->budgetService->delete($id);
    }

    #[Route('/show-budget', name: 'app_show_budget', methods: ['POST'])]
    public function show(Request $request): Response
    {
        $data = $request->request->all();
        return $this->budgetService->show($data);
    }

    #[Route('/update-budget', name: 'app_update_budget', methods: ['POST'])]
    public function update($id, Request $request): Response
    {
        $data = $request->request->all();
        return $this->budgetService->update($id, $data);
    }

    #[Route('/get-budgets', name: 'app_get_budgets', methods: ['GET'])]
    public function getAllBudget(): Response
    {
        return $this->budgetService->getAll();
    }
}
