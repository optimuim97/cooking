<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\CookingTricksRepository;
use App\Repository\CourseRepository;
use App\Repository\RecipeRepository;
use App\Repository\RecipeTypeRepository;
use App\Repository\UserRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class ExecutorController extends AbstractController
{
    public function __construct(
        private RecipeRepository $recipeRepo,
        private EntityManagerInterface $em,
        private RecipeTypeRepository $recipeTypeRepo,
        private CookingTricksRepository $cookingTrickRepo,
        private CourseRepository $courseRepo,
        private UserPasswordHasherInterface $password_hasher,
        private UserRepository $userRepo
    ) {
    }

    #[Route('/generate-slug-for-recipe', name: 'app_executor')]
    public function generateSlugForRecipe(): Response
    {

        //All recipe 5400 - 500
        try {

            $slugsList = $this->getAllSlug();
            $slug_added = [];
            $slug_alreadly_generate = [];

            $recipes = $this->courseRepo->findAll();

            set_time_limit(100);

            foreach ($recipes as $key => $recipe) {
                // if($key < 400){
                set_time_limit(300);

                $recipeSlug = $recipe->getSlug();

                if ($recipeSlug == "" || empty($recipeSlug)  || $recipeSlug == null) {

                    $name = $recipe->getName();
                    $slug = (new Slugify())->slugify($name);

                    if (in_array($slug, $slugsList)) {

                        $rand = random_int(10, 20);
                        $recipeSlug = "$recipeSlug-$rand";
                        $recipe->setSlug($slug);

                        array_push($slug_alreadly_generate, $recipe->getSlug());

                        $this->em->persist($recipe);
                        $this->em->flush();
                    } else {

                        $recipe->setSlug($slug);
                        array_push($slug_added, $recipe->getSlug());
                        $this->em->persist($recipe);
                        $this->em->flush();
                    }
                } elseif (empty($recipeSlug)) {

                    $recipeSlug = $recipe->getSlug();

                    if (in_array($recipeSlug, $slugsList)) {

                        $rand = random_int(10, 20);
                        $slug = "$recipeSlug-$rand";
                        $recipe->setSlug($slug);

                        array_push($slug_alreadly_generate, $recipe->getSlug());

                        $this->em->persist($recipe);
                        $this->em->flush();
                    }
                }
                // }
            }

            return $this->json([
                "slug_added" => $slug_added,
                "counta_added" => count($slug_added),
                /*"slugs"=>$slugsList, */
                "slug_alreadly_generate" => $slug_alreadly_generate
            ]);
        } catch (Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);
        }
    }

    private function getAllSlug()
    {

        $slugsList = [];
        $recipes = $this->recipeRepo->findAll();

        foreach ($recipes as  $recipe) {
            if (!empty($recipe->getSlug())) {
                array_push($slugsList, $recipe->getSlug());
            }
        }


        return $slugsList;
    }

    #[Route('/utils-generator', name: 'app_executor')]
    public function createUser()
    {
        try {

            $recipes = $this->recipeRepo->findAll();
            $users = $this->userRepo->findAll();

            foreach ($recipes as $recipe) {

                set_time_limit(20000);

                if ($recipe->getUserEmail() != null && $recipe->getUserEmail() != '') {

                    $user = $this->userRepo->findOneBy(["email" => $recipe->getUserEmail()]);

                    if (empty($user) && in_array($recipe, $recipes)) {
                        
                        $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';

                        $emailaddress = $recipe->getUserEmail();

                        if (preg_match($pattern, $emailaddress) === 1) {
                            if ($recipe->getUserEmail() != null || $recipe->getUserEmail() == '') {

                                $user = new User();

                                $username = $recipe?->getUserEmail() ?? "";
                                $lastname = $recipe?->getUserName() ?? "";
                                $firstname = "" ?? "";
                                $dial_code = "" ?? "";
                                $phone_number = "" ?? "";
                                $password = "000000000" ?? "";
                                $country_code = "CI" ?? "";

                                $user
                                    ->setEmail($username)
                                    ->setUsername($username)
                                    ->setNom($firstname)
                                    ->setPrenoms($lastname)
                                    ->setCodePays($country_code)
                                    ->setNumero($phone_number);

                                $plaintextPassword = $password;
                                $password = $this->password_hasher->hashPassword($user, $plaintextPassword);
                                $user->setPassword($password);

                                $this->em->persist($user);
                                $this->em->flush();

                            }
                        }

                    } 

                }

            }

            return $this->json(['OK']);

        } catch (Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);

        }
    }

    #[Route('create-user-generator', methods : ["GET"])]
    public function createUserGenerator(){

        try{
            
            $users = $this->userRepo->getUtilisateurs();

            foreach($users as $oldUser){
                set_time_limit(500000);

                $recipes = $this->userRepo->getUtilisateurRecipe($oldUser["id"]);
                $check = $this->userRepo->findOneBy(["email"=> $oldUser["email"]]);
    
                $username = $oldUser['username'] ?? "";
                $email = $oldUser['email'] ?? "";
                $lastname = $oldUser['nom'] ?? "";
                $firstname = $oldUser['prenoms'] ?? "";
                $phone_number = $oldUser['numero'] ?? "";
                $password = random_int(8,10);
                $country_code = $oldUser['country_code'] ?? "";
    
                if($check == null){
    
                    $newUser = new User();
    
                    $newUser
                        ->setEmail($email)
                        ->setUsername($username)
                        ->setNom($firstname)
                        ->setPrenoms($lastname)
                        ->setNomComplet("$firstname $lastname")
                        ->setIsAdmin(false)
                        ->setTypeCompte("particulier")
                        ->setRoles(["ROLE_USER"])
                        ->setCodePays($country_code)
                        ->setNumero($phone_number);
    
                        $plaintextPassword = $password;
                        $password = $this->password_hasher->hashPassword($newUser, $plaintextPassword);
                        $newUser->setPassword($password);
    
                        $this->em->persist($newUser);
                        $this->em->flush();
    
                    if(!empty($recipes)){
    
                        foreach ($recipes as $item) {
    
                            $recipe = $this->recipeRepo->find($item['id']);
                            $recipe->setUser($newUser);
    
                            if($recipe->getId() == $newUser->getId()){
                                $this->em->persist($recipe);
                                $this->em->flush();
                            }
                            
                        }
    
                    }
    
                }
            }

            return $this->json(["status_code"=> 200, "message"=> "OK" ]);

        }catch(Exception $e){
            
            return $this->json([
                "message"=> $e->getMessage(),
                "status_code"=> $e->getCode()
            ], 500);

        }
        
    }

    #[Route('add-username', methods : ["GET"])]
    public function addUsername(){

        $users = $this->userRepo->findAll();

        foreach ($users as $user) {
            set_time_limit(5000000000);

            $user->setUsername($user->getEmail());
            $this->em->persist($user);
            $this->em->flush();
        }

        return $this->json([
            "message"=> "OK",
            "code"=> 200
        ]);

    }
}
