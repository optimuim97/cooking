<?php

namespace App\Controller\Course;

use App\Entity\Course;
use App\Helpers\SerializerHelper;
use App\Repository\CourseParticipantsRepository;
use App\Repository\CourseRepository;
use App\Repository\UserRepository;
use App\Services\CourseService;
use App\Services\SendMail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class CourseController extends AbstractController
{

    public function __construct(
        private CourseService $CourseService,
        private CourseRepository $courseRepo,
        private SerializerInterface $serializer,
        private SendMail $mailer,
        private UserRepository $userRepo,
        private EntityManagerInterface $em,
        private CourseParticipantsRepository $courseParticipantsRepo
    )
    {
    }

    #[Route('/api/add-course', name: 'add_course', methods: ["POST"])]
    public function add(Request $request): Response
    {
        $data = $request->request->all();

        if($data == []){
            $data = json_decode($request->getContent(), true);
        }

        $file = $request->files->get('imageUrl');
        
        return $this->CourseService->add($data, $file);
    }

    #[Route('/api/delete-course/{id}', name: 'delete_course', methods: ["DELETE"])]
    public function deleteRecipe($id): Response
    {
        return $this->CourseService->delete($id);
    }

    #[Route('/show-course/{slug}', name: 'show_course', methods: ["GET"]
    // requirements: ["slug"=>"[a-zA-Z0-9\-]*"]
    )]
    public function showCourse($slug): Response
    {
        return $this->CourseService->show($slug);
    }

    #[Route('/api/update-course/{id}', name: 'update_course', methods: ["POST"])]
    public function updateCourse($id, Request $request): Response
    {
        $data = $request->request->all();

        if($data == []){
            $data = json_decode($request->getContent(), true);
        }

        $file = $request->files->get('imageUrl');
        return $this->CourseService->update($id, $data, $file);
    }

    #[Route('get-courses', name: 'get_courses', methods: ["GET"])]
    public function getAll()
    {
        return $this->CourseService->getAll();
    }
      
    #[Route('get-course-types', name: 'get_course_types', methods: ["GET"])]
    public function getAllCourseType()
    {
        return $this->CourseService->getAllCourseType();
    }

    #[Route('api/enroll-to-course/{id}', name: 'enroll_to_course', methods: ["GET"])]
    public function enrollToCourse($id)
    {
        return $this->CourseService->enroll($id);
    }
    
    #[Route('search-course-by-name/{keyword}', name: 'search_course_by_name', methods: ["GET"])]
    public function searchCourseByName($keyword){
        return $this->CourseService->searchCourseByName($keyword);
    }
    
    #[Route("api/add-lesson", name:"add_course_lesson", methods:["POST"])]
    public function addCourse(Request $request){
        $data = $request->request->all();

        if($data == []){
            $data = json_decode($request->getContent(), true);
        }

        return $this->CourseService->addLesson($data);
    }
  
    #[Route("show-lesson/{id}", name:"show_course_lesson", methods:["GET"])]
    public function getLesson($id){
        return $this->CourseService->getLesson($id);
    }

    
    #[Route("course-participants/{id}", name:"show_course_participant", methods:["GET"])]
    public function getCourse($id){

        $course = $this->courseRepo->find($id);

        if(!empty($course)){
            
            $courseJson = SerializerHelper::getJson($this->serializer, $course, "show_course");
    
            return $this->json(
                [
                    "status_code"=> Response::HTTP_OK,
                    "message"=> "Course list",
                    "data"=> $courseJson
                ]
            );

        }

        return $this->json([
            "message"=>"KO",
            "status_code"=> 500
        ], 500);

    }

    #[Route('api/confirm-participation/{id}/{userId}',  methods: ["GET"])]
    public function accept($id, $userId){

        try{
            $course = $this->courseRepo->find($id);
            $user = $this->userRepo->find($userId);

            $courseParticipants = $this->courseParticipantsRepo->findBy(["course"=> $course]);

            foreach ($courseParticipants as $part) {
                // return $this->json(SerializerHelper::getJson($this->serializer, $part->getParticipants(), "user_info"));
                if($part->getParticipants()->contains($user)){
                    
                    $bodyMail = $this->mailer->createBodyMail('mails/course_accept_mail.html.twig', ['user' => $user, "course" => $course]);
                    $this->mailer->sendEmail(from: 'yemakarios70@gmail.com', to: $user->getEmail(), subject: 'Demande participation', body: $bodyMail);

                    $part->setIsAcceped(true);

                    $this->em->persist($part);
                    $this->em->flush();

                }

            }

            $user = SerializerHelper::getJson($this->serializer, $part->getParticipants(), "user_info");
            $courseJson = SerializerHelper::getJson($this->serializer, $course, "show_course");

            $this->em->persist($course);
            $this->em->flush();

            return $this->json([
                "data"=> $courseJson,
                "message"=> "OK",
                "status_code"=> 200
            ]);

        }catch(\Exception $e){

            return $this->json([
                "message"=>$e->getMessage(),
                "code"=> $e->getCode()
            ], 500);

        }

    }

    #[Route('api/reject-participation/{id}/{userId}',  methods: ["GET"])]
    public function reject($id, $userId){
        
        try{

            $course = $this->courseRepo->find($id);
            $user = $this->userRepo->find($userId);

            $bodyMail = $this->mailer->createBodyMail('mails/course_reject_mail.html.twig', ['user' => $user, "course" => $course]);
            $this->mailer->sendEmail(from: 'yemakarios70@gmail.com', to: $user->getEmail(), subject: 'Demande participation', body: $bodyMail);

            $this->em->persist($course);
            $this->em->flush();

            $courseJson = SerializerHelper::getJson($this->serializer, $course, "show_course");

            return $this->json([
                "data"=> $courseJson,
                "message"=> "OK",
                "status_code"=> 200
            ]);

        }catch(\Exception $e){

            return $this->json([
                "message"=>$e->getMessage(),
                "code"=> $e->getCode()
            ], 500);

        }

    }

    #[Route('api/delete-user-to-participants/{id}/{userId}',  methods: ["GET"])]
    public function deleteUser($id, $userId){
        
        try{

            $course = $this->courseRepo->find($id);
            $user = $this->userRepo->find($userId);

            $courseParticipants = $this->courseParticipantsRepo->findBy(["course"=> $course]);

            foreach ($courseParticipants as $part) {
                if($part->getParticipants()->contains($user)){
                    $bodyMail = $this->mailer->createBodyMail('mails/course_delete_mail.html.twig', ['user' => $user, "course" => $course]);
                    $this->mailer->sendEmail(from: 'yemakarios70@gmail.com', to: $user->getEmail(), subject: 'Demande participation', body: $bodyMail);

                    $part->setIsAcceped(false);

                    $this->em->persist($part);
                    $this->em->flush();
                }

            }

            $courseJson = SerializerHelper::getJson($this->serializer, $course, "show_course");

            return $this->json([
                "data"=> $courseJson,
                "message"=> "OK",
                "status_code"=> 200
            ]);

        }catch(\Exception $e){

            return $this->json([
                "message"=>$e->getMessage(),
                "code"=> $e->getCode()
            ], 500);

        }

    }

}
