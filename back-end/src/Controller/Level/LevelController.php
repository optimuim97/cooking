<?php

namespace App\Controller\Level;

use App\Services\LevelService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LevelController extends AbstractController
{
     private $levelService;

     public function __construct(LevelService $levelService)
     {
          $this->levelService = $levelService;
     }

     #[Route('/api/add-level', name: 'app_add_level', methods: ['POST'])]
     public function add(Request $request)
     {
          $data = $request->request->all();

          return $this->levelService->add($data);
     }

     #[Route('/api/delete-level/{id}', name: 'app_delete_level', methods: ['DELETE'])]
     public function delete($id)
     {
          // dd($id);
          return $this->levelService->delete($id);
     }

     #[Route('/api/update-level', name: 'app_update_level', methods: ['PATCH'])]
     public function update($id, Request $request)
     {
          $data = $request->request->all();
          return $this->levelService->update($id, $data);
     }

     #[Route('/show-level/{id}', name: 'app_show_level', methods: ['GET'])]
     public function show($id)
     {
          return $this->levelService->show($id);
     }

     #[Route('/get-levels', name: 'app_get_level', methods: ['GET'])]
     public function getAll()
     {
          return $this->levelService->getAll();
     }
}
