<?php

namespace App\Controller;

use App\traits\ApiCaller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    use ApiCaller;

    #[Route('/home', name: 'app_home')]
    public function index()
    {
        $recipes = $this->getRecipes();
        $recipeTypes = $this->getRecipeTypes();
        $restaurants = $this->getRestaurants();
        $cookingTricks = $this->getCookingTricks();
        $cookingEvents = $this->getCookingEvents();
        $cookingCourses = $this->getCookingEvents();

        return $this->render(
            'FrontOffice/index.html.twig',
            [
                "recipes" => $recipes,
                "restaurants" => $restaurants,
                "cookingTricks" => $cookingTricks,
                "cookingEvents" => $cookingEvents,
                "recipeTypes" => $recipeTypes,
                "cookingCourses" => $cookingCourses
            ]
        );
    }
}
