<?php

namespace App\Controller\Restaurant;

use App\Services\RestaurantService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RestaurantController extends AbstractController
{
    private $restoService;

    public function __construct(RestaurantService $restoService)
    {
        $this->restoService = $restoService;
    }

    #[Route('/api/add-restaurant', name: 'app_restaurant', methods: ["POST"])]
    public function addRestaurant(Request $request): Response
    {
        $data = $request->request->all();
        $files = $request->files;

        return $this->restoService->add($data, $files);
    }

    #[Route('/api/delete-restaurant/{id}', name: 'delete_restaurant', methods: ["DELETE"])]
    public function deleteRestaurant($id): Response
    {
        return $this->restoService->delete($id);
    }

    #[Route('/show-restaurant/{slug}', name: 'show_restaurant', methods: ["GET"])]
    public function showRestaurant($slug): Response
    {
        return $this->restoService->show($slug);
    }

    #[Route('get-restaurant-spotlight', name: 'get_restaurant_spotlight', methods: ["GET"])]
    public function getRestaurantSpotLight(): Response
    {
        return $this->restoService->spotLight();
    }

    #[Route('/api/update-restaurant/{id}', name: 'update_restaurant', methods: ["POST"])]
    public function updateRestaurant($id, Request $request): Response
    {
        $data = $request->request->all();
        $files = $request->files;
            
        return $this->restoService->update($id, $data, $files);
    }

    #[Route('/get-restaurants', name: 'get_restaurant', methods: ["POST"])]
    public function getRestaurant(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        return $this->restoService->getAll($data);
    } 

    #[Route('/add-dish', name: 'app_add_dish', methods: ["POST"])]
    public function addDish(Request $request): Response
    {
        $data = $request->request->all();
        $files = $request->files->get('image_url');         
        return $this->restoService->addDish($data, $files);
    }

    #[Route('/get-dishes', name: 'app_get_dishs', methods: ["GET"])]
    public function getDishes(): Response
    {
        return $this->restoService->getDishes();
    }

    #[Route('/show-dish/{id}', name: 'app_get_dish_show', methods: ["GET"])]
    public function showDish($id): Response
    {
        return $this->restoService->showDish($id);
    }

    #[Route('/api/update-dish/{id}', name: 'app_get_dish_update', methods: ["POST"])]
    public function updateDish($id,Request $request): Response
    {
        $data = $request->request->all();        
        
        $files = $request->files->get('image_url');
        return $this->restoService->updateDish($id,$data, $files);
    }

    #[Route('/get-dishes_by_type/{id}/{resto_id}', name: 'app_get_dishes_by_types', methods: ["GET"])]
    public function getDishByType($id, $resto_id): Response
    {
        return $this->restoService->getDishesByType($id, $resto_id);
    }

    #[Route('/api/delete-dish/{id}', name: 'app_delete_dish', methods: ["DELETE"])]
    public function deleteDish($id): Response
    {
        return $this->restoService->deleteDish($id);
    }

    #[Route('search-restaurant-by-name/{keyword}')]
    public function getRestaurantByName($keyword){
        return $this->restoService->getRestoByName($keyword);    
    }

    #[Route('/api/get-resto-orders/{id}')]
    public function getRestoOrders($id){
        return $this->restoService->getRestoOrders($id);    
    }

    #[Route('get-all-restaurants', name: "all_resto",methods:["GET"])]
    public function getAllResto(){
        return $this->restoService->getAllRestoForDash();    
    }
   
    #[Route('active-or-desactive-restaurant/{id}', name: "change_resto_status",methods:["GET"])]
    public function activeOrDesactive($id){
        return $this->restoService->activeOrDesactive($id);    
    }

}
