<?php

namespace App\Controller;

use App\Services\SubscriptionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SubscriptionController extends AbstractController
{

    public function __construct(private SubscriptionService $subscriptionService){}
        
    #[Route('/api/subscribe/{id}', name: 'app_subscription_subscribe', methods:["POST"])]
    public function subscribe(Request $request, $id): Response
    {   
        $data = $request->request->all();
        return $this->subscriptionService->subscribe($id, $data);
    }
    
    #[Route('/api/subscribe-as-cooker/{id}', name: 'app_subscription', methods:["GET"])]
    public function subscribeAsCooker($id): Response
    {   
        return $this->subscriptionService->subscribeAsCooker($id);
    }
}
