<?php

namespace App\Controller\CookingTrick;

use App\Services\CookingTrickService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CookingTricksController extends AbstractController
{

    public function __construct(private CookingTrickService $cookingTrickService)
    {
        $this->cookingTrickService = $cookingTrickService;
    }

    #[Route('/api/add-cooking_trick', name: 'add_cooking_trick', methods: ['POST'])]
    public function add(Request $request): Response
    {
        $data = $request->request->all();
        $file = $request->files;

        return $this->cookingTrickService->add($data, $file);
    }

    #[Route('/api/delete-cooking_trick/{id}', name: 'delete_cooking_trick', methods: ['DELETE'])]
    public function delete($id): Response
    {
        return $this->cookingTrickService->delete($id);
    }

    #[Route('/show-cooking_trick/{slug}', name: 'show_cooking_trick', methods: ['GET'])]
    public function show($slug): Response
    {
        return $this->cookingTrickService->show($slug);
    }

    #[Route('api/update-cooking_trick/{slug}', name: 'app_cooking_trick', methods: ['POST'])]
    public function update($slug, Request $request): Response
    {
        $data = $request->request->all();
        $file = $request->files;
        return $this->cookingTrickService->update($slug, $data, $file);
    }

    #[Route('get-cooking_tricks', name: 'cooking_tricks', methods: ["GET"])]
    public function getAll()
    {
        return $this->cookingTrickService->getAll();
    }

    #[Route('search-cooking_trick-by-name/{keyword}', name: 'search_cooking_tricks', methods: ["GET"])]
    public function getCookingTrickByName($keyword)
    {
        return $this->cookingTrickService->getCookingTrickByName($keyword);
    }
}
