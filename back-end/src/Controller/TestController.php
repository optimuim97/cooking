<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class TestController extends AbstractController
{
    #[Route('/test', name: 'app_test')]
    public function index(): Response
    {
        $uuid = "926ac730-5507-422f-9bdf-7e91904cfac7-169-abj-cuisine";

        setcookie("__ga__", value : "Sidik", domain:"abjcuisine.weblogy.net");
        setcookie("__ga__", value: "Sidik", domain:"abjannonces.weblogy.net");
        
        //Local
        setcookie("__ga__", "Sidik", domain:"");
        
        setcookie("__ga__", $uuid, domain:"abjannonces.weblogy.net/");
        setcookie("__ga__", $uuid, domain:"abjcuisine.weblogy.net/");
 
        //Local
         setcookie("__ga__", $uuid, domain:"");

        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }
}
