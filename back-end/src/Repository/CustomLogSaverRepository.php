<?php

namespace App\Repository;

use App\Entity\CustomLogSaver;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CustomLogSaver>
 *
 * @method CustomLogSaver|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomLogSaver|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomLogSaver[]    findAll()
 * @method CustomLogSaver[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomLogSaverRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomLogSaver::class);
    }

    public function save(CustomLogSaver $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CustomLogSaver $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return CustomLogSaver[] Returns an array of CustomLogSaver objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CustomLogSaver
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
