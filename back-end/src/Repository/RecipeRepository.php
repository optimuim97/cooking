<?php

namespace App\Repository;

use App\Entity\Recipe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Recipe>
 *
 * @method Recipe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recipe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recipe[]    findAll()
 * @method Recipe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Recipe::class);
    }

    public function save(Recipe $entity, bool $flush = false): void
    { 
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Recipe $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function orderByCreatedAtDesc()
    {
        return $this->createQueryBuilder('r')
            ->orderBy('r.createdAt', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    public function getRecipeWithImage()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM recipe where recipe.image_url LIKE "%images/%"';
        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery();

        return $result->fetchAllAssociative();
    }

    public function getByRecipeType($id, $limite = 10)
    {

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT 
                r.name, r.id, r.image_url, r.description
                FROM recipe r JOIN recipe_type rt ON r.recipe_type_id = rt.id 
                WHERE  r.recipe_type_id = :id 
                
                ORDER BY r.created_at DESC LIMIT :limit';
        // $sql = 'SELECT * FROM recipe r JOIN recipe_type rt ON r.recipe_type_id = rt.id WHERE  r.recipe_type_id = 3 ORDER BY r.id DESC LIMIT 10';
        $stmt = $conn->prepare($sql);

        $stmt->bindValue('id', $id);
        $stmt->bindValue('limit', $limite, \PDO::PARAM_INT);

        $resultSet = $stmt->executeQuery();

        // dd($resultSet->fetchAllAssociative());

        return $resultSet->fetchAllAssociative();
    }

    public function getByLevel($id, $limite = 10)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql =
            'SELECT r.name, r.id, r.image_url, r.description 
        FROM recipe r JOIN level l ON r.level_id = l.id WHERE  r.level_id = :id 
        ORDER BY r.created_at DESC LIMIT :limit';

        $stmt = $conn->prepare($sql);

        $stmt->bindValue('id', $id);
        $stmt->bindValue('limit', $limite, \PDO::PARAM_INT);

        $resultSet = $stmt->executeQuery();

        return $resultSet->fetchAllAssociative();
    }

    public function getByBudget($id, $limite)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql =
            'SELECT r.name, r.id, r.image_url, r.description 
        FROM recipe r JOIN r JOIN budget b ON r.budget_id = b.id WHERE  r.level_id = :id 
        ORDER BY r.created_at DESC LIMIT :limit';
        // $sql = 'SELECT * FROM recipe r JOIN recipe_type rt ON r.recipe_type_id = rt.id WHERE  r.recipe_type_id = 3 ORDER BY r.id DESC LIMIT 10';
        $stmt = $conn->prepare($sql);

        $stmt->bindValue('id', $id);
        $stmt->bindValue('limit', $limite, \PDO::PARAM_INT);

        $resultSet = $stmt->executeQuery();

        return $resultSet->fetchAllAssociative();
    }

    public function getByCountry($country_code, $limite)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql =
            'SELECT r.name, r.id, r.image_url, r.description 
        FROM recipe WHERE  r.level_id = :id 
        ORDER BY r.created_at DESC LIMIT :limit';
        $sql = 'SELECT * FROM recipe WHERE country_code = :country_code ORDER BY createdAt DESC LIMIT 10';
        $stmt = $conn->prepare($sql);

        $stmt->bindValue('country_code', $country_code);
        $stmt->bindValue('limit', $limite, \PDO::PARAM_INT);

        $resultSet = $stmt->executeQuery();

        return $resultSet->fetchAllAssociative();
    }

    public function getRecipeByName($name, $limite)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql =
            'SELECT * FROM recipe WHERE name LIKE "%":name"%" LIMIT :limit';
        $stmt = $conn->prepare($sql);

        $stmt->bindValue('name', $name);
        $stmt->bindValue('limit', $limite, \PDO::PARAM_INT);

        $resultSet = $stmt->executeQuery();

        return $resultSet->fetchAllAssociative();
    }

    public function getRecipeByField($fieldName, $valueName, $limite)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql =
            'SELECT * FROM recipe WHERE :fieldName LIKE "%":name"%" LIMIT :limit';
        $stmt = $conn->prepare($sql);

        $stmt->bindValue('fieldName', $fieldName);
        $stmt->bindValue('name', $valueName);
        $stmt->bindValue('limit', $limite, \PDO::PARAM_INT);

        $resultSet = $stmt->executeQuery();

        return $resultSet->fetchAllAssociative();
    }



    public function findByWord($keyword)
    {
        $query = $this->createQueryBuilder('r')
            ->where('r.name LIKE :key')
            // ->orWhere('a.prenom LIKE :key')
            ->setParameter('key', '%' . $keyword . '%')
            // ->orderBy(["r.createdAt" => "DESC"])
            ->getQuery();

        return $query->getResult();
    }

    public function countAllRecipe(){

        $query = $this->createQueryBuilder('r')
        ->select('count(r.id)')
        ->getQuery();

        return $query->getOneOrNullResult();   

    }


    //    /**
    //     * @return Recipe[] Returns an array of Recipe objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Recipe
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
