<?php

namespace App\Repository;

use App\Entity\CookingEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CookingEvent>
 *
 * @method CookingEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method CookingEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method CookingEvent[]    findAll()
 * @method CookingEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CookingEventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CookingEvent::class);
    }

    public function save(CookingEvent $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CookingEvent $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function orderByCreatedAt()
    {
        return $this->createQueryBuilder('ce')
            ->orderBy('ce.createdAt', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    public function findByWord($keyword)
    {
        $query = $this->createQueryBuilder('ce')
            ->where('ce.name LIKE :key')
            // ->orWhere('a.prenom LIKE :key')
            ->setParameter('key', '%' . $keyword . '%')
            // ->orderBy(["r.createdAt" => "DESC"])
            ->getQuery();

        return $query->getResult();
    }

    //    /**
    //     * @return CookingEvent[] Returns an array of CookingEvent objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?CookingEvent
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
