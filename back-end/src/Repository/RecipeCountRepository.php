<?php

namespace App\Repository;

use App\Entity\RecipeCount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RecipeCount>
 *
 * @method RecipeCount|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecipeCount|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecipeCount[]    findAll()
 * @method RecipeCount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeCountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RecipeCount::class);
    }

    public function save(RecipeCount $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(RecipeCount $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    

//    /**
//     * @return RecipeCount[] Returns an array of RecipeCount objects
//     */
//    public function findOneByUserId($id): array
//    {
//        return $this->createQueryBuilder('rc')
//            ->andWhere('rc.user_id = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RecipeCount
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
