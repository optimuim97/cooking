<?php

namespace App\Repository;

use App\Entity\BookMaker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BookMaker>
 *
 * @method BookMaker|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookMaker|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookMaker[]    findAll()
 * @method BookMaker[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookMakerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BookMaker::class);
    }

    public function save(BookMaker $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(BookMaker $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    public function findSavedRecipe($user_id, $recipe_id){
        $conn = $this->getEntityManager()->getConnection();
        // $sql =
        //     'SELECT * FROM recipe r JOIN level l ON r.level_id = l.id WHERE  r.level_id = :id 
        // ORDER BY r.created_at DESC LIMIT :limit';

        //$sql = 'SELECT `book_maker_user`.`book_maker_id`,`book_maker_user`.`user_id` FROM `cuisine_abj`.`book_maker_user`;WHERE book_maker_id = :book_marke AND user_id = 145'; 

        // $stmt = $conn->prepare($sql);

        // $stmt->bindValue('id', $id);
        // $stmt->bindValue('limit', $limite, \PDO::PARAM_INT);

        // $resultSet = $stmt->executeQuery();

        // return $resultSet->fetchAllAssociative();
    }

//    /**
//     * @return BookMaker[] Returns an array of BookMaker objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?BookMaker
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
