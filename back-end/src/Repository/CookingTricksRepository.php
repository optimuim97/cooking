<?php

namespace App\Repository;

use App\Entity\CookingTricks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CookingTricks>
 *
 * @method CookingTricks|null find($id, $lockMode = null, $lockVersion = null)
 * @method CookingTricks|null findOneBy(array $criteria, array $orderBy = null)
 * @method CookingTricks[]    findAll()
 * @method CookingTricks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CookingTricksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CookingTricks::class);
    }

    public function save(CookingTricks $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CookingTricks $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

   public function orderByCreatedAt()
   {
       return $this->createQueryBuilder('c')
           ->orderBy('c.createdAt', 'DESC')
           ->setMaxResults(10)
           ->getQuery()
           ->getResult()
       ;
   }

}
