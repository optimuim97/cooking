<?php

namespace App\Repository;

use App\Entity\SusbcriptionType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SusbcriptionType>
 *
 * @method SusbcriptionType|null find($id, $lockMode = null, $lockVersion = null)
 * @method SusbcriptionType|null findOneBy(array $criteria, array $orderBy = null)
 * @method SusbcriptionType[]    findAll()
 * @method SusbcriptionType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SusbcriptionTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SusbcriptionType::class);
    }

    public function save(SusbcriptionType $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SusbcriptionType $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return SusbcriptionType[] Returns an array of SusbcriptionType objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SusbcriptionType
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
