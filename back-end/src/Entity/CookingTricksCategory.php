<?php

namespace App\Entity;

use App\Entity\traits\UniqueCode;
use App\Repository\CookingTricksCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PrePersist;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: CookingTricksCategoryRepository::class)]
#[HasLifecycleCallbacks]
class CookingTricksCategory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    use UniqueCode;

    #[ORM\Column(nullable:true)]
    private ?int $oldId = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'cookingTricksCategory', targetEntity: CookingTricks::class)]
    private Collection $cookingTricks;

    #[PrePersist]
    public function addUniqueCode(){
        $this->uuid = Uuid::v1();
    }

    public function __construct()
    {
        $this->cookingTricks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, CookingTricks>
     */
    public function getCookingTricks(): Collection
    {
        return $this->cookingTricks;
    }

    public function addCookingTrick(CookingTricks $cookingTrick): self
    {
        if (!$this->cookingTricks->contains($cookingTrick)) {
            $this->cookingTricks->add($cookingTrick);
            $cookingTrick->setCookingTricksCategory($this);
        }

        return $this;
    }

    public function removeCookingTrick(CookingTricks $cookingTrick): self
    {
        if ($this->cookingTricks->removeElement($cookingTrick)) {
            // set the owning side to null (unless already changed)
            if ($cookingTrick->getCookingTricksCategory() === $this) {
                $cookingTrick->setCookingTricksCategory(null);
            }
        }

        return $this;
    }

    public function getOldId()
    {
        return $this->oldId;
    }

    /**
     * Set the value of oldId
     *
     * @return  self
     */ 
    public function setOldId($oldId)
    {
        $this->oldId = $oldId;

        return $this;
    }
}
