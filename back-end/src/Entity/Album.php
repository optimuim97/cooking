<?php

namespace App\Entity;

use App\Repository\AlbumRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AlbumRepository::class)]
class Album
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    private ?string $vue = null;

    #[ORM\Column(length: 255)]
    private ?string $byNight = null;

    #[ORM\ManyToOne(inversedBy: 'album')]
    private ?TypeAlbum $typeAlbum = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getVue(): ?string
    {
        return $this->vue;
    }

    public function setVue(string $vue): self
    {
        $this->vue = $vue;

        return $this;
    }

    public function getByNight(): ?string
    {
        return $this->byNight;
    }

    public function setByNight(string $byNight): self
    {
        $this->byNight = $byNight;

        return $this;
    }

    public function getTypeAlbum(): ?TypeAlbum
    {
        return $this->typeAlbum;
    }

    public function setTypeAlbum(?TypeAlbum $typeAlbum): self
    {
        $this->typeAlbum = $typeAlbum;

        return $this;
    }
}
