<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\OrderStatusRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderStatusRepository::class)]
class OrderStatus
{
    CONST WAITING = "waiting";
    CONST CANCEL = "cancel";
    CONST CONFIRMED = "confirmed";
    CONST TREATED = "treated";
    CONST BEING_PROCESSED = "being_processed";

    use Timestapable;
    use base64Field;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $color_code = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_enabled = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getColorCode(): ?string
    {
        return $this->color_code;
    }

    public function setColorCode(?string $color_code): self
    {
        $this->color_code = $color_code;

        return $this;
    }

    public function isIsEnabled(): ?bool
    {
        return $this->is_enabled;
    }

    public function setIsEnabled(?bool $is_enabled): self
    {
        $this->is_enabled = $is_enabled;

        return $this;
    }
}
