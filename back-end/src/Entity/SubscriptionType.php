<?php

namespace App\Entity;

use App\Repository\SubscriptionTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SubscriptionTypeRepository::class)]
class SubscriptionType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["show_subscription_formula", 'show_subscription'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription_formula", 'show_subscription'])]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(["show_subscription_formula", 'show_subscription'])]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'subscriptionType', targetEntity: SubscriptionFormula::class)]
    private Collection $subscriptionFormulas;

    public function __construct()
    {
        $this->subscriptionFormulas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, SubscriptionFormula>
     */
    public function getSubscriptionFormulas(): Collection
    {
        return $this->subscriptionFormulas;
    }

    public function addSubscriptionFormula(SubscriptionFormula $subscriptionFormula): self
    {
        if (!$this->subscriptionFormulas->contains($subscriptionFormula)) {
            $this->subscriptionFormulas->add($subscriptionFormula);
            $subscriptionFormula->setSubscriptionType($this);
        }

        return $this;
    }

    public function removeSubscriptionFormula(SubscriptionFormula $subscriptionFormula): self
    {
        if ($this->subscriptionFormulas->removeElement($subscriptionFormula)) {
            // set the owning side to null (unless already changed)
            if ($subscriptionFormula->getSubscriptionType() === $this) {
                $subscriptionFormula->setSubscriptionType(null);
            }
        }

        return $this;
    }
}
