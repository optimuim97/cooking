<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\InvoiceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: InvoiceRepository::class)]
class Invoice
{
    use Timestapable;
    use base64Field;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $service = null;

    #[ORM\Column(length: 255)]
    private ?string $reference = null;

    #[ORM\Column(length: 255)]
    private ?string $link = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $subtotal = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $tax = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $total = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_paid = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $currency_code = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $currency_name = null;

    #[ORM\ManyToOne(inversedBy: 'invoices')]
    private ?order $orderInvoice = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getSubtotal(): ?string
    {
        return $this->subtotal;
    }

    public function setSubtotal(?string $subtotal): self
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getTax(): ?string
    {
        return $this->tax;
    }

    public function setTax(?string $tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(?string $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function isIsPaid(): ?bool
    {
        return $this->is_paid;
    }

    public function setIsPaid(?bool $is_paid): self
    {
        $this->is_paid = $is_paid;

        return $this;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currency_code;
    }

    public function setCurrencyCode(?string $currency_code): self
    {
        $this->currency_code = $currency_code;

        return $this;
    }

    public function getCurrencyName(): ?string
    {
        return $this->currency_name;
    }

    public function setCurrencyName(?string $currency_name): self
    {
        $this->currency_name = $currency_name;

        return $this;
    }

    public function getOrderInvoice(): ?order
    {
        return $this->orderInvoice;
    }

    public function setOrderInvoice(?order $orderInvoice): self
    {
        $this->orderInvoice = $orderInvoice;

        return $this;
    }
}
