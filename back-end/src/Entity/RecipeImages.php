<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\RecipeImagesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RecipeImagesRepository::class)]
class RecipeImages
{
    use Timestapable;
    use base64Field;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'recipeImages', cascade:['persist', 'remove'])]
    #[ORM\JoinColumn(name:"recipe_id", referencedColumnName:"id", onDelete:"CASCADE")]
    private ?Recipe $recipe = null;

    #[ORM\Column(length: 255)]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $url = null;

    #[ORM\Column(length: 255, nullable:true)]
    private ?string $mimeType = null;

    #[ORM\Column(length: 255, nullable:true)]
    private ?string $originalName = null;

    #[ORM\Column(length: 255, nullable:true)]
    private ?string $caption = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecipe($need = false): ?Recipe
    {
        if($need){
            return $this->recipe;
        }else{
            return null;
        }
    }

    public function setRecipe(?Recipe $recipe): self
    {
        $this->recipe = $recipe;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getCaption(): ?string
    {
        return $this->caption;
    }

    public function setCaption(string $caption): self
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get the value of mimeType
     */ 
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Set the value of mimeType
     *
     * @return  self
     */ 
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get the value of originalName
     */ 
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * Set the value of originalName
     *
     * @return  self
     */ 
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;

        return $this;
    }
}
