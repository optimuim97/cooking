<?php

namespace App\Entity;

use App\Repository\RateRestaurantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RateRestaurantRepository::class)]
class RateRestaurant
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToMany(targetEntity: Restaurant::class, inversedBy: 'rateRestaurants')]
    private Collection $restaurant;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'rateRestaurants')]
    private Collection $user;

    #[ORM\Column(length: 255)]
    private ?string $numberOfStart = null;

    public function __construct()
    {
        $this->restaurant = new ArrayCollection();
        $this->user = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Restaurant>
     */
    // public function getRestaurant(): Collection
    // {
    //     return $this->restaurant;
    // }

    public function addRestaurant(Restaurant $restaurant): self
    {
        if (!$this->restaurant->contains($restaurant)) {
            $this->restaurant->add($restaurant);
        }

        return $this;
    }

    public function removeRestaurant(Restaurant $restaurant): self
    {
        $this->restaurant->removeElement($restaurant);

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
   /*  public function getUser(): Collection
    {
        return $this->user;
    } */

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user->add($user);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->user->removeElement($user);

        return $this;
    }

    public function getNumberOfStart(): ?string
    {
        return $this->numberOfStart;
    }

    public function setNumberOfStart(string $numberOfStart): self
    {
        $this->numberOfStart = $numberOfStart;

        return $this;
    }
}
