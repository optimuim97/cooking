<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Entity\traits\UniqueCode;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PrePersist;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
#[HasLifecycleCallbacks]
class Order
{

    CONST ORDER_TYPE_RESTO="Restaurant";
    CONST ORDER_TYPE_SUBSCRIPTION="Souscription";
    CONST ORDER_TYPE_SUBSCRIPTION_COOKER="Souscription Cooker";
    CONST ORDER_TYPE_COURSE="Souscription Cours";

    use Timestapable;
    use base64Field;
    use UniqueCode;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["show_order", "user_info", 'show_course'])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'orders')]
    #[Groups(["show_order","show_course"])]
    private ?User $user = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["show_order", "user_info", 'show_course'])]
    private ?bool $is_waiting = null;

    #[ORM\Column]
    #[Groups(["show_order", "user_info", 'show_course', 'show_subscription'])]
    private ?bool $is_paid = null;

    #[ORM\Column(length: 255, nullable:true)]
    #[Groups(["show_order", "user_info", 'show_course', 'show_subscription'])]
    private ?string $currency_code = null;

    #[ORM\Column(length: 255 , nullable:true)]
    #[Groups(["show_order", "user_info", 'show_course'])]
    private ?string $currency_name = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_order", "user_info", 'show_course', 'show_subscription'])]
    private ?string $amount = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_order", "user_info", 'show_course'])]
    private ?string $tax = null;

    #[ORM\Column]
    #[Groups(["show_order", "user_info", 'show_course'])]
    private ?int $orderStatus = null;

    #[ORM\Column(nullable:true)]
    #[Groups(["show_order", "user_info", 'show_course', 'show_subscription'])]
    private ?string $reference = null;

    #[ORM\OneToMany(mappedBy: 'orderInvoice', targetEntity: Invoice::class)]
    #[Groups(["show_order", "user_info", 'show_course'])]
    private Collection $invoices;

    #[ORM\Column(length: 255)]
    #[Groups(["show_order", "user_info", 'show_course'])]
    private ?string $total_amount = null;

    #[ORM\OneToMany(mappedBy: 'commande', targetEntity: OrderItems::class)]
    #[Groups(["show_order", "user_info", 'show_course'])]
    private Collection $OrderItems;

    #[ORM\ManyToOne(inversedBy: 'orders')]
    #[Groups(["show_order", "user_info", 'show_course'])]
    private ?Restaurant $resto = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_order", "user_info", 'show_course'])]
    private ?string $OrderStatusStr = null;

    #[ORM\Column(length: 255)]
    private ?string $referenceApaym = null;

    #[ORM\Column(length: 255)]
    private ?string $orderPaymentStatus = null;

    #[ORM\OneToMany(mappedBy: 'commande', targetEntity: OrderHistory::class)]
    private Collection $orderHistories;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(["show_order", "user_info", 'show_course'])]
    private ?OrderDelivery $orderDelivery;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(["show_order"])]
    private ?Course $course;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(["show_order"])]
    private ?Subscription $subscription = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_order", "user_info"])]
    private ?string $commandeType = null;

    public function __construct()
    {
        $this->invoices = new ArrayCollection();
        $this->OrderItems = new ArrayCollection();
        $this->orderHistories = new ArrayCollection();
    }

    #[PrePersist]
    public function addUniqueCode(){
        $this->uuid = Uuid::v1();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function isIsWaiting(): ?bool
    {
        return $this->is_waiting;
    }

    public function setIsWaiting(?bool $is_waiting): self
    {
        $this->is_waiting = $is_waiting;

        return $this;
    }

    public function isIsPaid(): ?bool
    {
        return $this->is_paid;
    }

    public function setIsPaid(bool $is_paid): self
    {
        $this->is_paid = $is_paid;

        return $this;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currency_code;
    }

    public function setCurrencyCode(string $currency_code): self
    {
        $this->currency_code = $currency_code;
        return $this;
    }

    public function getCurrencyName(): ?string
    {
        return $this->currency_name;
    }

    public function setCurrencyName(string $currency_name): self
    {
        $this->currency_name = $currency_name;
        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    public function getOrderStatus(): ?int
    {
        return $this->orderStatus;
    }

    public function setOrderStatus(int $orderStatus): self
    {
        $this->orderStatus = $orderStatus;
        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return Collection<int, Invoice>
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices->add($invoice);
            $invoice->setOrderInvoice($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getOrderInvoice() === $this) {
                $invoice->setOrderInvoice(null);
            }
        }

        return $this;
    }

    public function getTotalAmount(): ?string
    {
        return $this->total_amount;
    }

    public function setTotalAmount(string $total_amount): self
    {
        $this->total_amount = $total_amount;

        return $this;
    }

    /**
     * @return Collection<int, OrderItems>
     */
    public function getOrderItems(): Collection
    {
        return $this->OrderItems;
    }

    public function addOrderItem(OrderItems $orderItem): self
    {
        if (!$this->OrderItems->contains($orderItem)) {
            $this->OrderItems->add($orderItem);
            $orderItem->setCommande($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItems $orderItem): self
    {
        if ($this->OrderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getCommande() === $this) {
                $orderItem->setCommande(null);
            }
        }

        return $this;
    }

    public function getResto(): ?Restaurant
    {
        return $this->resto;
    }

    public function setResto(?Restaurant $resto): self
    {
        $this->resto = $resto;

        return $this;
    }

    public function getTax()
    {
        return $this->tax;
    }
    
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    public function getOrderStatusStr(): ?string
    {
        return $this->OrderStatusStr;
    }

    public function setOrderStatusStr(string $OrderStatusStr): self
    {
        $this->OrderStatusStr = $OrderStatusStr;

        return $this;
    }

    public function getReferenceApaym(): ?string
    {
        return $this->referenceApaym;
    }

    public function setReferenceApaym(string $referenceApaym): self
    {
        $this->referenceApaym = $referenceApaym;

        return $this;
    }

    public function getOrderPaymentStatus(): ?string
    {
        return $this->orderPaymentStatus;
    }

    public function setOrderPaymentStatus(string $orderPaymentStatus): self
    {
        $this->orderPaymentStatus = $orderPaymentStatus;

        return $this;
    }

    /**
     * @return Collection<int, OrderHistory>
     */
    public function getOrderHistories(): Collection
    {
        return $this->orderHistories;
    }

    public function addOrderHistory(OrderHistory $orderHistory): self
    {
        if (!$this->orderHistories->contains($orderHistory)) {
            $this->orderHistories->add($orderHistory);
            $orderHistory->setCommande($this);
        }

        return $this;
    }

    public function removeOrderHistory(OrderHistory $orderHistory): self
    {
        if ($this->orderHistories->removeElement($orderHistory)) {
            // set the owning side to null (unless already changed)
            if ($orderHistory->getCommande() === $this) {
                $orderHistory->setCommande(null);
            }
        }

        return $this;
    }

    public function getOrderDelivery(){
        return $this->orderDelivery;
    }

    public function setOrderDelivery(OrderDelivery $orderDelivery)
    {
        $this->orderDelivery = $orderDelivery;
        return $this;
    }
    
    public function setCourse($course)
    {
        $this->course = $course;

        return $this;
    }
    
    public function getCourse()
    {
        return $this->course;
    }

    public function getSubscription()
    {
        return $this->subscription;
    }

    public function setSubscription($subscription) : self
    {
        $this->subscription = $subscription;

        return $this;
    }

    public function getCommandeType(): ?string
    {
        return $this->commandeType;
    }

    public function setCommandeType(string $commandeType): self
    {
        $this->commandeType = $commandeType;

        return $this;
    }
}
