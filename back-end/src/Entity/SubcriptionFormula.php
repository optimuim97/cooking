<?php

namespace App\Entity;

use App\Repository\SubcriptionFormulaRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SubcriptionFormulaRepository::class)]
class SubcriptionFormula
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['show_subscription'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['show_subscription'])]
    private ?string $currencyCode = null;

    #[ORM\Column(length: 255)]
    #[Groups(['show_subscription'])]
    private ?string $currencyName = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function getCurrencyName(): ?string
    {
        return $this->currencyName;
    }

    public function setCurrencyName(string $currencyName): self
    {
        $this->currencyName = $currencyName;

        return $this;
    }
}
