<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\RecipeTypeRepository;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;
use Symfony\Component\Serializer\Annotation\Groups;



#[ORM\Entity(repositoryClass: RecipeTypeRepository::class)]
#[HasLifecycleCallbacks]
class RecipeType
{
    use Timestapable;
    use base64Field;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["show_recipe", "show_recipe_type"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["list_recipes", "show_recipe", "user_info", "show_recipe_type"])]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $cattype = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $cataff = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["list_recipes", "show_recipe", "user_info", "show_recipe_type"])]
    private ?string $slug = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["list_recipes", "user_info", "show_recipe_type"])]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["list_recipes", "user_info", "show_recipe_type"])]
    private ?string $image_url = null;

    #[ORM\OneToMany(mappedBy: 'recipeType', targetEntity: Recipe::class)]
    private Collection $recipes;

    #[PrePersist]
    public function generateSlug()
    {
        $this->slug = (new Slugify())->slugify($this->name);
    }

    public function __construct()
    {
        $this->recipes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    public function setImageUrl(string $image_url): self
    {
        $this->image_url = $image_url;

        return $this;
    }

    /**
     * @return Collection<int, Recipe>
     */
    public function getRecipes($need = false)
    {
        if ($need) {
            return $this->recipes;
        } else {
            return [];
        }
    }

    public function addRecipe(Recipe $recipe): self
    {
        if (!$this->recipes->contains($recipe)) {
            $this->recipes->add($recipe);
            $recipe->setRecipeType($this);
        }

        return $this;
    }

    public function removeRecipe(Recipe $recipe): self
    {
        if ($this->recipes->removeElement($recipe)) {
            // set the owning side to null (unless already changed)
            if ($recipe->getRecipeType() === $this) {
                $recipe->setRecipeType(null);
            }
        }

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }
}
