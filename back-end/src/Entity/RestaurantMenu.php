<?php

namespace App\Entity;

use App\Repository\RestaurantMenuRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RestaurantMenuRepository::class)]
class RestaurantMenu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups("show_restaurant")]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'restaurantMenus')]
    private ?restaurant $restaurant = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRestaurant(): ?restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }
}
