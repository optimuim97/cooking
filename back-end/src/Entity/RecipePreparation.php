<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\RecipePreparationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RecipePreparationRepository::class)]
class RecipePreparation
{
    use Timestapable;
    use base64Field;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $total_time = null;

    #[ORM\Column(length: 255)]
    private ?string $preparation_time = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotalTime(): ?string
    {
        return $this->total_time;
    }

    public function setTotalTime(string $total_time): self
    {
        $this->total_time = $total_time;

        return $this;
    }

    public function getPreparationTime(): ?string
    {
        return $this->preparation_time;
    }

    public function setPreparationTime(string $preparation_time): self
    {
        $this->preparation_time = $preparation_time;

        return $this;
    }
}
