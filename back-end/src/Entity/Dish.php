<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\DishRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Types;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: DishRepository::class)]
class Dish
{
    use Timestapable;
    use base64Field;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["user_info", "show_restaurant","show_order", "show_dish"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user_info", "show_restaurant","show_order", "show_dish"])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user_info", "show_restaurant","show_order", "show_dish"])]
    private ?string $prix = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(["user_info", "show_restaurant","show_order", "show_dish"])]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user_info", "show_restaurant","show_order", "show_dish"])]
    private ?string $image_url = null;

    #[ORM\ManyToOne(inversedBy: 'dishes')]
    private ?restaurant $restaurant = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["user_info", "show_restaurant","show_order", "show_dish"])]
    private ?bool $is_buyable = null;

    #[ORM\ManyToOne]
    #[Groups(["user_info", "show_restaurant","show_order", "show_dish"])]
    private ?DishType $dishType = null;

    #[ORM\ManyToMany(targetEntity: OrderItems::class, inversedBy: 'dish')]
    #[Groups(["show_order"])]
    private Collection $orderItems;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    public function setImageUrl(string $image_url): self
    {
        $this->image_url = $image_url;

        return $this;
    }

    // public function getRestaurant(): ?restaurant
    // {
    //     return $this->restaurant;
    // }

    public function setRestaurant(?restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function isIsBuyable(): ?bool
    {
        return $this->is_buyable;
    }

    public function setIsBuyable(?bool $is_buyable): self
    {
        $this->is_buyable = $is_buyable;

        return $this;
    }

    public function getDishType(): ?DishType
    {
        return $this->dishType;
    }

    public function setDishType(?DishType $dishType): self
    {
        $this->dishType = $dishType;

        return $this;
    }

    /**
     * Get the value of prix
     */
    public function getPrix()
    {
        return $this->prix;
    }

    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    public function getOrderItems(){

        return $this->orderItems;
        
    }

    public function setOrderItems(?OrderItems $orderItems) : self
    {
        if(!$this->orderItems->contains($orderItems)){
            $this->orderItems->add($orderItems);
        }

        return $this;
    }
}
