<?php

namespace App\Entity;

use App\Entity\traits\Timestapable;
use App\Repository\CommentRecipeRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PrePersist;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CommentRecipeRepository::class)]
#[HasLifecycleCallbacks]
class CommentRecipe
{
    use Timestapable;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'commentRecipes', cascade:['persist', 'remove'])]
    #[ORM\JoinColumn(name:"recipe_id", referencedColumnName:"id", onDelete:"CASCADE", )]
    #[Groups(["comment_list"])]
    private ?recipe $recipe = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(["show_recipe", "comment_list"])]
    private ?string $content = null;

    #[Groups(["show_recipe", "comment_list"])]
    #[ORM\ManyToOne(inversedBy: 'commentRecipes')]
    private ?User $user = null;

    #[PrePersist]
    public function prePersist()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getRecipe($need = false)
    {
        if ($need == true) {
            return $this->recipe;
        } else {
            return null;
        }
    }

    public function setRecipe(?recipe $recipe): self
    {
        $this->recipe = $recipe;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getUser($need = true)
    {
        if ($need == true) {
            return $this->user;
        } else {
            return null;
        }
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
