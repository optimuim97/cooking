<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\HasSlug;
use App\Entity\traits\Timestapable;
use App\Entity\traits\UniqueCode;
use App\Repository\SubscriptionRepository;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PrePersist;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SubscriptionRepository::class)]
#[HasLifecycleCallbacks]
class Subscription
{
    use Timestapable;
    use base64Field;
    use UniqueCode;
    use HasSlug;
    use Timestapable;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["show_subscription", "user_info"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    // #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["show_subscription", "user_info"])]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    // #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["show_subscription", "user_info"])]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["show_subscription", "user_info"])]
    private ?string $slug = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["show_subscription", "user_info"])]
    private ?string $price = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription", "user_info"])]
    private ?string $duration = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["show_subscription", "user_info"])]
    private ?string $currency_code = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["show_subscription", "user_info"])]
    private ?string $currency_name = null;
 
    #[ORM\Column]
    #[Groups(["show_subscription", "user_info"])]
    private ?bool $is_totally_free = null;

    #[ORM\ManyToOne(inversedBy: 'susbcription')]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["show_subscription", "user_info"])]
    private ?SubscriptionType $subscriptionType = null;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'subscriptions')]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["show_subscription"])]
    private Collection $subscriber;

    #[ORM\ManyToOne(inversedBy: 'subscriptions')]
    #[Groups(["show_subscription", "user_info"])]
    private ?SubscriptionFormula $subscriptionFormula = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(["show_subscription", "user_info"])]
    private ?\DateTimeInterface $startDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(["show_subscription", "user_info"])]
    private ?\DateTimeInterface $endDate = null;

    #[ORM\Column(length: 255, options:["default"=> 1])]
    private ?string $unit = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(["show_subscription", "user_info"])]
    private ?Order $commande = null;

    #[PrePersist]
    public function generateSlug()
    {
        $this->slug = (new Slugify())->slugify($this->title);
    }

    public function __construct()
    {
        // $this->subscription = new ArrayCollection();
        $this->subscriber = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currency_code;
    }

    public function setCurrencyCode(?string $currency_code): self
    {
        $this->currency_code = $currency_code;

        return $this;
    }

    public function getCurrencyName(): ?string
    {
        return $this->currency_name;
    }

    public function setCurrencyName(string $currency_name): self
    {
        $this->currency_name = $currency_name;

        return $this;
    }

    public function isIsTotallyFree(): ?bool
    {
        return $this->is_totally_free;
    }

    public function setIsTotallyFree(bool $is_totally_free): self
    {
        $this->is_totally_free = $is_totally_free;

        return $this;
    }

    public function getSubscriptionType(): ?SubscriptionType
    {
        return $this->subscriptionType;
    }

    public function setSubscriptionType(?SubscriptionType $subscriptionType): self
    {
        $this->subscriptionType = $subscriptionType;

        return $this;
    }
    /**
     * @return Collection<int, User>
     */
    public function getSubscriber(): Collection
    {
        return $this->subscriber;
    }

    public function addSubscriber(User $subscriber): self
    {
        if (!$this->subscriber->contains($subscriber)) {
            $this->subscriber->add($subscriber);
        }

        return $this;
    }

    public function removeSubscriber(User $subscriber): self
    {
        $this->subscriber->removeElement($subscriber);

        return $this;
    }

    public function getSubscriptionFormula(): ?SubscriptionFormula
    {
        return $this->subscriptionFormula;
    }

    public function setSubscriptionFormula(?SubscriptionFormula $subscriptionFormula): self
    {
        $this->subscriptionFormula = $subscriptionFormula;

        return $this;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(string $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    public function getCommande(): ?Order
    {
        return $this->commande;
    }

    public function setCommande(?Order $commande): self
    {
        $this->commande = $commande;

        return $this;
    }
}
