<?php

namespace App\Entity;

use App\Repository\RateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RateRepository::class)]
class Rate
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToMany(targetEntity: Recipe::class, inversedBy: 'rates')]
    private Collection $recipe;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'rates')]
    #[Groups(["show_recipe"])]
    private Collection $user;

    #[ORM\Column(length: 255)]
    #[Groups(["show_recipe"])]
    private ?string $number_of_start = null;

    public function __construct()
    {
        $this->recipe = new ArrayCollection();
        $this->user = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Recipe>
     */

    /* 
        public function getRecipe(): Collection
        {
            return $this->recipe;
        }

       

        public function removeRecipe(Recipe $recipe): self
        {
            $this->recipe->removeElement($recipe);

            return $this;
        }
    */

    public function addRecipe(Recipe $recipe): self
    {
        if (!$this->recipe->contains($recipe)) {
            $this->recipe->add($recipe);
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user->add($user);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->user->removeElement($user);

        return $this;
    }

    public function getNumberOfStart(): ?string
    {
        return $this->number_of_start;
    }

    public function setNumberOfStart(string $number_of_start): self
    {
        $this->number_of_start = $number_of_start;

        return $this;
    }
}
