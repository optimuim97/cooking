<?php

namespace App\Entity;

use App\Entity\traits\UniqueCode;
use App\Repository\SubscriptionFormulaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SubscriptionFormulaRepository::class)]
class SubscriptionFormula
{
    use UniqueCode;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["show_subscription_formula", "show_subscription"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription_formula", "show_subscription"])]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(["show_subscription_formula", "show_subscription"])]
    private ?string $description = null;

    #[ORM\Column]
    #[Groups(["show_subscription_formula"])]
    private ?bool $actif = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription_formula"])]
    private ?string $expirationDate = null;
    
    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription_formula"])]
    private ?string $startDate = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription_formula"])]
    private ?string $price = null;

    #[ORM\Column]
    #[Groups(["show_subscription_formula"])]
    private ?bool $isPromo = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription_formula"])]
    private ?string $promotionPrice = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(["show_subscription_formula"])]
    private ?string $avantages = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription_formula"])]
    private ?string $image_url = null;

    #[ORM\OneToMany(mappedBy: 'subscriptionFormula', targetEntity: Subscription::class)]
    private Collection $subscriptions;

    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription_formula"])]
    private ?string $duration = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription_formula"])]
    private ?string $currencyCode = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription_formula"])]
    private ?string $slug = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_subscription_formula"])]
    private ?string $currencyName = null;

    #[ORM\ManyToOne(inversedBy: 'subscriptionFormulas')]
    #[Groups(["show_subscription_formula"])]
    private ?SubscriptionType $subscriptionType = null;

    #[ORM\Column(length: 255, options:["default"=> 1])]
    private ?string $unit = null;

    #[ORM\OneToMany(mappedBy: 'subscription', targetEntity: SubscriptionAvantages::class)]
    private Collection $subscriptionAvantages;

    public function __construct()
    {
        $this->subscriptionAvantages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getExpirationDate(): ?string
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(string $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function isIsPromo(): ?bool
    {
        return $this->isPromo;
    }

    public function setIsPromo(bool $isPromo): self
    {
        $this->isPromo = $isPromo;

        return $this;
    }

    public function getPromotionPrice(): ?string
    {
        return $this->promotionPrice;
    }

    public function setPromotionPrice(string $promotionPrice): self
    {
        $this->promotionPrice = $promotionPrice;

        return $this;
    }

    public function getAvantages(): ?string
    {
        return $this->avantages;
    }

    public function setAvantages(string $avantages): self
    {
        $this->avantages = $avantages;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    public function setImageUrl(string $image_url): self
    {
        $this->image_url = $image_url;

        return $this;
    }


    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getCurrencyName(): ?string
    {
        return $this->currencyName;
    }

    public function setCurrencyName(string $currencyName): self
    {
        $this->currencyName = $currencyName;

        return $this;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function getSubscriptionType(): ?SubscriptionType
    {
        return $this->subscriptionType;
    }

    public function setSubscriptionType(?SubscriptionType $subscriptionType): self
    {
        $this->subscriptionType = $subscriptionType;

        return $this;
    }

 
    public function getUnit()
    {
        return $this->unit;
    }

    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * @return Collection<int, SubscriptionAvantages>
     */
    public function getSubscriptionAvantages(): Collection
    {
        return $this->subscriptionAvantages;
    }

    public function addSubscriptionAvantage(SubscriptionAvantages $subscriptionAvantage): self
    {
        if (!$this->subscriptionAvantages->contains($subscriptionAvantage)) {
            $this->subscriptionAvantages->add($subscriptionAvantage);
            $subscriptionAvantage->setSubscription($this);
        }

        return $this;
    }

    public function removeSubscriptionAvantage(SubscriptionAvantages $subscriptionAvantage): self
    {
        if ($this->subscriptionAvantages->removeElement($subscriptionAvantage)) {
            // set the owning side to null (unless already changed)
            if ($subscriptionAvantage->getSubscription() === $this) {
                $subscriptionAvantage->setSubscription(null);
            }
        }

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }
}
