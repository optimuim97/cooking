<?php

namespace App\Entity;

use App\Repository\RecipeCountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RecipeCountRepository::class)]
class RecipeCount
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_recipe_count","list_recipes", "show_recipe"])]
    private ?string $identifiant = null;

    #[ORM\Column(length: 255)]
    private ?string $user = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_recipe_count","list_recipes", "show_recipe"])]
    private ?string $rf = null;

    #[ORM\ManyToMany(targetEntity: Recipe::class, inversedBy: 'recipeCounts')]
    private Collection $recipe;

    #[ORM\Column(length: 255)]
    private ?string $number = null;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'recipeCounts')]
    private Collection $visitor;


    public function __construct()
    {
        $this->recipe = new ArrayCollection();
        $this->visitor = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifiant(): ?string
    {
        return $this->identifiant;
    }

    public function setIdentifiant(string $identifiant): self
    {
        $this->identifiant = $identifiant;

        return $this;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRf(): ?string
    {
        return $this->rf;
    }

    public function setRf(string $rf): self
    {
        $this->rf = $rf;

        return $this;
    }

    /**
     * @return Collection<int, Recipe>
     */
    public function getRecipe(): Collection
    {
        return $this->recipe;
    }

    public function addRecipe(Recipe $recipe): self
    {
        if (!$this->recipe->contains($recipe)) {
            $this->recipe->add($recipe);
        }

        return $this;
    }

    public function removeRecipe(Recipe $recipe): self
    {
        $this->recipe->removeElement($recipe);

        return $this;
    }



    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getVisitor(): Collection
    {
        return $this->visitor;
    }

    public function addVisitor(User $visitor): self
    {
        if (!$this->visitor->contains($visitor)) {
            $this->visitor->add($visitor);
        }

        return $this;
    }

    public function removeVisitor(User $visitor): self
    {
        $this->visitor->removeElement($visitor);

        return $this;
    }
}
