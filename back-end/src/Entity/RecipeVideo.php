<?php

namespace App\Entity;

use App\Repository\RecipeVideoRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RecipeVideoRepository::class)]
class RecipeVideo
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(type:Types::TEXT)]
    private ?string $mediaName = null;

    #[ORM\Column(length: 255)]
    private ?string $Source = null;

    #[ORM\Column(length: 255)]
    private ?string $videoThumb = null;

    #[ORM\Column(length: 255)]
    private ?string $pe = null;

    #[ORM\Column(length: 255)]
    private ?string $live = null;

    #[ORM\ManyToOne(inversedBy: 'recipeVideo')]
    private ?RecipeVideoCategory $recipeVideoCategory = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getMediaName(): ?string
    {
        return $this->mediaName;
    }

    public function setMediaName(string $mediaName): self
    {
        $this->mediaName = $mediaName;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->Source;
    }

    public function setSource(string $Source): self
    {
        $this->Source = $Source;

        return $this;
    }

    public function getVideoThumb(): ?string
    {
        return $this->videoThumb;
    }

    public function setVideoThumb(string $videoThumb): self
    {
        $this->videoThumb = $videoThumb;

        return $this;
    }

    public function getPe(): ?string
    {
        return $this->pe;
    }

    public function setPe(string $pe): self
    {
        $this->pe = $pe;

        return $this;
    }

    public function getLive(): ?string
    {
        return $this->live;
    }

    public function setLive(string $live): self
    {
        $this->live = $live;

        return $this;
    }

    public function getRecipeVideoCategory(): ?RecipeVideoCategory
    {
        return $this->recipeVideoCategory;
    }

    public function setRecipeVideoCategory(?RecipeVideoCategory $recipeVideoCategory): self
    {
        $this->recipeVideoCategory = $recipeVideoCategory;

        return $this;
    }
}
