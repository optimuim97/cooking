<?php

namespace App\Entity;

use App\Repository\OrderPaymentStatusRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderPaymentStatusRepository::class)]
class OrderPaymentStatus
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    CONST IS_PAID = "is_paid";
    CONST IS_NOT_PAID = "is_not_paid";
    CONST PARTIALLY_PAID = "partially_paid";

    public function getId(): ?int
    {
        return $this->id;
    }
}
