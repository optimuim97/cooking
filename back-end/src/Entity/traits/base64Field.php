<?php

namespace App\Entity\traits;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait base64Field{

    #[ORM\Column(type:Types::TEXT, nullable:true)]
    private ?string $base64Code = null;

    public function getBase64Code(){
        return $this->base64Code;
    }

    public function setBase64Code($base64Code): ?string
    {
        return $this->base64Code = $base64Code;
    }
    
}