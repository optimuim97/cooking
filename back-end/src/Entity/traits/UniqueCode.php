<?php

namespace App\Entity\traits;

use Doctrine\ORM\Mapping as ORM;

trait UniqueCode{
    #[ORM\Column(nullable:true)]
    private ?string $uuid;
}