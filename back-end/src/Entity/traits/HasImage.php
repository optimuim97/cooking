<?php

namespace App\Entity\traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

trait HasImage
{

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'Champ image est requis')]
    #[Assert\NotNull(message: 'Champ image est requis')]
    #[Groups(["user_info"])]
    private ?string $url = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $mimeType = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $originalName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $caption = null;

    public function setImageUrl($url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }
}
