<?php

namespace App\Entity\traits;

use Cocur\Slugify\Slugify;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\PrePersist;
use Symfony\Component\Serializer\Annotation\Groups;

trait HasSlug
{
    #[ORM\Column(length: 255)]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $slug = null;

    #[PrePersist]
    public function generateSlug()
    {
        $this->slug = (new Slugify())->slugify($this->name);
    }
}
