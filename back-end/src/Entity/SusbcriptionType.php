<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\SusbcriptionTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SusbcriptionTypeRepository::class)]
class SusbcriptionType
{
    use Timestapable;
    use base64Field;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'susbcriptionType', targetEntity: Subscription::class)]
    private Collection $susbcription;

    public function __construct()
    {
        $this->susbcription = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Subscription>
     */
    public function getSusbcription(): Collection
    {
        return $this->susbcription;
    }

    public function addSusbcription(Subscription $susbcription): self
    {
        if (!$this->susbcription->contains($susbcription)) {
            $this->susbcription->add($susbcription);
            $susbcription->setSusbcriptionType($this);
        }

        return $this;
    }

    public function removeSusbcription(Subscription $susbcription): self
    {
        if ($this->susbcription->removeElement($susbcription)) {
            // set the owning side to null (unless already changed)
            if ($susbcription->getSusbcriptionType() === $this) {
                $susbcription->setSusbcriptionType(null);
            }
        }

        return $this;
    }
}
