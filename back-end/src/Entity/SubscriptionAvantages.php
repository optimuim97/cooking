<?php

namespace App\Entity;

use App\Repository\SubscriptionAvantagesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SubscriptionAvantagesRepository::class)]
class SubscriptionAvantages
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    private ?string $description = null;

    #[ORM\ManyToOne(inversedBy: 'subscriptionAvantages')]
    private ?SubscriptionFormula $subscription = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSubscription(): ?SubscriptionFormula
    {
        return $this->subscription;
    }

    public function setSubscription(?SubscriptionFormula $subscription): self
    {
        $this->subscription = $subscription;

        return $this;
    }
}
