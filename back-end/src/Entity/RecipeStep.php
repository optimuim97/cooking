<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\RecipeStepRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RecipeStepRepository::class)]
class RecipeStep
{
    use Timestapable;
    use base64Field;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    // #[Groups(["list_recipes", "show_recipe"])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $title = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $image_url = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $desription = null;

    #[ORM\Column]
    private ?int $step_number = null;

    #[ORM\ManyToOne(inversedBy: 'recipeSteps', cascade:['persist', 'remove'])]
    #[ORM\JoinColumn(name:"recipe_id", referencedColumnName:"id", onDelete:"CASCADE", )]
    private ?Recipe $recipe = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    public function setImageUrl(string $image_url): self
    {
        $this->image_url = $image_url;

        return $this;
    }

    public function getDesription(): ?string
    {
        return $this->desription;
    }

    public function setDesription(?string $desription): self
    {
        $this->desription = $desription;

        return $this;
    }

    public function getStepNumber(): ?string
    {
        return $this->step_number;
    }

    public function setStepNumber(string $step_number): self
    {
        $this->step_number = $step_number;

        return $this;
    }

    public function getRecipe($need = false)
    {
        if ($need == true) {
            return $this->recipe;
        } else {
            return null;
        }
    }

    public function setRecipe(?Recipe $recipe): self
    {
        $this->recipe = $recipe;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }
}
