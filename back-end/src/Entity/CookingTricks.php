<?php

namespace App\Entity;

use App\Entity\traits\HasImage;
use App\Entity\traits\HasSlug;
use App\Entity\traits\Timestapable;
use App\Repository\CookingTricksRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\PrePersist;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CookingTricksRepository::class)]
class CookingTricks
{
    use Timestapable;
    use HasImage;
    use HasSlug;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["user_info"])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'cookingTricks')]
    #[Groups(["user_info"])]
    private ?User $user = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\NotNull(message: 'Le Champ titre est requis')]
    #[Assert\NotBlank(message: 'Le Champ titre est requis')]
    #[Groups(["user_info"])]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user_info"])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user_info"])]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user_info"])]
    private ?string $phone_number = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotNull(message: 'Champ Contenu est requis')]
    #[Assert\NotBlank(message: 'Ce Champ est requis')]
    #[Groups(["user_info"])]
    private ?string $body = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["user_info"])]
    private ?bool $isActive = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info"])]
    private ?string $PE = null;

    #[ORM\ManyToOne(inversedBy: 'cookingTricks')]
    #[Groups(["user_info"])]
    private ?CookingTricksCategory $cookingTricksCategory = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user_info"])]
    private ?string $slug = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    #[PrePersist]
    public function addCreatedAt()
    {
        $this->setCreatedAt(new \DateTimeImmutable());
        $this->setUpdatedAt(new \DateTimeImmutable());
    }

    public function getUser($need = false)
    {
        if ($need == true) {
            return $this->user;
        }

        return null;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getPE(): ?string
    {
        return $this->PE;
    }

    public function setPE(string $PE): self
    {
        $this->PE = $PE;

        return $this;
    }

    public function getCookingTricksCategory(): ?CookingTricksCategory
    {
        return $this->cookingTricksCategory;
    }

    public function setCookingTricksCategory(?CookingTricksCategory $cookingTricksCategory): self
    {
        $this->cookingTricksCategory = $cookingTricksCategory;

        return $this;
    }

    /**
     * Get the value of phone_number
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * Set the value of phone_number
     *
     * @return  self
     */
    public function setPhoneNumber($phone_number)
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }


    public function getUrl()
    {
        return $this->url;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
