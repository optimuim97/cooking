<?php

namespace App\Entity;

use DateTimeImmutable;
use Cocur\Slugify\Slugify;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\RecipeRepository;
use Carbon\Carbon;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\PostLoad;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RecipeRepository::class)]
#[HasLifecycleCallbacks]
class Recipe
{
    use Timestapable;
    use base64Field;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["list_recipes", "user_info", "show_recipe"])]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $userOldId = null;

    #[Groups(["list_recipes", "show_recipe", "user_info"])]
    private $rateAverage;

    #[Groups(["list_recipes", "show_recipe", "user_info"])]
    private $raterCount;

    #[Groups(["list_recipes"])]
    #[ORM\Column(options: ["comment" => "Reference to RecetteID"], nullable: true)]
    private ?int $recetteId = null;

    #[Groups(["list_recipes"])]
    #[ORM\Column(options: ["comment" => "Reference to hits"], nullable: true)]
    private ?int $hits = null;

    #[ORM\Column(length: 255, nullable: true, options: ["comment" => "Reference to image_url"])]
    #[Groups(["list_recipes", "show_recipe", "user_info"])]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    private ?string $imageUrl = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["list_recipes", "user_info", "show_recipe"])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(["list_recipes", "user_info", "show_recipes"])]
    private ?string $slug = null;

    #[ORM\Column(length: 255, nullable: true, options: ["comment" => "Reference to Nom on old database"])]
    #[Groups(["list_recipes"])]
    private ?string $user_name = null;

    #[ORM\Column(length: 255, nullable: true, options: ["comment" => "Reference to Email"])]
    #[Groups(["list_recipes"])]
    private ?string $user_email = null;

    #[ORM\Column(length: 255, nullable: true, options: ["comment" => "Reference to SiteWeb"])]
    #[Groups(["list_recipes"])]
    private ?string $web_site = null;

    #[ORM\Column(length: 255, nullable: true, options: ["comment" => "Reference to pays"])]
    #[Groups(["list_recipes", "user_info"])]
    private ?string $country = null;

    #[ORM\Column(nullable: true)]
    // #[Groups(["list_recipes"])]
    private ?int $recipe_type = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Number of persons for the recipe"])]
    #[Groups(["list_recipes", "show_recipe", "user_info"])]
    private ?string $number_of_persons = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Total cooking time"])]
    #[Groups(["list_recipes", "show_recipe", "user_info"])]
    // #[Assert\NotBlank(message: 'Ce champ est requis')]
    private ?string $total_time = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Total time for cuisson"])]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["list_recipes", "user_info", "show_recipe"])]
    private ?string $cooking = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Total time for marinade"])]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["list_recipes", "user_info", "show_recipe"])]
    private ?string $marinade = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Reference to Difficulte"])]
    #[Groups(["list_recipes", "user_info"])]
    private ?string $level_name = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Reference to Cout"])]
    #[Groups(["list_recipes", "user_info"])]
    private ?string $pricing = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Reference to Dedicace"])]
    #[Groups(["list_recipes", "show_recipe", "show_recipe"])]
    private ?string $dedicace = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Reference to 	Pref"])]
    #[Groups(["list_recipes", "user_info", "show_recipe"])]
    private ?string $pref = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Reference to 	Pref"])]
    #[Groups(["list_recipes", "user_info", "show_recipe"])]
    private ?string $published_at = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Reference to dateUpdate"])]
    #[Groups(["list_recipes"])]
    private ?string $last_updated = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Reference to IP"])]
    #[Groups(["list_recipes"])]
    private ?string $ip = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Reference to PE"])]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $pe = null;

    #[ORM\Column(nullable: true, options: ["comment" => "Reference to Regime"])]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $regime = null;

    #[ORM\Column(options: ["default" => true])]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?bool $is_active;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["list_recipes", "show_recipe", "user_info"])]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["list_recipes", "show_recipe", "user_info"])]
    private ?string $country_code = null;

    #[ORM\ManyToOne(inversedBy: 'recipes', cascade: ['persist', 'remove'])]
    #[Assert\NotBlank(message: 'Veuillez choisir une categorie s\'il vous plait')]
    #[Groups(["list_recipes", "show_recipe", "user_info"])]
    #[ORM\JoinColumn(name: "recipe_type_id", referencedColumnName: "id", nullable: true)]
    private ?RecipeType $recipeType = null;

    #[ORM\ManyToOne(inversedBy: 'recipes', cascade: ['persist', 'remove'])]
    #[Assert\NotBlank(message: 'Choisissez le niveau s\'il vous plait')]
    #[Groups(["list_recipes", "show_recipe"])]
    #[ORM\JoinColumn(name: "level_id", referencedColumnName: "id", nullable: true)]
    private ?Level $level = null;

    #[ORM\OneToMany(mappedBy: 'recipe', targetEntity: RecipeImages::class)]
    #[Groups(["list_recipes", "show_recipe"])]
    private Collection $recipeImages;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $suggestions = null;

    #[ORM\Column(options: ["default" => false], nullable: true)]
    private ?bool $is_tutorial = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["show_recipe"])]
    private ?string $video_url = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $ingredient_text = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $ustensilText = null;

    #[ORM\OneToMany(mappedBy: 'recipe', targetEntity: CommentRecipe::class)]
    #[Groups(["show_recipe"])]
    private Collection $commentRecipes;

    #[ORM\ManyToOne(inversedBy: 'recipes')]
    #[Groups(["list_recipes", "show_recipe"])]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    private ?Budget $budget = null;

    #[ORM\Column(nullable: true, options: ["default" => false])]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?bool $has_multiple_step = null;

    #[ORM\Column(nullable: true, options: ["default" => false])]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?bool $isSpotlight = null;

    #[ORM\OneToMany(mappedBy: 'recipe', targetEntity: RecipeStep::class)]
    #[Groups(["list_recipes", "show_recipe"])]
    private Collection $recipeSteps;

    #[ORM\ManyToMany(targetEntity: Utensil::class, mappedBy: 'recipe')]
    private Collection $utensils;

    #[ORM\Column(type: Types::TEXT, nullable: true, options: ["comment" => "Reference to preparation"])]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $preparationInstructions = null;

    #[ORM\ManyToOne(inversedBy: 'recipes')]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?User $user = null;


    #[ORM\ManyToMany(targetEntity: Ingredient::class, inversedBy: 'recipes')]
    #[Groups(["list_recipes", "show_recipe"])]
    #[Assert\NotBlank(message: 'Ajouter au moins 2 ingredients')]
    private Collection $ingredients;

    #[ORM\OneToMany(mappedBy: 'recipe', targetEntity: Like::class)]
    #[Groups(["list_recipes", "show_recipe"])]
    private Collection $likes;

    #[ORM\ManyToMany(targetEntity: BookMaker::class, mappedBy: 'recipe')]
    #[Groups(["list_recipes", "show_recipe"])]
    private Collection $bookMakers;

    #[ORM\ManyToMany(targetEntity: Rate::class, mappedBy: 'recipe')]
    #[Groups(["list_recipes"])]
    private Collection $rates;

    #[ORM\ManyToMany(targetEntity: RecipeCount::class, mappedBy: 'recipe')]
    #[Groups(["list_recipes", "show_recipe", "user_info"])]
    private Collection $recipeCounts;

    #[Groups(["list_recipes", "show_recipe", "user_info"])]
    private $numberOfViews;

    #[Groups(["list_recipes", "show_recipe"])]
    private $checkAbjIsSpotLight;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $SpotLightEnd = null;

    #[ORM\Column(length: 255)]
    private ?string $SpotLightStart = null;

    #[ORM\Column]
    private ?bool $isSpotLightAbj = null;

    #[Groups(["list_recipes", "show_recipe"])]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $endAbjDate = null;

    #[Groups(["list_recipes", "show_recipe"])]
    #[ORM\Column(length: 255)]
    private ?string $startAbjDate = null;

    #[ORM\Column(length: 255)]
    private ?string $origin_country = null;

    #[ORM\Column(length: 255)]
    private ?string $originPlateform = null;

    // #[ORM\OneToMany(mappedBy: 'recipe', targetEntity: BookMaker::class)]
    // private Collection $savedNb;

    #[PrePersist]
    public function prePersist()
    {
        $this->slug = (new Slugify())->slugify($this->name);
        $this->createdAt = new \DateTimeImmutable();
    }

    //Update before apply any updates
    #[ORM\PreUpdate]
    public function updatedAt()
    {
        $this->setUpdatedAt(new DateTimeImmutable());
    }   

    public function __construct()
    {
        $this->recipeImages = new ArrayCollection();
        $this->commentRecipes = new ArrayCollection();
        $this->recipeSteps = new ArrayCollection();
        $this->utensils = new ArrayCollection();
        $this->ingredients = new ArrayCollection();
        $this->likes = new ArrayCollection();
        // $this->savedNb = new ArrayCollection();
        $this->bookMakers = new ArrayCollection();
        $this->rates = new ArrayCollection();
        $this->recipeCounts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCountryCode(): ?string
    {
        return $this->country_code;
    }

    public function setCountryCode(string $country_code): self
    {
        $this->country_code = $country_code;

        return $this;
    }

    public function getRecipeType(): ?RecipeType
    {
        return $this->recipeType;
    }

    public function setRecipeType(?RecipeType $recipeType): self
    {
        $this->recipeType = $recipeType;

        return $this;
    }

    public function getLevel(): ?Level
    {
        return $this->level;
    }

    public function setLevel(?Level $level): self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return Collection<int, RecipeImages>
     */
    public function getRecipeImages(): Collection
    {
        return $this->recipeImages;
    }

    public function addRecipeImage(RecipeImages $recipeImage): self
    {
        if (!$this->recipeImages->contains($recipeImage)) {
            $this->recipeImages->add($recipeImage);
            $recipeImage->setRecipe($this);
        }

        return $this;
    }

    public function removeRecipeImage(RecipeImages $recipeImage): self
    {
        if ($this->recipeImages->removeElement($recipeImage)) {
            // set the owning side to null (unless already changed)
            if ($recipeImage->getRecipe() === $this) {
                $recipeImage->setRecipe(null);
            }
        }

        return $this;
    }

    public function getSuggestions(): ?string
    {
        return $this->suggestions;
    }

    public function setSuggestions(string $suggestions): self
    {
        $this->suggestions = $suggestions;

        return $this;
    }

    public function isIsTutorial(): ?bool
    {
        return $this->is_tutorial;
    }

    public function setIsTutorial(?bool $is_tutorial): self
    {
        $this->is_tutorial = $is_tutorial;

        return $this;
    }

    public function getVideoUrl(): ?string
    {
        return $this->video_url;
    }

    public function setVideoUrl(?string $video_url): self
    {
        $this->video_url = $video_url;

        return $this;
    }

    /**
     * @return Collection<int, CommentRecipe>
     */
    public function getCommentRecipes(): Collection
    {
        return $this->commentRecipes;
    }

    public function addCommentRecipe(CommentRecipe $commentRecipe): self
    {
        if (!$this->commentRecipes->contains($commentRecipe)) {
            $this->commentRecipes->add($commentRecipe);
            $commentRecipe->setRecipe($this);
        }

        return $this;
    }

    public function removeCommentRecipe(CommentRecipe $commentRecipe): self
    {
        if ($this->commentRecipes->removeElement($commentRecipe)) {
            // set the owning side to null (unless already changed)
            if ($commentRecipe->getRecipe() === $this) {
                $commentRecipe->setRecipe(null);
            }
        }

        return $this;
    }

    public function getBudget(): ?Budget
    {
        return $this->budget;
    }

    public function setBudget(?Budget $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function isHasMultipleStep(): ?bool
    {
        return $this->has_multiple_step;
    }

    public function setHasMultipleStep(?bool $has_multiple_step): self
    {
        $this->has_multiple_step = $has_multiple_step;

        return $this;
    }

    /**
     * @return Collection<int, RecipeStep>
     */
    public function getRecipeSteps(): Collection
    {
        return $this->recipeSteps;
    }

    public function addRecipeStep(RecipeStep $recipeStep): self
    {
        if (!$this->recipeSteps->contains($recipeStep)) {
            $this->recipeSteps->add($recipeStep);
            $recipeStep->setRecipe($this);
        }

        return $this;
    }

    public function removeRecipeStep(RecipeStep $recipeStep): self
    {
        if ($this->recipeSteps->removeElement($recipeStep)) {
            // set the owning side to null (unless already changed)
            if ($recipeStep->getRecipe() === $this) {
                $recipeStep->setRecipe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Utensil>
     */
    public function getUtensils(): Collection
    {
        return $this->utensils;
    }

    public function addUtensil(Utensil $utensil): self
    {
        if (!$this->utensils->contains($utensil)) {
            $this->utensils->add($utensil);
            $utensil->addRecipe($this);
        }

        return $this;
    }

    public function removeUtensil(Utensil $utensil): self
    {
        if ($this->utensils->removeElement($utensil)) {
            $utensil->removeRecipe($this);
        }

        return $this;
    }

    /**
     * Get the value of user_name
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * Set the value of user_name
     *
     * @return  self
     */
    public function setUserName($user_name)
    {
        $this->user_name = $user_name;

        return $this;
    }

    /**
     * Get the value of user_email
     */
    public function getUserEmail()
    {
        return $this->user_email;
    }

    /**
     * Set the value of user_email
     *
     * @return  self
     */
    public function setUserEmail($user_email)
    {
        $this->user_email = $user_email;

        return $this;
    }

    /**
     * Get the value of imageUrl
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set the value of imageUrl
     *
     * @return  self
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * Get the value of web_site
     */
    public function getWebSite()
    {
        return $this->web_site;
    }

    /**
     * Set the value of web_site
     *
     * @return  self
     */
    public function setWebSite($web_site)
    {
        $this->web_site = $web_site;

        return $this;
    }

    /**
     * Get the value of country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set the value of country
     *
     * @return  self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Set the value of recipe_type
     *
     * @return  self
     */
    /*   public function setRecipe_type($recipe_type)
    {
        $this->recipe_type = $recipe_type;

        return $this;
    } */

    /**
     * Get the value of number_of_persons
     */
    public function getNumberOfPersons()
    {
        return $this->number_of_persons;
    }

    /**
     * Set the value of number_of_persons
     *
     * @return  self
     */
    public function setNumberOfPersons($number_of_persons)
    {
        $this->number_of_persons = $number_of_persons;

        return $this;
    }

    /**
     * Get the value of total_time
     */
    public function getTotalTime()
    {
        return $this->total_time;
    }

    /**
     * Set the value of total_time
     *
     * @return  self
     */
    public function setTotalTime($total_time)
    {
        $this->total_time = $total_time;

        return $this;
    }

    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;

        return $this;
    }

    /**
     * Get the value of level_name
     */
    public function getLevelName()
    {
        return $this->level_name;
    }

    /**
     * Set the value of level_name
     *
     * @return  self
     */
    public function setLevelName($level_name)
    {
        $this->level_name = $level_name;

        return $this;
    }

    /**
     * Get the value of pricing
     */
    public function getPricing()
    {
        return $this->pricing;
    }

    /**
     * Set the value of pricing
     *
     * @return  self
     */
    public function setPricing($pricing)
    {
        $this->pricing = $pricing;

        return $this;
    }

    /**
     * Get the value of dedicace
     */
    public function getDedicace()
    {
        return $this->dedicace;
    }

    /**
     * Set the value of dedicace
     *
     * @return  self
     */
    public function setDedicace($dedicace)
    {
        $this->dedicace = $dedicace;

        return $this;
    }

    /**
     * Get the value of pref
     */
    public function getPref()
    {
        return $this->pref;
    }

    /**
     * Set the value of pref
     *
     * @return  self
     */
    public function setPref($pref)
    {
        $this->pref = $pref;

        return $this;
    }

    /**
     * Get the value of ip
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set the value of ip
     *
     * @return  self
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get the value of pe
     */
    public function getPe()
    {
        return $this->pe;
    }

    /**
     * Set the value of pe
     *
     * @return  self
     */
    public function setPe($pe)
    {
        $this->pe = $pe;

        return $this;
    }

    /**
     * Get the value of regime
     */
    public function getRegime()
    {
        return $this->regime;
    }

    /**
     * Set the value of regime
     *
     * @return  self
     */
    public function setRegime($regime)
    {
        $this->regime = $regime;

        return $this;
    }

    /**
     * Get the value of marinade
     */
    public function getMarinade()
    {
        return $this->marinade;
    }

    /**
     * Set the value of marinade
     *
     * @return  self
     */
    public function setMarinade($marinade)
    {
        $this->marinade = $marinade;

        return $this;
    }


    public function getCooking()
    {
        return $this->cooking;
    }

    /**
     * Set the value of cooking
     *
     * @return  self
     */
    public function setCooking($cooking)
    {
        $this->cooking = $cooking;

        return $this;
    }


    public function getPreparationInstructions()
    {
        return $this->preparationInstructions;
    }

    /**
     * Set the value of preparationInstructions
     *
     * @return  self
     */
    public function setPreparationInstructions($preparationInstructions)
    {
        $this->preparationInstructions = $preparationInstructions;

        return $this;
    }

    public function getIngredientText()
    {
        return $this->ingredient_text;
    }

    /**
     * Set the value of preparationInstructions
     *
     * @return  self
     */
    public function setIngredientText($ingredient_text)
    {
        $this->ingredient_text = $ingredient_text;

        return $this;
    }


    public function getUstensilText()
    {
        return $this->ustensilText;
    }

    /**
     * Set the value of ustensilText
     *
     * @return  self
     */
    public function setUstensilText($ustensilText)
    {
        $this->ustensilText = $ustensilText;

        return $this;
    }

    public function getUser($need = true)
    {
        if ($need == true) {
            return $this->user;
        }
        return null;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Ingredient>
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredient $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients->add($ingredient);
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): self
    {
        $this->ingredients->removeElement($ingredient);

        return $this;
    }

    /**
     * @return Collection<int, Like>
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Like $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes->add($like);
            $like->setRecipe($this);
        }

        return $this;
    }

    public function removeLike(Like $like): self
    {
        if ($this->likes->removeElement($like)) {
            // set the owning side to null (unless already changed)
            if ($like->getRecipe() === $this) {
                $like->setRecipe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, BookMaker>
     */
    public function getBookMakers()
    {
        return $this->bookMakers;
    }

    public function addBookMaker(BookMaker $bookMaker): self
    {
        if (!$this->bookMakers->contains($bookMaker)) {
            $this->bookMakers->add($bookMaker);
            $bookMaker->addRecipe($this);
        }

        return $this;
    }

    public function removeBookMaker(BookMaker $bookMaker): self
    {
        if ($this->bookMakers->removeElement($bookMaker)) {
            $bookMaker->removeRecipe($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Rate>
     */
    public function getRates(): Collection
    {
        return $this->rates;
    }

    #[PostLoad]
    public function getRateAverage()
    {
        $rates = $this->getRates();

        $rateSum = 0;
        $this->raterCount = count($rates);

        foreach ($rates as $rate) {
            $rateSum += $rate->getNumberOfStart();
        }

        if (count($rates) != 0) {
            $average = round($rateSum / $this->raterCount, 1);
        } else {
            $average = 0;
        }

        $this->rateAverage = $average;

        return $this->rateAverage;
    }


    public function getRaterCount()
    {
        return $this->raterCount;
    }

    public function addRate(Rate $rate): self
    {
        if (!$this->rates->contains($rate)) {
            $this->rates->add($rate);
            $rate->addRecipe($this);
        }

        return $this;
    }

    public function removeRate(Rate $rate): self
    {
        if ($this->rates->removeElement($rate)) {
            $rate->removeRecipe($this);
        }

        return $this;
    }

    public function getRecetteId()
    {
        return $this->recetteId;
    }

    public function getHits()
    {
        return $this->recetteId;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getPublishedAt()
    {
        return $this->published_at;
    }

    public function getLastUpdated()
    {
        return $this->last_updated;
    }

    public function getIsActive()
    {
        return $this->is_active;
    }

    public function getIsSpotlight()
    {
        return $this->isSpotlight;
    }

    public function setIsSpotlight($state) : self
    {
        $this->isSpotlight = $state;
        return $this;
    }
  
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return Collection<int, RecipeCount>
     */
    public function getRecipeCounts(): Collection
    {
        return $this->recipeCounts;
    }

    public function getNumberOfViews(){
        return $this->recipeCounts->count();
    }   

    public function addRecipeCount(RecipeCount $recipeCount): self
    {
        if (!$this->recipeCounts->contains($recipeCount)) {
            $this->recipeCounts->add($recipeCount);
            $recipeCount->addRecipe($this);
        }

        return $this;
    }

    public function removeRecipeCount(RecipeCount $recipeCount): self
    {
        if ($this->recipeCounts->removeElement($recipeCount)) {
            $recipeCount->removeRecipe($this);
        }

        return $this;
    }

    public function getSpotLightEnd(): ?string
    {
        return $this->SpotLightEnd;
    }

    public function setSpotLightEnd(?string $SpotLightEnd): self
    {
        $this->SpotLightEnd = $SpotLightEnd;

        return $this;
    }

    public function getSpotLightStart(): ?string
    {
        return $this->SpotLightStart;
    }

    public function setSpotLightStart(string $SpotLightStart): self
    {
        $this->SpotLightStart = $SpotLightStart;

        return $this;
    }

    public function isIsSpotLightAbj(): ?bool
    {
        return $this->isSpotLightAbj;
    }

    public function setIsSpotLightAbj(bool $isSpotLightAbj): self
    {
        $this->isSpotLightAbj = $isSpotLightAbj;

        return $this;
    }

    public function getEndAbjDate(): ?string
    {
        return $this->endAbjDate;
    }

    public function setEndAbjDate(?string $endAbjDate): self
    {
        $this->endAbjDate = $endAbjDate;

        return $this;
    }

    public function getStartAbjDate(): ?string
    {
        return $this->startAbjDate;
    }

    public function setStartAbjDate(string $startAbjDate): self
    {
        $this->startAbjDate = $startAbjDate;

        return $this;
    }

    public function getCheckAbjIsSpotLight(){

        $startDate = Carbon::parse($this->startAbjDate);
        $endDate =Carbon::parse( $this->endAbjDate);
    
        $check = Carbon::now()->between($startDate,$endDate);

        if($check){
            return true;
        }else{
            return false;
        }        

    }

    public function setUserOldId($userOldId)
    {
        $this->userOldId = $userOldId;

        return $this;
    }

    public function getOriginCountry(): ?string
    {
        return $this->origin_country;
    }

    public function setOriginCountry(string $originCountry): self
    {
        $this->origin_country = $originCountry;

        return $this;
    }

    public function getOriginPlateform(): ?string
    {
        return $this->originPlateform;
    }

    public function setOriginPlateform(string $originPlateform): self
    {
        $this->originPlateform = $originPlateform;

        return $this;
    }


}
