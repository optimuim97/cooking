<?php

namespace App\Entity;

use App\Entity\traits\HasSlug;
use App\Repository\LessonRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: LessonRepository::class)]
class Lesson
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["show_course"])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'lessons')]
    private ?Course $course = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_course"])]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_course"])]
    private ?string $overview = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_course"])]
    private ?string $video_url = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(["show_course"])]
    private ?string $content = null;

    use HasSlug;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getOverview(): ?string
    {
        return $this->overview;
    }

    public function setOverview(string $overview): self
    {
        $this->overview = $overview;

        return $this;
    }

    public function getVideoUrl(): ?string
    {
        return $this->video_url;
    }

    public function setVideoUrl(string $video_url): self
    {
        $this->video_url = $video_url;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
