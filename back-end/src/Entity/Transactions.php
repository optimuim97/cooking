<?php

namespace App\Entity;

use App\Repository\TransactionsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TransactionsRepository::class)]
class Transactions
{

    CONST IS_PAID = "is_paid";
    CONST IS_NOT_PAID = "is_not_paid";
    CONST PARTIAL_PAID = "partial_paid";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $amount = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $currencyCode = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $currencyName = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $fees = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $reference = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $externalReference = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $totalAmount = null;

    #[ORM\Column]
    #[Groups(["show_transaction"])]
    private ?bool $isPaid = null;

    #[ORM\ManyToOne(inversedBy: 'transactions')]
    private ?User $user = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $customerName = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $customerEmail = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $customerPhoneNumber = null;

    #[ORM\ManyToOne(inversedBy: 'transactions')]
    private ?TransactionType $transactionType = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $raison = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_transaction"])]
    private ?string $paymentMethod = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function getCurrencyName(): ?string
    {
        return $this->currencyName;
    }

    public function setCurrencyName(string $currencyName): self
    {
        $this->currencyName = $currencyName;

        return $this;
    }

    public function getFees(): ?string
    {
        return $this->fees;
    }

    public function setFees(string $fees): self
    {
        $this->fees = $fees;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getExternalReference(): ?string
    {
        return $this->externalReference;
    }

    public function setExternalReference(string $externalReference): self
    {
        $this->externalReference = $externalReference;

        return $this;
    }

    public function getTotalAmount(): ?string
    {
        return $this->totalAmount;
    }

    public function setTotalAmount(string $totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    public function isIsPaid(): ?bool
    {
        return $this->isPaid;
    }

    public function setIsPaid(bool $isPaid): self
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCustomerName(): ?string
    {
        return $this->customerName;
    }

    public function setCustomerName(string $customerName): self
    {
        $this->customerName = $customerName;

        return $this;
    }

    public function getCustomerEmail(): ?string
    {
        return $this->customerEmail;
    }

    public function setCustomerEmail(string $customerEmail): self
    {
        $this->customerEmail = $customerEmail;

        return $this;
    }

    public function getCustomerPhoneNumber(): ?string
    {
        return $this->customerPhoneNumber;
    }

    public function setCustomerPhoneNumber(string $customerPhoneNumber): self
    {
        $this->customerPhoneNumber = $customerPhoneNumber;

        return $this;
    }

    public function getTransactionType(): ?TransactionType
    {
        return $this->transactionType;
    }

    public function setTransactionType(?TransactionType $transactionType): self
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    public function getRaison(): ?string
    {
        return $this->raison;
    }

    public function setRaison(string $raison): self
    {
        $this->raison = $raison;

        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }
}
