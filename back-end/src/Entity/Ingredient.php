<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\IngredientRepository;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: IngredientRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Ingredient
{
    use Timestapable;
    use base64Field;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $quantity = null;

    #[ORM\Column(length: 255)]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $mesureUnit = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["list_recipes", "show_recipe"])]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $image_url = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $prix = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_buyable = null;

    #[ORM\Column(nullable: true, options: ["default" => 0])]
    private ?int $stock = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\ManyToMany(targetEntity: Recipe::class, mappedBy: 'ingredients')]
    private Collection $recipes;

    public function __construct()
    {
        $this->recipes = new ArrayCollection();
    }

    #[ORM\PrePersist]
    public function addSlug()
    {
        $this->setSlug(
            (new Slugify())->slugify(
                $this->name
            )
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    public function setImageUrl(string $image_url): self
    {
        $this->image_url = $image_url;
        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $prix): self
    {
        $this->prix = $prix;
        return $this;
    }

    public function isIsBuyable(): ?bool
    {
        return $this->is_buyable;
    }

    public function setIsBuyable(?bool $is_buyable): self
    {
        $this->is_buyable = $is_buyable;
        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;
        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }


    /**
     * Get the value of quantity
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set the value of quantity
     *
     * @return  self
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get the value of mesureUnit
     */
    public function getMesureUnit()
    {
        return $this->mesureUnit;
    }

    /**
     * Set the value of mesureUnit
     *
     * @return  self
     */
    public function setMesureUnit($mesureUnit)
    {
        $this->mesureUnit = $mesureUnit;

        return $this;
    }


    public function getRecipes($need = false)
    {
        if ($need == true) {
            return $this->recipes;
        } else {
            return null;
        }
    }

    public function addRecipe(Recipe $recipe): self
    {
        if (!$this->recipes->contains($recipe)) {
            $this->recipes->add($recipe);
            $recipe->addIngredient($this);
        }

        return $this;
    }

    public function removeRecipe(Recipe $recipe): self
    {
        if ($this->recipes->removeElement($recipe)) {
            $recipe->removeIngredient($this);
        }

        return $this;
    }
}
