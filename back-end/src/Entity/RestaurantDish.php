<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\RestaurantDishRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RestaurantDishRepository::class)]
class RestaurantDish
{
    use Timestapable;
    use base64Field;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'restaurantDishes')]
    private ?Restaurant $restaurant = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_restaurant"])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_restaurant"])]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_restaurant"])]
    private ?string $image_url = null;
    
    #[ORM\Column(length: 255)]
    #[Groups(["show_restaurant"])]
    private ?string $price = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["show_restaurant"])]
    private ?bool $is_buyable = null;

    #[ORM\ManyToOne]
    private ?DishType $dishType = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    public function setImageUrl(string $image_url): self
    {
        $this->image_url = $image_url;

        return $this;
    }

    public function isIsBuyable(): ?bool
    {
        return $this->is_buyable;
    }

    public function setIsBuyable(?bool $is_buyable): self
    {
        $this->is_buyable = $is_buyable;

        return $this;
    }

    public function getDishType(): ?DishType
    {
        return $this->dishType;
    }

    public function setDishType(?DishType $dishType): self
    {
        $this->dishType = $dishType;

        return $this;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

   /*  public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    } */

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }
}
