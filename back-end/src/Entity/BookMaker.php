<?php

namespace App\Entity;

use App\Repository\BookMakerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BookMakerRepository::class)]
class BookMaker
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'bookMakers')]
    #[Groups(["show_recipe"])]
    private Collection $saver;

    #[ORM\ManyToMany(targetEntity: Recipe::class, inversedBy: 'bookMakers')]
    #[Groups("user_info")]
    private Collection $recipe;

    public function __construct()
    {
        $this->saver = new ArrayCollection();
        $this->recipe = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, User>
     */
    public function getSaver(): Collection
    {
        return $this->saver;
    }

    public function addSaver(User $saver): self
    {
        if (!$this->saver->contains($saver)) {
            $this->saver->add($saver);
        }

        return $this;
    }

    public function removeSaver(User $saver): self
    {
        $this->saver->removeElement($saver);

        return $this;
    }

    /**
     * @return Collection<int, Recipe>
     */
    public function getRecipe(): Collection
    {
        return $this->recipe;
    }

    public function addRecipe(Recipe $recipe): self
    {
        if (!$this->recipe->contains($recipe)) {
            $this->recipe->add($recipe);
        }

        return $this;
    }

    public function removeRecipe(Recipe $recipe): self
    {
        $this->recipe->removeElement($recipe);

        return $this;
    }
}