<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    use Timestapable;
    use base64Field;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $descritpion = null;

    #[ORM\Column(nullable: true)]
    private ?int $stock = null;

    #[ORM\Column]
    private ?bool $is_buyable = null;

    #[ORM\Column(length: 255)]
    private ?string $price = null;

    #[ORM\Column(length: 255)]
    private ?string $real_prix = null;

    #[ORM\ManyToOne(inversedBy: 'products')]
    private ?ProductCondition $productCondition = null;

    #[ORM\ManyToMany(targetEntity: order::class)]
    private Collection $productOrders;

    #[ORM\ManyToOne(inversedBy: 'products')]
    private ?Shop $shop = null;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: ProductImages::class)]
    private Collection $productImages;

    public function __construct()
    {
        $this->productOrders = new ArrayCollection();
        $this->productImages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescritpion(): ?string
    {
        return $this->descritpion;
    }

    public function setDescritpion(?string $descritpion): self
    {
        $this->descritpion = $descritpion;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(?int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function isIsBuyable(): ?bool
    {
        return $this->is_buyable;
    }

    public function setIsBuyable(bool $is_buyable): self
    {
        $this->is_buyable = $is_buyable;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getRealPrix(): ?string
    {
        return $this->real_prix;
    }

    public function setRealPrix(string $real_prix): self
    {
        $this->real_prix = $real_prix;

        return $this;
    }

    public function getProductCondition(): ?ProductCondition
    {
        return $this->productCondition;
    }

    public function setProductCondition(?ProductCondition $productCondition): self
    {
        $this->productCondition = $productCondition;

        return $this;
    }

    /**
     * @return Collection<int, order>
     */
    public function getProductOrders(): Collection
    {
        return $this->productOrders;
    }

    public function addProductOrder(order $productOrder): self
    {
        if (!$this->productOrders->contains($productOrder)) {
            $this->productOrders->add($productOrder);
        }

        return $this;
    }

    public function removeProductOrder(order $productOrder): self
    {
        $this->productOrders->removeElement($productOrder);

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * @return Collection<int, ProductImages>
     */
    public function getProductImages(): Collection
    {
        return $this->productImages;
    }

    public function addProductImage(ProductImages $productImage): self
    {
        if (!$this->productImages->contains($productImage)) {
            $this->productImages->add($productImage);
            $productImage->setProduct($this);
        }

        return $this;
    }

    public function removeProductImage(ProductImages $productImage): self
    {
        if ($this->productImages->removeElement($productImage)) {
            // set the owning side to null (unless already changed)
            if ($productImage->getProduct() === $this) {
                $productImage->setProduct(null);
            }
        }

        return $this;
    }
}
