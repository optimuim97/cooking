<?php

namespace App\Entity;

use App\Repository\RecipeVideoCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RecipeVideoCategoryRepository::class)]
class RecipeVideoCategory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type:Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'recipeVideoCategory', targetEntity: RecipeVideo::class)]
    private Collection $recipeVideo;

    public function __construct()
    {
        $this->recipeVideo = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, RecipeVideo>
     */
    public function getRecipeVideo(): Collection
    {
        return $this->recipeVideo;
    }

    public function addRecipeVideo(RecipeVideo $recipeVideo): self
    {
        if (!$this->recipeVideo->contains($recipeVideo)) {
            $this->recipeVideo->add($recipeVideo);
            $recipeVideo->setRecipeVideoCategory($this);
        }

        return $this;
    }

    public function removeRecipeVideo(RecipeVideo $recipeVideo): self
    {
        if ($this->recipeVideo->removeElement($recipeVideo)) {
            // set the owning side to null (unless already changed)
            if ($recipeVideo->getRecipeVideoCategory() === $this) {
                $recipeVideo->setRecipeVideoCategory(null);
            }
        }

        return $this;
    }
}
