<?php

namespace App\Entity;

use App\Repository\GlossaryRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GlossaryRepository::class)]
class Glossary
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $word = null;

    #[ORM\Column(length: 255)]
    private ?string $word_en = null;

    #[ORM\Column(type:Types::TEXT)]
    private ?string $definition_fr = null;

    #[ORM\Column(type:Types::TEXT)]
    private ?string $definition_en = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWord(): ?string
    {
        return $this->word;
    }

    public function setWord(string $word): self
    {
        $this->word = $word;

        return $this;
    }

    public function getWordEn(): ?string
    {
        return $this->word_en;
    }

    public function setWordEn(string $word_en): self
    {
        $this->word_en = $word_en;

        return $this;
    }

    public function getDefinitionFr(): ?string
    {
        return $this->definition_fr;
    }

    public function setDefinitionFr(string $definition_fr): self
    {
        $this->definition_fr = $definition_fr;

        return $this;
    }

    public function getDefinitionEn(): ?string
    {
        return $this->definition_en;
    }

    public function setDefinitionEn(string $definition_en): self
    {
        $this->definition_en = $definition_en;

        return $this;
    }
}
