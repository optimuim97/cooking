<?php

namespace App\Entity;

use App\Repository\LikeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: LikeRepository::class)]
#[ORM\Table(name: '`like`')]
class Like
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'likes', targetEntity:Recipe::class)]
    #[ORM\JoinColumn(name:"recipe_id", referencedColumnName:"id", onDelete:"CASCADE")]
    private ?Recipe $recipe = null;

    #[ORM\ManyToOne(inversedBy: 'likes')]
    #[Groups(["show_recipe"])]
    private ?User $liker = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecipe($need = false)
    {

        if ($need == true) {
            return $this->recipe;
        }

        return null;
    }

    public function setRecipe(?Recipe $recipe): self
    {
        $this->recipe = $recipe;

        return $this;
    }

    public function getLiker($need = true)
    {
        if ($need == true) {

            return $this->liker;
        }

        return null;
    }

    public function setLiker(?User $liker): self
    {
        $this->liker = $liker;

        return $this;
    }
}
