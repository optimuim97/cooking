<?php

namespace App\Entity;

use App\Entity\traits\HasSlug;
use App\Entity\traits\Timestapable;
use App\Repository\CourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CourseRepository::class)]
class Course
{
    use Timestapable;
    use HasSlug;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["list_courses", "show_course","user_info"])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'courses')]
    private ?User $user = null;

    #[ORM\Column(length: 255)]
    #[Groups(["list_courses", "user_info"])]
    private ?string $slogan = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["list_courses", "user_info", "show_course"])]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    private ?string $description = null;

    #[Groups(["show_course", "list_courses", "user_info"])]
    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(["list_courses", "show_course", "user_info"])]
    private ?string $language = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["list_courses", "user_info"])]
    private ?string $video_url = null;
    
    #[Groups(["show_course", "list_courses", "user_info"])]
    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    private ?string $imageUrl = null;

    #[ORM\Column(nullable: true)]
    private ?bool $has_multiple_step = null;

    #[ORM\OneToMany(mappedBy: 'course', targetEntity: CourseStep::class)]
    private Collection $courseSteps;
    
    #[ORM\ManyToOne(inversedBy: 'courses')]
    #[Groups(["show_course", "list_courses", "user_info"])]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    private ?CourseType $courseType = null;

    #[Groups(["show_course", "user_info"])]
    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    private ?string $price = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $dateStr = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Groups(["show_course", "user_info"])]
    private ?\DateTimeInterface $date = null;

    #[Groups(["show_course", "user_info"])]
    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'Ce champ est requis')]
    private ?string $place = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_course", "user_info"])]
    private ?string $instructor = null;

    #[Groups(["show_course"])]
    #[ORM\ManyToOne(inversedBy: 'cookingCourses')]
    private ?User $publisher = null;

    #[Groups(["show_course"])]
    #[ORM\ManyToMany(targetEntity: User::class)]
    private Collection $participants;

    #[ORM\Column(length: 255)]
    #[Groups(["list_courses", "show_course", "user_info"])]
    private ?string $slug = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(["show_course"])]
    private ?Order $commande = null;

    #[ORM\OneToMany(mappedBy: 'course', targetEntity: Lesson::class)]
    #[Groups(["show_course"])]
    private Collection $lessons;

    #[ORM\OneToMany(mappedBy: 'course', targetEntity: CourseParticipants::class)]
      #[Groups(["show_course"])]
    private Collection $courseParticipants;
    
    public function __construct()
    {
        $this->courseSteps = new ArrayCollection();
        $this->participants = new ArrayCollection();
        $this->lessons = new ArrayCollection();
        $this->courseParticipants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser($need = false): ?User
    {
        if ($need == true) {
            return $this->user;
        }

        return null;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }


    public function getSlogan(): ?string
    {
        return $this->slogan;
    }

    public function setSlogan(string $slogan): self
    {
        $this->slogan = $slogan;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getVideoUrl(): ?string
    {
        return $this->video_url;
    }

    public function setVideoUrl(?string $video_url): self
    {
        $this->video_url = $video_url;

        return $this;
    }

    public function isHasMultipleStep(): ?bool
    {
        return $this->has_multiple_step;
    }

    public function setHasMultipleStep(?bool $has_multiple_step): self
    {
        $this->has_multiple_step = $has_multiple_step;

        return $this;
    }

    /**
     * @return Collection<int, CourseStep>
     */
    public function getCourseSteps(): Collection
    {
        return $this->courseSteps;
    }

    public function addCourseStep(CourseStep $courseStep): self
    {
        if (!$this->courseSteps->contains($courseStep)) {
            $this->courseSteps->add($courseStep);
            $courseStep->setCourse($this);
        }

        return $this;
    }

    public function removeCourseStep(CourseStep $courseStep): self
    {
        if ($this->courseSteps->removeElement($courseStep)) {
            // set the owning side to null (unless already changed)
            if ($courseStep->getCourse() === $this) {
                $courseStep->setCourse(null);
            }
        }

        return $this;
    }

    public function getCourseType(): ?CourseType
    {
        return $this->courseType;
    }

    public function setCourseType(?CourseType $courseType): self
    {
        $this->courseType = $courseType;

        return $this;
    }

    /**
     * Get the value of imageUrl
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set the value of imageUrl
     *
     * @return  self
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDateStr(): ?string
    {
        return $this->dateStr;
    }

    public function setDateStr(?string $dateStr): self
    {
        $this->dateStr = $dateStr;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getInstructor(): ?string
    {
        return $this->instructor;
    }

    public function setInstructor(string $instructor): self
    {
        $this->instructor = $instructor;

        return $this;
    }

    public function getPublisher(): ?user
    {
        return $this->publisher;
    }

    public function setPublisher(?user $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(User $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants->add($participant);
        }

        return $this;
    }

    public function removeParticipant(User $participant): self
    {
        $this->participants->removeElement($participant);

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCommande(): ?Order
    {
        return $this->commande;
    }

    public function setCommande(?Order $commande): self
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * @return Collection<int, Lesson>
     */
    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    public function addLesson(Lesson $lesson): self
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons->add($lesson);
            $lesson->setCourse($this);
        }

        return $this;
    }

    public function removeLesson(Lesson $lesson): self
    {
        if ($this->lessons->removeElement($lesson)) {
            // set the owning side to null (unless already changed)
            if ($lesson->getCourse() === $this) {
                $lesson->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CourseParticipants>
     */
    public function getCourseParticipants(): Collection
    {
        return $this->courseParticipants;
    }

    public function addCourseParticipant(CourseParticipants $courseParticipant): self
    {
        if (!$this->courseParticipants->contains($courseParticipant)) {
            $this->courseParticipants->add($courseParticipant);
            $courseParticipant->setCourse($this);
        }

        return $this;
    }

    public function removeCourseParticipant(CourseParticipants $courseParticipant): self
    {
        if ($this->courseParticipants->removeElement($courseParticipant)) {
            // set the owning side to null (unless already changed)
            if ($courseParticipant->getCourse() === $this) {
                $courseParticipant->setCourse(null);
            }
        }

        return $this;
    }

}
