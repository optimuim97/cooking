<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\UserRepository;
use App\Helpers\BaseRequest;
use Carbon\Carbon;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\Entity(repositoryClass: UserRepository::class)]
#[HasLifecycleCallbacks]
#[UniqueEntity('username', message: 'Le compte existe déja')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["list_recipes", "user_info", "show_course", "comment_list", "show_subscription", "show_recipe_count"])]
    private ?int $id = null;
    
    #[ORM\Column(length: 180, unique: true)]
    #[Assert\NotBlank(message: 'Champ mot de passe est requis')]
    #[Groups(["show_course", "comment_list","list_recipes", "show_recipe", "user_info", "show_subscription", "show_order", "show_recipe_count"])]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[Assert\NotBlank(message: 'Champ mot de passe est requis')]
    #[Assert\Length(min: 8, minMessage: 'Le mot de passe doit contenir au moins 8 caratères')]
    #[ORM\Column]
    private ?string $password = null;

    #[Assert\NotBlank(message: 'Le champ Nom est requis')]
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["show_course", "comment_list", "list_recipes", "show_recipe", "user_info", "show_subscription", "show_order", "show_restaurant"])]
    private ?string $nom = null;

    #[Assert\NotBlank(message: 'Le champ Prénom(s) est requis')]
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["show_course", "comment_list", "list_recipes", "show_recipe", "user_info", "show_subscription", "show_order", "show_restaurant"])]
    private ?string $prenoms = null;

    #[Assert\NotBlank(message: 'Le champ email est requis')]
    #[Groups(["user_info"])]
    #[ORM\Column(length: 255, nullable: true, unique: true)]
    private ?string $username = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $birthday = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["show_course", "comment_list", "list_recipes", "show_recipe", "user_info"])]
    private ?string $adresse = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["show_course", "comment_list", "list_recipes", "show_recipe", "user_info"])]
    private ?string $ville = null;

    // #[Assert\NotBlank(message: 'Ce champ est requis')]
    #[Groups(["user_info"])]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $dial_code = null;

    #[Assert\NotBlank(message: 'Le Numéro de télépone est requis')]
    #[Groups(["user_info", "show_order", "show_subscription", "show_course"])]
    #[ORM\Column(length: 255)]
    private ?string $numero = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $code_pays = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?DateTime  $passwordRequestedAt = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $token = null;
   
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $pays = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $lien_facebook = null;
   
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nom_complet = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $lien_site_web = null;
    
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $type_compte = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?bool $actif = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?bool $email_verified = null;

    #[ORM\Column(nullable: true, options: ["default" => 0])]
    #[Groups(["user_info","list_recipes"])]
    private ?bool $is_cooker = null;

    #[ORM\Column(nullable: true, options: ["default" => 0])]
    #[Groups(["user_info"])]
    private ?bool $is_admin = null;

    #[ORM\Column(options: ["default" => 0], nullable: true)]
    private ?bool $has_restaurant = null;

    #[Groups(["user_info", "show_restaurant"])]
    private $checkSubscriptionTime;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Course::class)]
    #[Groups(["user_info"])]
    private Collection $courses;

    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Restaurant::class)]
    #[Groups(["user_info"])]
    private Collection $restaurants;

    #[ORM\Column(length: 255, nullable: true, options: ["default" => "https://i.imgur.com/zCL2LAh.png"])]
    private ?string $photo = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Order::class)]
    #[Groups(["user_info"])]
    private Collection $orders;

    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Shop::class)]
    private Collection $shops;

    #[ORM\ManyToMany(targetEntity: Subscription::class, mappedBy: 'subscriber')]
    #[Groups(["user_info"])]
    #[ORM\JoinColumn(referencedColumnName: 'subscription_id', nullable: true, onDelete: "CASCADE")]
    private Collection $subscriptions;

    #[Groups(['user_info'])]
    private $currentSubscription = [];

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: CookingTricks::class)]
    #[Groups(["user_info"])]
    private Collection $cookingTricks;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Recipe::class)]
    #[Groups(["user_info"])]
    private Collection $recipes;

    #[ORM\OneToMany(mappedBy: 'liker', targetEntity: Like::class)]
    #[Groups(["user_info"])]
    private Collection $likes;

    #[ORM\ManyToMany(targetEntity: BookMaker::class, mappedBy: 'saver')]
    #[Groups(["user_info"])]
    private Collection $bookMakers;

    #[ORM\ManyToMany(targetEntity: Rate::class, mappedBy: 'user')]
    private Collection $rates;

    #[ORM\ManyToMany(targetEntity: RateRestaurant::class, mappedBy: 'user')]
    private Collection $rateRestaurants;

    #[ORM\OneToMany(mappedBy: 'publisher', targetEntity: Course::class)]
    #[Groups(["user_info"])]
    private Collection $cookingCourses;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Transactions::class)]
    private Collection $transactions;

    #[ORM\ManyToMany(targetEntity: RecipeCount::class, mappedBy: 'visitor')]
    private Collection $recipeCounts;

    #[ORM\Column(type: "datetime", nullable: true)]
    #[Groups(["list_recipes", "user_info", "show_recipe", "show_order","comment_list"])]
    private \DateTimeInterface $date_creation;

    #[ORM\Column(type: "datetime", nullable: true)]
    #[Groups(["list_recipes", "user_info", "show_recipe", "show_order", "comment_list"])]
    private ?\DateTimeInterface $dateModification;

    #[ORM\Column(type: "datetime", nullable: true)]
    #[Groups(["list_recipes", "user_info", "show_recipe", "show_order", "comment_list"])]
    private ?\DateTimeInterface $dateSuppression;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserSessions::class)]
    private Collection $userSessions;

    #[ORM\ManyToMany(targetEntity: CourseParticipants::class, mappedBy: 'participants')]
    private Collection $courseParticipants;

    // use Timestapable;
    // use base64Field;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
        $this->restaurants = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->shops = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
        $this->cookingTricks = new ArrayCollection();
        $this->recipes = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->bookMakers = new ArrayCollection();
        // $this->rates = new ArrayCollection();
        // $this->rateRestaurants = new ArrayCollection();
        $this->cookingCourses = new ArrayCollection();
        $this->transactions = new ArrayCollection();
        $this->recipeCounts = new ArrayCollection();
        $this->userSessions = new ArrayCollection();
        $this->courseParticipants = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface   
     */
    public function getUserIdentifier(): string
    {
        // return (string) $this->username;
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = $this->password;
    }

    // public function getSpeudo(): ?string
    // {
    //     return $this->speudo;
    // }

    // public function setSpeudo(string $speudo): self
    // {
    //     $this->speudo = $speudo;

    //     return $this;
    // }


    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    public function setBirthday(?string $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }


    public function getDialCode(): ?string
    {
        return $this->dial_code;
    }

    public function setDialCode(?string $dial_code): self
    {
        $this->dial_code = $dial_code;

        return $this;
    }

    public function isIsCooker(): ?bool
    {
        return $this->is_cooker;
    }

    public function setIsCooker(?bool $is_cooker): self
    {
        $this->is_cooker = $is_cooker;

        return $this;
    }

    public function isHasRestaurant(): ?bool
    {
        return $this->has_restaurant;
    }

    public function setHasRestaurant(?bool $has_restaurant): self
    {
        $this->has_restaurant = $has_restaurant;

        return $this;
    }

    /**
     * @return Collection<int, Course>
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses->add($course);
            $course->setUser($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->removeElement($course)) {
            // set the owning side to null (unless already changed)
            if ($course->getUser() === $this) {
                $course->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Restaurant>
     */
    public function getRestaurants(): Collection
    {
        return $this->restaurants;
    }

    public function addRestaurant(Restaurant $restaurant): self
    {
        if (!$this->restaurants->contains($restaurant)) {
            $this->restaurants->add($restaurant);
            $restaurant->setOwner($this);
        }

        return $this;
    }

    public function removeRestaurant(Restaurant $restaurant): self
    {
        if ($this->restaurants->removeElement($restaurant)) {
            // set the owning side to null (unless already changed)
            if ($restaurant->getOwner() === $this) {
                $restaurant->setOwner(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection<int, Order>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
            $order->setUser($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getUser() === $this) {
                $order->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Shop>
     */
    public function getShops(): Collection
    {
        return $this->shops;
    }

    public function addShop(Shop $shop): self
    {
        if (!$this->shops->contains($shop)) {
            $this->shops->add($shop);
            $shop->setOwner($this);
        }

        return $this;
    }

    public function removeShop(Shop $shop): self
    {
        if ($this->shops->removeElement($shop)) {
            // set the owning side to null (unless already changed)
            if ($shop->getOwner() === $this) {
                $shop->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Subscription>
     */
    public function getSubscriptions(): Collection
    {
        $sub = $this->subscriptions->filter(
            function($subscription) {
                if($subscription?->getSubscriptionType()?->getTitle() != "cooker" ){
                    return true;
                }
            }
        );

        return $sub;
    }

    public function addSubscription(Subscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
            $subscription->addSubscriber($this);
        }

        return $this;
    }

    public function removeSubscription(Subscription $subscription): self
    {
        if ($this->subscriptions->removeElement($subscription)) {
            $subscription->removeSubscriber($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, CookingTricks>
     */
    public function getCookingTricks(): Collection
    {
        return $this->cookingTricks;
    }

    public function addCookingTrick(CookingTricks $cookingTrick): self
    {
        if (!$this->cookingTricks->contains($cookingTrick)) {
            $this->cookingTricks->add($cookingTrick);
            $cookingTrick->setUser($this);
        }

        return $this;
    }

    public function removeCookingTrick(CookingTricks $cookingTrick): self
    {
        if ($this->cookingTricks->removeElement($cookingTrick)) {
            // set the owning side to null (unless already changed)
            if ($cookingTrick->getUser() === $this) {
                $cookingTrick->setUser(null);
            }
        }

        return $this;
    }


    /**
     * Get the value of username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of username
     *
     * @return  self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return Collection<int, Recipe>
     */
    public function getRecipes()
    {
        return $this->recipes;
    }

    public function addRecipe(Recipe $recipe): self
    {
        if (!$this->recipes->contains($recipe)) {
            $this->recipes->add($recipe);
            $recipe->setUser($this);
        }

        return $this;
    }

    public function removeRecipe(Recipe $recipe): self
    {
        if ($this->recipes->removeElement($recipe)) {
            // set the owning side to null (unless already changed)
            if ($recipe->getUser() === $this) {
                $recipe->setUser(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection<int, Like>
     */
    public function getLikes($need = false)
    {
        if ($need == true) {
            return $this->likes;
        }

        return null;
    }

    public function addLike(Like $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes->add($like);
            $like->setLiker($this);
        }

        return $this;
    }

    public function removeLike(Like $like): self
    {
        if ($this->likes->removeElement($like)) {
            // set the owning side to null (unless already changed)
            if ($like->getLiker() === $this) {
                $like->setLiker(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, BookMaker>
     */
    public function getBookMakers()
    {
        return $this->bookMakers;
    }

    public function addBookMaker(BookMaker $bookMaker): self
    {
        if (!$this->bookMakers->contains($bookMaker)) {
            $this->bookMakers->add($bookMaker);
            $bookMaker->addSaver($this);
        }

        return $this;
    }

    public function removeBookMaker(BookMaker $bookMaker): self
    {
        if ($this->bookMakers->removeElement($bookMaker)) {
            $bookMaker->removeSaver($this);
        }

        return $this;
    }
    
    /**
     * @return Collection<int, Course>
     */
    public function getCookingCourses(): Collection
    {
        return $this->cookingCourses;
    }

    public function addCookingCourse(Course $cookingCourse): self
    {
        if (!$this->cookingCourses->contains($cookingCourse)) {
            $this->cookingCourses->add($cookingCourse);
            $cookingCourse->setPublisher($this);
        }

        return $this;
    }

    public function removeCookingCourse(Course $cookingCourse): self
    {
        if ($this->cookingCourses->removeElement($cookingCourse)) {
            // set the owning side to null (unless already changed)
            if ($cookingCourse->getPublisher() === $this) {
                $cookingCourse->setPublisher(null);
            }
        }

        return $this;
    }


    public function getCurrentSubscription()
    {
        $this->currentSubscription = $this->getSubscriptions();

        if(empty($this->currentSubscription)){
            return [];
        }else{

            $subscriptionPaid = $this->getSubscriptions()->filter(
                function($value) {

                    $endDate = $value->getEndDate();
                    if(!(Carbon::parse($endDate)->isPast())){
                        if($value->getCommande() != null){
                            if($value->getCommande()->isIsPaid() == true){
                                return true;
                            }
                        }    
                    }

                }
            );

            return $subscriptionPaid->last();
        }
    }

    public function getCheckSubscriptionTime()
    {
        if(!empty($this->getSubscriptions()?->last())){

            if($this->getSubscriptions()?->last()->getCommande()?->isIsPaid() ==  true){

                $startDate = Carbon::parse($this->getSubscriptions()?->last()?->getStartDate());
                $endDate =Carbon::parse( $this->getSubscriptions()?->last()?->getEndDate());
            
                $check = Carbon::now()->between($startDate,$endDate);
            }else{
                $check = false;
            }

    
            if($check){
                return true;
            }else{
                return false;
            }
        }
        
        return false;
    }

    // public function getIsactive()
    // {
    //     return $this->is_active;
    // }

    /**
     * @return Collection<int, Transactions>
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transactions $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions->add($transaction);
            $transaction->setUser($this);
        }

        return $this;
    }

    public function removeTransaction(Transactions $transaction): self
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getUser() === $this) {
                $transaction->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RecipeCount>
     */
    public function getRecipeCounts(): Collection
    {
        return $this->recipeCounts;
    }

    public function addRecipeCount(RecipeCount $recipeCount): self
    {
        if (!$this->recipeCounts->contains($recipeCount)) {
            $this->recipeCounts->add($recipeCount);
            $recipeCount->addVisitor($this);
        }

        return $this;
    }

    public function removeRecipeCount(RecipeCount $recipeCount): self
    {
        if ($this->recipeCounts->removeElement($recipeCount)) {
            $recipeCount->removeVisitor($this);
        }

        return $this;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenoms()
    {
        return $this->prenoms;
    }

    public function setPrenoms($prenoms)
    {
        $this->prenoms = $prenoms;

        return $this;
    }

    public function getAdresse()
    {
        return $this->adresse;
    }

    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getVille()
    {
        return $this->ville;
    }

    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    public function getCodePays()
    {
        return $this->code_pays;
    }

    public function setCodePays($code_pays)
    {
        $this->code_pays = $code_pays;

        return $this;
    }

    public function getPays()
    {
        return $this->pays;
    }

    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    public function getDateCreation()
    {
        return $this->date_creation;
    }
 
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    #[PrePersist]
    public function saveDateCreation()
    {
        $this->setDateCreation(new \DateTimeImmutable());
        $this->setDateModification(new \DateTimeImmutable());
    }

    public function getDateModification()
    {
        return $this->dateModification;
    }

    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

  
    public function getDateSuppression()
    {
        return $this->dateSuppression;
    }

    /**
     * Get the value of is_admin
     */ 
    public function getIsAdmin()
    {
        return $this->is_admin;
    }

    /**
     * @return Collection<int, UserSessions>
     */
    public function getUserSessions(): Collection
    {
        return $this->userSessions;
    }

    public function addUserSession(UserSessions $userSession): self
    {
        if (!$this->userSessions->contains($userSession)) {
            $this->userSessions->add($userSession);
            $userSession->setUser($this);
        }

        return $this;
    }

    public function removeUserSession(UserSessions $userSession): self
    {
        if ($this->userSessions->removeElement($userSession)) {
            // set the owning side to null (unless already changed)
            if ($userSession->getUser() === $this) {
                $userSession->setUser(null);
            }
        }

        return $this;
    }

    public function getType_compte()
    {
        return $this->type_compte;
    }

  
    public function setType_compte($type_compte)
    {
        $this->type_compte = $type_compte;

        return $this;
    }

    public function getNomComplet()
    {
        return $this->nom_complet;
    }

   
    public function setNomComplet($nom_complet)
    {
        $this->nom_complet = $nom_complet;

        return $this;
    }


    public function setIsAdmin($is_admin)
    {
        $this->is_admin = $is_admin;

        return $this;
    }

    public function getTypeCompte()
    {
        return $this->type_compte;
    }

    public function setTypeCompte($type_compte)
    {
        $this->type_compte = $type_compte;
        return $this;
    }

    public function setToken($token){
        $this->token = $token;
        return $this;
    }

    public function getToken(){
        return $this->token;
    }

    public function setPasswordRequestedAt($date){
        $this->passwordRequestedAt = $date;
    }

    public function getPasswordRequestedAt(){
        return $this->passwordRequestedAt;
    }

    /**
     * @return Collection<int, CourseParticipants>
     */
    public function getCourseParticipants(): Collection
    {
        return $this->courseParticipants;
    }

    public function addCourseParticipant(CourseParticipants $courseParticipant): self
    {
        if (!$this->courseParticipants->contains($courseParticipant)) {
            $this->courseParticipants->add($courseParticipant);
            $courseParticipant->addParticipant($this);
        }

        return $this;
    }

    public function removeCourseParticipant(CourseParticipants $courseParticipant): self
    {
        if ($this->courseParticipants->removeElement($courseParticipant)) {
            $courseParticipant->removeParticipant($this);
        }

        return $this;
    }
    
    public function setEmailVerified($email_verified)
    {
        $this->email_verified = $email_verified;

        return $this;
    }
}
