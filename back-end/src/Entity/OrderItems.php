<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\Timestapable;
use App\Repository\OrderItemsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: OrderItemsRepository::class)]
class OrderItems
{
    use Timestapable;
    use base64Field;

    public function __construct()
    {
        $this->dish = new ArrayCollection();
    }
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["show_order", "user_info"])]
    private ?int $id = null;
    
    #[ORM\Column(type: Types::TEXT)]
    #[Groups(["show_order", "user_info"])]
    private ?string $meta_data = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_order", "user_info"])]
    private ?string $quantity = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_order", "user_info"])]
    private ?string $quantity_unit = null;

    #[ORM\Column(length: 255)]
    #[Groups(["show_order", "user_info"])]
    private ?string $unit_price = null;

    #[ORM\ManyToOne(inversedBy: 'OrderItems', cascade:['persist'])]
    private ?Order $commande = null;

    #[ORM\ManyToMany(targetEntity: Dish::class, inversedBy: 'orderItems')]
    private Collection $dish;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMetaData(): ?string
    {
        return $this->meta_data;
    }

    public function setMetaData(string $meta_data): self
    {
        $this->meta_data = $meta_data;

        return $this;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getQuantityUnit(): ?string
    {
        return $this->quantity_unit;
    }

    public function setQuantityUnit(string $quantity_unit): self
    {
        $this->quantity_unit = $quantity_unit;

        return $this;
    }

    public function getUnitPrice(): ?string
    {
        return $this->unit_price;
    }

    public function setUnitPrice(string $unit_price): self
    {
        $this->unit_price = $unit_price;

        return $this;
    }

    public function getCommande(): ?Order
    {
        return $this->commande;
    }

    public function setCommande(?Order $commande): self
    {
        $this->commande = $commande;

        return $this;
    }

    public function getDish()
    {
        return $this->dish;
    }

    public function setDish(?Dish $dish): self
    {
        if(!$this->dish->contains($dish)){
            $this->dish->add($dish);
        }

        return $this;
    }
}
