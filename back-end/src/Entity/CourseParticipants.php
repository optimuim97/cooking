<?php

namespace App\Entity;

use App\Entity\traits\Timestapable;
use App\Repository\CourseParticipantsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CourseParticipantsRepository::class)]
class CourseParticipants
{
    use Timestapable;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'courseParticipants')]
    private ?Course $course = null;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'courseParticipants')]
    #[Groups(["show_course"])]
    private Collection $participants;

    #[ORM\Column(nullable: true)]
    #[Groups(["show_course"])]
    private ?bool $isAcceped = null;

    public function __construct()
    {
        $this->participants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(User $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants->add($participant);
        }

        return $this;
    }

    public function removeParticipant(User $participant): self
    {
        $this->participants->removeElement($participant);

        return $this;
    }

    public function isIsAcceped(): ?bool
    {
        return $this->isAcceped;
    }

    public function setIsAcceped(?bool $isAcceped): self
    {
        $this->isAcceped = $isAcceped;

        return $this;
    }
}
