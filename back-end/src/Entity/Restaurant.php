<?php

namespace App\Entity;

use App\Entity\traits\base64Field;
use App\Entity\traits\HasSlug;
use App\Entity\traits\Timestapable;
use App\Repository\RestaurantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PostLoad;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RestaurantRepository::class)]
#[HasLifecycleCallbacks]
class Restaurant
{
    use Timestapable;
    use base64Field;
    use HasSlug;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["user_info", "show_restaurant", "show_dish"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user_info", "show_restaurant", "show_order", "show_dish"])]
    private ?string $name = null;

    #[Groups(["user_info", "show_restaurant", "show_order"])]
    private $rateAverage;

    #[Groups(["user_info", "show_restaurant"])]
    private $raterCount;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $slogan = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["user_info", "show_restaurant", "show_order"])]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user_info", "show_restaurant", "show_order"])]
    private ?string $dial_code = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info", "show_restaurant", "show_order"])]
    private ?string $phone = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info", "show_restaurant", "show_order"])]
    private ?string $email = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info", "show_restaurant", "show_order"])]
    private ?string $contact1 = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $lat = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $lon = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info", "show_restaurant", "show_order"])]
    private ?string $country_code = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $country_name = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $town = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info", "show_restaurant", "show_order"])]
    private ?string $city = null;

    #[ORM\ManyToOne(inversedBy: 'restaurants')]
    #[Groups(["show_restaurant"])]
    private ?User $owner = null;

    #[ORM\Column(length: 255, nullable: true, options: ["default" => 'https://i.imgur.com/sJO7ysX.jpg'])]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $imageUrl = null;

    #[ORM\OneToMany(mappedBy: 'restaurant', targetEntity: RestaurantImage::class)]
    #[Groups(["user_info", "show_restaurant"])]
    private Collection $restaurantImages;

    #[ORM\OneToMany(mappedBy: 'restaurant', targetEntity: Dish::class)]
    #[Groups(["user_info", "show_restaurant"])]
    private Collection $dishes;

    #[ORM\OneToMany(mappedBy: 'restaurant', targetEntity: RestaurantMenu::class)]
    #[Groups(["user_info", "show_restaurant"])]
    private Collection $restaurantMenus;

    #[ORM\OneToMany(mappedBy: 'restaurant', targetEntity: RestaurantDish::class)]
    #[Groups(["user_info", "show_restaurant"])]
    private Collection $restaurantDishes;

    #[ORM\Column(options: ["default" => false])]
    private ?bool $isSpotLight = null;

    #[ORM\Column(type: Types::TEXT, options: ['default'=>""])]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $aboutUs = "";

    #[ORM\Column(length: 255)]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $chefImageUrl = "";

    #[ORM\Column(type: Types::TEXT, options: ['default'=>""])]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $chefDescription = null;

    #[ORM\OneToMany(mappedBy: 'resto', targetEntity: Order::class)]
    // #[Groups(["user_info"])]
    private Collection $orders;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $aboutOurChief = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $chiefImage = null;

    #[ORM\ManyToMany(targetEntity: RateRestaurant::class, mappedBy: 'restaurant')]
    // #[Groups(["user_info", "show_restaurant"])]
    private Collection $rateRestaurants;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user_info", "show_restaurant"])]
    private ?string $chefName = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user_info","show_restaurant"])]
    private ?string $scheduleStart = "9:30";

    #[ORM\Column(length: 255)]
    #[Groups(["user_info","show_restaurant"])]
    private ?string $scheduleEnd = "19:00";

    #[ORM\Column(length: 255)]
    private ?string $address = null;

    #[ORM\Column(length: 255)]
    private ?string $addressComp = null;

    #[ORM\Column]
    #[Groups(["user_info","show_restaurant"])]
    private ?bool $isValidedByAdmin = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $AdminValidationDate = null;

    public function __construct()
    {
        $this->restaurantImages = new ArrayCollection();
        $this->dishes = new ArrayCollection();
        $this->restaurantMenus = new ArrayCollection();
        $this->restaurantDishes = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->rateRestaurants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlogan(): ?string
    {
        return $this->slogan;
    }

    public function setSlogan(string $slogan): self
    {
        $this->slogan = $slogan;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getContact1(): ?string
    {
        return $this->contact1;
    }

    public function setContact1(?string $contact1): self
    {
        $this->contact1 = $contact1;

        return $this;
    }

    public function getLat(): ?string
    {
        return $this->lat;
    }

    public function setLat(?string $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLon(): ?string
    {
        return $this->lon;
    }

    public function setLon(string $lon): self
    {
        $this->lon = $lon;

        return $this;
    }

    public function getCountryCode(): ?string
    {
        return $this->country_code;
    }

    public function setCountryCode(string $country_code): self
    {
        $this->country_code = $country_code;

        return $this;
    }

    public function getCountryName(): ?string
    {
        return $this->country_name;
    }

    public function setCountryName(string $country_name): self
    {
        $this->country_name = $country_name;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection<int, RestaurantImage>
     */
    public function getRestaurantImages(): Collection
    {
        return $this->restaurantImages;
    }

    public function addRestaurantImage(RestaurantImage $restaurantImage): self
    {
        if (!$this->restaurantImages->contains($restaurantImage)) {
            $this->restaurantImages->add($restaurantImage);
            $restaurantImage->setRestaurant($this);
        }

        return $this;
    }

    public function removeRestaurantImage(RestaurantImage $restaurantImage): self
    {
        if ($this->restaurantImages->removeElement($restaurantImage)) {
            // set the owning side to null (unless already changed)
            if ($restaurantImage->getRestaurant() === $this) {
                $restaurantImage->setRestaurant(null);
            }
        }

        return $this;
    }

    public function getDialCode(): ?string
    {
        return $this->dial_code;
    }

    public function setDialCode(string $dial_code): self
    {
        $this->dial_code = $dial_code;

        return $this;
    }

    /**
     * @return Collection<int, Dish>
     */
    public function getDishes(): Collection
    {
        return $this->dishes;
    }

    public function addDish(Dish $dish): self
    {
        if (!$this->dishes->contains($dish)) {
            $this->dishes->add($dish);
            $dish->setRestaurant($this);
        }

        return $this;
    }

    public function removeDish(Dish $dish): self
    {
        if ($this->dishes->removeElement($dish)) {
            // set the owning side to null (unless already changed)
            if ($dish->getRestaurant() === $this) {
                $dish->setRestaurant(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, RestaurantMenu>
     */
    public function getRestaurantMenus(): Collection
    {
        return $this->restaurantMenus;
    }

    public function addRestaurantMenu(RestaurantMenu $restaurantMenu): self
    {
        if (!$this->restaurantMenus->contains($restaurantMenu)) {
            $this->restaurantMenus->add($restaurantMenu);
            $restaurantMenu->setRestaurant($this);
        }

        return $this;
    }

    public function removeRestaurantMenu(RestaurantMenu $restaurantMenu): self
    {
        if ($this->restaurantMenus->removeElement($restaurantMenu)) {
            // set the owning side to null (unless already changed)
            if ($restaurantMenu->getRestaurant() === $this) {
                $restaurantMenu->setRestaurant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RestaurantDish>
     */
    public function getRestaurantDishes(): Collection
    {
        return $this->restaurantDishes;
    }

    public function addRestaurantDish(RestaurantDish $restaurantDish): self
    {
        if (!$this->restaurantDishes->contains($restaurantDish)) {
            $this->restaurantDishes->add($restaurantDish);
            $restaurantDish->setRestaurant($this);
        }

        return $this;
    }

    public function removeRestaurantDish(RestaurantDish $restaurantDish): self
    {
        if ($this->restaurantDishes->removeElement($restaurantDish)) {
            // set the owning side to null (unless already changed)
            if ($restaurantDish->getRestaurant() === $this) {
                $restaurantDish->setRestaurant(null);
            }
        }

        return $this;
    }

    /**
     * Get the value of imageUrl
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set the value of imageUrl
     *
     * @return  self
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function isIsSpotLight(): ?bool
    {
        return $this->isSpotLight;
    }

    public function setIsSpotLight(bool $isSpotLight): self
    {
        $this->isSpotLight = $isSpotLight;

        return $this;
    }

    public function getAboutUs(): ?string
    {
        return $this->aboutUs;
    }

    public function setAboutUs(string $aboutUs): self
    {
        $this->aboutUs = $aboutUs;

        return $this;
    }

    public function getChefImageUrl(): ?string
    {
        return $this->chefImageUrl;
    }

    public function setChefImageUrl(string $chefImageUrl): self
    {
        $this->chefImageUrl = $chefImageUrl;

        return $this;
    }

    public function getChefDescription(): ?string
    {
        return $this->chefDescription;
    }

    public function setChefDescription(string $chefDescription): self
    {
        $this->chefDescription = $chefDescription;

        return $this;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
            $order->setResto($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getResto() === $this) {
                $order->setResto(null);
            }
        }

        return $this;
    }

    public function getAboutOurChief(): ?string
    {
        return $this->aboutOurChief;
    }

    public function setAboutOurChief(string $aboutOurChief): self
    {
        $this->aboutOurChief = $aboutOurChief;

        return $this;
    }

    public function getChiefImage(): ?string
    {
        return $this->chiefImage;
    }

    public function setChiefImage(string $chiefImage): self
    {
        $this->chiefImage = $chiefImage;

        return $this;
    }

    /**
     * @return Collection<int, RateRestaurant>
     */
    public function getRateRestaurants(): Collection
    {
        return $this->rateRestaurants;
    }


    #[PostLoad]
    public function getRateAverage()
    {
        $rates = $this->getRateRestaurants();

        $rateSum = 0;
        $this->raterCount = count($rates);

        foreach ($rates as $rate) {
            $rateSum += $rate->getNumberOfStart();
        }

        if(count($rates) != 0){
            $average = round($rateSum / $this->raterCount, 1);
        }else{
            $average = 0;
        }   

        $this->rateAverage = $average;

        return $this->rateAverage;
    }

    public function addRateRestaurant(RateRestaurant $rateRestaurant): self
    {
        if (!$this->rateRestaurants->contains($rateRestaurant)) {
            $this->rateRestaurants->add($rateRestaurant);
            $rateRestaurant->addRestaurant($this);
        }

        return $this;
    }

    public function removeRateRestaurant(RateRestaurant $rateRestaurant): self
    {
        if ($this->rateRestaurants->removeElement($rateRestaurant)) {
            $rateRestaurant->removeRestaurant($this);
        }

        return $this;
    }

    public function getRaterCount (){
         $this->raterCount;
    }

    /**
     * Get the value of chefName
     */ 
    public function getChefName()
    {
        return $this->chefName;
    }

    /**
     * Set the value of chefName
     *
     * @return  self
     */ 
    public function setChefName($chefName)
    {
        $this->chefName = $chefName;

        return $this;
    }

    public function getScheduleStart(): ?string
    {
        return $this->scheduleStart;
    }

    public function setScheduleStart(string $scheduleStart): self
    {
        $this->scheduleStart = $scheduleStart;

        return $this;
    }

    public function getScheduleEnd(): ?string
    {
        return $this->scheduleEnd;
    }

    public function setScheduleEnd(string $scheduleEnd): self
    {
        $this->scheduleEnd = $scheduleEnd;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddressComp(): ?string
    {
        return $this->addressComp;
    }

    public function setAddressComp(string $addressComp): self
    {
        $this->addressComp = $addressComp;

        return $this;
    }

    public function isIsValidedByAdmin(): ?bool
    {
        return $this->isValidedByAdmin;
    }

    public function setIsValidedByAdmin(bool $isValidedByAdmin): self
    {
        $this->isValidedByAdmin = $isValidedByAdmin;

        return $this;
    }

    public function getAdminValidationDate(): ?\DateTimeInterface
    {
        return $this->AdminValidationDate;
    }

    public function setAdminValidationDate(\DateTimeInterface $AdminValidationDate): self
    {
        $this->AdminValidationDate = $AdminValidationDate;

        return $this;
    }
}
