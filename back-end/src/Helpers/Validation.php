<?php

namespace App\Helpers;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Validation  extends AbstractController{

    public $validator;

    public function __construct( )
    {
        
    }

    public static function check($errors){

        if (count($errors) > 0) {

            $composeError = [];

            foreach ($errors as $error) {
                $composeError [$error->getPropertyPath()] = $error->getMessage();
            }

           return $composeError;
           
        }else{

            return null;
        }

    }

}