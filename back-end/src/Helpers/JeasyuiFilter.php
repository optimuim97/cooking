<?php

namespace App\Helpers;

class JeasyuiFilter
{
    private $op;
    private $tab_filter = [];
    private $tab_filterRules = [];

    public function __construct($tab_filterRules = null)
    {
        $this->tab_filter = [
            "equal" => "=",
            "notequal" => "!=",
            "less" => "<",
            "lessorequal" => "<=",
            "greater" => ">",
            "greaterorequal" => ">=",
            "contains" => "LIKE",
            "beginwith" => "LIKE",
            "endwith" => "LIKE",
        ];

        $this->tab_filterRules = $tab_filterRules;
    }

    private function op($op)
    {
        $this->op = $op;
        return $this->tab_filter[$op];
    }

    private function value($value)
    {
        $op = $this->op;
        switch ($op) {
            case 'contains':
                $value  = "%" . $value . "%";
                break;

            case 'beginwith':
                $value  = $value . "%";
                break;

            case 'endwith':
                $value  = "%" . $value;
                break;

            default:
                break;
        }
        return $value;
    }

    public function getWhere()
    {
        $filterRules = [];
        foreach ($this->tab_filterRules as $filter) {
            $filterRules[] = [
                $filter['field'],
                $this->op($filter['op']),
                $this->value($filter['value']),
            ];
        }
        return $filterRules;
    }
}
