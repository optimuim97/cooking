<?php

namespace App\Helpers;

class ImageHelper
{
    public static function uploadImage($file, $storagePath = null): ?string
    {
        $fileName =  md5(uniqid()) . $file->getClientOriginalName();

        if (!empty($storagePath) && $storagePath != null) {
            $storagePath = $storagePath;
        } else {
            $storagePath = 'images/';
        }

        $file->move($storagePath, $fileName);
        $image_url = $storagePath . $fileName;
        return $image_url;
    }
    
    public static function getFileInfo($file)
    {
        return [
            "original_name" => $file->getClientOriginalName(),
            "mime_type" => $file->getClientmimeType(),
            "real_path" => $file->getRealPath()
        ];
    }

    public static function uploadBase64($file)
    {
        $prepare_image = file_get_contents($file);
        $base64image = base64_encode($prepare_image);

        return $base64image;
    }

    public static function decode64($img)
    {
        $path = "images/";  
        $image_parts = explode(";base64,", $img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_en_base64 = base64_decode($image_parts[1]);
        $file = $path . uniqid() . '.png';

        file_put_contents($file, $image_en_base64);

        return $file;
    }
}
