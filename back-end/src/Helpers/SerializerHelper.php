<?php
namespace App\Helpers;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class SerializerHelper
{

    public function __construct(
        private SerializerInterface $serializer
    ){

    }

    public static function getJson(
        SerializerInterface $serializer,
        $data,
        string $groupname
    ) {
        
        $dataJson = $serializer->serialize(
            $data,
            JsonEncoder::FORMAT,
            [AbstractNormalizer::GROUPS => $groupname]
        );

        return json_decode($dataJson);
    }
}
