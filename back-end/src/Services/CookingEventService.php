<?php


namespace App\Services;

use App\Entity\CookingEvent;
use App\Helpers\ImageHelper;
use App\Helpers\Validation;
use App\Repository\CookingEventRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\traits\ApiCaller;
use Exception;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CookingEventService extends AbstractController
{

    public function __construct(
        public EntityManagerInterface $em,
        public CookingEventRepository $cookingEvent,
        private ValidatorInterface $validator,
        private HttpClientInterface $client
    ) {
    }

    public function add($data, $file)
    {
        $user = $this->getUser();

        try {

            $titre = $data["titre"];
            $eUser = $data["eUser"];
            $contact = $data["contact"];
            $sousTitre = $data["sousTitre"];
            $ville = $data["ville"];
            $commune = $data["commune"];
            $dateDebut = $data["dateDebut"];
            $dateFin = $data["dateFin"];
            $heureDebut = $data["heureDebut"];
            $heureFin = $data["heureFin"];
            $heureOuverture = $data["heureOuverture"];
            $heureOuverture = $data["heureOuverture"];
            $cible = $data["cible"];
            $noteInformation = $data["noteInformation"];
            $tickets = $data["tickets"] ?? [];
            $galleriePhoto = $data["galleriePhoto"] ?? [];
            $IdTypeEvenement = (int) $data["IdTypeEvenement"] ?? 14;
            $IdSalle = (int) $data["IdSalle"] ?? 1;
            $video = $data["video"] ?? "";
            $description = $data["description"];
            $eventTickets = [];
            $formule = $user->getCurrentSubscription();

            if($formule == false){
                $formule = "Gratuit";
            }

            $image = ImageHelper::uploadBase64($file);
            $photoCouverture = "data:image/webp;base64,$image";

            $sendData = [
                "titre" => $titre,
                "eUser" => (int) $user->getId() ?? 1,
                "contact" => $contact,
                "sousTitre" => $sousTitre,
                "description" => $description,
                "ville" => $ville,
                "commune" => $commune,
                "formule"=> $formule ?? "Gratuit",
                "heureDebut" => $heureDebut,
                "dateDebut" => $dateDebut,
                "dateFin" => $dateFin,
                "heureFin" => $heureFin,
                "heureOuverture" => $heureOuverture,
                "heureOuverture" => $heureOuverture,
                "cible" => $cible,
                "noteInformation" => $noteInformation,
                "tickets" => $tickets,
                "galleriePhoto" => $galleriePhoto,
                "IdTypeEvenement" => $IdTypeEvenement,
                "IdSalle" => $IdSalle,
                "eventTickets" => $eventTickets,
                "video" => $video,
                "photoCouverture" => $photoCouverture,
            ];

            $req = $this->client->request('POST', 'https://abjannonces.weblogy.net/api/v1/evenements/store', ["json"=>$sendData]);

            $statusCode = $req->getStatusCode();
            $content = $req->getContent();
            $content = $req->toArray();

            return $this->json([
                "message" => "OK",
                "status_code" => $statusCode,
                "data" => $content
            ], 200);

        } catch (Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);
        }

        /*     
            try {

                $cookingEvent = new CookingEvent();
                $cookingEvent->setName($name);
                $cookingEvent->setDescription($description);
                $cookingEvent->setPrice($price);
                $cookingEvent->setReduction($reduction);
                $cookingEvent->setDateStr($dateStr);

                $date = new DateTime();
                $cookingEvent->setDate($date);

                if (!empty($file)) {
                    $image_url = ImageHelper::uploadImage($file);
                    $cookingEvent->setImageUrl($image_url);
                }

                $errors = $this->validator->validate($cookingEvent);

                if (count($errors) > 0) {
                    $hasError = Validation::check($errors);

                    if ($hasError != null) {
                        return $this->json([
                            "errors" => $hasError,
                            "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                            "message" => Response::$statusTexts['422']
                        ], 422);
                    }
                }

                $this->em->persist($cookingEvent);
                $this->em->flush();

                return $this->json([
                    "message" => "Created",
                    "status_code" => Response::HTTP_CREATED,
                    "data" => $cookingEvent
                ]);
            } catch (\Exception $e) {

                return $this->json([
                    "status_code" => $e->getCode(),
                    "message" => $e->getMessage()
                ]);
            } 
        */
    }

    public function delete($id)
    {
        try {

            $cookingEvent = $this->cookingEvent->findOneBy(["id" => $id]);

            if (!empty($cookingEvent)) {
                $this->cookingEvent->remove($cookingEvent, true);

                return $this->json([
                    "message" => "Deleted",
                    "status_code" => Response::HTTP_NO_CONTENT,
                    "data" => null
                ]);
            } else {
                return $this->json([
                    "message" => "Not Found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => $e->getMessage()
            ]);
        }
    }

    public function show($id)
    {

        try {

            $cookingEvent = $this->cookingEvent->findOneBy(["id" => $id]);

            if (!empty($cookingEvent)) {
                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_FOUND,
                    "data" => $cookingEvent
                ]);
            } else {
                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => $e->getMessage()
            ]);
        }
    }

    //TODO update  $recipeType
    public function update($id, $data)
    {
        $cookingEvent = $this->cookingEvent->findOneBy(["id" => $id]);

        if (!empty($cookingEvent)) {
            $name = $data['name'] ?? "";
            $description = $data['description'] ?? "";
            $price = $data['price'] ?? "";
            $reduction = $data['reduction$reduction'] ?? "";
            $date = $data['date'] ?? "";

            $cookingEvent = new CookingEvent();
            $cookingEvent->setName($name);
            $cookingEvent->setDescription($description);
            $cookingEvent->setPrice($price);
            $cookingEvent->setReduction($reduction);
            $cookingEvent->setDate($date);

            $this->em->persist($cookingEvent);
            $this->em->flush();

            //TODO setValues
            return $this->json([
                "message" => "Found",
                "status_code" => Response::HTTP_FOUND,
                "data" => $cookingEvent
            ]);
        } else {
            return $this->json([
                "message" => "Not found",
                "status_code" => Response::HTTP_NOT_FOUND,
                "data" => null
            ]);
        }
    }

    public function getAll()
    {
        try {

            $annonce_url = $this->getParameter('app.annonce_url');
            $req = $this->client->request("GET", "$annonce_url/api/v1/evenements-par-category?eventType=evenements-culinaire");

            $statusCode = $req->getStatusCode();
            $content = $req->getContent();
            $content = $req->toArray();

            return $this->json([
                "message" => "Found",
                "status_code" => $statusCode,
                "data" => $content
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => $e->getMessage()
            ], 500);
        }
    }
}
