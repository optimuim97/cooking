<?php

namespace App\Services;

use App\Entity\course;
use App\Entity\CourseParticipants;
use App\Entity\Lesson;
use App\Entity\Order;
use App\Entity\OrderStatus;
use App\Entity\WebSite;
use App\Helpers\ImageHelper;
use App\Helpers\SerializerHelper;
use App\Helpers\Validation;
use App\Repository\CourseRepository;
use App\Repository\CourseTypeRepository;
use App\Repository\LessonRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CourseService extends AbstractController
{

    public function __construct(
        private SendMail $mailer,
        private EntityManagerInterface $em,
        private CourseRepository $courseRepo,
        private CourseTypeRepository  $courseTypesRepo,
        private SerializerInterface $serializer,
        private ValidatorInterface $validator,
        private LessonRepository $lessonRepo
    ) {
    }

    public function add($data, $file)
    {
        try {

            // dd($data);

            $course_type_id = $data['course_type_id'] ?? "";

            $user = $this->getUser();
            $course = new Course();
            $course->setUser($user);
            $course->setName($data["name"]);
            $slug = (new Slugify())->slugify($data["name"]);
            $course->setSlug($slug);
            $course->setSlogan($data["slogan"] ?? "");
            $course->setDescription($data["description"] ?? "");
            $course->setPrice($data["price"] ?? "");
            $course->setPlace($data["place"] ?? "");
            $course->setVideoUrl($data["video_url"] ?? "");

            if (!empty($course_type_id)) {
                $courseType = $this->courseTypesRepo->find($course_type_id);
                $course->setCourseType($courseType);
            }

            $course->setHasMultipleStep($data["has_multiple_step"] ?? "");
            $course->setInstructor($data['instructor'] ?? '');
            $course->setLanguage($data['language'] ?? '');
            $course->setPublisher($user);

            if (!empty($file)) {
                $image_url = ImageHelper::uploadImage($file);
                $course->setImageUrl($image_url);
            }

            $errors = $this->validator->validate($course);

            if (count($errors) > 0) {
                $hasError = Validation::check($errors);

                if ($hasError != null) {
                    return $this->json([
                        "errors" => $hasError,
                        "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                        "message" => Response::$statusTexts['422']
                    ], 422);
                }
            }

            $this->em->persist($course);
            $this->em->flush();

            $courseJson = $this->serializer->serialize(
                $course,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => "show_course"]
            );

            return $this->json([
                "message" => "Found",
                "status_code" => Response::HTTP_CREATED,
                "data" => json_decode($courseJson)
            ]);

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => 500
            ], 500);

        }
    }

    public function delete($id)
    {
        try {

            $resto = $this->courseRepo->findOneBy(["id" => $id]);

            if (!empty($resto)) {

                $this->courseRepo->remove($resto, true);

                return $this->json([
                    "message" => "Deleted",
                    "status_code" => Response::HTTP_NO_CONTENT,
                    "data" => null
                ]);
            } else {

                return $this->json([
                    "message" => "Not Found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function show($slug)
    {

        try {

            $course = $this->courseRepo->findOneBy(["slug" => $slug]);

            if (!empty($course)) {

                $courseJson = $this->serializer->serialize(
                    $course,
                    JsonEncoder::FORMAT,
                    [AbstractNormalizer::GROUPS => "show_course"]
                );

                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_FOUND,
                    "data" => json_decode($courseJson)
                ]);
            } else {
                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    //TODO update course
    public function update($id, $data, $file)
    {

        try {
            $course = $this->courseRepo->find($id);

            if (!empty($course)) {

                $user = $this->getUser();

                $course_type_id = $data['course_type_id'] ?? null;

                if (!empty($course_type_id)) {
                    $courseType = $this->courseTypesRepo->find($course_type_id);
                    $course->setCourseType($courseType ?? null);
                }

                $course->setUser($user);
                $course->setName($data["name"]);
                $course->setSlogan($data["slogan"] ?? $course->getSlogan());
                $course->setDescription($data["description"] ?? $course->getDescription());
                $course->setPrice($data["price"] ?? $course->getDescription());
                $course->setPlace($data["place"] ?? $course->getPlace());
                $course->setVideoUrl($data["video_url"] ?? $course->getVideoUrl());
                $course->setHasMultipleStep($data["has_multiple_step"] ?? false);
                $course->setInstructor($data['instructor'] ?? $course->getInstructor());
                $course->setLanguage($data['language'] ?? $course->getLanguage());

                if (!empty($file)) {
                    $image_url = ImageHelper::uploadImage($file);
                    $course->setImageUrl($image_url);
                }

                $this->em->persist($course);
                $this->em->flush();

                $courseJson = SerializerHelper::getJson($this->serializer, $course, "show_course");

                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_FOUND,
                    "data" => $courseJson
                ]);
            } else {

                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {
            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => 500
            ], 500);
        }
    }

    public function getAll()
    {
        try {
            $course = $this->courseRepo->orderByCreatedAtDesc();

            $length = count($course);

            $courseJson = $this->serializer->serialize(
                $course,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => 'list_courses']
            );

            return $this->json([
                "message" => "All course in database",
                "status_code" => Response::HTTP_OK,
                "length" => $length,
                "data" => json_decode($courseJson)
            ]);
        } catch (\Throwable $th) {

            return $this->json([
                "message" => $th,
                "status_code" => 500
            ], 500);
        }
    }

    public function getAllCourseType()
    {

        try {
            $courseType = $this->courseTypesRepo->findAll();

            $length = count($courseType);

            try {
                return $this->json([
                    "message" => "All courseType Types in database",
                    "status" => Response::HTTP_OK,
                    "length" => $length,
                    "data" => $courseType
                ]);
            } catch (\Throwable $th) {
                return $this->json([
                    "message" => $th
                ]);
            }
            
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);
            
        }
    }

    public function enroll($id)
    {
        try {

            $user = $this->getUser();
            $course = $this->courseRepo->find($id);
            $course->addParticipant($user);

            $user = $this->getUser();
            $currencyCode =  "XOF";
            $currencyName =  "FCFA";
            $amount = 0;

            //WAITING
            $orderStatus = 1;
            $tax = 1;
            $reference = WebSite::generateReference();
            $isPaid = false;

            //ORDER
            $order = new Order();
            $order->setUser($user)
                ->setCommandeType(Order::ORDER_TYPE_COURSE)
                ->setIsWaiting(true)
                ->setIsPaid(false)
                ->setCurrencyCode($currencyCode)
                ->setCurrencyName($currencyName)
                ->setReference($reference)
                ->setIsPaid($isPaid)
                ->setOrderStatus($orderStatus)
                ->setOrderStatusStr(OrderStatus::WAITING)
                ->setResto(null)
                ->setAmount($amount)
                ->setTax($tax)
                ->setCourse($course)
                ->setTotalAmount($amount * $tax);


            if ($course->getCourseParticipants()) {

                $prof = $course->getPublisher();

                foreach ($course->getCourseParticipants() as $item) {
                    if ($item->getParticipants()->contains($user)) {

                        $this->em->persist($course);
                        $this->em->flush();

                        $courseJson = SerializerHelper::getJson($this->serializer, $course, "show_course");
                        
                        return $this->json([
                            "data" => $courseJson,
                            "message" => "OK",
                            "status_code" => 200
                        ]);
                    }
                }

                $courseParticipant = new CourseParticipants();
                $courseParticipant->setCourse($course)->addParticipant($user)->setIsAcceped(false);
                $this->em->persist($courseParticipant);
                $this->em->flush();

                $course->addCourseParticipant($courseParticipant);

                $this->em->persist($order);
                $this->em->flush();

                $course->setCommande($order);


                $this->em->persist($course);
                $this->em->flush();
                
                $courseJson = SerializerHelper::getJson($this->serializer, $course, "show_course");
                
                $prof = $course->getPublisher();

                if (!empty($prof)) {
                    $bodyMail = $this->mailer->createBodyMail('mails/course_ask_mail.html.twig', ['user' => $user, "course" => $course, "prof" => $prof]);
                    $this->mailer->sendEmail(from: 'yemakarios70@gmail.com', to: $prof->getEmail(), subject: 'Demande participation', body: $bodyMail);
                }

                return $this->json([
                    "data" => $courseJson,
                    "message" => "OK",
                    "status_code" => 200
                ]);
            }

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function searchCourseByName($keyword)
    {

        try {

            $course = $this->courseRepo->createQueryBuilder('c')
                ->andWhere('c.name LIKE :keyword')
                ->orWhere('c.description LIKE :keyword')
                ->setParameter('keyword', "%$keyword%")
                ->getQuery()
                ->getArrayResult();

            $courseJson = SerializerHelper::getJson($this->serializer, $course, "show_course");

            if (!empty($course)) {

                return $this->json([
                    "data" => $courseJson,
                    "message" => "OK",
                    "status_code" => 200
                ], 200);
            } else {

                return $this->json([
                    "message" => Response::$statusTexts[204],
                    "status_code" => Response::HTTP_NO_CONTENT
                ], 204);
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => 500
            ], 500);
        }
    }

    public function getAllCourseForDash()
    {

        try {

            $restaurants = $this->courseRepo->findAll();

            return $this->json([
                "message" => "OK",
                "data" => $restaurants
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);
        }
    }

    public function addLesson($data)
    {
        try {

            $lesson = new Lesson();

            $course = $this->courseRepo->find($data['course_id']);
            $lesson->setTitle($data["title"])
                ->setCourse($course)
                ->setOverview($data["overview"])
                ->setVideoUrl($data["video_url"])
                ->setContent($data["body"]);

            $this->em->persist($lesson);
            $this->em->flush();

            return $this->json([
                // "data" => $lesson,
                "message" => "OK",
                "status_code" => 200
            ], 200);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => 500
            ], 500);
        }
    }

    public function updateLesson($slug, $data)
    {

        try {

            $lesson = $this->lessonRepo->find($slug);

            $lesson = new Lesson();
            $course = $this->courseRepo->find($data['course_id']);

            $lesson->setTitle($data["title"] ?? $lesson->getTitle())
                ->setCourse($course)
                ->setOverview($data["overview"] ?? $lesson->getOverview())
                ->setVideoUrl($data["video_url"] ?? $lesson->getVideoUrl())
                ->setContent($data["body"] ??  $lesson->getContent());

            $this->em->persist($lesson);
            $this->em->flush();

            return $this->json([
                // "data" => $lesson,
                "message" => "OK",
                "status_code" => 200
            ], 200);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => 500
            ], 500);
        }
    }

    public function deleteLesson($slug)
    {

        try {

            $lesson  = $this->lessonRepo->findOneBy(["slug" => $slug]);
            $this->lessonRepo->remove($lesson, true);

            return $this->json(
                [
                    "message" => "OK",
                    "status_code" => Response::HTTP_NO_CONTENT
                ],
                Response::HTTP_NO_CONTENT
            );
        } catch (Exception $e) {

            return $this->json([
                'message' => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function getLesson($id)
    {

        try {

            $lesson = $this->lessonRepo->findOneBy(["id" => $id]);
            $lessonJson = SerializerHelper::getJson($this->serializer, $lesson, "show_course");

            return $this->json([
                "message" => "OK",
                "data" => $lessonJson,
                "status_code" => 200
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => 500
            ], 500);
        }
    }
}
