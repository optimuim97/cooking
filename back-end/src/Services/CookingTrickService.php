<?php

namespace App\Services;

use App\Entity\CookingTricks;
use App\Helpers\ImageHelper;
use App\Helpers\Validation;
use App\Repository\CookingTricksRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CookingTrickService extends AbstractController
{
    public function __construct(public EntityManagerInterface $em, public CookingTricksRepository $cookTrickRepo, private ValidatorInterface $validator)
    {
    }

    public function add($data, $file)
    {
        $user = $this->getUser();

        try {

            $cookingTrick = new CookingTricks();
            $cookingTrick->setUser($user);
            $cookingTrick->setName($user->getPrenoms() . ' ' . $user->getNom());
            $cookingTrick->setEmail($user->getEmail());
            $cookingTrick->setPhoneNumber($user->getNumero());
            $cookingTrick->setBody($data["body"] ?? "");
            $title = $data["title"] ?? "";
            $cookingTrick->setTitle($title);
            $cookingTrick->setIsActive($data["isActive"] ?? "");
            $cookingTrick->setPE($data["PE"] ?? "cuisine.abijdan.net");
            $cookingTrick->setCreatedAt(new \DateTimeImmutable());
            $cookingTrick->setSlug((new Slugify())->slugify($title));

            if (!empty($file) && count($file)) {
                $image_url = ImageHelper::uploadImage($file->get('url'));
                $cookingTrick->setImageUrl($image_url);
            }

            $errors = $this->validator->validate($cookingTrick);

            if (count($errors) > 0) {
                $hasError = Validation::check($errors);

                if ($hasError != null) {
                    return $this->json([
                        "errors" => $hasError,
                        "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                        "message" => Response::$statusTexts['422']
                    ], 422);
                }
            }

            $this->em->persist($cookingTrick);
            $this->em->flush();

            return $this->json([
                "message" => "Created",
                "status_code" => Response::HTTP_CREATED,
                "data" => $cookingTrick
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => $e->getMessage()
            ]);

        }

    }

    public function delete($id)
    {
        try {
            $cookingTrick = $this->cookTrickRepo->findOneBy(["id" => $id]);

            if (!empty($cookingTrick)) {

                $this->cookTrickRepo->remove($cookingTrick, true);

                return $this->json([
                    "message" => "Deleted",
                    "status_code" => Response::HTTP_NO_CONTENT,
                    "data" => null
                ]);

            } else {

                return $this->json([
                    "message" => "Not Found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ], 404);

            }

        } catch (\Exception $e) {

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => 500
            ], 500);

        }
    }

    public function show($slug)
    {
        try {

            $cookingTrick = $this->cookTrickRepo->findOneBy(["slug" => $slug]);

            if (!empty($cookingTrick)) {
                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_FOUND,
                    "data" => $cookingTrick
                ]);
            } else {
                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }

        } catch (\Exception $e) {

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => $e->getMessage()
            ]);
        }

    }

    //TODO update  $recipeType
    public function update($slug, $data, $file)
    {
        $user = $this->getUser();

        try {

            $cookingTrick = $this->cookTrickRepo->findOneBy(["slug" => $slug]);

            if (!empty($cookingTrick)) {

                $title = $data["title"] ?? $cookingTrick->getTitle();

                $cookingTrick->setUser($user);
                $cookingTrick->setTitle($title);
                $cookingTrick->setName($user->getPrenoms() . ' ' . $user->getNom());
                $cookingTrick->setEmail($user->getEmail());
                $cookingTrick->setPhoneNumber($user->getNumero());
                $cookingTrick->setBody($data["body"] ?? $cookingTrick->getBody());
                $cookingTrick->setIsActive($data["isActive"] ?? true);
                $cookingTrick->setPE($data["PE"] ?? "cuisine.abijdan.net");
                $cookingTrick->setUpdatedAt(new \DateTimeImmutable());

                if (!empty($file) && count($file)) {
                    $image_url = ImageHelper::uploadImage($file->get('url'));
                    $cookingTrick->setImageUrl($image_url);
                }

                $slug = (new Slugify())->slugify($title);
                $cookingTrick->setSlug($slug);

                $this->em->persist($cookingTrick);
                $this->em->flush();

                //TODO setValues
                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_OK,
                    "data" => $cookingTrick
                ]);
            } else {

                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);

            }
        } catch (\Exception $e) {
            
            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode(),
            ], 500);

        }
    }

    public function getCookingTrickByName($keyword)
    {

        try {

            $cookingTricks = $this->cookTrickRepo->createQueryBuilder('r')
                ->andWhere('r.title LIKE :keyword')
                ->orWhere('r.body LIKE :keyword')
                ->setParameter('keyword', "%$keyword%")
                ->getQuery()
                ->getResult();

            if (!empty($cookingTricks)) {

                return $this->json([
                    "data" => $cookingTricks,
                    "message" => "OK",
                    "status_code" => 200
                ], 200);

            } else {

                return $this->json([
                    "message" => Response::$statusTexts[204],
                    "status_code" => Response::HTTP_NO_CONTENT
                ], 200);

            }
        } catch (Exception $e) {

            return $this->json([
                "message" => Response::$statusTexts[500],
                "status_code" => $e->getMessage()
            ], 500);
            
        }

    }

    public function getAll()
    {
     
        try {

            $cookingTricks = $this->cookTrickRepo->orderByCreatedAt();

            return $this->json([
                "message" => "Found",
                "status_code" => Response::HTTP_FOUND,
                "data" => $cookingTricks
            ]);

        } catch (\Exception $e) {

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => $e->getMessage()
            ]);

        }

    }
}
