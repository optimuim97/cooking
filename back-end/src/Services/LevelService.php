<?php

namespace App\Services;

use App\Entity\Level;
use App\Helpers\Validation;
use App\Repository\LevelRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class LevelService extends AbstractController
{

    public function __construct(
        private EntityManagerInterface $em,
        private LevelRepository $levelRepo,
        private ValidatorInterface $validator,
    ) {
    }

    public function add($data)
    {

        try {

            $level = new Level();
            $level->setName($data["name"]);
            $level->setDescription($data["description"]);

            $errors = $this->validator->validate($level);

            if (count($errors) > 0) {
                $hasError = Validation::check($errors);

                if ($hasError != null) {
                    return $this->json(
                        [
                            "errors" => $hasError,
                            "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                            "message" => Response::$statusTexts['422']
                        ],
                        422
                    );
                }
            }

            $this->em->persist($level);
            $this->em->flush();

            return $this->json([
                "message" => "Created",
                "status_code" => Response::HTTP_CREATED,
                "data" => $level
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function delete($id)
    {

        try {

            $level = $this->levelRepo->findOneBy(["id" => $id]);

            if (!empty($level)) {
                $this->levelRepo->remove($level, true);

                return $this->json([
                    "message" => "Deleted",
                    "status_code" => Response::HTTP_NO_CONTENT,
                    "data" => null
                ]);
            } else {
                return $this->json([
                    "message" => "Not Found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function show($id)
    {

        try {
            $level = $this->levelRepo->findOneBy(["id" => $id]);

            if (!empty($level)) {
                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_FOUND,
                    "data" => $level
                ]);
            } else {
                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    //TODO update  $recipeType
    public function update($id, $data)
    {
        $level = $this->levelRepo->findOneBy(["id" => $id]);

        if (!empty($level)) {

            //TODO setValues
            return $this->json([
                "message" => "Found",
                "status_code" => Response::HTTP_FOUND,
                "data" => $level
            ]);
        } else {
            return $this->json([
                "message" => "Not found",
                "status_code" => Response::HTTP_NOT_FOUND,
                "data" => null
            ]);
        }
    }

    public function getAll()
    {
        try {
            $levels = $this->levelRepo->orderByCreatedAt();

            return $this->json([
                "message" => "All level service",
                "status_code" => Response::HTTP_ACCEPTED,
                "data" => $levels
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }
}
