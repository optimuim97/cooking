<?php

namespace App\Services;

use App\Helpers\ImageHelper;
use App\Helpers\SerializerHelper;
use App\Repository\CourseRepository;
use App\Repository\RecipeRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;


class UserService extends AbstractController
{

    public function __construct(
        private EntityManagerInterface $em,
        // private EntityManager $manager,
        private RecipeRepository $recipeRespository,
        private UserRepository $userRepo,
        private CourseRepository $courseRepo,
        private SerializerInterface $serializer
    ) {
    }

    public function getUserDatas()
    {
        try {

            $user = $this->getUser();

            $recipeJson = $this->serializer->serialize(
                $user,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => "user_info"]
            );

            return $this->json([
                'message' => Response::$statusTexts['200'],
                'status_code' => Response::HTTP_OK,
                'data' => json_decode($recipeJson)
            ]);
        } catch (\Exception $e) {

            return $this->json([
                'message' => $e->getMessage(),
                'status_code' => $e->getCode()
            ], 500);
        }
    }

    public function getUserProfil($data)
    {
        try {
            $user = $this->userRepo->findOneBy(["email" => $data["email"]]);

            $recipeJson = $this->serializer->serialize(
                $user,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => "user_info"]
            );

            return $this->json([
                'message' => Response::$statusTexts['200'],
                'status_code' => Response::HTTP_OK,
                'data' => json_decode($recipeJson)
            ]);
        } catch (\Exception $e) {
            return $this->json([
                'message' => $e->getMessage(),
                'status_code' => $e->getCode()
            ], 500);
        }
    }

    public function updateUser($data, $files)
    {
        try {
            $user  = $this->getUser();

            $firstname = $data['prenoms']  == "" || $data['prenoms'] == null   ?  $user->getPrenoms() :  $data['prenoms'] ?? "";
            $lastname = $data['nom']  == "" || $data['nom'] == null   ?  $user->getNom() :  $data['nom'] ?? "";
            $phoneNumber = $data['numero']  == "" || $data['numero'] == null   ?  $user->getNumero() :  $data['numero'] ?? "";

            $user->setPrenoms($firstname);
            $user->setNom($lastname);
            $user->setNumero($phoneNumber);

            if(!empty($files) && $files != null){
                $avatar = $files->get("avatar")  ?? null;
                if (!empty($avatar)) {
                    $image_url = ImageHelper::uploadImage($avatar);
                    // $user->setAvatar($image_url);
                }
            }

            $this->em->persist($user);
            $this->em->flush();

            $userJson = SerializerHelper::getJson($this->serializer, $user, "user_info");

            return $this->json([
                'message' => Response::$statusTexts['200'],
                'status_code' => Response::HTTP_OK,
                'data' => $userJson
            ]);

        } catch (\Exception $e) {

            return $this->json([
                'message' => $e->getMessage(),
                'status_code' => $e->getCode()
            ], 500);
            
        }
    }

    public function getUserRecipes()
    {
        try {

            $user = $this->getUser();

            $recipe = $this->recipeRespository->findBy(['user' => $user]);


            return $this->json([
                'message' => Response::$statusTexts['200'],
                'status_code' => Response::HTTP_OK,
                'data' => $recipe
            ]);
        } catch (\Exception $e) {

            return $this->json([
                'message' => $e->getMessage(),
                'status_code' => $e->getCode()
            ], 500);
        }
    }

    public function getUserLikedRecipes()
    {
        try {
            $user = $this->getUser();
            $recipe = $this->recipeRespository->findBy(["liker" => $user]);

            return $this->json(
                [
                    "message" => "OK",
                    "data" => $recipe
                ]
            );
        } catch (\Exception $e) {
            return $this->json(['message' => $e->getCode(), 'status_code' => $e->getCode()], 500);
        }
    }

    public function getFollowedCourse($id)
    {

        $user = $this->getUser();
        $courses = $this->courseRepo->find($id);

        $this->json($courses);
    }
}

