<?php

namespace App\Services;

use App\Entity\User;
use App\Entity\UserSessions;
use App\Helpers\Validation;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Support\Facades\Date;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterService extends AbstractController
{
    private $em;
    private $password_hasher;
    private $jwtTokenInterface;

    public function __construct(
        protected ValidatorInterface $validator,
        EntityManagerInterface $em,
        UserPasswordHasherInterface $pass_hash,
        JWTTokenManagerInterface $jwtTokenInterface
    ) {
        $this->em = $em;
        $this->password_hasher = $pass_hash;
        $this->jwtTokenInterface = $jwtTokenInterface;
    }

    public function addUser($data)
    {

        try {

            $user = new User();

            $username = $data['username'] ?? "";
            $lastname = $data['lastname'] ?? "";
            $firstname = $data['firstname'] ?? "";
            $full_name = $firstname." ".$lastname;
            $dial_code = $data['dial_code'] ?? "";
            $phone_number = $data['phone_number'] ?? "";
            $password = $data['password'] ?? "";
            $country_code = $data['country_code'] ?? "";
            $role = ["ROLE_USER"];

            $user
                ->setEmail($username)
                ->setUsername($username)
                ->setNom($firstname)
                ->setPrenoms($lastname)
                ->setNomComplet($full_name)
                ->setCodePays($country_code)
                ->setEmailVerified(true)
                ->setNumero($phone_number)
                ->setRoles($role)
                ->setDateCreation(new \DateTimeImmutable());
            
            $plaintextPassword = $password;

            
            if($password != ""){
                $password = $this->password_hasher->hashPassword($user, $plaintextPassword);
                $user->setPassword($password);
            }

            $errors = $this->validator->validate($user);

            if (count($errors) > 0) {
                $hasError = Validation::check($errors);

                if ($hasError != null) {
                    return $this->json([
                        "errors" => $hasError,
                        "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                        "message" => Response::$statusTexts['422']
                    ]);
                }else{
                    return $this->json([
                        "status_code" => "500",
                        "message" => "KO"
                    ], 500);
                }
            }

            if($password== "" || $password== null){
                return $this->json([
                    "status_code" => "500",
                    "message" => "KO"
                ], 500);
            }

            if(count($errors) < 1){
                $this->em->persist($user);
                $this->em->flush();
            }

            //TODO send validation Mail

            $uuid = Uuid::v4().'-'.$user->getId().'-abj-cuisine';
            setcookie("token_abj", $uuid, domain:"");
            $time = time() + (3600 * 24 * 7);

            $session = new UserSessions();
            $session
                ->setUser($user)
                ->setSessionId($uuid)
                ->setTime($time)
                ->setDateCreation(new \DateTimeImmutable());

            $this->em->persist($session);
            $this->em->flush();

            return $this->json([
                "user" => $user,
                "status_code" => Response::HTTP_CREATED,
                "token" => $this->jwtTokenInterface->create($user)
            ]);

        } catch (\Exception $e) {

            // if($e->getCode() == 1062){

            //     return $this->json([
            //         "errors" => $hasError,
            //         "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
            //         "message" => Response::$statusTexts['422']
            //     ]);

            // }

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => $e->getMessage(),
            ]);
        }
        
    }
}
