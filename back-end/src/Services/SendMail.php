<?php
namespace App\Services;

use Twig\Environment;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\RawMessage;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;


class SendMail
{
     private $engine;
     private $mailer;
    public function __construct(MailerInterface $mailer, Environment $engine) {
        $this->engine = $engine;
        $this->mailer = $mailer;
    }

    public function sendEmail($from, $to, $subject, $body): void
    {
        $email = (new Email())
            ->from($from)
            ->to($to)
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            // ->replyTo($from)
            //->priority(Email::PRIORITY_HIGH)
            ->subject($subject)
            // ->text('Sending emails is fun again!')
            ->html($body)
            ;
             $this->mailer->send($email);
    }

     public function createBodyMail($view, array $parameters)
    {
        return $this->engine->render($view, $parameters);
    }
}
