<?php

namespace  App\Services;

use App\Entity\DishType;
use App\Helpers\Validation;
use App\Repository\DishTypeRepository;
use App\Repository\RestaurantRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DishTypeService extends AbstractController
{

    public function __construct(private EntityManagerInterface $em,  private RestaurantRepository $restoRepo, private DishTypeRepository $dishTypeRepository, private ValidatorInterface $validator)
    {
    }

    public function add($data)
    {

        try {

            $dish = new DishType();
            $dish->setName($data["name"]);
            $dish->setDescription($data["description"]);

            $errors = $this->validator->validate($dish);

            if (count($errors) > 0) {
                $hasError = Validation::check($errors);

                if ($hasError != null) {
                    return $this->json([
                        "errors" => $hasError,
                        "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                        "message" => Response::$statusTexts['422']
                    ], 422);
                }
            }

            $this->em->persist($dish);
            $this->em->flush();

            return $this->json([
                "message" => "Created",
                "status_code" => Response::HTTP_CREATED,
                "data" => $dish
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function delete($id)
    {

        try {

            $resto = $this->dishTypeRepository->findOneBy(["id" => $id]);

            if (!empty($resto)) {
                $this->dishTypeRepository->remove($resto, true);

                return $this->json([
                    "message" => "Deleted",
                    "status_code" => Response::HTTP_NO_CONTENT,
                    "data" => null
                ]);
            } else {
                return $this->json([
                    "message" => "Not Found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function show($id)
    {

        try {

            $resto = $this->dishTypeRepository->findOneBy(["id" => $id]);

            if (!empty($resto)) {
                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_FOUND,
                    "data" => $resto
                ]);
            } else {
                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    //TODO update dish
    public function update($id, $data)
    {
        $resto = $this->dishTypeRepository->findOneBy(["id" => $id]);

        if (!empty($resto)) {

            //TODO setValue
            return $this->json([
                "message" => "Found",
                "status_code" => Response::HTTP_FOUND,
                "data" => $resto
            ]);
        } else {
            return $this->json([
                "message" => "Not found",
                "status_code" => Response::HTTP_NOT_FOUND,
                "data" => null
            ]);
        }
    }

    public function getAll()
    {

        try {

            $dish = $this->dishTypeRepository->findAll([], ['createdAt' => 'DESC']);

            $length = count($dish);

            try {
                return $this->json([
                    "message" => "All dish in database",
                    "status" => Response::HTTP_OK,
                    "length" => $length,
                    "data" => $dish
                ]);
            } catch (\Throwable $th) {

                return $this->json([
                    "message" => $th
                ]);
                
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }
}
