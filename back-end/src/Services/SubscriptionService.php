<?php

namespace App\Services;

use App\Entity\Order;
use App\Entity\OrderStatus;
use App\Entity\Subscription;
use App\Entity\Transactions;
use App\Entity\WebSite;
use App\Helpers\SerializerHelper;
use App\Helpers\Validation;
use App\Repository\RecipeRepository;
use App\Repository\SubscriptionFormulaRepository;
use App\Repository\SubscriptionTypeRepository;
use App\Repository\TransactionTypeRepository;
use Carbon\Carbon;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SubscriptionService extends AbstractController
{
    public function __construct(
        private SubscriptionFormulaRepository $subscriptionFormulaRepo,
        private SubscriptionTypeRepository $subscriptionTypeRepo,
        private ValidatorInterface $validator,
        private SerializerInterface $serializer,
        private EntityManagerInterface $em,
        private TransactionTypeRepository $transactionTypeRepo,
        private RecipeRepository $recipeRepo
    ) {
    }

    public function subscribe($subsFormulaId, $data)
    {
        try {

            $user = $this->getUser();
            $recipes = $this->recipeRepo->findBy(["user"=> $user], ["createdAt"=> "DESC"]);

            // $recipeJSON = SerializerHelper::getJson($this->serializer, $recipes, "show_recipe");
            // return $this->json($recipeJSON);
            
            $subscriptionFormula = $this->subscriptionFormulaRepo->find($subsFormulaId);
            $subscriptionType = $this->subscriptionTypeRepo->find($subscriptionFormula->getSubscriptionType());
    
            if (empty($subscriptionType)) {
                $subscriptionType = null;
            }
    
            if (empty($subscriptionFormula)) {
                $subscriptionFormula = null;
            }

            /**Subscription */            
                $duration = $subscriptionFormula?->getDuration();
                $title = $subscriptionFormula?->getTitle();
                $description = $subscriptionFormula?->getDescription();
                $currencyCode = $subscriptionFormula?->getCurrencyCode();
                $currencyName = $subscriptionFormula?->getCurrencyName();
                $unit = $subscriptionFormula?->getUnit();

                $subscription = new Subscription();
                $reference = WebSite::generateReference();

                $subscription
                    ->addSubscriber($user)
                    ->setTitle($title)
                    ->setDescription($description)
                    ->setDuration($duration)
                    ->setCurrencyCode($currencyCode ?? "XOF")
                    ->setCurrencyName($currencyName  ?? "FCFA")
                    ->setIsTotallyFree($data["totally_free"]  ?? false)
                    ->setSubscriptionType($subscriptionType)
                    ->setPrice($subscriptionFormula->getPrice())
                    ->setUnit($subscriptionFormula->getUnit())
                    ->setSubscriptionFormula($subscriptionFormula)
                    ->setStartDate(Carbon::now())
                    ->setEndDate(Carbon::now()->add($unit,$duration));

                $errors = $this->validator->validate($subscription);

                if (count($errors) > 0) {
                    $hasError = Validation::check($errors);

                    if ($hasError != null) {
                        return $this->json([
                            "errors" => $hasError,
                            "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                            "message" => Response::$statusTexts['422']
                        ], 422);
                    }
                }
            /**Subscription */
              
            /**IsSpotLight */
                foreach($recipes as $key =>$recipe){
                    if(
                        ($key+1) < 2
                    ){     
                        $recipe->setIsSpotlight(true);
                        
                        $start = Carbon::now();
                        $time =  Carbon::now()->add($unit,$duration);

                        $end = $time;
                    
                        $recipe->setSpotLightEnd($end);
                        $recipe->setSpotLightStart($start);

                        if($subscriptionFormula->getTitle() == "Pro"){
                            $recipe->setIsSpotlight(true);
                            $recipe->setEndAbjDate(Carbon::now());
                            $recipe->setStartAbjDate(Carbon::now()->add("day", 2));
                        }

                        $this->em->persist($recipe);
                        $this->em->flush();
                    }
                }

            /**End IsSpotLight */

            /**Order */
                $orderStatus = 1;
                $tax = WebSite::getFees();

                $order = new Order();
                $order
                    ->setCommandeType(Order::ORDER_TYPE_SUBSCRIPTION)
                    ->setUser($user)
                    ->setIsWaiting(true)
                    ->setIsPaid(false)
                    ->setCurrencyCode($currencyCode)
                    ->setCurrencyName($currencyName)
                    ->setReference($reference)
                    ->setIsPaid(false)
                    ->setOrderStatus($orderStatus)
                    ->setOrderStatusStr(OrderStatus::WAITING)
                    ->setAmount($subscriptionFormula->getPrice())
                    ->setTotalAmount($subscriptionFormula->getPrice() * $tax)
                    ->setTax($tax);

                    $subscription->setCommande($order);

                    $this->em->persist($order);
                    $this->em->flush();

                    $this->em->persist($subscription);
                    $this->em->flush();
            /**Order End*/

            /**Transaction */
                // TODO TransactionType
                $transactionId = 1;
                $transactionType = $this->transactionTypeRepo->find($transactionId);
                
                $fees = WebSite::getFees();  
                $totalAmount = $fees * $subscription->getPrice();
                $fullName = $user->getPrenoms()." ".$user->getNom();
                $email = $user->getEmail();
                $phoneNumber = $user->getNumero();
                $raison = "subscription";
                $paymentMethod = "online";

                $transaction = new Transactions();
                $transaction
                    ->setAmount($subscription->getPrice())
                    ->setCurrencyCode($subscription->getCurrencyCode())
                    ->setCurrencyName($subscription->getCurrencyName())
                    ->setFees($fees)
                    ->setReference($reference)
                    ->setExternalReference("")
                    ->setTotalAmount($totalAmount)
                    ->setIsPaid(false)
                    ->setUser($user)
                    ->setCustomerName($fullName)
                    ->setCustomerEmail($email)
                    ->setCustomerPhoneNumber($phoneNumber)
                    ->setRaison($raison)
                    ->setPaymentMethod($paymentMethod)
                    ->setTransactionType($transactionType);
                
                $this->em->persist($transaction);
                $this->em->flush();
                    
                $subscriptionJson = SerializerHelper::getJson($this->serializer, $subscription, "show_subscription");
                $transactionJson = SerializerHelper::getJson($this->serializer, $transaction, "show_transaction");
            /**Transaction End*/

            return $this->json([
                "data" => [
                    "subscription"=>$subscriptionJson,
                    "transaction" => $transactionJson
                ],
                "status_code" => Response::HTTP_OK,
                "message" => "Created"
            ], 200);

        } catch (Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);

        }
    }

    public function subscribeAsCooker($id){
        
        $user = $this->getUser();

        $subscriptionFormula = $this->subscriptionFormulaRepo->find($id);
        $subscriptionType = $this->subscriptionTypeRepo->find($subscriptionFormula->getSubscriptionType());

        /**Subscription */      
            $duration = $subscriptionFormula?->getDuration();
            $title = $subscriptionFormula?->getTitle();
            $description = $subscriptionFormula?->getDescription();
            $currencyCode = $subscriptionFormula?->getCurrencyCode();
            $currencyName = $subscriptionFormula?->getCurrencyName();
            $unit = $subscriptionFormula?->getUnit();
            $price = $subscriptionFormula?->getPrice();

            $subscription = new Subscription();
            $reference = WebSite::generateReference();

            $subscription
                ->addSubscriber($user)
                ->setTitle($title)
                ->setDescription($description)
                ->setDuration($duration)
                ->setCurrencyCode($currencyCode ?? "XOF")
                ->setCurrencyName($currencyName  ?? "FCFA")
                ->setIsTotallyFree($data["totally_free"]  ?? false)
                ->setSubscriptionType($subscriptionType)
                ->setPrice($price)
                ->setUnit($unit)
                ->setSubscriptionFormula($subscriptionFormula)
                ->setStartDate(Carbon::now())
                ->setEndDate(Carbon::now()->add($unit,$duration));
        /** End Subscription */            

        /**Order */
            $orderStatus = 1;
            $tax = WebSite::getFees();

            $order = new Order();
            $order
                ->setCommandeType(Order::ORDER_TYPE_SUBSCRIPTION_COOKER)
                ->setUser($user)
                ->setIsWaiting(true)
                ->setIsPaid(false)
                ->setCurrencyCode($currencyCode)
                ->setCurrencyName($currencyName)
                ->setReference($reference)
                ->setIsPaid(false)
                ->setOrderStatus($orderStatus)
                ->setOrderStatusStr(OrderStatus::WAITING)
                ->setAmount($price)
                ->setTotalAmount($price * $tax)
                ->setTax($tax);

                $subscription->setCommande($order);

                $order->setSubscription($subscription);
                $this->em->persist($order);
                $this->em->flush();

                $this->em->persist($subscription);
                $this->em->flush();

            $errors = $this->validator->validate($subscription);

            if (count($errors) > 0) {
                $hasError = Validation::check($errors);

                if ($hasError != null) {
                    return $this->json([
                        "errors" => $hasError,
                        "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                        "message" => Response::$statusTexts['422']
                    ], 422);
                }
            }
        /**Order End */

        /**Transaction */
            // TODO TransactionType
            $transactionId = 1;
            $transactionType = $this->transactionTypeRepo->find($transactionId);
            
            $fees = WebSite::getFees();  
            $totalAmount = $fees * $subscription->getPrice();
            $fullName = $user->getPrenoms()." ".$user->getNom();
            $email = $user->getEmail();
            $phoneNumber = $user->getNumero();
            $raison = "subscription";
            $paymentMethod = "online";

            $transaction = new Transactions();
            $transaction
                ->setAmount($subscription->getPrice())
                ->setCurrencyCode($subscription->getCurrencyCode())
                ->setCurrencyName($subscription->getCurrencyName())
                ->setFees($fees)
                ->setReference($reference)
                ->setExternalReference("")
                ->setTotalAmount($totalAmount)
                ->setIsPaid(false)
                ->setUser($user)
                ->setCustomerName($fullName)
                ->setCustomerEmail($email)
                ->setCustomerPhoneNumber($phoneNumber)
                ->setRaison($raison)
                ->setPaymentMethod($paymentMethod)
                ->setTransactionType($transactionType);
            
            $this->em->persist($transaction);
            $this->em->flush();
                
            $subscriptionJson = SerializerHelper::getJson($this->serializer, $subscription, "show_subscription");
            $transactionJson = SerializerHelper::getJson($this->serializer, $transaction, "show_transaction");
        /**Transaction End*/

        return $this->json([
            "data" => [
                "subscription"=>$subscriptionJson,
                "transaction" => $transactionJson
            ],
            "status_code" => Response::HTTP_OK,
            "message" => "Created"
        ], 200);

    }
}
