<?php

namespace App\Services;

use App\Entity\BookMaker;
use App\Entity\CommentRecipe;
use App\Entity\Ingredient;
use App\Entity\Like;
use App\Entity\Recipe;
use App\Entity\RecipeImages;
use App\Entity\RecipeStep;
use App\Entity\Utensil;
use App\Helpers\ImageHelper;
use App\Helpers\SerializerHelper;
use App\Helpers\Validation;
use App\Repository\BookMakerRepository;
use App\Repository\BudgetRepository;
use App\Repository\CommentRecipeRepository;
use App\Repository\LevelRepository;
use App\Repository\LikeRepository;
use App\Repository\RecipeRepository;
use App\Repository\RecipeTypeRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class RecipeService extends AbstractController
{
    public function __construct(
        private PaginatorInterface $paginator,
        private EntityManagerInterface $em,
        private RecipeTypeRepository $recipeTypeRepository,
        private LevelRepository $levelRepo,
        private BudgetRepository $budgetRepo,
        private RecipeRepository $recipeRepository,
        private LikeRepository $likeRepository,
        private ValidatorInterface $validator,
        private CommentRecipeRepository $commentRespo,
        private BookMakerRepository $bookMarkRepo,
        private SerializerInterface $serializer
    ) {
    }

    public function add($data, $files)
    {

        try {

            $recipe = new Recipe();
            $user = $this->getUser();

            if (empty($user)) {
                return $this->json([
                    "message" => Response::$statusTexts['401'],
                    "status_code" => Response::HTTP_UNAUTHORIZED
                ]);
            }

            $ingredients = $data['ingredients'] ?? null;

            if (is_array($ingredients) && $ingredients != null) {

                foreach ($ingredients as $ingredientItem) {

                    $ingredient = new Ingredient();
                    $ingredient->setName($ingredientItem["name"] ?? "");
                    $ingredient->setQuantity($ingredientItem["quantity"] ?? "");
                    $ingredient->setMesureUnit($ingredientItem["unit"] ?? "");
                    $ingredient->addRecipe($recipe);

                    $errors = $this->validator->validate($ingredient);

                    if (count($errors) > 0) {
                        $hasError = Validation::check($errors);

                        if ($hasError != null) {
                            return $this->json([
                                "errors" => $hasError,
                                "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                                "message" => Response::$statusTexts['422']
                            ], 422);
                        }
                    }

                    $this->em->persist($ingredient);
                }
            }

            //DropZone Images
            $recipeImages = $files->get('recipeImages');

            if (is_array($recipeImages)) {
                foreach ($recipeImages as $file) {
                    $image = new RecipeImages();
                    $img = ImageHelper::uploadImage($file);
                    $image->setUrl($img);
                    $recipe->addRecipeImage($image);
                    $this->em->persist($image);
                }
            }

            //Get Recipe Images
            $recipe_cover = $files->get('recipe_images');

            //TODO add cover Image
            // $recipe_cover = $files->get('recipe_cover');
            $image_url = null;

            if (!empty($recipe_cover)) {
                $image_url = ImageHelper::uploadImage($recipe_cover);
                $recipe->setImageUrl($image_url);
            } else {
                return $this->json([
                    "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                    "message" => Response::$statusTexts['422']
                ]);
            }

            $preparation_instructions = $data['preparation_instructions'] ?? null;

            if (is_array($preparation_instructions) && $preparation_instructions != null) {
                foreach ($preparation_instructions as $key  => $recipeStep) {
                    $step = new RecipeStep();
                    $step->setName($recipeStep["name"] ?? "");
                    $step->setTitle($recipeStep["name"] ?? "");
                    $step->setDesription($recipeStep["description"] ?? "");
                    $step->setImageUrl($image_url);
                    $step->setStepNumber(intval($key) + 1);
                    $recipe->addRecipeStep($step);

                    $errors = $this->validator->validate($ingredient);

                    if (count($errors) > 0) {
                        $hasError = Validation::check($errors);

                        if ($hasError != null) {

                            return $this->json([
                                "errors" => $hasError,
                                "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                                "message" => Response::$statusTexts['422']
                            ], Response::$statusTexts['422']);
                        }
                    }

                    $this->em->persist($step);
                }
            }

            $ustentils = $data['ustentils'] ?? null;

            if (is_array($ustentils) && $ustentils != null) {
                foreach ($ustentils as $ustentilItem) {
                    $ustentil = new Utensil();
                    $ustentil->setNom($ustentilItem["name"] ?? "");
                    $ustentil->setDescription($ustentilItem["name"] ?? "");
                    $ustentil->addRecipe($recipe);

                    $errors = $this->validator->validate($ustentil);

                    if (count($errors) > 0) {
                        $hasError = Validation::check($errors);

                        if ($hasError != null) {
                            return $this->json([
                                "errors" => $hasError,
                                "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                                "message" => Response::$statusTexts['422']
                            ]);
                        }
                    }

                    $this->em->persist($ustentil);
                }
            }

            $dedicace_comp = "";
            $dedicaces = $data['dedicace'] ?? null;

            if (is_array($dedicaces) && $dedicaces != null) {
                $dedicade_tab = [];
                foreach ($dedicaces as $dedicaceItem) {
                    array_push($dedicade_tab, $dedicaceItem["name"]);
                }
                $dedicace_comp = implode(",", $dedicade_tab);
            }

            //RecipeType
            $recipeType = $this->recipeTypeRepository->findOneBy(["id" => $data['recipe_type_id'] ?? 0]);
            $recipe->setRecipeType($recipeType);

            //LevelType
            $level = $this->levelRepo->findOneBy(["id" => $data['level_id'] ?? 0]);
            $recipe->setLevel($level);

            //Budget
            $budget = $this->budgetRepo->findOneBy(["id" => $data['budget_id'] ?? 0]);
            $recipe->setBudget($budget);
            $country = $data["country"] ?? "";
            $name = $data['name'] ?? "";

            if (gettype($country) == "array") {
                $country_name = $data["country"]["name"];
                $country_code = $data["country"]["code"];
            }

            $description = $data['description'] ?? "";
            $number_of_persons = $data['number_of_persons'] ?? "";
            $suggestion = $data['suggestion'] ?? "";
            $ingredient_text = $data['ingredient_text'] ?? "";
            $ustentil_text = $data['ustentil_text'] ?? "";
            $is_tuto = $data['is_tuto'] ?? "";
            $video_url = $data['video_url'] ?? "";
            $country = $country_name ?? "";
            $country_code = $country_code ?? "";
            $dedicace = $dedicace_comp ?? "";
            $marinade = $data['marinade'] ?? "";
            $cooking = $data['cooking'] ?? "";
            $total_time = $data['total_time'] ?? "";
            $originCountry = $data['originCountry'] ?? "";
            $originPlateform = $data['originPlateform'] ?? "";
            $slug = (new Slugify())->slugify($name);

            $recipe
                ->setName($name)
                ->setDescription($description)
                ->setIsActive(true)
                ->setSuggestions($suggestion)
                ->setIngredientText($ingredient_text)
                ->setIngredientText($ustentil_text)
                ->setIsTutorial($is_tuto)
                ->setVideoUrl($video_url)
                ->setCountry($country)
                ->setCountryCode($country_code)
                ->setPricing($country_code)
                ->setDedicace($dedicace)
                ->setNumberOfPersons($number_of_persons)
                ->setMarinade($marinade)
                ->setCooking($cooking)
                ->setTotalTime($total_time)
                ->setoriginCountry($originCountry)
                ->setOriginPlateform($originPlateform)
                ->setUser($user);
            
            $recipe->setSlug($slug);

            $errors = $this->validator->validate($recipe);

            if (count($errors) > 0) {

                $hasError = Validation::check($errors);

                if ($hasError != null) {
                    return $this->json([
                        "errors" => $hasError,
                        "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                        "message" => Response::$statusTexts['422']
                    ]);
                }
            }
            

            $this->em->persist($recipe);
            $this->em->flush();

            $recipeJson = SerializerHelper::getJson($this->serializer, $recipe, "list_recipe");

            return $this->json([
                "message" => "Created",
                "status_code" => Response::HTTP_CREATED,
                "data" => $recipeJson
            ]);

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function delete($id)
    {

        try {

            $recipe = $this->recipeRepository->findOneBy(["id" => $id]);

            if (!empty($recipe)) {

                $this->recipeRepository->remove($recipe, true);

                return $this->json([
                    "message" => "Deleted",
                    "status_code" => Response::HTTP_NO_CONTENT,
                    "data" => null
                ]);
            } else {
                return $this->json([
                    "message" => "Not Found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function show($slug)
    {

        try {

            $recipe = $this->recipeRepository->findOneBy(["slug" => $slug]);

            $recipeJson = $this->serializer->serialize(
                $recipe,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => "show_recipe"]
            );

            if (!empty($recipe)) {

                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_FOUND,
                    "data" => json_decode($recipeJson)
                ]);
            } else {
                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {
            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function update($id, $data, $files)
    {
        $recipe = $this->recipeRepository->findOneBy(["id" => $id]);

        if (!empty($recipe)) {

            $name = $data["name"] ?? $recipe->getName();
            $description = $data["description"] ?? $recipe->getDescription();
            $budget = $this->budgetRepo->findOneBy(["id" => $data['budget_id'] ?? 0]);
            $recipe->setBudget($budget);
            $country = '';
            if (isset($data['country'])) {
                if (is_array($data['country'])) {
                    $country = $data["country"]["name"] ?? $recipe->getCountry();
                }
            }

            $suggestions = $data["suggestion"] ?? $recipe->getSuggestions();
            $videoUrl = $data["video_url"] ?? $recipe->getVideoUrl();
            $countryCode = $data["country_code"] ?? $recipe->getCountryCode();
            $number_of_persons = $data["number_of_persons"] ?? $recipe->getNumberOfPersons();
            $marinade = $data["marinade"] ?? $recipe->getMarinade();
            $cooking = $data["cooking"] ?? $recipe->getCooking();
            $total_time = $data["total_time"] ?? $recipe->getTotalTime();


            //Get Recipe Images
            $recipe_images = $files->get('recipe_images');

            //TODO add cover Image
            //  $recipe_cover = $files->get('recipe_cover');
            $image_url = null;

            if (is_array($recipe_images)) {

                foreach ($recipe_images as $file) {
                    $image = new RecipeImages();
                    $image->setUrl(ImageHelper::uploadImage($file));
                    $image->setOriginalName(ImageHelper::getFileInfo($file)['original_name']);
                    $recipe->addRecipeImage($image);
                    $this->em->persist($image);
                }
            } else {

                // if (!empty($recipe_images)) {
                //     $image_url = ImageHelper::uploadImage($recipe_images);
                //     $recipe->setImageUrl($image_url);
                // } else {
                //     return $this->json(["message" => "Il n'y a pas d'image"]);
                // }

            }

            $recipe->setName($name);
            $recipe->setDescription($description);
            $recipe->setCountry($country);
            $recipe->setSuggestions($suggestions);
            $recipe->setVideoUrl($videoUrl);
            $recipe->setCountryCode($countryCode);
            $recipe->setNumberOfPersons($number_of_persons);
            $recipe->setMarinade($marinade);
            $recipe->setCooking($cooking);
            $recipe->setTotalTime($total_time);

            $this->em->persist($recipe);
            $this->em->flush();

            //TODO setValue
            return $this->json([
                "message" => "Updated",
                "status_code" => Response::HTTP_OK,
            ]);
        } else {

            return $this->json([
                "message" => "Not found",
                "status_code" => Response::HTTP_NOT_FOUND,
                "data" => null
            ]);
        }
    }

    public function getAll($data)
    {
        try {

            $page = $data["page"] ?? 1;
            $per_page = $data["per_page"] ?? 6;

            $recipes = $this->recipeRepository->findBy(
                array(),
                array('createdAt' => 'DESC'),
                200,
                0
            );

            $recipesList = $this->paginator->paginate($recipes, page: $page, limit: $per_page);

            $currentpagenumber = $recipesList->getCurrentPageNumber();
            $itemnumberperpage = $recipesList->getItemNumberPerPage();
            $totalitemcount = $recipesList->getTotalItemCount();

            $recipesJson = $this->serializer->serialize(
                $recipesList,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => 'list_recipes']
                // [AbstractNormalizer::GROUPS => 'show_recipe']
            );

            $jTableResult = [];
            $jTableResult['current_page'] = $currentpagenumber;
            $jTableResult['total'] = $totalitemcount;
            $jTableResult['pages_count'] = ceil($totalitemcount / $itemnumberperpage);

            return $this->json([
                "message" => "Found",
                "status_code" => Response::HTTP_FOUND,
                "data" => json_decode($recipesJson),
                "pagination" => $jTableResult
            ]);

        } catch (\Exception $e) {
            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);
        }
    }

    public function getByType($slug, $data)
    {

        try {
            $per_page = $data['per_page'] ?? 12;
            $page = $data['page'] ?? 1;

            $recipeType = $this->recipeTypeRepository->findOneBy(["slug" => $slug]);

            $recipes = $this->recipeRepository->findBy(['recipeType' => $recipeType], ['createdAt' => "DESC"]);

            $recipesList = $this->paginator->paginate($recipes, page: $page, limit: $per_page);
            $currentpagenumber = $recipesList->getCurrentPageNumber();
            $itemnumberperpage = $recipesList->getItemNumberPerPage();
            $totalitemcount = $recipesList->getTotalItemCount();

            $jTableResult = [];
            $jTableResult['current_page'] = $currentpagenumber;
            $jTableResult['total'] = $totalitemcount;
            $jTableResult['pages_count'] = ceil($totalitemcount / $itemnumberperpage);
            // $jTableResult['rows'] = $items;

            // return $this->json([$jTableResult], 200);

            $recipesJson = $this->serializer->serialize(
                $recipesList,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => 'list_recipes']
            );

            return $this->json([
                "message" => "Found",
                "status_code" => Response::HTTP_FOUND,
                "data" => json_decode($recipesJson),
                "pagination" => $jTableResult
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function likeOrDislike($id, $user)
    {

        $recipe = $this->recipeRepository->findOneBy(["id" => $id]);

        if (!$recipe) {

            return $this->json([
                "message" => "recipe with id = $id doesn't exist!",
                "status_code" => Response::HTTP_NOT_FOUND,
                "data" => []
            ]);

            exit;
        }

        $recipe_likes = $recipe->getLikes();

        if (count($recipe_likes) >= 1) {

            foreach ($recipe_likes as $like) {
                if ($like->getLiker(true) == $this->getUser()) {
                    $this->likeRepository->remove($like, true);

                    $recipe->removeLike($like);
                    $this->em->flush();

                    return $this->json([
                        "message" => "Disliked !",
                        "status_code" => Response::HTTP_NO_CONTENT,
                        "nb_likes" => count($recipe->getLikes())
                    ]);
                }
            }
        }

        $like = new Like();
        $like->setLiker($user)
            ->setRecipe($recipe);

        $recipe->addLike($like);

        $this->em->persist($like);
        $this->em->flush();

        return $this->json([
            "message" => "like!",
            "status_code" => Response::HTTP_OK,
            "nb_likes" => count($recipe->getLikes()),
            // "like" => $like
        ]);
    }

    public function commentRecipe($slug, $data)
    {

        try {
            $user = $this->getUser();
            $recipe = $this->recipeRepository->findOneBy(["slug" => $slug]);

            // $publisher = $recipe->getUser();
            // SendMailService::send($publisher->getEmail());

            $comment = new CommentRecipe();
            $comment
                ->setContent($data["content"])
                ->setRecipe($recipe)
                ->setUser($user);


            $errors = $this->validator->validate($comment);

            if (count($errors) > 0) {
                $hasError = Validation::check($errors);

                if ($hasError != null) {
                    return $this->json([
                        "errors" => $hasError,
                        "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                        "message" => Response::$statusTexts['422']
                    ]);
                }
            }

            $this->em->persist($comment);

            //TODO add Validation to `comment` 
            $recipe->addCommentRecipe($comment);
            $this->em->persist($recipe);
            $this->em->flush();

            $commentJson = SerializerHelper::getJson($this->serializer, $comment, "comment_list");
            $commentJson = $commentJson;

            return $this->json([
                "message" => Response::$statusTexts["201"],
                "status_code" => Response::HTTP_CREATED,
                // "recipe" => $recipe,
                "comment" => $commentJson
            ]);

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function getByLevel($id)
    {

        try {
            $recipes = $this->recipeRepository->getByLevel($id);

            return $this->json([
                "message" => "Found",
                "status_code" => Response::HTTP_FOUND,
                "data" => $recipes
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function updateCommentRecipe($recipe_id, $comment_id, $data)
    {

        try {
            //Get Recipe    
            $recipe = $this->recipeRepository->find($recipe_id);

            //Get Comment
            $comment = $this->commentRespo->find($comment_id);
            $comment->setContent($data['content']);

            //set Datas
            $this->em->persist($comment);

            return $this->json([
                "message" => Response::$statusTexts['200'],
                "status_code" => Response::HTTP_OK,
                "data" => $comment
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function deleteCommentRecipe($recipe_id, $comment_id)
    {

        try {
            //Get Recipe   
            $recipe = $this->recipeRepository->find($recipe_id);

            //Get Comment
            $comment = $this->commentRespo->find($comment_id);
            $recipe->removeCommentRecipe($comment);

            return $this->json([
                "message" => Response::$statusTexts['204'],
                "status_code" => Response::HTTP_NO_CONTENT
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function getRecipeSpotLight()
    {

        try {
            //Get Recipe   
            $recipe = $this->recipeRepository->findBy(["isSpotlight" => true]);

            //Get Comment
            // $comment = $this->commentRespo->find($comment_id);
            // $recipe->removeCommentRecipe($comment);

            return $this->json([
                "message" => Response::$statusTexts['302'],
                "data" => $recipe,
                "status_code" => Response::HTTP_FOUND
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function getRecipeByName($keyword)
    {

        try {

            $recipes = $this->recipeRepository
                ->createQueryBuilder('r')
                ->andWhere('r.name LIKE :keyword')
                ->orWhere('r.description LIKE :keyword')
                ->setParameter('keyword', "%{$keyword}%")
                ->getQuery()
                ->getResult()
                ;

                $recipeJson = SerializerHelper::getJson($this->serializer, $recipes, "list_recipes");

                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_FOUND,
                    "data" => $recipeJson
                ]);
                    
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => Response::HTTP_NOT_FOUND,
            ], 500);

        }

    }

    public function addRecipeToBookMark($recipeID)
    {

        $user = $this->getUser();
        $userSavedRecipes = [];
        $recipe = $this->recipeRepository->find($recipeID);

        try {

            $allBookMarked = $this->bookMarkRepo->findAll();

            //Get user recipes
            foreach ($allBookMarked as $rec) {
                // return $this->json($rec->getSaver()['0']);
                if ($rec->getSaver()['0'] == $user) {
                    array_push($userSavedRecipes, $rec->getRecipe()['0']);
                }
            }

            //Check if $recipe is in array
            $alreadyAdded = in_array($recipe, $userSavedRecipes);

            if (!$alreadyAdded) {
                if ($recipe != null) {
                    $user = $this->getUser();
                    $bookMaker = new BookMaker();
                    $bookMaker->addSaver($user);
                    $bookMaker->addRecipe($recipe);
                    $recipe->addBookMaker($bookMaker);

                    $this->em->persist($bookMaker);
                    $this->em->persist($recipe);

                    $this->em->flush();


                    return $this->json([
                        "status_code" => Response::HTTP_CREATED,
                        "message" => Response::$statusTexts["201"]
                    ]);
                } else {

                    return $this->json([
                        "message" => "Not found recipe with id : $recipeID"
                    ]);
                }
            } else {

                return $this->json([
                    "message" => "ALREADY_ADD",
                    "status_code" => 204
                ]);
            }
        } catch (\Exception $e) {
            return $this->json([
                "message" => $e->getMessage()
            ]);
        }
    }

    public function getRecipeBookMark()
    {
        $user = $this->getUser();

        $userData = $this->serializer->serialize(
            $user,
            JsonEncoder::FORMAT,
            [AbstractNormalizer::GROUPS => "user_info"]
        );

        $userBookMakers = [];

        $bookMarks = json_decode($userData)->bookMakers;
        
        if (!empty($bookMarks)) {
            
            foreach ($bookMarks as $value) {
                if(isset($value->recipe["0"])){
                    array_push($userBookMakers, $value->recipe["0"]);
                }
            }

            return $this->json([
                'data' => $userBookMakers,
                "message" => "OK",
                "status_code" => 200
            ], 200);

        } else {
            return $this->json([
                "message" => "KO",
                "status_code" => 404
            ], 404);
        }
    }

    public function getRecipeAll()
    {
        try {
            $recipe = $this->recipeRepository->countAllRecipe();

            return $this->json([
                'count' => $recipe[1],
                'status_code' => 200
            ], 200);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => 500
            ], 500);
        }
    }

    public function getIsSpotifiyRecipe()
    {

        try {

            $recipe = $this->recipeRepository->findBy(['isSpotlight' => true], ["updatedAt" => "DESC"]);
            
            $recipeJson = SerializerHelper::getJson($this->serializer,$recipe,"list_recipes");

            return $this->json([
                'message' => "OK",
                'data' => $recipeJson
            ], 200);

            // return $this->json(['message' => "OK", "data" => $recipeJson], Response::HTTP_OK);
            
        } catch (\Exception $e) {
            
            return $this->json(
                [
                    'status_code'=> $e->getCode(),
                    'message' => $e->getMessage(),
                    500
                ]
            );
        }
    }

    public function getAllRecipeForDash(){
        
        try {

            $recipes = $this->recipeRepository->findBy(
                array(),
                array('id' => 'DESC'),
                100,
                0
            );

            $recipeJson = SerializerHelper::getJson($this->serializer,$recipes,"list_recipes");

            return $this->json([
                'message' => "OK",
                'data' => $recipeJson,
                'status_code'=> 200
            ], 200);

        } catch(\Exception $e) {

           return $this->json(
               [
                    'status_code'=> $e->getCode(),
                    'message' => $e->getMessage(),
                    500
                ]
           );

        }

    }
}
