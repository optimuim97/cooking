<?php

namespace App\Services;

use App\Entity\RecipeType;
use App\Helpers\ImageHelper;
use App\Helpers\JeasyuiFilter;
use App\Repository\RecipeTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class RecipeTypeService extends AbstractController
{

    private $em;
    private $recipeTypeRepo;

    public function __construct(
        EntityManagerInterface $em,
        RecipeTypeRepository $recipeTypeRepo ,
        private PaginatorInterface $paginator,
        private SerializerInterface $serializer
    )
    {
        $this->em = $em;
        $this->recipeTypeRepo = $recipeTypeRepo;
    }

    public function add($data, $file)
    {

        try {

            $recipeType = new RecipeType();
            $recipeType->setName($data["name"]);
            $recipeType->setDescription($data["description"]);

            if (!empty($file)) {
                $image_url = ImageHelper::uploadImage($file);
                $recipeType->setImageUrl($image_url);
            }

            $this->em->persist($recipeType);
            $this->em->flush();

            return $this->json([
                "message" => "Created",
                "status_code" => Response::HTTP_CREATED,
                "data" => $recipeType
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function delete($id)
    {
        try {

            $recipeType = $this->recipeTypeRepo->findOneBy(["id" => $id]);

            if (!empty($recipeType)) {
                $this->recipeTypeRepo->remove($recipeType, true);

                return $this->json([
                    "message" => "Deleted",
                    "status_code" => Response::HTTP_NO_CONTENT,
                    "data" => null
                ]);

            } else {
                return $this->json([
                    "message" => "Not Found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function show($slug)
    {
        try {

            $recipeType = $this->recipeTypeRepo->findOneBy(["slug" => $slug]);

            if (!empty($recipeType)) {
                
                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_FOUND,
                    "data" => $recipeType
                ]);

            } else {

                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);

            }

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function update($id, $data, $file)
    {
        try{

            $recipeType = $this->recipeTypeRepo->findOneBy(["id" => $id]);
    
            if (!empty($recipeType)) {
    
                $recipeType->setName($data["name"] ?? $recipeType->getName());
                $recipeType->setDescription($data["description"] ?? ($recipeType->getDescription() == null ? "" : $recipeType->getDescription()));
    
                if (!empty($file)) {
                    $image_url = ImageHelper::uploadImage($file);
                    $recipeType->setImageUrl($image_url);
                }
    
                $this->em->persist($recipeType);
                $this->em->flush();
    
                //TODO setValue
                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_FOUND,
                    "data" => $recipeType
                ]);
            } else {
    
                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        }catch(\Exception $e){

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode(),
            ], 500);

        }
    }

    public function getAll($data)
    {
        try {

            $page = $data["page"] ?? 1;
            $per_page = $data["per_page"] ?? 5;

            $recipeTypes = $this->recipeTypeRepo->findBy(
                array(),
                array('createdAt' => 'DESC'),
                200,
                0
            );

            $recipeTypesList = $this->paginator->paginate($recipeTypes, page: $page, limit: $per_page);

            $currentpagenumber = $recipeTypesList->getCurrentPageNumber();
            $itemnumberperpage = $recipeTypesList->getItemNumberPerPage();
            $totalitemcount = $recipeTypesList->getTotalItemCount();

            $recipeTypesJson = $this->serializer->serialize(
                $recipeTypesList,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => 'show_recipe_type']
                // [AbstractNormalizer::GROUPS => 'show_recipe']
            );

            $jTableResult = [];
            $jTableResult['current_page'] = $currentpagenumber;
            $jTableResult['total'] = $totalitemcount;
            $jTableResult['pages_count'] = ceil($totalitemcount / $itemnumberperpage);

            return $this->json([
                "message" => "Found",
                "status_code" => Response::HTTP_FOUND,
                "data" => json_decode($recipeTypesJson),
                "pagination" => $jTableResult
            ]);
            
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);

        }
        
    }

    public function getAllRT(){

        try{

            $recipeTypes = $this->recipeTypeRepo->findBy(
                array(),
                array('createdAt' => 'DESC'),
                200,
                0
            );

            return $this->json([
                "message" => "Found",
                "status_code" => Response::HTTP_FOUND,
                "data" => $recipeTypes
            ]);
            
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);

        }

    }

    public function getAllRecipeTypeForDash()
    {
        try {

            $recipeTypes = $this->recipeTypeRepo->findAll();

            return $this->json(
                [
                    "message" => "OK",
                    "data" => $recipeTypes,
                    "status_code" => 200
                ]
            );

        } catch (\Exception $e) {
            
            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode(),
            ], 500);
            
        }
    }
}

