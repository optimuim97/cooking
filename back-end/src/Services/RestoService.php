<?php
namespace App\Services;

use App\Entity\Restaurant;
use App\Helpers\ImageHelper;
use App\Repository\RestaurantRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class RestoService extends AbstractController{

    private $em;
    private $restoRepo;

    public function __construct(EntityManagerInterface $em, RestaurantRepository $restoRepo)
    {
        $this->em = $em;
        $this->restoRepo = $restoRepo;
    }
    
    public function add($data,$file){
        
        try{
            
            $resto = new Restaurant();
            $resto->setName($data["name"]);
            $resto->setDescription($data["description"]);
            $image_url = ImageHelper::uploadImage($file);
    
            $resto->setImageUrl($image_url);
            
            $this->em->persist($resto);
            $this->em->flush();
            
            return $this->json([
                "message"=>"Created",
                "status_code"=> Response::HTTP_CREATED,
                "data"=> $resto
            ]);

        } catch (\Exception $e) {

            return $this->json([
                "message"=>$e->getMessage(),
                "status_code"=> $e->getCode()
            ]);

        }

    }
    
    public function delete($id){
        
        try {

            $recipe = $this->restoRepo->findOneBy(["id"=>$id]);

            if(!empty($recipe)){
                $this->restoRepo->remove($recipe,true);
    
                return $this->json([
                    "message"=>"Deleted",
                    "status_code"=> Response::HTTP_NO_CONTENT,
                    "data"=> null
                ]);
            }else{
                return $this->json([
                    "message"=>"Not Found",
                    "status_code"=> Response::HTTP_NOT_FOUND,
                    "data"=> null
                ]);
            }

        } catch (\Exception $e) {

            return $this->json([
                "message"=>$e->getMessage(),
                "status_code"=> $e->getCode()
            ]);

        }
      
    }

    public function show($id){
        
        try{

            $recipe = $this->restoRepo->findOneBy(["id"=>$id]);
    
            if(!empty($recipe)){
                return $this->json([
                    "message"=>"Found",
                    "status_code"=> Response::HTTP_FOUND,
                    "data"=> $recipe
                ]);
    
            }else{
                return $this->json([
                    "message"=>"Not found",
                    "status_code"=> Response::HTTP_NOT_FOUND,
                    "data"=> null
                ]);
            }        
        } catch (\Exception $e) {

            return $this->json([
                "message"=>$e->getMessage(),
                "status_code"=> $e->getCode()
            ]);

        }

    }
    
    //TODO update resto
    public function update($id, $data){
        
        $recipe = $this->restoRepo->findOneBy(["id"=>$id]);

        if(!empty($recipe)){

            //TODO setValue
            return $this->json([
                "message"=>"Found",
                "status_code"=> Response::HTTP_FOUND,
                "data"=> $recipe
            ]);

        }else{
            return $this->json([
                "message"=>"Not found",
                "status_code"=> Response::HTTP_NOT_FOUND,
                "data"=> null
            ]);
        }
    }
  
    public function getAll(){
        
        try{
            
            $resto = $this->restoRepo->orderByCreatedAtDesc();
            $length = count($resto);
    
            try {

                return $this->json([
                    "message" => "All resto in database",
                    "status" => Response::HTTP_OK,
                    "length" => $length,
                    "data" => $resto
                ]);

            } catch (\Throwable $th) {

                return $this->json([
                    "message" => $th
                ]);
                
            }

        } catch (\Exception $e) {

            return $this->json([
                "message"=>$e->getMessage(),
                "status_code"=> $e->getCode()
            ]);

        }
        
    }

}
