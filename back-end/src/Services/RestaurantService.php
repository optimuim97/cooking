<?php

namespace App\Services;

use App\Entity\Dish;
use App\Entity\OrderStatus;
use App\Entity\Restaurant;
use App\Entity\RestaurantDish;
use App\Entity\RestaurantImage;
use App\Helpers\ImageHelper;
use App\Helpers\SerializerHelper;
use App\Repository\DishRepository;
use App\Repository\DishTypeRepository;
use App\Repository\OrderRepository;
use App\Repository\RestaurantRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class RestaurantService extends AbstractController
{

    public function __construct(
        private OrderRepository $orderRepo,
        private EntityManagerInterface $em,
        private RestaurantRepository $restoRepo,
        private DishRepository $dishRepo,
        private DishTypeRepository $dishTypeRepo,
        private PaginatorInterface $paginator,
        private SerializerInterface $serializer,
    ) {
    }

    public function add($data, $files)
    {
        try {

            $user = $this->getUser();

            $restaurant = new Restaurant();
            $restaurant->setName($data["name"] ?? "");
            $restaurant->setSlogan($data["slogan"] ?? "");
            $restaurant->setDescription($data["description"] ?? "");
            $restaurant->setDialCode($data["dial_code"] ?? "");
            $restaurant->setPhone($data["phone"] ?? "");
            $restaurant->setEmail($data["email"] ?? "");
            $restaurant->setContact1($data["contact1"] ?? "");
            $restaurant->setLat($data["lat"] ?? "");
            $restaurant->setLon($data["lon"] ?? "");
            $restaurant->setCountryCode($data["country_code"] ?? "");
            $restaurant->setCountryName($data["country_name"] ?? "");
            $restaurant->setTown($data["town"] ?? "");
            $restaurant->setCity($data["city"] ?? "");
            $restaurant->setChefName($data["chefName"] ?? "");
            $restaurant->setScheduleStart($data["scheduleStart"] ?? "");
            $restaurant->setScheduleEnd($data["scheduleEnd"] ?? "");
            $restaurant->setOwner($user);
            $restaurant->setChefDescription($data['chef_description'] ?? "");
            $restaurant->setAboutOurChief($data['about_our_chief'] ?? "");
            $restaurant->setChiefImage($data['chief_image'] ?? "");
            $restaurant->setIsSpotLight(false);
            $restaurant->setAddress($data['address'] ?? "");
            $restaurant->setAddress($data['address_comp'] ?? "");

            $user->setHasRestaurant(true);
            $this->em->persist($user);
            $this->em->flush();

            if (!empty($files->get('imageUrl'))) {
                $image_url = ImageHelper::uploadImage($files->get('imageUrl'));
                $restaurant->setImageUrl($image_url);
            }

            //DropZone Images
            $restaurantImages = $files->get("restaurantImages");

            if (is_array($restaurantImages)) {
                if (count($restaurantImages) >= 1) {
                    foreach ($restaurantImages as $file) {
                        $image = new RestaurantImage();
                        $img = ImageHelper::uploadImage($file);
                        $image->setUrl($img);
                        $restaurant->addRestaurantImage($image);
                        $this->em->persist($image);
                    }
                }
            }

            $this->em->persist($restaurant);
            $this->em->flush();

            $restaurantJson = SerializerHelper::getJson($this->serializer, $restaurant, "restaurant_list" );

            return $this->json([
                "message" => "Created",
                "status_code" => Response::HTTP_CREATED,
                "data" => $restaurantJson
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }

    }

    public function delete($id)
    {
        try {
            $resto = $this->restoRepo->findOneBy(["id" => $id]);

            if (!empty($resto)) {
                $this->restoRepo->remove($resto, true);

                return $this->json([
                    "message" => "Deleted",
                    "status_code" => Response::HTTP_NO_CONTENT,
                    "data" => null
                ]);
            } else {
                return $this->json([
                    "message" => "Not Found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function show($slug)
    {

        try {

            $resto = $this->restoRepo->findOneBy(["slug" => $slug]);
            $restoJson = SerializerHelper::getJson($this->serializer, $resto,"show_restaurant");
            
            if (!empty($resto)) {

                return $this->json(
                    [
                        "data" => $restoJson,
                        "message" => "Found",
                        "status_code" => Response::HTTP_FOUND,
                    ],
                    Response::HTTP_OK
                );

            } else {

                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);

            }

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
        
    }

    public function update($id, $data, $files)
    {        
        try {

            $restaurant = $this->restoRepo->findOneBy(["id" => $id]);

            if (!empty($restaurant)) {
                
                $name = $data["name"] ?? $restaurant->getName();
                $slogan = $data["slogan"] ?? $restaurant->getSlogan();
                $description = $data["description"] ?? $restaurant->getDescription();
                $dial_code = $data["dial_code"] ?? $restaurant->getDialCode();
                $phone = $data["phone"] ?? $restaurant->getPhone();
                $email = $data["email"] ?? $restaurant->getEmail();
                $contact1 = $data["contact1"] ?? $restaurant->getContact1();
                $lat = $data["lat"] ?? $restaurant->getLat();
                $lon = $data["lon"] ?? $restaurant->getLon();
                $country_code = $data["country_code"] ?? $restaurant->getCountryCode();
                $countryName = $data["country_name"] ?? $restaurant->getCountryName();
                $town = $data["town"] ?? $restaurant->getTown();
                $city = $data["city"] ?? $restaurant->getCity();
                $address = $data["address"] ?? $restaurant->getAddress();
                $aboutOurChief = $data["aboutOurChief"] ?? "";
                $chefName = $data["chefName"] ?? "";
                $scheduleStart = $data["scheduleStart"] ?? $restaurant->getScheduleStart();
                $scheduleEnd = $data["scheduleEnd"] ?? $restaurant->getScheduleEnd();

                $restaurant->setName($name);
                $restaurant->setSlogan($slogan);
                $restaurant->setScheduleStart($scheduleStart ?? "");
                $restaurant->setScheduleEnd($scheduleEnd ?? "");
                $restaurant->setDescription($description);
                $restaurant->setDialCode($dial_code);
                $restaurant->setPhone($phone);
                $restaurant->setEmail($email);
                $restaurant->setContact1($contact1);
                $restaurant->setLat($lat);
                $restaurant->setLon($lon);
                $restaurant->setCountryCode($country_code);
                $restaurant->setCountryName($countryName);
                $restaurant->setTown($town);
                $restaurant->setCity($city);
                $restaurant->setAboutOurChief($aboutOurChief);
                $restaurant->setChefDescription($aboutOurChief);
                $restaurant->setChefName($chefName);
                $restaurant->setAddress($address);

                if (!empty($files->get('imageUrl'))) {
                    $image_url = ImageHelper::uploadImage($files->get('imageUrl'));
                    $restaurant->setImageUrl($image_url);
                }

                if (!empty($files->get('chiefImage'))) {
                    $image = $files->get('chiefImage');
                    $image_url = ImageHelper::uploadImage($image);
                    $restaurant->setChefImageUrl($image_url);
                }

                //DropZone Images
                $restaurantImages = $files->get("restaurantImages");

                if (!empty($restaurantImages)) {
                    foreach ($restaurantImages as  $file) {
                        $image = new RestaurantImage();
                        $img = ImageHelper::uploadImage($file);
                        $image->setUrl($img);
                        $image->setRestaurant($restaurant);
                        // $restaurant->addRestaurantImage($file);
                        $this->em->persist($image);
                        $this->em->flush();
                    }
                }

                $this->em->persist($restaurant);
                $this->em->flush();

                $restaurantJson = SerializerHelper::getJson($this->serializer, $restaurant, "show_restaurant");

                //TODO setValue
                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_OK,
                    "data" => $restaurantJson
                ]);

            } else {
                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => Response::HTTP_NOT_FOUND
            ]);
        }
    }

    public function getAll($data)
    {
        try {

            $page = $data["page"] ?? 1;
            $per_page = $data["per_page"] ?? 4;
            $restoArray = []; 

            $resto = $this->restoRepo->findBy(
                array(),
                array('createdAt' => 'DESC'),
                80,
                0
            );
            
            // foreach ($resto as $item) {
            //     $itemJson = SerializerHelper::getJson($this->serializer,$item, "show_restaurant");

            //     if($itemJson?->owner?->checkSubscriptionTime == true){
            //         array_push($restoArray, $item);
            //     }
            // }

            $restoList = $this->paginator->paginate($restoArray, page: $page, limit: $per_page);
            $currentpagenumber = $restoList->getCurrentPageNumber();
            $itemnumberperpage = $restoList->getItemNumberPerPage();
            $totalitemcount = $restoList->getTotalItemCount();

            $jTableResult = [];
            $jTableResult['current_page'] = $currentpagenumber;
            $jTableResult['total'] = $totalitemcount;
            $jTableResult['pages_count'] = ceil($totalitemcount / $itemnumberperpage);

            $restoListJson = SerializerHelper::getJson($this->serializer,$restoList, "show_restaurant");
                
            return $this->json([
                "message" => "All restaurant in database",
                "status_code" => Response::HTTP_OK,
                "data" => $restoListJson,
                "pagination" => $jTableResult
            ]);

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
            
        }
    }

    public function addDish($data, $file)
    {
        try {

            $name = $data["name"] ?? "";
            $prix = $data["prix"] ?? "";
            $description = $data["description"] ?? "";
            $is_buyable = $data["is_buyable"] ?? false;

            //RecipeType
            $dishType = $this->dishTypeRepo->findOneBy(["id" => $data['dish_type_id'] ?? 0]);

            $restaurant = $data["restaurant_id"] ?? 1;
            $restaurant = $this->restoRepo->findOneBy(["id" => $data['restaurant_id'] ?? 0]);

            $dish = new Dish();
            $dish->setName($name)
                ->setDescription($description)
                ->setIsBuyable($is_buyable)
                ->setRestaurant($restaurant)
                ->setPrix($prix);

            $dish->setDishType($dishType);

            $restoDish = new RestaurantDish();
            $restoDish->setName($name)
                ->setDescription($description)
                ->setIsBuyable($is_buyable)
                ->setRestaurant($restaurant)
                ->setPrice($prix);

            if (!empty($file)) {
                $image_url = ImageHelper::uploadImage($file);
                $dish->setImageUrl($image_url);
                $restoDish->setImageUrl($image_url);
            }

            $this->em->persist($dish);
            $this->em->flush();
            $this->em->persist($restoDish);
            $this->em->flush();

            return $this->json([
                "message" => "Created",
                "status_code" => Response::HTTP_CREATED,
                "data" => $dish
            ]);

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);

        }
    }

    public function updateDish($id, $data, $file)
    {
        try {

            $dish = $this->dishRepo->find($id);

            $name = $data["name"] ?? $dish->getName();
            $prix = $data["prix"] ?? $dish->getPrix();
            $description = $data["description"] ?? $dish->getDescription();

            $is_buyable = $data["is_buyable"] ?? false;

            //RecipeType
            $dishType = $this->dishTypeRepo->findOneBy(["id" => $data['dish_type_id']]);

            $restaurant_id = $data["restaurant_id"];
            $restaurant = $this->restoRepo->findOneBy(["id" => $restaurant_id]);

            $dish->setName($name)
                ->setDescription($description)
                ->setIsBuyable($is_buyable)
                ->setRestaurant($restaurant)
                ->setDescription($description)
                ->setPrix($prix);

            $dish->setDishType($dishType);

            // $restoDish = $this->dishRepo->find($id);
            // $restoDish->setName($name)
            //     ->setDescription($description)
            //     ->setIsBuyable($is_buyable)
            //     ->setRestaurant($restaurant)
            //     ->setPrix($prix);

            if (!empty($file)) {
                if (
                    isset($file) && $file != null
                ) {
                    $image_url = ImageHelper::uploadImage($file);
                    $dish->setImageUrl($image_url);
                }
            } else {
                $dish->setImageUrl($dish->getImageUrl());
                // $restoDish->setImageUrl($restoDish->getImageUrl());
            }

            $this->em->persist($dish);
            $this->em->flush();
            // $this->em->persist($restoDish);
            // $this->em->flush();

            return $this->json([
                "message" => "Updated",
                "status_code" => Response::HTTP_OK,
                // "data" => $dish
            ]);
        } catch (\Exception $e) {

            return $this->json(
                [
                    "message" => $e->getMessage(),
                    "status_code" => 500
                ],
                500
            );
        }
    }

    public function getDishes()
    {
        try {

            $dishs = $this->dishRepo->findAll([], ['createdAt' => "DESC"]);

            return $this->json([
                "message" => "All Dish in database",
                "status" => Response::HTTP_OK,
                "data" => $dishs
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => 500
            ], 500);
        }
    }

    public function showDish($id)
    {
        try {

            $dish = $this->dishRepo->find($id);

            $dishJson = SerializerHelper::getJson($this->serializer,$dish,"show_dish");

            return $this->json([
                "message" => "Show Dish",
                "status" => Response::HTTP_OK,
                "data" => $dishJson
            ]);
            
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => 500
            ], 500);
        }
    }

    public function deleteDish($id)
    {
        try {

            $dish = $this->dishRepo->findOneBy(["id" => $id]);
            $this->dishRepo->remove($dish, true);

            return $this->json([
                "message" => "Deleted",
                "status_code" => Response::HTTP_NO_CONTENT,
                "data" => null
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    public function spotLight()
    {
        try {

            $restaurant = $this->restoRepo->findBy([
                'isSpotLight' => true
                // ,'updatedAt' => "DESC"
            ]);

            
            $restoJson = SerializerHelper::getJson($this->serializer, $restaurant, "show_restaurant");

            return $this->json(
                [
                    "message" => "OK",
                    "data" => $restoJson
                ],
                Response::HTTP_OK
            );
        } catch (\Exception $e) {

            return $this->json(
                [
                    "message" => $e->getMessage()
                ],
                500
            );
        }
    }

    public function getDishesByType($id, $resto_id)
    {
        try {

            $resto = $this->restoRepo->find($resto_id);
            $dishes = [];
            $dishType = $this->dishTypeRepo->find($id);


            if (count($resto->getDishes()) >= 1) {

                foreach ($resto->getDishes() as $dish) {
                    if ($dish->getDishType() == $dishType) {
                        array_push($dishes, $dish);
                    }
                }
            } else {
                return $this->json([
                    "message" => "OK",
                    "data" => $dishes,
                    "status_code" => Response::HTTP_OK
                ]);
            }


            return $this->json([
                "message" => "OK",
                "data" => $dishes,
                "status_code" => Response::HTTP_OK
            ]);
        } catch (\Exception $e) {

            return $this->json(
                [
                    "message" => $e->getMessage()
                ],
                500
            );
        }
    }

    public function getRestoByName($keyword)
    {

        try {
            $restaurant = $this->restoRepo->createQueryBuilder('r')
                ->andWhere('r.name LIKE :keyword')
                ->orWhere('r.description LIKE :keyword')
                ->setParameter('keyword', "%$keyword%")
                ->getQuery()
                ->getResult();            

            if (!empty($restaurant)) {

                $restaurantJson =  SerializerHelper::getJson($this->serializer,$restaurant, "show_restaurant");

                return $this->json([
                    "data" => $restaurantJson,
                    "message" => "OK",
                    "status_code" => 200
                ], 200);

            } else {

                return $this->json([
                    "message" => Response::$statusTexts[204],
                    "status_code" => Response::HTTP_NO_CONTENT
                ], 204);
            }

        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => 500
            ], 500);

        }
    }

    public function getRestoOrders($id){
        
        try{
            $resto = $this->restoRepo->find($id);
    
            $orders = $this->orderRepo->findBy(["resto" => $resto]);
    
            $orderJson = SerializerHelper::getJson($this->serializer, $orders, "show_order");
            
            if (!empty($orders)) {
    
                return $this->json([
                    "data" => $orderJson,
                    "message" => "OK",
                    "status_code" => 200
                ], 200);
    
            } else {
    
                return $this->json([
                    "message" => Response::$statusTexts[204],
                    "status_code" => Response::HTTP_NO_CONTENT
                ], 204);
            }

        } catch (Exception $e){

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);

        }
        
    }

    public function cancelOrder($id){
        
        try{
            $user = $this->getUser();
            $order = $this->orderRepo->findOneBy(["id"=>$id]);
            $orderJson = SerializerHelper::getJson($this->serializer, $order, "show_order");
            $checkUser = ($orderJson->user->email == $user->getEmail());    
            
            if(!empty($order) && $checkUser){
                
                $order->setOrderStatusStr(OrderStatus::CANCEL);
                $order->setIsWaiting(false);

                $this->em->persist($order);
                $this->em->flush();

                return $this->json([
                    "data"=> $orderJson,
                    "message"=> "OK",
                    "status_code"=> Response::HTTP_OK
                ], 200);
                
            }else{

                return $this->json([
                    "data"=> $orderJson,
                    "message"=> "KO",
                    "status_code"=> Response::HTTP_NOT_FOUND
                ], 404);

            }

        }catch(Exception $e){

            return $this->json([
                "message"=> $e->getMessage(),
                "status_code"=> $e->getCode()
            ], 500);

        }
    }

    public function getAllRestoForDash(){
        
        try{

            $restaurants = $this->restoRepo->findAll();
            $restoJson = SerializerHelper::getJson($this->serializer, $restaurants, "show_restaurant");

            return $this->json([
                "message"=> "OK",
                "data"=> $restoJson,
                "status_code"=> 200
            ]);

        }catch(\Exception $e){

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ], 500);

        }
        
    }

    public function activeOrDesactive($id){

        try{

            $resto = $this->restoRepo->find($id);
            
            if(empty($resto)){
                return $this->json([
                    "message"=> "restaurant with $id not found",
                    "status_code"=> 401
                ], 500);
            }
    
            $resto->setIsValidedByAdmin(!($resto->isIsValidedByAdmin()));
            $this->em->persist($resto);
            $this->em->flush();
    
            $restoJson = SerializerHelper::getJson($this->serializer, $resto, "show_order");
            
            return $this->json([
                "data"=> $restoJson,
                "message"=> "OK",
                "status_code"=> Response::HTTP_OK
            ], 200);
    
        }catch(\Exception $e){
            return $this->json([
                "message"=> $e->getMessage(),
                "status_code"=> $e->getCode()
            ], 500);
        }


    }
}
