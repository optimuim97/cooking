<?php

namespace App\Services;

use App\Entity\Budget;
use App\Helpers\ImageHelper;
use App\Helpers\Validation;
use App\Repository\BudgetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BudgetService extends AbstractController
{

    public function __construct(
        private EntityManagerInterface $em,
        private BudgetRepository $budgetRepo,
        private ValidatorInterface $validator
    )
    {}

    public function add($data)
    {

        try {

            $budget = new Budget();
            $budget->setName($data["name"]);
            $budget->setDescription($data["description"]);

            $errors = $this->validator->validate($budget);

            if (count($errors) > 0) {
                $hasError = Validation::check($errors);

                if ($hasError != null) {
                    return $this->json([
                        "errors" => $hasError,
                        "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY,
                        "message" => Response::$statusTexts['422']
                    ], 422);
                }
            }

            $this->em->persist($budget);
            $this->em->flush();

            return $this->json([
                "message" => "Created",
                "status_code" => Response::HTTP_CREATED,
                "data" => $budget
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => $e->getMessage()
            ]);
        }
    }

    public function delete($id)
    {

        try {

            $budget = $this->budgetRepo->findOneBy(["id" => $id]);

            if (!empty($budget)) {
                $this->budgetRepo->remove($budget, true);

                return $this->json([
                    "message" => "Deleted",
                    "status_code" => Response::HTTP_NO_CONTENT,
                    "data" => null
                ]);
            } else {
                return $this->json([
                    "message" => "Not Found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => $e->getMessage()
            ]);
        }
    }

    public function show($id)
    {

        try {
            $budget = $this->budgetRepo->findOneBy(["id" => $id]);

            if (!empty($budget)) {
                return $this->json([
                    "message" => "Found",
                    "status_code" => Response::HTTP_FOUND,
                    "data" => $budget
                ]);
            } else {
                return $this->json([
                    "message" => "Not found",
                    "status_code" => Response::HTTP_NOT_FOUND,
                    "data" => null
                ]);
            }
        } catch (\Exception $e) {

            return $this->json([
                "message" => $e->getMessage(),
                "status_code" => $e->getCode()
            ]);
        }
    }

    //TODO update  $recipeType
    public function update($id, $data)
    {
        $budget = $this->budgetRepo->findOneBy(["id" => $id]);

        if (!empty($budget)) {

            //TODO setValues
            return $this->json([
                "message" => "Found",
                "status_code" => Response::HTTP_FOUND,
                "data" => $budget
            ]);
        } else {
            return $this->json([
                "message" => "Not found",
                "status_code" => Response::HTTP_NOT_FOUND,
                "data" => null
            ]);
        }
    }

    public function getAll()
    {

        try {
            $budgets = $this->budgetRepo->orderByCreatedAt();

            return $this->json([
                "message" => "Accepted",
                "status_code" => Response::HTTP_ACCEPTED,
                "data" => $budgets
            ]);
        } catch (\Exception $e) {

            return $this->json([
                "status_code" => $e->getCode(),
                "message" => $e->getMessage()
            ]);
        }
    }
}
