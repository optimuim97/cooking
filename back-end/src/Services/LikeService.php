<?php

namespace App\Services;

use App\Entity\Like;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class LikeService extends AbstractController
{
    public function likeOrDislike($id, $user)
    {
        $recipe = $this->recipesRepository->find($id);

        if (!$recipe) {

            return $this->json([
                "message" => "recipe with id = $id doesn't exist!",
                "status" => Response::HTTP_NOT_FOUND,
                "data" => []
            ]);

            exit;
        }

        $recipe_likes = $recipe->getLikes();

        foreach ($recipe_likes as $like) {

            if ($like->getLiker() == $this->getUser()) {

                $this->likeRepository->remove($like, true);

                $recipe->removeLike($like);
                $this->em->flush();

                return $this->json([
                    "message" => "Disliked !",
                    "status" => Response::HTTP_NO_CONTENT,
                    "recipe" => $recipe
                ]);
            }
        }

        $like = new Like();
        $like->setLiker($user)
            ->setRecipe($recipe);

        $recipe->addLike($like);

        $this->em->persist($like);
        $this->em->flush();

        return $this->json([
            "message" => "like!",
            "status" => Response::HTTP_OK,
            "recipe" => $recipe,
            "like" => $like
        ]);
    }
}
