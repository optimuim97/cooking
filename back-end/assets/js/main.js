import Vue from "vue";
// import App from './views/App';
import AppHome from "./views/AppHome";
import Routes from "./routes";
import Vuetify from "vuetify";
import axios from "axios";
import User from "./utils/User.js";
import AppStorage from "./utils/AppStorage.js";
import Token from "./utils/Token.js";
import { Editor } from "@toast-ui/vue-editor";
import mdiVue from "mdi-vue/v2";
import * as mdijs from "@mdi/js";
import VueLoading from "vuejs-loading-plugin";
import VueMomentLocalePlugin from "vue-moment-locale";
import VueCountryCode from "vue-country-code";
import Toast from "vue-toastification";
import VModal from "vue-js-modal";
import VueMeta from "vue-meta";
import "vue-js-modal/dist/styles.css";
import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
import "vue2-dropzone/dist/vue2Dropzone.min.css";
import vue2Dropzone from "vue2-dropzone";
import vueDebounce from "vue-debounce";
import CoolLightBox from "vue-cool-lightbox";
import LazyTube from "vue-lazytube";
import VuePageTransition from "vue-page-transition";
import shareIt from "vue-share-it";
import VS2 from "vue-script2";
import { LMap, LTileLayer, LMarker } from "vue2-leaflet";
// import VueTinyLazyloadImg from "vue-tiny-lazyload-img";

import "leaflet/dist/leaflet.css";
import "leaflet-geosearch/dist/geosearch.css";
import VuePusher from "vue-pusher";

//CSS
import "leaflet/dist/leaflet.css";
import "vue-toastification/dist/index.css";
import "../../node_modules/nprogress/nprogress.css";
import "vue-multiselect/dist/vue-multiselect.min.css";
import "vue-cool-lightbox/dist/vue-cool-lightbox.min.css";

// import Vue
const options = {
  // position: "top",
  duration: 1000,
  timeout: 2000,
  closeOnClick: true,
  // pauseOnFocusLoss: false,
  //   pauseOnHover: true,
  draggable: true,
  draggablePercent: 0.33,
  // showCloseButtonOnHover: true,
  hideProgressBar: false,
  closeButton: "button",
  icon: true,
  rtl: false,
  // position: 'POSITION.TOP_RIGTH'
};

Vue.use(Toast, options);
const moment = require("moment");
require("moment/locale/fr");
Vue.use(VueMomentLocalePlugin, {
  lang: "fr",
});
// overwrite defaults
Vue.use(VueLoading, {
  text: "Chargement ....", // default 'Loading'
  background: "rgb(255,255,255)",
});

Vue.use(vueDebounce);

Vue.use(require("vue-moment"), {
  moment,
});

Vue.use(mdiVue, {
  icons: mdijs,
});

var vueDebOptions = {
  lock: false,
  listenTo: "keyup",
  defaultTime: "300ms",
  fireOnEmpty: false,
  trim: false,
};

Vue.config.productionTip = false;
Vue.use(Vuetify);
Vue.use(axios);
Vue.use(Editor);
Vue.use(VueCountryCode);
Vue.use(VModal);
Vue.use(VueMeta);
Vue.use(vue2Dropzone);
Vue.use(CoolLightBox);
Vue.use(LazyTube);
Vue.use(shareIt);
Vue.use(VS2);
// Vue.use(VueTinyLazyloadImg);

// app_id = "1125560"
// key = "97dc542e7823683daa99"
// secret = "aa76c69f4db6bbd8a3ce"
// cluster = "eu"

Vue.use(VuePusher, {
  app_id: "1125560",
  key: "97dc542e7823683daa99",
  secret: "aa76c69f4db6bbd8a3ce",
  api_key: "1125560",
  options: {
    cluster: "eu",
    encrypted: false,
  },
});

// Vue.use(vueDebounce,
//     vueDebOptions
// )
Vue.use(VueSweetalert2, { confirmButtonColor: "#D21D3A" });
Vue.use(VuePageTransition);

window.User = User;
window.Token = Token;

window.Storage = AppStorage;
window.EventBus = new Vue();

window.axios = require("axios");
const JWTToken = `Bearer ${localStorage.getItem("token")}`;

// console.log(window.axios.defaults.headers)
window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
window.axios.defaults.headers.common["Authorization"] = JWTToken;
// axios.defaults.headers.common['content-type'] = 'application/json';

localStorage.getItem("user");

// console.log(`getTokenUser ...${User.loggIn()}`)
// console.log(`getToken ...${AppStorage.getToken()}`)
// console.log(`hasToken  ...${User.hasToken()}`)
// console.log(`storedToken ... ${Token.isValid(AppStorage.getToken())}`)

// Declaration
Vue.filter("strLimit", function (value, size) {
  if (!value) return "";
  value = value.toString();

  if (value.length <= size) {
    return value;
  }
  return value.substr(0, size) + " ...";
});

Vue.filter("formatAmount", function (value) {
  if (!value) return "";
  let formatedValue = new Intl.NumberFormat().format(value);
  return formatedValue;
});

const app = new Vue({
  el: "#app",
  router: Routes,
  render: (h) => h(AppHome),
  created() {
    EventBus.$on("login_event", () => {
      this.$router.push({ name: "Login" });
    }),
      EventBus.$on("register_event", () => {
        this.$router.push({ name: "Register" });
      });

    EventBus.$on("my_account_event", () => {
      this.$router.push({ name: "Dashboard" });
    });

    EventBus.$on("deconnexion", () => {
      this.$router.push({ name: "Logout" });
    });
  }
});

export default app;
