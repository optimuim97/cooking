import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from './components/Login'
import Logout from './components/Logout'
import Register from './components/Register'
import Dashboard from './components/views/user/Dashboard.vue'
import UserProfil from './components/pages/profil/UserProfilRecipe.vue'
import TestComponent from './components/TestComponent.vue'
import Category from './components/pages/category/Category.vue'
import Accueil from "./components/pages/accueil/Accueil.vue"
import CookingTricks from './components/pages/cookingTricks/CookingTricks.vue'
import CookingTrickDetails from './components/pages/cookingTricks/CookingTrickDetails.vue'
import PostCookingTrick from './components/pages/cookingTricks/PostCookingTricks.vue'
import EditCookingTrick from './components/pages/cookingTricks/EditCookingTrick.vue'
import CookingEvent from './components/pages/cookingEvent/CookingEvent.vue'
import PostCookingEvent from './components/pages/cookingEvent/PostCookingEvent.vue'
import CookingEventDetails from './components/pages/cookingEvent/CookingEventDetails.vue'
import Home from './components/views/Home'
import RecipeDetails from './components/pages/recipe/Details.vue'
import PostRecipe from './components/pages/recipe/PostRecipe.vue'
import EditRecipe from './components/pages/recipe/EditRecipe.vue'
import Restaurants from "./components/pages/restaurant/Restaurants.vue"
import RestaurantDetails from "./components/pages/restaurant/RestaurantDetails.vue"
import PostRestoDish from "./components/pages/restaurant/dish/PostRestoDish.vue"
import EditRestoDish from "./components/pages/restaurant/dish/EditRestoDish.vue"
import CookingCourse from "./components/pages/course/Course.vue"
import EditCookingCourse from "./components/pages/course/EditCookingCourse.vue"
import CookingCourseDetails from "./components/pages/course/CourseDetails.vue"
import PostCourse from "./components/pages/course/PostCourse.vue"
import PasswordForget from "./components/PasswordForget.vue"
import addNewPassword from "./components/addNewPassword.vue"
import Lesson from './components/pages/course/_partials/Lesson.vue'

import NProgress from 'nprogress';
const BASEURL = "Abidjan.net";

const router = new VueRouter({
    // hashbang: false,
    // history: true,
    // mode: 'history',
    // mode: 'hash',
    beforeEach(toRoute, fromRoute, next) {
        window.document.title = toRoute.meta && toRoute.meta.title ? toRoute.meta.title : 'Home';
        next();
    },
    routes: [
        {
            path: '/accueil',
            name: 'Accueil',
            component: Accueil,
            meta: {
                title: "Accueil",
                page: Accueil
            }
        },
        {
            path: '/restaurants',
            name: 'Restaurants',
            component: Restaurants,
            meta: {
                title: "Restaurants",
                page: Accueil
            }
        },
        {
            path: '/restaurant/:slug',
            name: 'RestaurantDetails',
            component: RestaurantDetails,
            meta: {
                title: 'Détails-Restaurant',
                page: RestaurantDetails
            }
        },
        {
            path: '/restaurant-ajouter-plat/:id',
            name: 'PostRestoDish',
            component: PostRestoDish,
            meta: {
                title: 'Ajouter-Plat',
                page: RestaurantDetails
            }
        },
        {
            path: '/restaurant-editer-plat/:id/:dish_id',
            name: 'EditRestoDish',
            component: EditRestoDish,
            meta: {
                title: 'Editer-Plat',
                page: RestaurantDetails
            }
        },
        {
            path: '/',
            redirect: { name: 'Accueil' },
            meta: {
                title: 'Accueil',
                page: Accueil
            }
        },
        {
            path: '/astuces-culinaire',
            name: 'CookingTricks',
            component: CookingTricks,
            meta: {
                title: 'Astuces Culinaire',
                page: CookingTricks
            }
        },
        {
            path: '/publier-astuces-culinaire',
            name: 'PostCookingTrick',
            component: PostCookingTrick,
            meta: {
                title: 'Publier astuces culinaire',
                page: PostCookingTrick
            }
        },
        {
            path: '/astuce-culinaire/:slug',
            name: 'CookingTrickDetails',
            component: CookingTrickDetails,
            meta: {
                title: 'Détails  - Astuce Culinaire',
                page: CookingTrickDetails
            }
        },
        {
            path: '/editer-culinaire/:slug',
            name: 'EditCookingTrick',
            component: EditCookingTrick,
            meta: {
                title: 'Détails  - Editer Astuce Culinaire',
                page: EditCookingTrick
            }
        },
        {
            path: '/evenements-culinaire',
            name: 'CookingEvent',
            component: CookingEvent,
            meta: {
                title: 'Évènement Culinaire',
                page: CookingEvent
            }
        },
        {
            path: '/event-culinaire/:id',
            name: 'CookingEventDetails',
            component: CookingEventDetails,
            meta: {
                title: 'Détails - Évènement Culinaire',
                page: CookingEventDetails
            }
        },
        {
            path: '/ajouter-culinaire',
            name: 'PostCookingEvent',
            component: PostCookingEvent,
            meta: {
                title: 'Ajouter - Évènement Culinaire',
                page: PostCookingEvent
            }
        },
        {
            path: '/cours-de-cuisines',
            name: 'CookingCourse',
            component: CookingCourse,
            meta: {
                title: 'Cours de Cuisine',
                page: CookingCourse
            }
        },
        {
            path: '/cours-de-cuisine/:slug',
            name: 'CookingCourseDetails',
            component: CookingCourseDetails,
            meta: {
                title: 'Cours de Cuisine',
                page: CookingCourseDetails
            }
        },
        {
            path: '/editer-cours-de-cuisine/:id',
            name: 'EditCookingCourse',
            component: EditCookingCourse,
            meta: {
                title: 'Editer Cours de Cuisine',
                page: EditCookingCourse
            }
        },
        {
            path: '/ajouter-cours-cuisine',
            name: 'PostCourse',
            component: PostCourse,
            meta: {
                title: 'Ajouter Cours de Cuisine',
                page: PostCourse
            }
        },
        {
            path: '/test',
            name: 'Test',
            component: TestComponent
        },
        {
            path: '/accueil',
            name: 'Home',
            component: Home,
            meta: {
                title: '',
                page: Home
            }
        },
        {
            path: '/publier-recette',
            name: 'PostRecipe',
            component: PostRecipe,
            meta: {
                title: '',
                page: PostRecipe
            }
        },
       {
            path: '/editer-recette/:slug',
            name: 'EditRecipe',
            component: EditRecipe,
            meta: {
                title: '',
                page: EditRecipe
            }
        }, 
        {
            path: '/tableau-de-bord',
            name: 'Dashboard',
            component: Dashboard,
            alias : "/compte",
            meta: {
                title: '',
                page: Dashboard
            }
        },
        {
            path: '/se-connecter',
            name: 'Login',
            component: Login,
            meta: {
                title: '',
                page: Login
            }
        },
        {
            path: '/s-inscrire',
            name: 'Register',
            component: Register,
            meta: {
                title: '',
                page: Register
            }
        },
        {
            path: '/details-recette/:slug',
            name: 'RecipeDetails',
            component: RecipeDetails,
            meta: {
                title: '',
                page: RecipeDetails
            }
        },
        {
            path: '/categorie/:slug',
            name: 'Category',
            component: Category,
            meta: {
                title: '',
                page: Category
            }
        },
        {
            path: '/logout',
            component: Logout,
            name : "Logout",
            meta: {
                title: '',
                page: Logout
            }
        },
        {
            path: '/profil-utilisateur/:email',
            component: UserProfil,
            name : "UserProfil",
            meta: {
                title: '',
                page: UserProfil
            }
        },
        {
            path: '/mot-de-passe-oublie',
            component: PasswordForget,
            name : "PasswordForget",
            meta: {
                title: '',
                page: PasswordForget
            },
            alias : "passwordforget"
        },
        {
            path: '/creer-mot-de-passe/:id/:token',
            component: addNewPassword,
            name : "addNewPassword",
            meta: {
                title: '',
                page: addNewPassword
            },
            alias : "addnewpassword"
        },
        {
            path: '/contenu-cours',
            component: Lesson,
            name : "contenuCours",
            meta: {
                title: '',
                page: Lesson
            }
        }

    ],
    linkActiveClass: "active__link",
    scrollBehavior() {
        return { x: 0, y: 0 }
    }
})

Vue.use(VueRouter)

export default router