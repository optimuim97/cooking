<?php

namespace ContainerGxH6wm4;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getRateRestaurantControllerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\RateRestaurantController' shared autowired service.
     *
     * @return \App\Controller\RateRestaurantController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/RateRestaurantController.php';

        $container->services['App\\Controller\\RateRestaurantController'] = $instance = new \App\Controller\RateRestaurantController(($container->privates['App\\Repository\\RestaurantRepository'] ?? $container->load('getRestaurantRepositoryService')), ($container->services['doctrine.orm.default_entity_manager'] ?? $container->getDoctrine_Orm_DefaultEntityManagerService()));

        $instance->setContainer(($container->privates['.service_locator.jIxfAsi'] ?? $container->load('get_ServiceLocator_JIxfAsiService'))->withContext('App\\Controller\\RateRestaurantController', $container));

        return $instance;
    }
}
