<?php

namespace ContainerGxH6wm4;
include_once \dirname(__DIR__, 4).'/vendor/knplabs/knp-components/src/Knp/Component/Pager/PaginatorInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/knplabs/knp-components/src/Knp/Component/Pager/Paginator.php';

class PaginatorInterface_82dac15 implements \ProxyManager\Proxy\VirtualProxyInterface, \Knp\Component\Pager\PaginatorInterface
{
    /**
     * @var \Knp\Component\Pager\PaginatorInterface|null wrapped object, if the proxy is initialized
     */
    private $valueHolderf8884 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer95532 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties670cf = [
        
    ];

    public function paginate(mixed $target, int $page = 1, ?int $limit = null, array $options = []) : \Knp\Component\Pager\Pagination\PaginationInterface
    {
        $this->initializer95532 && ($this->initializer95532->__invoke($valueHolderf8884, $this, 'paginate', array('target' => $target, 'page' => $page, 'limit' => $limit, 'options' => $options), $this->initializer95532) || 1) && $this->valueHolderf8884 = $valueHolderf8884;

        if ($this->valueHolderf8884 === $returnValue = $this->valueHolderf8884->paginate($target, $page, $limit, $options)) {
            return $this;
        }

        return $returnValue;
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        $instance->initializer95532 = $initializer;

        return $instance;
    }

    public function __construct()
    {
        static $reflection;

        if (! $this->valueHolderf8884) {
            $reflection = $reflection ?? new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');
            $this->valueHolderf8884 = $reflection->newInstanceWithoutConstructor();
        }
    }

    public function & __get($name)
    {
        $this->initializer95532 && ($this->initializer95532->__invoke($valueHolderf8884, $this, '__get', ['name' => $name], $this->initializer95532) || 1) && $this->valueHolderf8884 = $valueHolderf8884;

        if (isset(self::$publicProperties670cf[$name])) {
            return $this->valueHolderf8884->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf8884;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderf8884;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer95532 && ($this->initializer95532->__invoke($valueHolderf8884, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer95532) || 1) && $this->valueHolderf8884 = $valueHolderf8884;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf8884;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderf8884;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer95532 && ($this->initializer95532->__invoke($valueHolderf8884, $this, '__isset', array('name' => $name), $this->initializer95532) || 1) && $this->valueHolderf8884 = $valueHolderf8884;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf8884;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderf8884;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer95532 && ($this->initializer95532->__invoke($valueHolderf8884, $this, '__unset', array('name' => $name), $this->initializer95532) || 1) && $this->valueHolderf8884 = $valueHolderf8884;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf8884;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderf8884;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer95532 && ($this->initializer95532->__invoke($valueHolderf8884, $this, '__clone', array(), $this->initializer95532) || 1) && $this->valueHolderf8884 = $valueHolderf8884;

        $this->valueHolderf8884 = clone $this->valueHolderf8884;
    }

    public function __sleep()
    {
        $this->initializer95532 && ($this->initializer95532->__invoke($valueHolderf8884, $this, '__sleep', array(), $this->initializer95532) || 1) && $this->valueHolderf8884 = $valueHolderf8884;

        return array('valueHolderf8884');
    }

    public function __wakeup()
    {
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer95532 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer95532;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer95532 && ($this->initializer95532->__invoke($valueHolderf8884, $this, 'initializeProxy', array(), $this->initializer95532) || 1) && $this->valueHolderf8884 = $valueHolderf8884;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderf8884;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderf8884;
    }
}

if (!\class_exists('PaginatorInterface_82dac15', false)) {
    \class_alias(__NAMESPACE__.'\\PaginatorInterface_82dac15', 'PaginatorInterface_82dac15', false);
}
