<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/api/token/refresh' => [[['_route' => 'gesdinet_jwt_refresh_token'], null, null, null, false, false, null]],
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/xdebug' => [[['_route' => '_profiler_xdebug', '_controller' => 'web_profiler.controller.profiler::xdebugAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'admin', '_controller' => 'App\\Controller\\Admin\\DashboardController::index'], null, null, null, false, false, null]],
        '/auth/sign-up' => [[['_route' => 'app_auth_user_register', '_controller' => 'App\\Controller\\Auth\\AuthUserRegisterController::signUp'], null, ['POST' => 0], null, false, false, null]],
        '/api/add-budget' => [[['_route' => 'app_add_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::add'], null, ['POST' => 0], null, false, false, null]],
        '/delete-budget' => [[['_route' => 'app_delete_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::delete'], null, ['DELETE' => 0], null, false, false, null]],
        '/show-budget' => [[['_route' => 'app_show_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::show'], null, ['POST' => 0], null, false, false, null]],
        '/update-budget' => [[['_route' => 'app_update_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::update'], null, ['POST' => 0], null, false, false, null]],
        '/get-budgets' => [[['_route' => 'app_get_budgets', '_controller' => 'App\\Controller\\Budget\\BudgetController::getAllBudget'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-cooking_event' => [[['_route' => 'add_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-cooking_events' => [[['_route' => 'cooking_events', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-cooking_trick' => [[['_route' => 'add_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-cooking_tricks' => [[['_route' => 'cooking_tricks', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-course' => [[['_route' => 'add_course', '_controller' => 'App\\Controller\\Course\\CourseController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-courses' => [[['_route' => 'get_courses', '_controller' => 'App\\Controller\\Course\\CourseController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/get-course-types' => [[['_route' => 'get_course_types', '_controller' => 'App\\Controller\\Course\\CourseController::getAllCourseType'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-lesson' => [[['_route' => 'add_course_lesson', '_controller' => 'App\\Controller\\Course\\CourseController::addCourse'], null, ['POST' => 0], null, false, false, null]],
        '/api/add-dish_type' => [[['_route' => 'app_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-dish_types' => [[['_route' => 'get_dish_types', '_controller' => 'App\\Controller\\DishType\\DishTypeController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/utils-generator' => [[['_route' => 'app_executor', '_controller' => 'App\\Controller\\ExecutorController::createUser'], null, null, null, false, false, null]],
        '/create-user-generator' => [[['_route' => 'app_executor_createusergenerator', '_controller' => 'App\\Controller\\ExecutorController::createUserGenerator'], null, ['GET' => 0], null, false, false, null]],
        '/add-username' => [[['_route' => 'app_executor_addusername', '_controller' => 'App\\Controller\\ExecutorController::addUsername'], null, ['GET' => 0], null, false, false, null]],
        '/recipes-for-abj' => [[['_route' => 'app_front_getrecipeforabj', '_controller' => 'App\\Controller\\FrontController::getRecipeForAbj'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_front_home_front', '_controller' => 'App\\Controller\\FrontController::getIndexPage'], null, null, null, false, false, null]],
        '/home' => [[['_route' => 'app_home', '_controller' => 'App\\Controller\\HomeController::index'], null, null, null, false, false, null]],
        '/api/add-level' => [[['_route' => 'app_add_level', '_controller' => 'App\\Controller\\Level\\LevelController::add'], null, ['POST' => 0], null, false, false, null]],
        '/api/update-level' => [[['_route' => 'app_update_level', '_controller' => 'App\\Controller\\Level\\LevelController::update'], null, ['PATCH' => 0], null, false, false, null]],
        '/get-levels' => [[['_route' => 'app_get_level', '_controller' => 'App\\Controller\\Level\\LevelController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/email' => [[['_route' => 'app_mailer_sendemail', '_controller' => 'App\\Controller\\MailerController::sendEmail'], null, null, null, false, false, null]],
        '/api/make-orders' => [[['_route' => 'app_order', '_controller' => 'App\\Controller\\OrderController::OrderDish'], null, ['POST' => 0], null, false, false, null]],
        '/web-hooks' => [[['_route' => 'app_order_callbackurl', '_controller' => 'App\\Controller\\OrderController::callBackUrl'], null, ['POST' => 0], null, false, false, null]],
        '/api/add-recipe' => [[['_route' => 'app_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::addRecipe'], null, ['POST' => 0], null, false, false, null]],
        '/get-recipes' => [[['_route' => 'get_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipes'], null, ['POST' => 0], null, false, false, null]],
        '/get-recipe_types/spot_light' => [[['_route' => 'get_recipes_spot_light', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeSpotLight'], null, ['GET' => 0], null, false, false, null]],
        '/api/get-recipe-bookmaker' => [[['_route' => 'app_get_recipe_book_maker', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeBookMark'], null, ['GET' => 0], null, false, false, null]],
        '/get-recipes-count' => [[['_route' => 'app_get_all_recipe_count', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeAllCount'], null, ['GET' => 0], null, false, false, null]],
        '/get-recipe-is-spotlight' => [[['_route' => 'app_get_is_spot_ligth_recipes_by_name', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getIsSpotLight'], null, ['GET' => 0], null, false, false, null]],
        '/get-all-recipes' => [[['_route' => 'app_get_all_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getAllRecipeForDash'], null, ['GET' => 0], null, false, false, null]],
        '/add-recipe_type' => [[['_route' => 'app_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-recipe_types' => [[['_route' => 'get_recipe_types', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::getAll'], null, ['POST' => 0], null, false, false, null]],
        '/get_all_recipe_types' => [[['_route' => 'get_recipe_types_all', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::getAllRT'], null, ['GET' => 0], null, false, false, null]],
        '/get-all-recipe_types' => [[['_route' => 'get_all_recipe_types_for_dash', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::getAllRecipeTypeForDash'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-restaurant' => [[['_route' => 'app_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::addRestaurant'], null, ['POST' => 0], null, false, false, null]],
        '/get-restaurant-spotlight' => [[['_route' => 'get_restaurant_spotlight', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestaurantSpotLight'], null, ['GET' => 0], null, false, false, null]],
        '/get-restaurants' => [[['_route' => 'get_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestaurant'], null, ['POST' => 0], null, false, false, null]],
        '/add-dish' => [[['_route' => 'app_add_dish', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::addDish'], null, ['POST' => 0], null, false, false, null]],
        '/get-dishes' => [[['_route' => 'app_get_dishs', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getDishes'], null, ['GET' => 0], null, false, false, null]],
        '/get-all-restaurants' => [[['_route' => 'all_resto', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getAllResto'], null, ['GET' => 0], null, false, false, null]],
        '/add-subscription_avatange' => [[['_route' => 'add_subscription_avatange', '_controller' => 'App\\Controller\\SubscriptionAvatangeController::add'], null, null, null, false, false, null]],
        '/api/add-subscription_formula' => [[['_route' => 'app_subscription_formula', '_controller' => 'App\\Controller\\SubscriptionFormulaController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-subscription_formulas' => [[['_route' => 'app_subscription_formulas', '_controller' => 'App\\Controller\\SubscriptionFormulaController::getSubscriptions'], null, ['GET' => 0], null, false, false, null]],
        '/test' => [[['_route' => 'app_test', '_controller' => 'App\\Controller\\TestController::index'], null, null, null, false, false, null]],
        '/api/get-user_datas' => [[['_route' => 'app_user', '_controller' => 'App\\Controller\\User\\UserController::index'], null, ['GET' => 0], null, false, false, null]],
        '/get-user_profil' => [[['_route' => 'app_user_data', '_controller' => 'App\\Controller\\User\\UserController::userProfil'], null, ['POST' => 0], null, false, false, null]],
        '/api/update-user-profil' => [[['_route' => 'app_update_user', '_controller' => 'App\\Controller\\User\\UserController::update'], null, ['POST' => 0], null, false, false, null]],
        '/api/get-user-recipes' => [[['_route' => 'app_user_recipes', '_controller' => 'App\\Controller\\User\\UserController::getUserRecipes'], null, ['GET' => 0], null, false, false, null]],
        '/api/get-user-liked' => [[['_route' => 'app_user_liked_recipes', '_controller' => 'App\\Controller\\User\\UserController::getUserLikedRecipes'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-cookies' => [[['_route' => 'app_user_add_cookies', '_controller' => 'App\\Controller\\User\\UserController::addCookies'], null, ['GET' => 0], null, false, false, null]],
        '/reset-password-send-mail' => [[['_route' => 'app_reset_password', '_controller' => 'App\\Controller\\User\\UserController::request'], null, ['POST' => 0], null, false, false, null]],
        '/api/login_check' => [[['_route' => 'api_login_check', '_controller' => 'App\\Controller\\Auth\\AuthUserRegisterController::getTokenUser'], null, null, null, false, false, null]],
        '/auth/refresh' => [[['_route' => 'jwt_refresh'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/api/(?'
                    .'|delete\\-(?'
                        .'|co(?'
                            .'|oking_(?'
                                .'|event/([^/]++)(*:216)'
                                .'|trick/([^/]++)(*:238)'
                            .')'
                            .'|urse/([^/]++)(*:260)'
                        .')'
                        .'|user\\-to\\-participants/([^/]++)/([^/]++)(*:309)'
                        .'|dish_type/([^/]++)(*:335)'
                    .')'
                    .'|update\\-(?'
                        .'|co(?'
                            .'|oking_trick/([^/]++)(*:380)'
                            .'|urse/([^/]++)(*:401)'
                        .')'
                        .'|dish_type/([^/]++)(*:428)'
                    .')'
                    .'|enroll\\-to\\-course/([^/]++)(*:464)'
                    .'|confirm\\-participation/([^/]++)/([^/]++)(*:512)'
                    .'|reject\\-participation/([^/]++)/([^/]++)(*:559)'
                    .'|views\\-counter/([^/]++)(*:590)'
                .')'
                .'|/s(?'
                    .'|how\\-(?'
                        .'|co(?'
                            .'|oking_(?'
                                .'|event/([^/]++)(*:640)'
                                .'|trick/([^/]++)(*:662)'
                            .')'
                            .'|urse/([^/]++)(*:684)'
                        .')'
                        .'|lesson/([^/]++)(*:708)'
                        .'|dish_type/([^/]++)(*:734)'
                    .')'
                    .'|earch\\-co(?'
                        .'|oking_trick\\-by\\-name/([^/]++)(*:785)'
                        .'|urse\\-by\\-name/([^/]++)(*:816)'
                    .')'
                .')'
                .'|/update\\-cooking_event/([^/]++)(*:857)'
                .'|/course\\-participants/([^/]++)(*:895)'
                .'|/mot\\-de\\-passe/([^/]++)(*:927)'
                .'|/views\\-counter/([^/]++)(*:959)'
                .'|/((?:[a-z0-9-].?!.*)+(?::[0-9]+).+)(?'
                    .'|(*:1005)'
                .')'
                .'|/a(?'
                    .'|pi/(?'
                        .'|delete\\-(?'
                            .'|level/([^/]++)(*:1051)'
                            .'|re(?'
                                .'|cipe(?'
                                    .'|/([^/]++)(*:1081)'
                                    .'|_type/([^/]++)(*:1104)'
                                .')'
                                .'|staurant/([^/]++)(*:1131)'
                            .')'
                            .'|comment/([^/]++)/([^/]++)(*:1166)'
                            .'|dish/([^/]++)(*:1188)'
                        .')'
                        .'|c(?'
                            .'|ancel\\-orders/([^/]++)(*:1224)'
                            .'|omment\\-recipe/([^/]++)(*:1256)'
                        .')'
                        .'|mark\\-as\\-treated/([^/]++)(*:1292)'
                        .'|rate(?'
                            .'|/([^/]++)(*:1317)'
                            .'|\\-restaurant/([^/]++)(*:1347)'
                        .')'
                        .'|update\\-(?'
                            .'|re(?'
                                .'|cipe(?'
                                    .'|/([^/]++)(*:1389)'
                                    .'|_type/([^/]++)(*:1412)'
                                .')'
                                .'|staurant/([^/]++)(*:1439)'
                            .')'
                            .'|comment/([^/]++)/([^/]++)(*:1474)'
                            .'|dish/([^/]++)(*:1496)'
                        .')'
                        .'|like\\-recipe/([^/]++)(*:1527)'
                        .'|get\\-re(?'
                            .'|cipe\\-by\\-level/([^/]++)(*:1570)'
                            .'|sto\\-orders/([^/]++)(*:1599)'
                        .')'
                        .'|add\\-to\\-recipe\\-to\\-bookmaker/([^/]++)(*:1648)'
                        .'|subscribe(?'
                            .'|/([^/]++)(*:1678)'
                            .'|\\-as\\-cooker/([^/]++)(*:1708)'
                        .')'
                    .')'
                    .'|ctive\\-or\\-desactive\\-restaurant/([^/]++)(*:1760)'
                .')'
                .'|/s(?'
                    .'|how(?'
                        .'|\\-(?'
                            .'|level/([^/]++)(*:1800)'
                            .'|re(?'
                                .'|cipe(?'
                                    .'|/([^/]++)(*:1830)'
                                    .'|_type/([^/]++)(*:1853)'
                                .')'
                                .'|staurant/([^/]++)(*:1880)'
                            .')'
                            .'|dish/([^/]++)(*:1903)'
                        .')'
                        .'|_subscription_formula/([^/]++)(*:1943)'
                    .')'
                    .'|earch\\-restaurant\\-by\\-name/([^/]++)(*:1989)'
                .')'
                .'|/get\\-(?'
                    .'|r(?'
                        .'|ate(?'
                            .'|/(?'
                                .'|rate/([^/]++)(*:2035)'
                                .'|average/([^/]++)(*:2060)'
                            .')'
                            .'|\\-restaurant/(?'
                                .'|([^/]++)(*:2094)'
                                .'|average/([^/]++)(*:2119)'
                            .')'
                        .')'
                        .'|ecipe(?'
                            .'|s\\-by\\-type/([^/]++)(*:2158)'
                            .'|\\-by\\-name/([^/]++)(*:2186)'
                        .')'
                    .')'
                    .'|dishes_by_type/([^/]++)/([^/]++)(*:2229)'
                    .'|user\\-by\\-token/([^/]++)(*:2262)'
                    .'|password\\-reset\\-page/([^/]++)/([^/]++)(*:2310)'
                .')'
                .'|/update\\-subscription_formula/([^/]++)(*:2358)'
                .'|/creer\\-mot\\-de\\-passe/([^/]++)/([^/]++)(*:2407)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        216 => [[['_route' => 'delete_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        238 => [[['_route' => 'delete_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        260 => [[['_route' => 'delete_course', '_controller' => 'App\\Controller\\Course\\CourseController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        309 => [[['_route' => 'app_course_course_deleteuser', '_controller' => 'App\\Controller\\Course\\CourseController::deleteUser'], ['id', 'userId'], ['GET' => 0], null, false, true, null]],
        335 => [[['_route' => 'delete_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        380 => [[['_route' => 'app_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::update'], ['slug'], ['POST' => 0], null, false, true, null]],
        401 => [[['_route' => 'update_course', '_controller' => 'App\\Controller\\Course\\CourseController::updateCourse'], ['id'], ['POST' => 0], null, false, true, null]],
        428 => [[['_route' => 'update_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::updateRecipeType'], ['id'], ['POST' => 0], null, false, true, null]],
        464 => [[['_route' => 'enroll_to_course', '_controller' => 'App\\Controller\\Course\\CourseController::enrollToCourse'], ['id'], ['GET' => 0], null, false, true, null]],
        512 => [[['_route' => 'app_course_course_accept', '_controller' => 'App\\Controller\\Course\\CourseController::accept'], ['id', 'userId'], ['GET' => 0], null, false, true, null]],
        559 => [[['_route' => 'app_course_course_reject', '_controller' => 'App\\Controller\\Course\\CourseController::reject'], ['id', 'userId'], ['GET' => 0], null, false, true, null]],
        590 => [[['_route' => 'views-counter-auth', '_controller' => 'App\\Controller\\FrontController::countViewAuth'], ['slug'], null, null, false, true, null]],
        640 => [[['_route' => 'show_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        662 => [[['_route' => 'show_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::show'], ['slug'], ['GET' => 0], null, false, true, null]],
        684 => [[['_route' => 'show_course', '_controller' => 'App\\Controller\\Course\\CourseController::showCourse'], ['slug'], ['GET' => 0], null, false, true, null]],
        708 => [[['_route' => 'show_course_lesson', '_controller' => 'App\\Controller\\Course\\CourseController::getLesson'], ['id'], ['GET' => 0], null, false, true, null]],
        734 => [[['_route' => 'show_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::showRecipeType'], ['id'], ['GET' => 0], null, false, true, null]],
        785 => [[['_route' => 'search_cooking_tricks', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::getCookingTrickByName'], ['keyword'], ['GET' => 0], null, false, true, null]],
        816 => [[['_route' => 'search_course_by_name', '_controller' => 'App\\Controller\\Course\\CourseController::searchCourseByName'], ['keyword'], ['GET' => 0], null, false, true, null]],
        857 => [[['_route' => 'app_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::update'], ['id'], ['PATCH' => 0], null, false, true, null]],
        895 => [[['_route' => 'show_course_participant', '_controller' => 'App\\Controller\\Course\\CourseController::getCourse'], ['id'], ['GET' => 0], null, false, true, null]],
        927 => [[['_route' => 'app_front_password', '_controller' => 'App\\Controller\\FrontController::getIndexPasswordForget'], ['path'], null, null, false, true, null]],
        959 => [[['_route' => 'views-counter', '_controller' => 'App\\Controller\\FrontController::countView'], ['slug'], null, null, false, true, null]],
        1005 => [
            [['_route' => 'app_front_getindexpage', '_controller' => 'App\\Controller\\FrontController::getIndexPage'], ['page'], null, null, false, true, null],
            [['_route' => 'app_front_getindexpage_1', '_controller' => 'App\\Controller\\FrontController::getIndexPage'], ['page'], null, null, false, true, null],
        ],
        1051 => [[['_route' => 'app_delete_level', '_controller' => 'App\\Controller\\Level\\LevelController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1081 => [[['_route' => 'delete_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1104 => [[['_route' => 'delete_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1131 => [[['_route' => 'delete_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::deleteRestaurant'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1166 => [[['_route' => 'app_delete_comment_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::deleteCommentRecipe'], ['recipe_id', 'comment_id'], ['PATCH' => 0], null, false, true, null]],
        1188 => [[['_route' => 'app_delete_dish', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::deleteDish'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1224 => [[['_route' => 'app_order_cancelorder', '_controller' => 'App\\Controller\\OrderController::cancelOrder'], ['id'], null, null, false, true, null]],
        1256 => [[['_route' => 'app_comment_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::commentRecipe'], ['slug'], ['POST' => 0], null, false, true, null]],
        1292 => [[['_route' => 'app_order_markastreated', '_controller' => 'App\\Controller\\OrderController::markAsTreated'], ['id'], ['GET' => 0], null, false, true, null]],
        1317 => [[['_route' => 'app_rate', '_controller' => 'App\\Controller\\RateController::rate'], ['id'], ['POST' => 0], null, false, true, null]],
        1347 => [[['_route' => 'app_rate_restaurant', '_controller' => 'App\\Controller\\RateRestaurantController::rate'], ['id'], ['POST' => 0], null, false, true, null]],
        1389 => [[['_route' => 'update_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::updateRecipe'], ['id'], ['POST' => 0], null, false, true, null]],
        1412 => [[['_route' => 'update_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::updateRecipeType'], ['id'], ['POST' => 0], null, false, true, null]],
        1439 => [[['_route' => 'update_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::updateRestaurant'], ['id'], ['POST' => 0], null, false, true, null]],
        1474 => [[['_route' => 'app_update_comment_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::updateCommentRecipe'], ['recipe_id', 'comment_id'], ['POST' => 0], null, false, true, null]],
        1496 => [[['_route' => 'app_get_dish_update', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::updateDish'], ['id'], ['POST' => 0], null, false, true, null]],
        1527 => [[['_route' => 'app_like_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::likeRecipe'], ['id'], ['GET' => 0], null, false, true, null]],
        1570 => [[['_route' => 'app_get_recipes_by_level', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeByLevel'], ['id'], ['GET' => 0], null, false, true, null]],
        1599 => [[['_route' => 'app_restaurant_restaurant_getrestoorders', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestoOrders'], ['id'], null, null, false, true, null]],
        1648 => [[['_route' => 'app_add_recipe_to_book_maker', '_controller' => 'App\\Controller\\Recipe\\RecipeController::addRecipeToBookMaker'], ['id'], ['GET' => 0], null, false, true, null]],
        1678 => [[['_route' => 'app_subscription_subscribe', '_controller' => 'App\\Controller\\SubscriptionController::subscribe'], ['id'], ['POST' => 0], null, false, true, null]],
        1708 => [[['_route' => 'app_subscription', '_controller' => 'App\\Controller\\SubscriptionController::subscribeAsCooker'], ['id'], ['GET' => 0], null, false, true, null]],
        1760 => [[['_route' => 'change_resto_status', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::activeOrDesactive'], ['id'], ['GET' => 0], null, false, true, null]],
        1800 => [[['_route' => 'app_show_level', '_controller' => 'App\\Controller\\Level\\LevelController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        1830 => [[['_route' => 'show_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::showRecipe'], ['slug'], ['GET' => 0], null, false, true, null]],
        1853 => [[['_route' => 'show_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::showRecipeType'], ['slug'], ['GET' => 0], null, false, true, null]],
        1880 => [[['_route' => 'show_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::showRestaurant'], ['slug'], ['GET' => 0], null, false, true, null]],
        1903 => [[['_route' => 'app_get_dish_show', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::showDish'], ['id'], ['GET' => 0], null, false, true, null]],
        1943 => [[['_route' => 'show_subscription_formula', '_controller' => 'App\\Controller\\SubscriptionFormulaController::getSubscriptionFormula'], ['slug'], ['GET' => 0], null, false, true, null]],
        1989 => [[['_route' => 'app_restaurant_restaurant_getrestaurantbyname', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestaurantByName'], ['keyword'], null, null, false, true, null]],
        2035 => [[['_route' => 'app_get_rate', '_controller' => 'App\\Controller\\RateController::getRecipeRate'], ['recipeID'], ['GET' => 0], null, false, true, null]],
        2060 => [[['_route' => 'app_get_rate_average', '_controller' => 'App\\Controller\\RateController::getRecipeAverage'], ['recipeID'], ['GET' => 0], null, false, true, null]],
        2094 => [[['_route' => 'app_get_rate_restaurant', '_controller' => 'App\\Controller\\RateRestaurantController::getRestaurantRate'], ['restaurantID'], ['GET' => 0], null, false, true, null]],
        2119 => [[['_route' => 'app_get_rate_restaurant_average', '_controller' => 'App\\Controller\\RateRestaurantController::getRestaurantAverage'], ['restaurantID'], ['GET' => 0], null, false, true, null]],
        2158 => [[['_route' => 'get_by_cat_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipesByType'], ['slug'], ['POST' => 0], null, false, true, null]],
        2186 => [[['_route' => 'app_get_recipes_by_name', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeByName'], ['keyword'], ['GET' => 0], null, false, true, null]],
        2229 => [[['_route' => 'app_get_dishes_by_types', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getDishByType'], ['id', 'resto_id'], ['GET' => 0], null, false, true, null]],
        2262 => [[['_route' => 'app_user_by_token', '_controller' => 'App\\Controller\\User\\UserController::getUserByToken'], ['token'], ['GET' => 0], null, false, true, null]],
        2310 => [[['_route' => 'pwd_reset_page', '_controller' => 'App\\Controller\\User\\UserController::resetting'], ['id', 'token'], null, null, false, true, null]],
        2358 => [[['_route' => 'update_subscription_formula', '_controller' => 'App\\Controller\\SubscriptionFormulaController::updateSubscriptionFormula'], ['id'], ['PUT' => 0], null, false, true, null]],
        2407 => [
            [['_route' => 'create_new_password', '_controller' => 'App\\Controller\\User\\UserController::addNewPassword'], ['id', 'token'], ['POST' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
