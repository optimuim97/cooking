<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* FrontOffice/base-front.html copy.twig */
class __TwigTemplate_c33a75a8267056bc18054dff7ce2a9b5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "FrontOffice/base-front.html copy.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "FrontOffice/base-front.html copy.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
  <head>
    <title>
      ";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        // line 6
        echo "    </title>
    <meta charset=\"utf-8\">
    ";
        // line 9
        echo "    <meta name=\"apple-mobile-web-app-capable\" content=\"yes\" />
    <meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\" />
    <link href=\"_themes/assets/css/theme.css\" rel=\"stylesheet\" />
    <link href=\"assets/css/custom.css\" rel=\"stylesheet\" />
    <link href=\"assets/css/font-awesome.min.css\" rel=\"stylesheet\" />
    <link rel=\"stylesheet\" href=\"assets/css/main.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/scd-main.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/main-devices.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/main-devices.css\">

    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css\" rel=\"stylesheet\" />
    <link rel=\"stylesheet\" href=\"https://pro.fontawesome.com/releases/v5.10.0/css/all.css\" integrity=\"sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p\" crossorigin=\"anonymous\" />
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css\" />
    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v6.2.0/css/all.css\" />
    <link src=\"http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" src=\"assets/font-awesome-4.7.0/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css\" integrity=\"sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==\" crossorigin=\"anonymous\" />

    ";
        // line 32
        echo "
    ";
        // line 33
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 36
        echo "
    ";
        // line 37
        $this->displayBlock('javascripts', $context, $blocks);
        // line 259
        echo "
  </head>
  <body>
    ";
        // line 262
        echo twig_include($this->env, $context, "FrontOffice/partials/navbar.html.twig");
        echo "
    ";
        // line 263
        $this->displayBlock('body', $context, $blocks);
        // line 264
        echo "    ";
        echo twig_include($this->env, $context, "FrontOffice/partials/footer.html.twig");
        echo "
  </body>
</html>
";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 33
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 34
        echo "      ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("app");
        echo "
    ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 37
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 38
        echo "      ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("app");
        echo "
      <script src=\"_themes/vendors/is/is.min.js\"></script>
      <script src=\"https://polyfill.io/v3/polyfill.min.js?features= window.scroll\"></script>
      <script src=\"_themes/vendors/fontawesome/all.min.js\"></script>
      <script src=\"_themes/assets/js/theme.js\"></script>
      ";
        // line 44
        echo "      <script>  
            document.addEventListener('DOMContentLoaded', function() {

            //Login Register
            const loginButton  = document.getElementById(\"seconnecter_btn\")
            const registerButton = document.getElementById(\"creercompte_btn\")
  
            loginButton.addEventListener(\"click\", (e)=>{                
              EventBus.\$emit(\"login_event\")
            })
          
            registerButton.addEventListener(\"click\", (e)=>{
              EventBus.\$emit(\"register_event\")
            })

            const myAccount = document.getElementById(\"moncompte_btn\")
            const deconnexion = document.getElementById(\"deconnexion_btn\")

            myAccount.addEventListener(\"click\", ()=>{
                EventBus.\$emit(\"my_account_event\")
            })

            deconnexion.addEventListener(\"click\", ()=>{
                EventBus.\$emit(\"deconnexion\")
            })

            var header = document.querySelector('header')
            var menu = document.querySelector('header nav.menu')

            EventBus.\$on(\"check_nav\", ()=>{
              checkNav()
            })

            checkNav()
            scroll()
            photos()
            search()
            menuxs()
        
            /*\$(\"a\").each(function () {
                if (this.href.indexOf('https://beta-business.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-business.abidjan.net', 'https://business.abidjan.net');
                } else if (this.href.indexOf('https://beta-news.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-news.abidjan.net', 'https://news.abidjan.net');
                }else if (this.href.indexOf('https://beta.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta.abidjan.net', 'https://www.abidjan.net');
                }else if (this.href.indexOf('https://beta-necrologie.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-necrologie.abidjan.net', 'https://necrologie.abidjan.net');
                }else if (this.href.indexOf('https://beta-sports.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-sports.abidjan.net', 'https://sports.abidjan.net');
                }
            });
            \$(\"link\").each(function () {
                if (this.href.indexOf('/public') != -1) {
                    this.href = this.href.replace('/public', 'https://beta-news.abidjan.net/public');
                }
            });*/
        
            // Search
            // _____________________________
        
            function search() {
        
              searchBtn = document.getElementById('btnSearch')
              searchBloc = document.getElementById('blocSearch')
        
              searchBtn.addEventListener('click', function(e) {
        
                e.preventDefault()
        
                if (searchBtn.classList.contains('on')) {
        
                  searchBloc.classList.remove('show')
                  searchBtn.classList.remove('on')
        
                } else {
        
                  searchBloc.classList.add('show')
                  searchBtn.classList.add('on')
        
                  searchBloc.querySelector('input').focus()
        
                }
        
              })
        
            }

            function checkNav(){
              const moncompte = document.getElementById(\"moncompte\")
              const deconnexion = document.getElementById(\"deconnexion\")

              const loginLi  = document.getElementById(\"seconnecter\")
              const registerLi = document.getElementById(\"creercompte\")
              
              if(User.loggIn()){

                moncompte.style.display=\"block\";
                deconnexion.style.display=\"block\";

              
                loginLi.style.display=\"none\";
                registerLi.style.display=\"none\";

              }else{

                moncompte.style.display=\"none\";
                deconnexion.style.display=\"none\";

              }
            }

            // Menu Mobile
            // _____________________________
        
            function menuxs() {
        
              menuburger = document.querySelector('.menu-xs')
        
              menuburger.addEventListener('click', function() {
        
                if (menuburger.classList.contains('on')) {
        
                  menu.classList.remove('show')
                  menuburger.classList.remove('on')
        
                } else {
        
                  menu.classList.add('show')
                  menuburger.classList.add('on')
        
                }
        
              })
        
              if(isXS()||isSM()) {
        
                var links = document.querySelectorAll('header nav.menu span > a')
                Array.prototype.forEach.call(links, function(link, i) {
                  // console.log(i)
                  link.addEventListener('click', function(e){
                    e.preventDefault()
                    let parent = link.parentNode
                    if (parent.classList.contains('on')) {
                      parent.classList.remove('on')
                    } else {
                      parent.classList.add('on')
                    }
                  })
                })
        
              }
        
            }

            // On affiche le mini-header au scroll
            // ________________________________________
        
            function scroll() {
              document.body.onscroll = function() {
        
                st = window.pageYOffset | document.body.scrollTop
        
                mini = (st>50) ? header.classList.add('mini') : header.classList.remove('mini')
        
              }
            }
        
            // Affichage de toutes les photos (page Article)
            // ____________________________________________________
        
            function photos() {
        
              if (document.getElementById('allPhotos')) {
        
                btn = document.getElementById('btnPhotos')
                all = document.getElementById('allPhotos')
        
                btn.addEventListener('click', function(e) {
        
                  e.preventDefault()
                  all.classList.add('show')
        
                })
        
                all.addEventListener('click', function(e) {
        
                  all.classList.remove('show')
        
                })
        
              }
        
            }
        
            // Fonctions responsive
            // ____________________________________________________
        
            function isXS() {
        
              element = document.getElementById('chck-xs')
              retour =  element.currentStyle ? element.currentStyle.display : getComputedStyle(element, null).display
              return ( retour == 'block' )
        
            }
        
            function isSM() {
              element = document.getElementById('chck-sm')
              retour =  element.currentStyle ? element.currentStyle.display : getComputedStyle(element, null).display
              return ( retour == 'block' )
            }
          })

      </script>
    ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 263
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "FrontOffice/base-front.html copy.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  399 => 263,  175 => 44,  166 => 38,  156 => 37,  143 => 34,  133 => 33,  115 => 5,  100 => 264,  98 => 263,  94 => 262,  89 => 259,  87 => 37,  84 => 36,  82 => 33,  79 => 32,  59 => 9,  55 => 6,  53 => 5,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
  <head>
    <title>
      {% block title %}{% endblock %}
    </title>
    <meta charset=\"utf-8\">
    {# <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0\" /> #}
    <meta name=\"apple-mobile-web-app-capable\" content=\"yes\" />
    <meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\" />
    <link href=\"_themes/assets/css/theme.css\" rel=\"stylesheet\" />
    <link href=\"assets/css/custom.css\" rel=\"stylesheet\" />
    <link href=\"assets/css/font-awesome.min.css\" rel=\"stylesheet\" />
    <link rel=\"stylesheet\" href=\"assets/css/main.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/scd-main.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/main-devices.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/main-devices.css\">

    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css\" rel=\"stylesheet\" />
    <link rel=\"stylesheet\" href=\"https://pro.fontawesome.com/releases/v5.10.0/css/all.css\" integrity=\"sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p\" crossorigin=\"anonymous\" />
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css\" />
    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v6.2.0/css/all.css\" />
    <link src=\"http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" src=\"assets/font-awesome-4.7.0/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css\" integrity=\"sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==\" crossorigin=\"anonymous\" />

    {# 
      <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\"
        integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css\"> 
    #}

    {% block stylesheets %}
      {{ encore_entry_link_tags('app') }}
    {% endblock %}

    {% block javascripts %}
      {{ encore_entry_script_tags('app') }}
      <script src=\"_themes/vendors/is/is.min.js\"></script>
      <script src=\"https://polyfill.io/v3/polyfill.min.js?features= window.scroll\"></script>
      <script src=\"_themes/vendors/fontawesome/all.min.js\"></script>
      <script src=\"_themes/assets/js/theme.js\"></script>
      {# <script src=\"https://media-files.abidjan.net/public/js/ui.js\"></script> #}
      <script>  
            document.addEventListener('DOMContentLoaded', function() {

            //Login Register
            const loginButton  = document.getElementById(\"seconnecter_btn\")
            const registerButton = document.getElementById(\"creercompte_btn\")
  
            loginButton.addEventListener(\"click\", (e)=>{                
              EventBus.\$emit(\"login_event\")
            })
          
            registerButton.addEventListener(\"click\", (e)=>{
              EventBus.\$emit(\"register_event\")
            })

            const myAccount = document.getElementById(\"moncompte_btn\")
            const deconnexion = document.getElementById(\"deconnexion_btn\")

            myAccount.addEventListener(\"click\", ()=>{
                EventBus.\$emit(\"my_account_event\")
            })

            deconnexion.addEventListener(\"click\", ()=>{
                EventBus.\$emit(\"deconnexion\")
            })

            var header = document.querySelector('header')
            var menu = document.querySelector('header nav.menu')

            EventBus.\$on(\"check_nav\", ()=>{
              checkNav()
            })

            checkNav()
            scroll()
            photos()
            search()
            menuxs()
        
            /*\$(\"a\").each(function () {
                if (this.href.indexOf('https://beta-business.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-business.abidjan.net', 'https://business.abidjan.net');
                } else if (this.href.indexOf('https://beta-news.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-news.abidjan.net', 'https://news.abidjan.net');
                }else if (this.href.indexOf('https://beta.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta.abidjan.net', 'https://www.abidjan.net');
                }else if (this.href.indexOf('https://beta-necrologie.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-necrologie.abidjan.net', 'https://necrologie.abidjan.net');
                }else if (this.href.indexOf('https://beta-sports.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-sports.abidjan.net', 'https://sports.abidjan.net');
                }
            });
            \$(\"link\").each(function () {
                if (this.href.indexOf('/public') != -1) {
                    this.href = this.href.replace('/public', 'https://beta-news.abidjan.net/public');
                }
            });*/
        
            // Search
            // _____________________________
        
            function search() {
        
              searchBtn = document.getElementById('btnSearch')
              searchBloc = document.getElementById('blocSearch')
        
              searchBtn.addEventListener('click', function(e) {
        
                e.preventDefault()
        
                if (searchBtn.classList.contains('on')) {
        
                  searchBloc.classList.remove('show')
                  searchBtn.classList.remove('on')
        
                } else {
        
                  searchBloc.classList.add('show')
                  searchBtn.classList.add('on')
        
                  searchBloc.querySelector('input').focus()
        
                }
        
              })
        
            }

            function checkNav(){
              const moncompte = document.getElementById(\"moncompte\")
              const deconnexion = document.getElementById(\"deconnexion\")

              const loginLi  = document.getElementById(\"seconnecter\")
              const registerLi = document.getElementById(\"creercompte\")
              
              if(User.loggIn()){

                moncompte.style.display=\"block\";
                deconnexion.style.display=\"block\";

              
                loginLi.style.display=\"none\";
                registerLi.style.display=\"none\";

              }else{

                moncompte.style.display=\"none\";
                deconnexion.style.display=\"none\";

              }
            }

            // Menu Mobile
            // _____________________________
        
            function menuxs() {
        
              menuburger = document.querySelector('.menu-xs')
        
              menuburger.addEventListener('click', function() {
        
                if (menuburger.classList.contains('on')) {
        
                  menu.classList.remove('show')
                  menuburger.classList.remove('on')
        
                } else {
        
                  menu.classList.add('show')
                  menuburger.classList.add('on')
        
                }
        
              })
        
              if(isXS()||isSM()) {
        
                var links = document.querySelectorAll('header nav.menu span > a')
                Array.prototype.forEach.call(links, function(link, i) {
                  // console.log(i)
                  link.addEventListener('click', function(e){
                    e.preventDefault()
                    let parent = link.parentNode
                    if (parent.classList.contains('on')) {
                      parent.classList.remove('on')
                    } else {
                      parent.classList.add('on')
                    }
                  })
                })
        
              }
        
            }

            // On affiche le mini-header au scroll
            // ________________________________________
        
            function scroll() {
              document.body.onscroll = function() {
        
                st = window.pageYOffset | document.body.scrollTop
        
                mini = (st>50) ? header.classList.add('mini') : header.classList.remove('mini')
        
              }
            }
        
            // Affichage de toutes les photos (page Article)
            // ____________________________________________________
        
            function photos() {
        
              if (document.getElementById('allPhotos')) {
        
                btn = document.getElementById('btnPhotos')
                all = document.getElementById('allPhotos')
        
                btn.addEventListener('click', function(e) {
        
                  e.preventDefault()
                  all.classList.add('show')
        
                })
        
                all.addEventListener('click', function(e) {
        
                  all.classList.remove('show')
        
                })
        
              }
        
            }
        
            // Fonctions responsive
            // ____________________________________________________
        
            function isXS() {
        
              element = document.getElementById('chck-xs')
              retour =  element.currentStyle ? element.currentStyle.display : getComputedStyle(element, null).display
              return ( retour == 'block' )
        
            }
        
            function isSM() {
              element = document.getElementById('chck-sm')
              retour =  element.currentStyle ? element.currentStyle.display : getComputedStyle(element, null).display
              return ( retour == 'block' )
            }
          })

      </script>
    {% endblock %}

  </head>
  <body>
    {{ include('FrontOffice/partials/navbar.html.twig') }}
    {% block body %}{% endblock %}
    {{ include('FrontOffice/partials/footer.html.twig') }}
  </body>
</html>
", "FrontOffice/base-front.html copy.twig", "/Users/ouattarasidik/Documents/forDeploy/experience/cooking/back-end/templates/FrontOffice/base-front.html copy.twig");
    }
}
