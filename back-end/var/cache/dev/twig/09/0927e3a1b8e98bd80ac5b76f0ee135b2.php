<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mails/commande.html.twig */
class __TwigTemplate_f5ec47a4f0133b2df0b663e9f3bca1f5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "mails/commande.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "mails/commande.html.twig"));

        // line 1
        echo "
<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"UTF-8\" />
    <title>Notification Abidjan.net</title>
  </head>

  <body>
    <table
      style=\"
        width: 100%;
        border-collapse: collapse;
        background-color: #f7f9fa;
        background-image: url();
        font-family: 'Lato', Arial, Helvetica, sans-serif;
        font-size: 14px;
        margin: 0;
        padding: 0;
        color: #5e656b;
      \"
      cellpadding=\"0\"
      cellspacing=\"0\"
    >
      <tbody>
        <tr>
          <td align=\"center\" style=\"word-break: break-word\">
            <table
              style=\"
                width: 100%;
                max-width: 600px;
                border-collapse: collapse;
                border-spacing: 0;
                margin: 0 auto;
                background-color: #fff;
                margin-top: 20px;
              \"
              id=\"m_665443110472732713centerImageb888ce93-e9d6-42a1-bdd8-0133c0aca46d\"
            >
              <tbody>
                <tr>
                  <td
                    width=\"156\"
                    align=\"center\"
                    class=\"m_665443110472732713centerImg\"
                    style=\"padding: 30px\"
                  >
                    <div
                      id=\"m_665443110472732713centerImgb888ce93-e9d6-42a1-bdd8-0133c0aca46d\"
                    >
                      <a href=\"https://www.Abidjan.net.com/\" target=\"_blank\"
                        ><img
                          border=\"0\"
                          src=\"https://media-files.abidjan.net/logos/logo.svg\"
                          width=\"230\"
                          height=\"\"
                          alt=\"LastPass\"
                          style=\"
                            font-family: Helvetica, Arial, sans-serif;
                            color: #5e656b;
                            font-size: 16px;
                          \"
                          class=\"CToWUd\"
                      /></a>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            <table
              style=\"
                width: 600px;
                border-collapse: collapse;
                border-spacing: 0;
                margin: 0 auto;
                background-color: #ffffff;
              \"
              id=\"m_665443110472732713banner45760820-29c6-406a-931e-a5164c94a9f6\"
            >
              <tbody>
                <tr>
                  <td style=\"text-align: center\">
                    <a href=\"https://www.Abidjan.net.com/\" target=\"_blank\"
                      ><img
                        src=\"https://business.abidjan.net/public/img/cover-searchPJ.jpg\"
                        alt=\"\"
                        style=\"width: 60%; height: auto; margin: 10px auto\"
                    /></a>
                    <div
                      class=\"a6S\"
                      dir=\"ltr\"
                      style=\"opacity: 0.01; left: 712px; top: 273px\"
                    >
                      <div
                        id=\":57\"
                        class=\"T-I J-J5-Ji aQv T-I-ax7 L3 a5q\"
                        role=\"button\"
                        tabindex=\"0\"
                        aria-label=\"Télécharger la pièce jointe \"
                        data-tooltip-class=\"a1V\"
                        data-tooltip=\"Télécharger\"
                      >
                        <div class=\"aSK J-J5-Ji aYr\"></div>
                      </div>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            <table
              style=\"
                width: 600px;
                border-collapse: collapse;
                border-spacing: 0;
                margin: 0 auto;
                padding: 0;
                background-color: #ffffff;
                background-image: url();
              \"
              cellpadding=\"0\"
              cellspacing=\"0\"
            >
              <tbody>
                <tr>
                  <td
                    id=\"m_665443110472732713trBodyText29c88a0d-a584-44bc-9ea8-d95d9846fad887d5a783-401b-4602-b06f-5399a9cde7e8\"
                  >
                    <table style=\"width: 100%; border-collapse: collapse\">
                      <tbody>
                        <tr>
                          <td style=\"padding: 15px\" align=\"center\">
                            <table
                              style=\"
                                border-collapse: collapse;
                                border-spacing: 0;
                                color: #5e656b;
                              \"
                            >
                              <tbody>
                                <tr>
                                  <td
                                    align=\"left\"
                                    style=\"
                                      font-size: 26px;
                                      line-height: 30px;
                                      color: #1f2a34;
                                    \"
                                  >
                                    VOTRE COMMANDE 
                                  </td>
                                </tr>
                                <tr>
                                  <td
                                    style=\"
                                      padding: 30px 0 0 0;
                                      display: block;
                                      font-size: 16px;
                                      line-height: 24px;
                                    \"
                                    class=\"m_665443110472732713text\"
                                  >
                                    <p>
                                        Bonjour 
                                        ";
        // line 164
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 164, $this->source); })()), "nom", [], "any", false, false, false, 164), "html", null, true);
        echo "
                                        ";
        // line 165
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 165, $this->source); })()), "prenoms", [], "any", false, false, false, 165), "html", null, true);
        echo "
                                        ,vous venez d'effectuer une commande.
                                      <br />

                                        <div class=\"d-flex justify-content-center\">
                                            ";
        // line 170
        if (((isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 170, $this->source); })()) && array_key_exists("order", $context))) {
            // line 171
            echo "                                                <div class=\"title_part\">
                                                    <div class=\"text-center title_part\">
                                                        <div class=\"d-flex justify-content-start\">
                                                            <h2 class=\"titre mb-2\">
                                                                Commande
                                                            </h2>
                                                        </div>
                                                        <div class=\"d-flex justify-content-start\">
                                                            <p>
                                                                <strong>
                                                                    Reference 
                                                                </strong>
                                                                : # ";
            // line 183
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 183, $this->source); })()), "reference", [], "any", false, false, false, 183), "html", null, true);
            echo "
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                            
                                                <div style=\"display:flex; justify-content:center;\">

                                                    <div class=\"card_item\">
                                                        <div class=\"d-flex align-center\">
                                                            <p>
                                                                Montant Total : 
                                                                <span class=\"mx-2\" style=\"
                                                                    color: #D21D3A !important;
                                                                    font-size: 14px;
                                                                    font-weight: bold !important;
                                                                \">
                                                                    ";
            // line 200
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 200, $this->source); })()), "amount", [], "any", false, false, false, 200), "html", null, true);
            echo "
                                                                    ";
            // line 201
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 201, $this->source); })()), "currency_name", [], "any", false, false, false, 201), "html", null, true);
            echo "
                                                                </span>
                                                            </p>
                                                        </div>
                            
                                                        <div class=\"d-flex\">
                                                            Date: ";
            // line 207
            echo twig_escape_filter($this->env, $this->extensions['Twig\Extra\Intl\IntlExtension']->formatDateTime($this->env, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 207, $this->source); })()), "createdAt", [], "any", false, false, false, 207), "medium", "medium", "EEEE dd MMMM YYYY", null, "gregorian", "fr"), "html", null, true);
            echo "
                                                        </div>

                                                        <div class=\"d-flex\">
                                                            Heure:
                                                            <span class=\"mx-2\" style=\"
                                                                  color: #D21D3A !important;
                                                                  font-size: 14px;
                                                                  font-weight: bold !important;
                                                                \">
                                                                ";
            // line 217
            echo twig_escape_filter($this->env, $this->extensions['Twig\Extra\Intl\IntlExtension']->formatDateTime($this->env, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 217, $this->source); })()), "createdAt", [], "any", false, false, false, 217), "medium", "medium", "hh:mm:ss", null, "gregorian", "fr"), "html", null, true);
            echo "
                                                            </span>
                                                        </div>
                            
                                                        <div class=\"d-flex\">
                                                            Payé ? :
                                                            <span class=\"mx-2\" style=\"
                                                                  color: #D21D3A !important;
                                                                  font-size: 14px;
                                                                  font-weight: bold !important;
                                                                \">
                                                                ";
            // line 228
            echo (((twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 228, $this->source); })()), "is_paid", [], "any", false, false, false, 228) == true)) ? ("OUI") : ("NON"));
            echo "
                                                            </span>
                                                        </div>

                                                    </div>
                                                </div>
                                            ";
        }
        // line 235
        echo "                                        </div>
                                    </p>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
            <table
              style=\"
                display: table;
                width: 600px;
                border-collapse: collapse;
                border-spacing: 0;
                margin: 0 auto;
                padding: 0;
                background-color: #ffffff;
                background-image: url();
              \"
              id=\"m_665443110472732713button292c00ed-548b-4951-8d25-95240b2e01d9\"
              cellpadding=\"0\"
              cellspacing=\"0\"
            >
              <tbody>
                <tr>
                  <td
                    style=\"padding: 15px 30px 60px 30px\"
                    id=\"m_665443110472732713trButton292c00ed-548b-4951-8d25-95240b2e01d9\"
                  >
                    <center>
                      <table
                        class=\"m_665443110472732713button\"
                        style=\"border-radius: 30px; background-color: #d21d3a\"
                      >
                        <tbody>
                          <tr>
                            <td style=\"text-align: center\">
                              <a
                                style=\"
                                  display: block;
                                  color: #fff;
                                  font-size: 16px;
                                  text-decoration: none;
                                  padding: 15px 30px;
                                  font-weight: bold;
                                \"
                                href=\"https://www.abidjan.net/\"
                                target=\"_blank\"
                                >Lien de page</a
                              >
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </center>
                  </td>
                </tr>
              </tbody>
            </table>
            <table
              style=\"
                width: 600px;
                border-collapse: collapse;
                border-spacing: 0;
                margin: 0 auto;
                padding: 0;
                background-color: #ecf5fb;
                background-image: url();
              \"
              id=\"m_665443110472732713bodyText29c88a0d-a584-44bc-9ea8-d95d9846fad887d5a783-401b-4602-b06f-5399a9cde7e81ccaeb83-9bb9-4ea7-840b-4de0c755015bc2c2f9a9-8147-489b-b21a-1b5a405febe6\"
              cellpadding=\"0\"
              cellspacing=\"0\"
            >
              <tbody>
                <tr>
                  <td
                    id=\"m_665443110472732713trBodyText29c88a0d-a584-44bc-9ea8-d95d9846fad887d5a783-401b-4602-b06f-5399a9cde7e81ccaeb83-9bb9-4ea7-840b-4de0c755015bc2c2f9a9-8147-489b-b21a-1b5a405febe6\"
                  >
                    <table
                      style=\"
                        width: 100%;
                        border-collapse: collapse;
                        background-color: #fff;
                      \"
                    >
                      <tbody>
                        <tr>
                          <td style=\"padding: 10px\" align=\"center\"></td>
                        </tr>
                        <tr>
                          <td style=\"padding: 5px\" align=\"center\">
                            <table
                              style=\"
                                border-collapse: collapse;
                                border-spacing: 0;
                                color: #5e656b;
                              \"
                            >
                              <tbody>
                                <tr>
                                  <td
                                    align=\"center\"
                                    style=\"
                                      font-size: 18px;
                                      line-height: 30px;
                                      color: #1f2a34;
                                    \"
                                  ></td>
                                </tr>
                                <tr>
                                  <td
                                    style=\"
                                      padding: 5px 0 0 0;
                                      display: block;
                                      font-size: 16px;
                                      line-height: 24px;
                                    \"
                                    class=\"m_665443110472732713text\"
                                  >
                                    <br />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td style=\"height: 8px; background-color: #ffb800\">&nbsp;</td>
        </tr>
        <tr>
          <td style=\"padding: 20px; background-color: #1e1e1e\">
            <table
              align=\"center\"
              style=\"
                width: 600px;
                border-collapse: collapse;
                color: #ffffff;
                font-size: 11px;
                line-height: 18px;
              \"
            >
              <tbody>
                <tr>
                  <td
                    align=\"center\"
                    style=\"
                      font-size: 11px;
                      line-height: 18px;
                      font-family: Arial, Helvetica, sans-serif;
                      color: #ffffff;
                    \"
                  >
                    Copyright © 1998-2023 Weblogy Group Ltd. Tous droits
                    réservés
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "mails/commande.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  306 => 235,  296 => 228,  282 => 217,  269 => 207,  260 => 201,  256 => 200,  236 => 183,  222 => 171,  220 => 170,  212 => 165,  208 => 164,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"UTF-8\" />
    <title>Notification Abidjan.net</title>
  </head>

  <body>
    <table
      style=\"
        width: 100%;
        border-collapse: collapse;
        background-color: #f7f9fa;
        background-image: url();
        font-family: 'Lato', Arial, Helvetica, sans-serif;
        font-size: 14px;
        margin: 0;
        padding: 0;
        color: #5e656b;
      \"
      cellpadding=\"0\"
      cellspacing=\"0\"
    >
      <tbody>
        <tr>
          <td align=\"center\" style=\"word-break: break-word\">
            <table
              style=\"
                width: 100%;
                max-width: 600px;
                border-collapse: collapse;
                border-spacing: 0;
                margin: 0 auto;
                background-color: #fff;
                margin-top: 20px;
              \"
              id=\"m_665443110472732713centerImageb888ce93-e9d6-42a1-bdd8-0133c0aca46d\"
            >
              <tbody>
                <tr>
                  <td
                    width=\"156\"
                    align=\"center\"
                    class=\"m_665443110472732713centerImg\"
                    style=\"padding: 30px\"
                  >
                    <div
                      id=\"m_665443110472732713centerImgb888ce93-e9d6-42a1-bdd8-0133c0aca46d\"
                    >
                      <a href=\"https://www.Abidjan.net.com/\" target=\"_blank\"
                        ><img
                          border=\"0\"
                          src=\"https://media-files.abidjan.net/logos/logo.svg\"
                          width=\"230\"
                          height=\"\"
                          alt=\"LastPass\"
                          style=\"
                            font-family: Helvetica, Arial, sans-serif;
                            color: #5e656b;
                            font-size: 16px;
                          \"
                          class=\"CToWUd\"
                      /></a>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            <table
              style=\"
                width: 600px;
                border-collapse: collapse;
                border-spacing: 0;
                margin: 0 auto;
                background-color: #ffffff;
              \"
              id=\"m_665443110472732713banner45760820-29c6-406a-931e-a5164c94a9f6\"
            >
              <tbody>
                <tr>
                  <td style=\"text-align: center\">
                    <a href=\"https://www.Abidjan.net.com/\" target=\"_blank\"
                      ><img
                        src=\"https://business.abidjan.net/public/img/cover-searchPJ.jpg\"
                        alt=\"\"
                        style=\"width: 60%; height: auto; margin: 10px auto\"
                    /></a>
                    <div
                      class=\"a6S\"
                      dir=\"ltr\"
                      style=\"opacity: 0.01; left: 712px; top: 273px\"
                    >
                      <div
                        id=\":57\"
                        class=\"T-I J-J5-Ji aQv T-I-ax7 L3 a5q\"
                        role=\"button\"
                        tabindex=\"0\"
                        aria-label=\"Télécharger la pièce jointe \"
                        data-tooltip-class=\"a1V\"
                        data-tooltip=\"Télécharger\"
                      >
                        <div class=\"aSK J-J5-Ji aYr\"></div>
                      </div>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            <table
              style=\"
                width: 600px;
                border-collapse: collapse;
                border-spacing: 0;
                margin: 0 auto;
                padding: 0;
                background-color: #ffffff;
                background-image: url();
              \"
              cellpadding=\"0\"
              cellspacing=\"0\"
            >
              <tbody>
                <tr>
                  <td
                    id=\"m_665443110472732713trBodyText29c88a0d-a584-44bc-9ea8-d95d9846fad887d5a783-401b-4602-b06f-5399a9cde7e8\"
                  >
                    <table style=\"width: 100%; border-collapse: collapse\">
                      <tbody>
                        <tr>
                          <td style=\"padding: 15px\" align=\"center\">
                            <table
                              style=\"
                                border-collapse: collapse;
                                border-spacing: 0;
                                color: #5e656b;
                              \"
                            >
                              <tbody>
                                <tr>
                                  <td
                                    align=\"left\"
                                    style=\"
                                      font-size: 26px;
                                      line-height: 30px;
                                      color: #1f2a34;
                                    \"
                                  >
                                    VOTRE COMMANDE 
                                  </td>
                                </tr>
                                <tr>
                                  <td
                                    style=\"
                                      padding: 30px 0 0 0;
                                      display: block;
                                      font-size: 16px;
                                      line-height: 24px;
                                    \"
                                    class=\"m_665443110472732713text\"
                                  >
                                    <p>
                                        Bonjour 
                                        {{ user.nom }}
                                        {{ user.prenoms }}
                                        ,vous venez d'effectuer une commande.
                                      <br />

                                        <div class=\"d-flex justify-content-center\">
                                            {% if order and order is defined %}
                                                <div class=\"title_part\">
                                                    <div class=\"text-center title_part\">
                                                        <div class=\"d-flex justify-content-start\">
                                                            <h2 class=\"titre mb-2\">
                                                                Commande
                                                            </h2>
                                                        </div>
                                                        <div class=\"d-flex justify-content-start\">
                                                            <p>
                                                                <strong>
                                                                    Reference 
                                                                </strong>
                                                                : # {{ order.reference }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                            
                                                <div style=\"display:flex; justify-content:center;\">

                                                    <div class=\"card_item\">
                                                        <div class=\"d-flex align-center\">
                                                            <p>
                                                                Montant Total : 
                                                                <span class=\"mx-2\" style=\"
                                                                    color: #D21D3A !important;
                                                                    font-size: 14px;
                                                                    font-weight: bold !important;
                                                                \">
                                                                    {{ order.amount }}
                                                                    {{ order.currency_name }}
                                                                </span>
                                                            </p>
                                                        </div>
                            
                                                        <div class=\"d-flex\">
                                                            Date: {{ order.createdAt|format_datetime(locale='fr',pattern=\"EEEE dd MMMM YYYY\") }}
                                                        </div>

                                                        <div class=\"d-flex\">
                                                            Heure:
                                                            <span class=\"mx-2\" style=\"
                                                                  color: #D21D3A !important;
                                                                  font-size: 14px;
                                                                  font-weight: bold !important;
                                                                \">
                                                                {{ order.createdAt|format_datetime(locale='fr',pattern=\"hh:mm:ss\") }}
                                                            </span>
                                                        </div>
                            
                                                        <div class=\"d-flex\">
                                                            Payé ? :
                                                            <span class=\"mx-2\" style=\"
                                                                  color: #D21D3A !important;
                                                                  font-size: 14px;
                                                                  font-weight: bold !important;
                                                                \">
                                                                {{ order.is_paid == true ? \"OUI\" : \"NON\" }}
                                                            </span>
                                                        </div>

                                                    </div>
                                                </div>
                                            {% endif %}
                                        </div>
                                    </p>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
            <table
              style=\"
                display: table;
                width: 600px;
                border-collapse: collapse;
                border-spacing: 0;
                margin: 0 auto;
                padding: 0;
                background-color: #ffffff;
                background-image: url();
              \"
              id=\"m_665443110472732713button292c00ed-548b-4951-8d25-95240b2e01d9\"
              cellpadding=\"0\"
              cellspacing=\"0\"
            >
              <tbody>
                <tr>
                  <td
                    style=\"padding: 15px 30px 60px 30px\"
                    id=\"m_665443110472732713trButton292c00ed-548b-4951-8d25-95240b2e01d9\"
                  >
                    <center>
                      <table
                        class=\"m_665443110472732713button\"
                        style=\"border-radius: 30px; background-color: #d21d3a\"
                      >
                        <tbody>
                          <tr>
                            <td style=\"text-align: center\">
                              <a
                                style=\"
                                  display: block;
                                  color: #fff;
                                  font-size: 16px;
                                  text-decoration: none;
                                  padding: 15px 30px;
                                  font-weight: bold;
                                \"
                                href=\"https://www.abidjan.net/\"
                                target=\"_blank\"
                                >Lien de page</a
                              >
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </center>
                  </td>
                </tr>
              </tbody>
            </table>
            <table
              style=\"
                width: 600px;
                border-collapse: collapse;
                border-spacing: 0;
                margin: 0 auto;
                padding: 0;
                background-color: #ecf5fb;
                background-image: url();
              \"
              id=\"m_665443110472732713bodyText29c88a0d-a584-44bc-9ea8-d95d9846fad887d5a783-401b-4602-b06f-5399a9cde7e81ccaeb83-9bb9-4ea7-840b-4de0c755015bc2c2f9a9-8147-489b-b21a-1b5a405febe6\"
              cellpadding=\"0\"
              cellspacing=\"0\"
            >
              <tbody>
                <tr>
                  <td
                    id=\"m_665443110472732713trBodyText29c88a0d-a584-44bc-9ea8-d95d9846fad887d5a783-401b-4602-b06f-5399a9cde7e81ccaeb83-9bb9-4ea7-840b-4de0c755015bc2c2f9a9-8147-489b-b21a-1b5a405febe6\"
                  >
                    <table
                      style=\"
                        width: 100%;
                        border-collapse: collapse;
                        background-color: #fff;
                      \"
                    >
                      <tbody>
                        <tr>
                          <td style=\"padding: 10px\" align=\"center\"></td>
                        </tr>
                        <tr>
                          <td style=\"padding: 5px\" align=\"center\">
                            <table
                              style=\"
                                border-collapse: collapse;
                                border-spacing: 0;
                                color: #5e656b;
                              \"
                            >
                              <tbody>
                                <tr>
                                  <td
                                    align=\"center\"
                                    style=\"
                                      font-size: 18px;
                                      line-height: 30px;
                                      color: #1f2a34;
                                    \"
                                  ></td>
                                </tr>
                                <tr>
                                  <td
                                    style=\"
                                      padding: 5px 0 0 0;
                                      display: block;
                                      font-size: 16px;
                                      line-height: 24px;
                                    \"
                                    class=\"m_665443110472732713text\"
                                  >
                                    <br />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td style=\"height: 8px; background-color: #ffb800\">&nbsp;</td>
        </tr>
        <tr>
          <td style=\"padding: 20px; background-color: #1e1e1e\">
            <table
              align=\"center\"
              style=\"
                width: 600px;
                border-collapse: collapse;
                color: #ffffff;
                font-size: 11px;
                line-height: 18px;
              \"
            >
              <tbody>
                <tr>
                  <td
                    align=\"center\"
                    style=\"
                      font-size: 11px;
                      line-height: 18px;
                      font-family: Arial, Helvetica, sans-serif;
                      color: #ffffff;
                    \"
                  >
                    Copyright © 1998-2023 Weblogy Group Ltd. Tous droits
                    réservés
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
", "mails/commande.html.twig", "/Users/ouattarasidik/Documents/forDeploy/experience/cooking/back-end/templates/mails/commande.html.twig");
    }
}
