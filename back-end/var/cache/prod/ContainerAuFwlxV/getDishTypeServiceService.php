<?php

namespace ContainerAuFwlxV;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getDishTypeServiceService extends App_KernelProdContainer
{
    /*
     * Gets the public 'App\Services\DishTypeService' shared autowired service.
     *
     * @return \App\Services\DishTypeService
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->services['App\\Services\\DishTypeService'] = $instance = new \App\Services\DishTypeService(($container->services['doctrine.orm.default_entity_manager'] ?? $container->load('getDoctrine_Orm_DefaultEntityManagerService')), ($container->privates['App\\Repository\\RestaurantRepository'] ?? $container->load('getRestaurantRepositoryService')), ($container->privates['App\\Repository\\DishTypeRepository'] ?? $container->load('getDishTypeRepositoryService')), ($container->privates['validator'] ?? $container->load('getValidatorService')));

        $instance->setContainer(($container->privates['.service_locator.jIxfAsi'] ?? $container->load('get_ServiceLocator_JIxfAsiService'))->withContext('App\\Services\\DishTypeService', $container));

        return $instance;
    }
}
