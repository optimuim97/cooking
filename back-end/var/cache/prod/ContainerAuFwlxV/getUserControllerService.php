<?php

namespace ContainerAuFwlxV;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getUserControllerService extends App_KernelProdContainer
{
    /*
     * Gets the public 'App\Controller\User\UserController' shared autowired service.
     *
     * @return \App\Controller\User\UserController
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->services['App\\Controller\\User\\UserController'] = $instance = new \App\Controller\User\UserController(($container->services['App\\Services\\UserService'] ?? $container->load('getUserServiceService')), ($container->privates['App\\Repository\\RecipeRepository'] ?? $container->load('getRecipeRepositoryService')), ($container->privates['App\\Repository\\UserSessionsRepository'] ?? $container->load('getUserSessionsRepositoryService')), ($container->privates['serializer'] ?? $container->load('getSerializerService')), ($container->privates['http_client'] ?? $container->getHttpClientService()), ($container->services['lexik_jwt_authentication.jwt_manager'] ?? $container->load('getLexikJwtAuthentication_JwtManagerService')), ($container->services['doctrine.orm.default_entity_manager'] ?? $container->load('getDoctrine_Orm_DefaultEntityManagerService')), ($container->privates['App\\Repository\\UserRepository'] ?? $container->load('getUserRepositoryService')), ($container->privates['security.user_password_hasher'] ?? $container->load('getSecurity_UserPasswordHasherService')));

        $instance->setContainer(($container->privates['.service_locator.jIxfAsi'] ?? $container->load('get_ServiceLocator_JIxfAsiService'))->withContext('App\\Controller\\User\\UserController', $container));

        return $instance;
    }
}
