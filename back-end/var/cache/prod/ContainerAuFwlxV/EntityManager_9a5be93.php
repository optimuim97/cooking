<?php

namespace ContainerAuFwlxV;

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHoldera8e9c = null;
    private $initializer1cc8b = null;
    private static $publicProperties749f3 = [
        
    ];
    public function getConnection()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getConnection', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getConnection();
    }
    public function getMetadataFactory()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getMetadataFactory', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getMetadataFactory();
    }
    public function getExpressionBuilder()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getExpressionBuilder', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getExpressionBuilder();
    }
    public function beginTransaction()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'beginTransaction', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->beginTransaction();
    }
    public function getCache()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getCache', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getCache();
    }
    public function transactional($func)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'transactional', array('func' => $func), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->transactional($func);
    }
    public function wrapInTransaction(callable $func)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'wrapInTransaction', array('func' => $func), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->wrapInTransaction($func);
    }
    public function commit()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'commit', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->commit();
    }
    public function rollback()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'rollback', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->rollback();
    }
    public function getClassMetadata($className)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getClassMetadata', array('className' => $className), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getClassMetadata($className);
    }
    public function createQuery($dql = '')
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'createQuery', array('dql' => $dql), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->createQuery($dql);
    }
    public function createNamedQuery($name)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'createNamedQuery', array('name' => $name), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->createNamedQuery($name);
    }
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->createNativeQuery($sql, $rsm);
    }
    public function createNamedNativeQuery($name)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->createNamedNativeQuery($name);
    }
    public function createQueryBuilder()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'createQueryBuilder', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->createQueryBuilder();
    }
    public function flush($entity = null)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'flush', array('entity' => $entity), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->flush($entity);
    }
    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->find($className, $id, $lockMode, $lockVersion);
    }
    public function getReference($entityName, $id)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getReference($entityName, $id);
    }
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getPartialReference($entityName, $identifier);
    }
    public function clear($entityName = null)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'clear', array('entityName' => $entityName), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->clear($entityName);
    }
    public function close()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'close', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->close();
    }
    public function persist($entity)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'persist', array('entity' => $entity), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->persist($entity);
    }
    public function remove($entity)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'remove', array('entity' => $entity), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->remove($entity);
    }
    public function refresh($entity, ?int $lockMode = null)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'refresh', array('entity' => $entity, 'lockMode' => $lockMode), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->refresh($entity, $lockMode);
    }
    public function detach($entity)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'detach', array('entity' => $entity), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->detach($entity);
    }
    public function merge($entity)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'merge', array('entity' => $entity), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->merge($entity);
    }
    public function copy($entity, $deep = false)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->copy($entity, $deep);
    }
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->lock($entity, $lockMode, $lockVersion);
    }
    public function getRepository($entityName)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getRepository', array('entityName' => $entityName), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getRepository($entityName);
    }
    public function contains($entity)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'contains', array('entity' => $entity), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->contains($entity);
    }
    public function getEventManager()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getEventManager', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getEventManager();
    }
    public function getConfiguration()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getConfiguration', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getConfiguration();
    }
    public function isOpen()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'isOpen', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->isOpen();
    }
    public function getUnitOfWork()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getUnitOfWork', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getUnitOfWork();
    }
    public function getHydrator($hydrationMode)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getHydrator($hydrationMode);
    }
    public function newHydrator($hydrationMode)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->newHydrator($hydrationMode);
    }
    public function getProxyFactory()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getProxyFactory', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getProxyFactory();
    }
    public function initializeObject($obj)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'initializeObject', array('obj' => $obj), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->initializeObject($obj);
    }
    public function getFilters()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'getFilters', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->getFilters();
    }
    public function isFiltersStateClean()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'isFiltersStateClean', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->isFiltersStateClean();
    }
    public function hasFilters()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'hasFilters', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return $this->valueHoldera8e9c->hasFilters();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);
        $instance->initializer1cc8b = $initializer;
        return $instance;
    }
    public function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, ?\Doctrine\Common\EventManager $eventManager = null)
    {
        static $reflection;
        if (! $this->valueHoldera8e9c) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHoldera8e9c = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
        }
        $this->valueHoldera8e9c->__construct($conn, $config, $eventManager);
    }
    public function & __get($name)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__get', ['name' => $name], $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        if (isset(self::$publicProperties749f3[$name])) {
            return $this->valueHoldera8e9c->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8e9c;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHoldera8e9c;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8e9c;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHoldera8e9c;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__isset', array('name' => $name), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8e9c;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHoldera8e9c;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__unset', array('name' => $name), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8e9c;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHoldera8e9c;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__clone', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        $this->valueHoldera8e9c = clone $this->valueHoldera8e9c;
    }
    public function __sleep()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__sleep', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return array('valueHoldera8e9c');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1cc8b = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1cc8b;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'initializeProxy', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldera8e9c;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHoldera8e9c;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
