<?php

namespace ContainerAuFwlxV;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getOrderHistoryRepositoryService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Repository\OrderHistoryRepository' shared autowired service.
     *
     * @return \App\Repository\OrderHistoryRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['App\\Repository\\OrderHistoryRepository'] = new \App\Repository\OrderHistoryRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));
    }
}
