<?php

namespace ContainerAuFwlxV;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getRecipeStepRepositoryService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Repository\RecipeStepRepository' shared autowired service.
     *
     * @return \App\Repository\RecipeStepRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['App\\Repository\\RecipeStepRepository'] = new \App\Repository\RecipeStepRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));
    }
}
