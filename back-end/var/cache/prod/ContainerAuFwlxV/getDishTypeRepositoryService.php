<?php

namespace ContainerAuFwlxV;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getDishTypeRepositoryService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Repository\DishTypeRepository' shared autowired service.
     *
     * @return \App\Repository\DishTypeRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['App\\Repository\\DishTypeRepository'] = new \App\Repository\DishTypeRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));
    }
}
