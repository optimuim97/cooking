<?php

namespace ContainerAuFwlxV;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getCookingEventRepositoryService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Repository\CookingEventRepository' shared autowired service.
     *
     * @return \App\Repository\CookingEventRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['App\\Repository\\CookingEventRepository'] = new \App\Repository\CookingEventRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));
    }
}
