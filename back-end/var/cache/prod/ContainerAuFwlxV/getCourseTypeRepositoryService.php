<?php

namespace ContainerAuFwlxV;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getCourseTypeRepositoryService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Repository\CourseTypeRepository' shared autowired service.
     *
     * @return \App\Repository\CourseTypeRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['App\\Repository\\CourseTypeRepository'] = new \App\Repository\CourseTypeRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));
    }
}
