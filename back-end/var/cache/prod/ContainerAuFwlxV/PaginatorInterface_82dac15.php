<?php

namespace ContainerAuFwlxV;

class PaginatorInterface_82dac15 implements \ProxyManager\Proxy\VirtualProxyInterface, \Knp\Component\Pager\PaginatorInterface
{
    private $valueHoldera8e9c = null;
    private $initializer1cc8b = null;
    private static $publicProperties749f3 = [
        
    ];
    public function paginate(mixed $target, int $page = 1, ?int $limit = null, array $options = []) : \Knp\Component\Pager\Pagination\PaginationInterface
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'paginate', array('target' => $target, 'page' => $page, 'limit' => $limit, 'options' => $options), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        if ($this->valueHoldera8e9c === $returnValue = $this->valueHoldera8e9c->paginate($target, $page, $limit, $options)) {
            return $this;
        }
        return $returnValue;
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        $instance->initializer1cc8b = $initializer;
        return $instance;
    }
    public function __construct()
    {
        static $reflection;
        if (! $this->valueHoldera8e9c) {
            $reflection = $reflection ?? new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');
            $this->valueHoldera8e9c = $reflection->newInstanceWithoutConstructor();
        }
    }
    public function & __get($name)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__get', ['name' => $name], $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        if (isset(self::$publicProperties749f3[$name])) {
            return $this->valueHoldera8e9c->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8e9c;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHoldera8e9c;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8e9c;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHoldera8e9c;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__isset', array('name' => $name), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8e9c;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHoldera8e9c;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__unset', array('name' => $name), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8e9c;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHoldera8e9c;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__clone', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        $this->valueHoldera8e9c = clone $this->valueHoldera8e9c;
    }
    public function __sleep()
    {
        $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, '__sleep', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
        return array('valueHoldera8e9c');
    }
    public function __wakeup()
    {
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1cc8b = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1cc8b;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1cc8b && ($this->initializer1cc8b->__invoke($valueHoldera8e9c, $this, 'initializeProxy', array(), $this->initializer1cc8b) || 1) && $this->valueHoldera8e9c = $valueHoldera8e9c;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldera8e9c;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHoldera8e9c;
    }
}

if (!\class_exists('PaginatorInterface_82dac15', false)) {
    \class_alias(__NAMESPACE__.'\\PaginatorInterface_82dac15', 'PaginatorInterface_82dac15', false);
}
