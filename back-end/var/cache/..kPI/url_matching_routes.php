<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/api/token/refresh' => [[['_route' => 'gesdinet_jwt_refresh_token'], null, null, null, false, false, null]],
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/xdebug' => [[['_route' => '_profiler_xdebug', '_controller' => 'web_profiler.controller.profiler::xdebugAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'admin', '_controller' => 'App\\Controller\\Admin\\DashboardController::index'], null, null, null, false, false, null]],
        '/auth/sign-up' => [[['_route' => 'app_auth_user_register', '_controller' => 'App\\Controller\\Auth\\AuthUserRegisterController::signUp'], null, ['POST' => 0], null, false, false, null]],
        '/api/add-budget' => [[['_route' => 'app_add_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::add'], null, ['POST' => 0], null, false, false, null]],
        '/delete-budget' => [[['_route' => 'app_delete_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::delete'], null, ['DELETE' => 0], null, false, false, null]],
        '/show-budget' => [[['_route' => 'app_show_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::show'], null, ['POST' => 0], null, false, false, null]],
        '/update-budget' => [[['_route' => 'app_update_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::update'], null, ['POST' => 0], null, false, false, null]],
        '/get-budgets' => [[['_route' => 'app_get_budgets', '_controller' => 'App\\Controller\\Budget\\BudgetController::getAllBudget'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-cooking_event' => [[['_route' => 'add_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-cooking_events' => [[['_route' => 'cooking_events', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-cooking_trick' => [[['_route' => 'add_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-cooking_tricks' => [[['_route' => 'cooking_tricks', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-course' => [[['_route' => 'app_course', '_controller' => 'App\\Controller\\Course\\CourseController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-courses' => [[['_route' => 'get_courses', '_controller' => 'App\\Controller\\Course\\CourseController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/get-course-types' => [[['_route' => 'get_course_types', '_controller' => 'App\\Controller\\Course\\CourseController::getAllCourseType'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-lesson' => [[['_route' => 'add_course', '_controller' => 'App\\Controller\\Course\\CourseController::addCourse'], null, ['POST' => 0], null, false, false, null]],
        '/api/add-dish_type' => [[['_route' => 'app_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-dish_types' => [[['_route' => 'get_dish_types', '_controller' => 'App\\Controller\\DishType\\DishTypeController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/generate-slug-for-recipe' => [[['_route' => 'app_executor', '_controller' => 'App\\Controller\\ExecutorController::createUser'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_front_home_front', '_controller' => 'App\\Controller\\FrontController::getIndexPage'], null, null, null, false, false, null]],
        '/recipes-for-abj' => [[['_route' => 'app_front_getrecipeforabj', '_controller' => 'App\\Controller\\FrontController::getRecipeForAbj'], null, null, null, false, false, null]],
        '/home' => [[['_route' => 'app_home', '_controller' => 'App\\Controller\\HomeController::index'], null, null, null, false, false, null]],
        '/api/add-level' => [[['_route' => 'app_add_level', '_controller' => 'App\\Controller\\Level\\LevelController::add'], null, ['POST' => 0], null, false, false, null]],
        '/api/update-level' => [[['_route' => 'app_update_level', '_controller' => 'App\\Controller\\Level\\LevelController::update'], null, ['PATCH' => 0], null, false, false, null]],
        '/get-levels' => [[['_route' => 'app_get_level', '_controller' => 'App\\Controller\\Level\\LevelController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/email' => [[['_route' => 'app_mailer_sendemail', '_controller' => 'App\\Controller\\MailerController::sendEmail'], null, null, null, false, false, null]],
        '/api/make-orders' => [[['_route' => 'app_order', '_controller' => 'App\\Controller\\OrderController::OrderDish'], null, ['POST' => 0], null, false, false, null]],
        '/web-hooks' => [[['_route' => 'app_order_callbackurl', '_controller' => 'App\\Controller\\OrderController::callBackUrl'], null, ['POST' => 0], null, false, false, null]],
        '/api/add-recipe' => [[['_route' => 'app_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::addRecipe'], null, ['POST' => 0], null, false, false, null]],
        '/get-recipes' => [[['_route' => 'get_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipes'], null, ['POST' => 0], null, false, false, null]],
        '/get-recipe_types/spot_light' => [[['_route' => 'get_recipes_spot_light', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeSpotLight'], null, ['GET' => 0], null, false, false, null]],
        '/api/get-recipe-bookmaker' => [[['_route' => 'app_get_recipe_book_maker', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeBookMark'], null, ['GET' => 0], null, false, false, null]],
        '/get-recipes-count' => [[['_route' => 'app_get_all_recipe_count', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeAllCount'], null, ['GET' => 0], null, false, false, null]],
        '/get-recipe-is-spotlight' => [[['_route' => 'app_get_is_spot_ligth_recipes_by_name', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getIsSpotLight'], null, ['GET' => 0], null, false, false, null]],
        '/get-all-recipes' => [[['_route' => 'app_get_all_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getAllRecipeForDash'], null, ['GET' => 0], null, false, false, null]],
        '/add-recipe_type' => [[['_route' => 'app_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-recipe_types' => [[['_route' => 'get_recipe_types', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/get-all-recipe_types' => [[['_route' => 'get_all_recipe_types_for_dash', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::getAllRecipeTypeForDash'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-restaurant' => [[['_route' => 'app_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::addRestaurant'], null, ['POST' => 0], null, false, false, null]],
        '/get-restaurant-spotlight' => [[['_route' => 'get_restaurant_spotlight', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestaurantSpotLight'], null, ['GET' => 0], null, false, false, null]],
        '/get-restaurants' => [[['_route' => 'get_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestaurant'], null, ['POST' => 0], null, false, false, null]],
        '/add-dish' => [[['_route' => 'app_add_dish', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::addDish'], null, ['POST' => 0], null, false, false, null]],
        '/get-dishes' => [[['_route' => 'app_get_dishs', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getDishes'], null, ['GET' => 0], null, false, false, null]],
        '/get-all-restaurants' => [[['_route' => 'all_resto', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getAllResto'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-subscription_formula' => [[['_route' => 'app_subscription_formula', '_controller' => 'App\\Controller\\SubscriptionFormulaController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-subscription_formulas' => [[['_route' => 'app_subscription_formulas', '_controller' => 'App\\Controller\\SubscriptionFormulaController::getSubscriptions'], null, ['GET' => 0], null, false, false, null]],
        '/api/get-user_datas' => [[['_route' => 'app_user', '_controller' => 'App\\Controller\\User\\UserController::index'], null, ['GET' => 0], null, false, false, null]],
        '/get-user_profil' => [[['_route' => 'app_user_data', '_controller' => 'App\\Controller\\User\\UserController::userProfil'], null, ['POST' => 0], null, false, false, null]],
        '/api/update-user-profil' => [[['_route' => 'app_update_user', '_controller' => 'App\\Controller\\User\\UserController::update'], null, ['PUT' => 0], null, false, false, null]],
        '/api/get-user-recipes' => [[['_route' => 'app_user_recipes', '_controller' => 'App\\Controller\\User\\UserController::getUserRecipes'], null, ['GET' => 0], null, false, false, null]],
        '/api/get-user-liked' => [[['_route' => 'app_user_liked_recipes', '_controller' => 'App\\Controller\\User\\UserController::getUserLikedRecipes'], null, ['GET' => 0], null, false, false, null]],
        '/api/login_check' => [[['_route' => 'api_login_check'], null, null, null, false, false, null]],
        '/auth/refresh' => [[['_route' => 'jwt_refresh'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/api/(?'
                    .'|delete\\-(?'
                        .'|co(?'
                            .'|oking_(?'
                                .'|event/([^/]++)(*:216)'
                                .'|trick/([^/]++)(*:238)'
                            .')'
                            .'|urse/([^/]++)(*:260)'
                        .')'
                        .'|dish_type/([^/]++)(*:287)'
                    .')'
                    .'|update\\-(?'
                        .'|co(?'
                            .'|oking_trick/([^/]++)(*:332)'
                            .'|urse/([^/]++)(*:353)'
                        .')'
                        .'|dish_type/([^/]++)(*:380)'
                    .')'
                    .'|enroll\\-to\\-course/([^/]++)(*:416)'
                .')'
                .'|/s(?'
                    .'|how\\-(?'
                        .'|co(?'
                            .'|oking_(?'
                                .'|event/([^/]++)(*:466)'
                                .'|trick/([^/]++)(*:488)'
                            .')'
                            .'|urse/([^/]++)(*:510)'
                        .')'
                        .'|dish_type/([^/]++)(*:537)'
                    .')'
                    .'|earch\\-co(?'
                        .'|oking_trick\\-by\\-name/([^/]++)(*:588)'
                        .'|urse\\-by\\-name/([^/]++)(*:619)'
                    .')'
                .')'
                .'|/update\\-cooking_event/([^/]++)(*:660)'
                .'|/((?:[a-z0-9-].?!.*)+(?::[0-9]+).+)(?'
                    .'|(*:706)'
                .')'
                .'|/views\\-counter/([^/]++)(*:739)'
                .'|/api/(?'
                    .'|views\\-counter/([^/]++)(*:778)'
                    .'|delete\\-(?'
                        .'|level/([^/]++)(*:811)'
                        .'|re(?'
                            .'|cipe(?'
                                .'|/([^/]++)(*:840)'
                                .'|_type/([^/]++)(*:862)'
                            .')'
                            .'|staurant/([^/]++)(*:888)'
                        .')'
                        .'|comment/([^/]++)/([^/]++)(*:922)'
                        .'|dish/([^/]++)(*:943)'
                    .')'
                    .'|c(?'
                        .'|ancel\\-orders/([^/]++)(*:978)'
                        .'|omment\\-recipe/([^/]++)(*:1009)'
                    .')'
                    .'|rate(?'
                        .'|/([^/]++)(*:1035)'
                        .'|\\-restaurant/([^/]++)(*:1065)'
                    .')'
                    .'|update\\-(?'
                        .'|re(?'
                            .'|cipe(?'
                                .'|/([^/]++)(*:1107)'
                                .'|_type/([^/]++)(*:1130)'
                            .')'
                            .'|staurant/([^/]++)(*:1157)'
                        .')'
                        .'|comment/([^/]++)/([^/]++)(*:1192)'
                        .'|dish/([^/]++)(*:1214)'
                    .')'
                    .'|like\\-recipe/([^/]++)(*:1245)'
                    .'|get\\-re(?'
                        .'|cipe\\-by\\-level/([^/]++)(*:1288)'
                        .'|sto\\-orders/([^/]++)(*:1317)'
                    .')'
                    .'|add\\-to\\-recipe\\-to\\-bookmaker/([^/]++)(*:1366)'
                    .'|subscribe\\-as\\-cooker/([^/]++)(*:1405)'
                .')'
                .'|/s(?'
                    .'|how(?'
                        .'|\\-(?'
                            .'|level/([^/]++)(*:1445)'
                            .'|re(?'
                                .'|cipe(?'
                                    .'|/([^/]++)(*:1475)'
                                    .'|_type/([^/]++)(*:1498)'
                                .')'
                                .'|staurant/([^/]++)(*:1525)'
                            .')'
                            .'|dish/([^/]++)(*:1548)'
                        .')'
                        .'|_subscription_formula/([^/]++)(*:1588)'
                    .')'
                    .'|earch\\-restaurant\\-by\\-name/([^/]++)(*:1634)'
                .')'
                .'|/get\\-(?'
                    .'|r(?'
                        .'|ate(?'
                            .'|/(?'
                                .'|rate/([^/]++)(*:1680)'
                                .'|average/([^/]++)(*:1705)'
                            .')'
                            .'|\\-restaurant/(?'
                                .'|([^/]++)(*:1739)'
                                .'|average/([^/]++)(*:1764)'
                            .')'
                        .')'
                        .'|ecipe(?'
                            .'|s\\-by\\-type/([^/]++)(*:1803)'
                            .'|\\-by\\-name/([^/]++)(*:1831)'
                        .')'
                    .')'
                    .'|dishes_by_type/([^/]++)/([^/]++)(*:1874)'
                .')'
                .'|/update\\-subscription_formula/([^/]++)(*:1922)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        216 => [[['_route' => 'delete_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        238 => [[['_route' => 'delete_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        260 => [[['_route' => 'delete_course', '_controller' => 'App\\Controller\\Course\\CourseController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        287 => [[['_route' => 'delete_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        332 => [[['_route' => 'app_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::update'], ['id'], ['POST' => 0], null, false, true, null]],
        353 => [[['_route' => 'update_course', '_controller' => 'App\\Controller\\Course\\CourseController::updateCourse'], ['id'], ['POST' => 0], null, false, true, null]],
        380 => [[['_route' => 'update_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::updateRecipeType'], ['id'], ['POST' => 0], null, false, true, null]],
        416 => [[['_route' => 'enroll_to_course', '_controller' => 'App\\Controller\\Course\\CourseController::enrollToCourse'], ['id'], ['GET' => 0], null, false, true, null]],
        466 => [[['_route' => 'show_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        488 => [[['_route' => 'show_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::show'], ['slug'], ['GET' => 0], null, false, true, null]],
        510 => [[['_route' => 'show_course', '_controller' => 'App\\Controller\\Course\\CourseController::showCourse'], ['slug'], ['GET' => 0], null, false, true, null]],
        537 => [[['_route' => 'show_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::showRecipeType'], ['id'], ['GET' => 0], null, false, true, null]],
        588 => [[['_route' => 'search_cooking_tricks', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::getCookingTrickByName'], ['keyword'], ['GET' => 0], null, false, true, null]],
        619 => [[['_route' => 'search_course_by_name', '_controller' => 'App\\Controller\\Course\\CourseController::searchCourseByName'], ['keyword'], ['GET' => 0], null, false, true, null]],
        660 => [[['_route' => 'app_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::update'], ['id'], ['PATCH' => 0], null, false, true, null]],
        706 => [
            [['_route' => 'app_front_getindexpage', '_controller' => 'App\\Controller\\FrontController::getIndexPage'], ['page'], null, null, false, true, null],
            [['_route' => 'app_front_getindexpage_1', '_controller' => 'App\\Controller\\FrontController::getIndexPage'], ['page'], null, null, false, true, null],
        ],
        739 => [[['_route' => 'views-counter', '_controller' => 'App\\Controller\\FrontController::countView'], ['slug'], null, null, false, true, null]],
        778 => [[['_route' => 'views-counter-auth', '_controller' => 'App\\Controller\\FrontController::countViewAuth'], ['slug'], null, null, false, true, null]],
        811 => [[['_route' => 'app_delete_level', '_controller' => 'App\\Controller\\Level\\LevelController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        840 => [[['_route' => 'delete_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        862 => [[['_route' => 'delete_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        888 => [[['_route' => 'delete_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::deleteRestaurant'], ['id'], ['DELETE' => 0], null, false, true, null]],
        922 => [[['_route' => 'app_delete_comment_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::deleteCommentRecipe'], ['recipe_id', 'comment_id'], ['PATCH' => 0], null, false, true, null]],
        943 => [[['_route' => 'app_delete_dish', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::deleteDish'], ['id'], ['DELETE' => 0], null, false, true, null]],
        978 => [[['_route' => 'app_order_cancelorder', '_controller' => 'App\\Controller\\OrderController::cancelOrder'], ['id'], null, null, false, true, null]],
        1009 => [[['_route' => 'app_comment_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::commentRecipe'], ['slug'], ['POST' => 0], null, false, true, null]],
        1035 => [[['_route' => 'app_rate', '_controller' => 'App\\Controller\\RateController::rate'], ['id'], ['POST' => 0], null, false, true, null]],
        1065 => [[['_route' => 'app_rate_restaurant', '_controller' => 'App\\Controller\\RateRestaurantController::rate'], ['id'], ['POST' => 0], null, false, true, null]],
        1107 => [[['_route' => 'update_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::updateRecipe'], ['id'], ['POST' => 0], null, false, true, null]],
        1130 => [[['_route' => 'update_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::updateRecipeType'], ['id'], ['POST' => 0], null, false, true, null]],
        1157 => [[['_route' => 'update_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::updateRestaurant'], ['id'], ['POST' => 0], null, false, true, null]],
        1192 => [[['_route' => 'app_update_comment_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::updateCommentRecipe'], ['recipe_id', 'comment_id'], ['POST' => 0], null, false, true, null]],
        1214 => [[['_route' => 'app_get_dish_update', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::updateDish'], ['id'], ['POST' => 0], null, false, true, null]],
        1245 => [[['_route' => 'app_like_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::likeRecipe'], ['id'], ['GET' => 0], null, false, true, null]],
        1288 => [[['_route' => 'app_get_recipes_by_level', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeByLevel'], ['id'], ['GET' => 0], null, false, true, null]],
        1317 => [[['_route' => 'app_restaurant_restaurant_getrestoorders', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestoOrders'], ['id'], null, null, false, true, null]],
        1366 => [[['_route' => 'app_add_recipe_to_book_maker', '_controller' => 'App\\Controller\\Recipe\\RecipeController::addRecipeToBookMaker'], ['id'], ['GET' => 0], null, false, true, null]],
        1405 => [[['_route' => 'app_subscription', '_controller' => 'App\\Controller\\SubscriptionController::subscribeAsCooker'], ['id'], ['GET' => 0], null, false, true, null]],
        1445 => [[['_route' => 'app_show_level', '_controller' => 'App\\Controller\\Level\\LevelController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        1475 => [[['_route' => 'show_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::showRecipe'], ['slug'], ['GET' => 0], null, false, true, null]],
        1498 => [[['_route' => 'show_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::showRecipeType'], ['slug'], ['GET' => 0], null, false, true, null]],
        1525 => [[['_route' => 'show_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::showRestaurant'], ['slug'], ['GET' => 0], null, false, true, null]],
        1548 => [[['_route' => 'app_get_dish_show', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::showDish'], ['id'], ['GET' => 0], null, false, true, null]],
        1588 => [[['_route' => 'show_subscription_formula', '_controller' => 'App\\Controller\\SubscriptionFormulaController::getSubscriptionFormula'], ['slug'], ['GET' => 0], null, false, true, null]],
        1634 => [[['_route' => 'app_restaurant_restaurant_getrestaurantbyname', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestaurantByName'], ['keyword'], null, null, false, true, null]],
        1680 => [[['_route' => 'app_get_rate', '_controller' => 'App\\Controller\\RateController::getRecipeRate'], ['recipeID'], ['GET' => 0], null, false, true, null]],
        1705 => [[['_route' => 'app_get_rate_average', '_controller' => 'App\\Controller\\RateController::getRecipeAverage'], ['recipeID'], ['GET' => 0], null, false, true, null]],
        1739 => [[['_route' => 'app_get_rate_restaurant', '_controller' => 'App\\Controller\\RateRestaurantController::getRestaurantRate'], ['restaurantID'], ['GET' => 0], null, false, true, null]],
        1764 => [[['_route' => 'app_get_rate_restaurant_average', '_controller' => 'App\\Controller\\RateRestaurantController::getRestaurantAverage'], ['restaurantID'], ['GET' => 0], null, false, true, null]],
        1803 => [[['_route' => 'get_by_cat_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipesByType'], ['slug'], ['POST' => 0], null, false, true, null]],
        1831 => [[['_route' => 'app_get_recipes_by_name', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeByName'], ['keyword'], ['GET' => 0], null, false, true, null]],
        1874 => [[['_route' => 'app_get_dishes_by_types', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getDishByType'], ['id', 'resto_id'], ['GET' => 0], null, false, true, null]],
        1922 => [
            [['_route' => 'update_subscription_formula', '_controller' => 'App\\Controller\\SubscriptionFormulaController::updateSubscriptionFormula'], ['id'], ['PUT' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
