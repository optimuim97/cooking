<?php

namespace ContainerK3AHsxN;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getRecipeServiceService extends App_KernelProductionDebugContainer
{
    /**
     * Gets the public 'App\Services\RecipeService' shared autowired service.
     *
     * @return \App\Services\RecipeService
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Services/RecipeService.php';

        $container->services['App\\Services\\RecipeService'] = $instance = new \App\Services\RecipeService(($container->services['knp_paginator'] ?? $container->load('getKnpPaginatorService')), ($container->services['doctrine.orm.default_entity_manager'] ?? $container->load('getDoctrine_Orm_DefaultEntityManagerService')), ($container->privates['App\\Repository\\RecipeTypeRepository'] ?? $container->load('getRecipeTypeRepositoryService')), ($container->privates['App\\Repository\\LevelRepository'] ?? $container->load('getLevelRepositoryService')), ($container->privates['App\\Repository\\BudgetRepository'] ?? $container->load('getBudgetRepositoryService')), ($container->privates['App\\Repository\\RecipeRepository'] ?? $container->load('getRecipeRepositoryService')), ($container->privates['App\\Repository\\LikeRepository'] ?? $container->load('getLikeRepositoryService')), ($container->privates['validator'] ?? $container->load('getValidatorService')), ($container->privates['App\\Repository\\CommentRecipeRepository'] ?? $container->load('getCommentRecipeRepositoryService')), ($container->privates['App\\Repository\\BookMakerRepository'] ?? $container->load('getBookMakerRepositoryService')), ($container->privates['serializer'] ?? $container->load('getSerializerService')));

        $instance->setContainer(($container->privates['.service_locator.jIxfAsi'] ?? $container->load('get_ServiceLocator_JIxfAsiService'))->withContext('App\\Services\\RecipeService', $container));

        return $instance;
    }
}
