<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/api/token/refresh' => [[['_route' => 'gesdinet_jwt_refresh_token'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'admin', '_controller' => 'App\\Controller\\Admin\\DashboardController::index'], null, null, null, false, false, null]],
        '/auth/sign-up' => [[['_route' => 'app_auth_user_register', '_controller' => 'App\\Controller\\Auth\\AuthUserRegisterController::signUp'], null, ['POST' => 0], null, false, false, null]],
        '/api/add-budget' => [[['_route' => 'app_add_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::add'], null, ['POST' => 0], null, false, false, null]],
        '/delete-budget' => [[['_route' => 'app_delete_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::delete'], null, ['DELETE' => 0], null, false, false, null]],
        '/show-budget' => [[['_route' => 'app_show_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::show'], null, ['POST' => 0], null, false, false, null]],
        '/update-budget' => [[['_route' => 'app_update_budget', '_controller' => 'App\\Controller\\Budget\\BudgetController::update'], null, ['POST' => 0], null, false, false, null]],
        '/get-budgets' => [[['_route' => 'app_get_budgets', '_controller' => 'App\\Controller\\Budget\\BudgetController::getAllBudget'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-cooking_event' => [[['_route' => 'add_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-cooking_events' => [[['_route' => 'cooking_events', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-cooking_trick' => [[['_route' => 'add_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-cooking_tricks' => [[['_route' => 'cooking_tricks', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-course' => [[['_route' => 'app_course', '_controller' => 'App\\Controller\\Course\\CourseController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-courses' => [[['_route' => 'get_courses', '_controller' => 'App\\Controller\\Course\\CourseController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/get-course-types' => [[['_route' => 'get_course_types', '_controller' => 'App\\Controller\\Course\\CourseController::getAllCourseType'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-dish_type' => [[['_route' => 'app_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-dish_types' => [[['_route' => 'get_dish_types', '_controller' => 'App\\Controller\\DishType\\DishTypeController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/generate-slug-for-recipe' => [[['_route' => 'app_executor', '_controller' => 'App\\Controller\\ExecutorController::createUser'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_front_home', '_controller' => 'App\\Controller\\FrontController::getIndexPage'], null, null, null, false, false, null]],
        '/recipes-for-abj' => [[['_route' => 'app_front_getrecipeforabj', '_controller' => 'App\\Controller\\FrontController::getRecipeForAbj'], null, null, null, false, false, null]],
        '/home' => [[['_route' => 'app_home', '_controller' => 'App\\Controller\\HomeController::index'], null, null, null, false, false, null]],
        '/api/add-level' => [[['_route' => 'app_add_level', '_controller' => 'App\\Controller\\Level\\LevelController::add'], null, ['POST' => 0], null, false, false, null]],
        '/api/update-level' => [[['_route' => 'app_update_level', '_controller' => 'App\\Controller\\Level\\LevelController::update'], null, ['PATCH' => 0], null, false, false, null]],
        '/get-levels' => [[['_route' => 'app_get_level', '_controller' => 'App\\Controller\\Level\\LevelController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/email' => [[['_route' => 'app_mailer_sendemail', '_controller' => 'App\\Controller\\MailerController::sendEmail'], null, null, null, false, false, null]],
        '/api/make-orders' => [[['_route' => 'app_order', '_controller' => 'App\\Controller\\OrderController::OrderDish'], null, ['POST' => 0], null, false, false, null]],
        '/web-hooks' => [[['_route' => 'app_order_callbackurl', '_controller' => 'App\\Controller\\OrderController::callBackUrl'], null, ['POST' => 0], null, false, false, null]],
        '/api/add-recipe' => [[['_route' => 'app_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::addRecipe'], null, ['POST' => 0], null, false, false, null]],
        '/get-recipes' => [[['_route' => 'get_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipes'], null, ['POST' => 0], null, false, false, null]],
        '/get-recipe_types/spot_light' => [[['_route' => 'get_recipes_spot_light', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeSpotLight'], null, ['GET' => 0], null, false, false, null]],
        '/api/get-recipe-bookmaker' => [[['_route' => 'app_get_recipe_book_maker', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeBookMark'], null, ['GET' => 0], null, false, false, null]],
        '/get-recipes-count' => [[['_route' => 'app_get_all_recipe_count', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeAllCount'], null, ['GET' => 0], null, false, false, null]],
        '/get-recipe-is-spotlight' => [[['_route' => 'app_get_is_spot_ligth_recipes_by_name', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getIsSpotLight'], null, ['GET' => 0], null, false, false, null]],
        '/get-all-recipes' => [[['_route' => 'app_get_all_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getAllRecipeForDash'], null, ['GET' => 0], null, false, false, null]],
        '/add-recipe_type' => [[['_route' => 'app_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-recipe_types' => [[['_route' => 'get_recipe_types', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/get-all-recipe_types' => [[['_route' => 'get_all_recipe_types_for_dash', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::getAllRecipeTypeForDash'], null, ['GET' => 0], null, false, false, null]],
        '/api/add-restaurant' => [[['_route' => 'app_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::addRestaurant'], null, ['POST' => 0], null, false, false, null]],
        '/get-restaurant-spotlight' => [[['_route' => 'get_restaurant_spotlight', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestaurantSpotLight'], null, ['GET' => 0], null, false, false, null]],
        '/get-restaurants' => [[['_route' => 'get_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestaurant'], null, ['POST' => 0], null, false, false, null]],
        '/add-dish' => [[['_route' => 'app_add_dish', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::addDish'], null, ['POST' => 0], null, false, false, null]],
        '/get-dishes' => [[['_route' => 'app_get_dishs', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getDishes'], null, ['GET' => 0], null, false, false, null]],
        '/get-all-restaurants' => [[['_route' => 'app_restaurant_restaurant_getallresto', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getAllResto'], null, null, null, false, false, null]],
        '/api/add-subscription_formula' => [[['_route' => 'app_subscription_formula', '_controller' => 'App\\Controller\\SubscriptionFormulaController::add'], null, ['POST' => 0], null, false, false, null]],
        '/get-subscription_formulas' => [[['_route' => 'app_subscription_formulas', '_controller' => 'App\\Controller\\SubscriptionFormulaController::getSubscriptions'], null, ['GET' => 0], null, false, false, null]],
        '/api/get-user_datas' => [[['_route' => 'app_user', '_controller' => 'App\\Controller\\User\\UserController::index'], null, ['GET' => 0], null, false, false, null]],
        '/get-user_profil' => [[['_route' => 'app_user_data', '_controller' => 'App\\Controller\\User\\UserController::userProfil'], null, ['POST' => 0], null, false, false, null]],
        '/api/update-user-profil' => [[['_route' => 'app_update_user', '_controller' => 'App\\Controller\\User\\UserController::update'], null, ['PUT' => 0], null, false, false, null]],
        '/api/get-user-recipes' => [[['_route' => 'app_user_recipes', '_controller' => 'App\\Controller\\User\\UserController::getUserRecipes'], null, ['GET' => 0], null, false, false, null]],
        '/api/get-user-liked' => [[['_route' => 'app_user_liked_recipes', '_controller' => 'App\\Controller\\User\\UserController::getUserLikedRecipes'], null, ['GET' => 0], null, false, false, null]],
        '/api/login_check' => [[['_route' => 'api_login_check'], null, null, null, false, false, null]],
        '/auth/refresh' => [[['_route' => 'jwt_refresh'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/api/(?'
                    .'|delete\\-(?'
                        .'|co(?'
                            .'|oking_(?'
                                .'|event/([^/]++)(*:54)'
                                .'|trick/([^/]++)(*:75)'
                            .')'
                            .'|urse/([^/]++)(*:96)'
                            .'|mment/([^/]++)/([^/]++)(*:126)'
                        .')'
                        .'|dish(?'
                            .'|_type/([^/]++)(*:156)'
                            .'|/([^/]++)(*:173)'
                        .')'
                        .'|level/([^/]++)(*:196)'
                        .'|re(?'
                            .'|cipe(?'
                                .'|/([^/]++)(*:225)'
                                .'|_type/([^/]++)(*:247)'
                            .')'
                            .'|staurant/([^/]++)(*:273)'
                        .')'
                    .')'
                    .'|update\\-(?'
                        .'|co(?'
                            .'|oking_trick/([^/]++)(*:319)'
                            .'|urse/([^/]++)(*:340)'
                            .'|mment/([^/]++)/([^/]++)(*:371)'
                        .')'
                        .'|dish(?'
                            .'|_type/([^/]++)(*:401)'
                            .'|/([^/]++)(*:418)'
                        .')'
                        .'|re(?'
                            .'|cipe(?'
                                .'|/([^/]++)(*:448)'
                                .'|_type/([^/]++)(*:470)'
                            .')'
                            .'|staurant/([^/]++)(*:496)'
                        .')'
                    .')'
                    .'|enroll\\-to\\-course/([^/]++)(*:533)'
                    .'|views\\-counter/([^/]++)(*:564)'
                    .'|c(?'
                        .'|ancel\\-orders/([^/]++)(*:598)'
                        .'|omment\\-recipe/([^/]++)(*:629)'
                    .')'
                    .'|rate(?'
                        .'|/([^/]++)(*:654)'
                        .'|\\-restaurant/([^/]++)(*:683)'
                    .')'
                    .'|like\\-recipe/([^/]++)(*:713)'
                    .'|get\\-re(?'
                        .'|cipe\\-by\\-level/([^/]++)(*:755)'
                        .'|sto\\-orders/([^/]++)(*:783)'
                    .')'
                    .'|add\\-to\\-recipe\\-to\\-bookmaker/([^/]++)(*:831)'
                    .'|subscribe/([^/]++)(*:857)'
                .')'
                .'|/s(?'
                    .'|how\\-(?'
                        .'|co(?'
                            .'|oking_(?'
                                .'|event/([^/]++)(*:907)'
                                .'|trick/([^/]++)(*:929)'
                            .')'
                            .'|urse/([^/]++)(*:951)'
                        .')'
                        .'|dish(?'
                            .'|_type/([^/]++)(*:981)'
                            .'|/([^/]++)(*:998)'
                        .')'
                        .'|level/([^/]++)(*:1021)'
                        .'|re(?'
                            .'|cipe(?'
                                .'|/([^/]++)(*:1051)'
                                .'|_type/([^/]++)(*:1074)'
                            .')'
                            .'|staurant/([^/]++)(*:1101)'
                        .')'
                    .')'
                    .'|earch\\-(?'
                        .'|co(?'
                            .'|oking_trick\\-by\\-name/([^/]++)(*:1157)'
                            .'|urse\\-by\\-name/([^/]++)(*:1189)'
                        .')'
                        .'|restaurant\\-by\\-name/([^/]++)(*:1228)'
                    .')'
                .')'
                .'|/update\\-(?'
                    .'|cooking_event/([^/]++)(*:1273)'
                    .'|subscription_formula/([^/]++)(*:1311)'
                .')'
                .'|/views\\-counter/([^/]++)(*:1345)'
                .'|/get\\-(?'
                    .'|r(?'
                        .'|ate(?'
                            .'|/(?'
                                .'|rate/([^/]++)(*:1390)'
                                .'|average/([^/]++)(*:1415)'
                            .')'
                            .'|\\-restaurant/(?'
                                .'|([^/]++)(*:1449)'
                                .'|average/([^/]++)(*:1474)'
                            .')'
                        .')'
                        .'|ecipe(?'
                            .'|s\\-by\\-type/([^/]++)(*:1513)'
                            .'|\\-by\\-name/([^/]++)(*:1541)'
                        .')'
                    .')'
                    .'|dishes_by_type/([^/]++)/([^/]++)(*:1584)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        54 => [[['_route' => 'delete_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        75 => [[['_route' => 'delete_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        96 => [[['_route' => 'delete_course', '_controller' => 'App\\Controller\\Course\\CourseController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        126 => [[['_route' => 'app_delete_comment_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::deleteCommentRecipe'], ['recipe_id', 'comment_id'], ['PATCH' => 0], null, false, true, null]],
        156 => [[['_route' => 'delete_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        173 => [[['_route' => 'app_delete_dish', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::deleteDish'], ['id'], ['DELETE' => 0], null, false, true, null]],
        196 => [[['_route' => 'app_delete_level', '_controller' => 'App\\Controller\\Level\\LevelController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        225 => [[['_route' => 'delete_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        247 => [[['_route' => 'delete_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::deleteRecipe'], ['id'], ['DELETE' => 0], null, false, true, null]],
        273 => [[['_route' => 'delete_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::deleteRestaurant'], ['id'], ['DELETE' => 0], null, false, true, null]],
        319 => [[['_route' => 'app_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::update'], ['id'], ['POST' => 0], null, false, true, null]],
        340 => [[['_route' => 'update_course', '_controller' => 'App\\Controller\\Course\\CourseController::updateCourse'], ['id'], ['POST' => 0], null, false, true, null]],
        371 => [[['_route' => 'app_update_comment_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::updateCommentRecipe'], ['recipe_id', 'comment_id'], ['POST' => 0], null, false, true, null]],
        401 => [[['_route' => 'update_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::updateRecipeType'], ['id'], ['POST' => 0], null, false, true, null]],
        418 => [[['_route' => 'app_get_dish_update', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::updateDish'], ['id'], ['POST' => 0], null, false, true, null]],
        448 => [[['_route' => 'update_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::updateRecipe'], ['id'], ['POST' => 0], null, false, true, null]],
        470 => [[['_route' => 'update_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::updateRecipeType'], ['id'], ['POST' => 0], null, false, true, null]],
        496 => [[['_route' => 'update_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::updateRestaurant'], ['id'], ['POST' => 0], null, false, true, null]],
        533 => [[['_route' => 'enroll_to_course', '_controller' => 'App\\Controller\\Course\\CourseController::enrollToCourse'], ['id'], ['GET' => 0], null, false, true, null]],
        564 => [[['_route' => 'views-counter-auth', '_controller' => 'App\\Controller\\FrontController::countViewAuth'], ['slug'], null, null, false, true, null]],
        598 => [[['_route' => 'app_order_cancelorder', '_controller' => 'App\\Controller\\OrderController::cancelOrder'], ['id'], null, null, false, true, null]],
        629 => [[['_route' => 'app_comment_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::commentRecipe'], ['slug'], ['POST' => 0], null, false, true, null]],
        654 => [[['_route' => 'app_rate', '_controller' => 'App\\Controller\\RateController::rate'], ['id'], ['POST' => 0], null, false, true, null]],
        683 => [[['_route' => 'app_rate_restaurant', '_controller' => 'App\\Controller\\RateRestaurantController::rate'], ['id'], ['POST' => 0], null, false, true, null]],
        713 => [[['_route' => 'app_like_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::likeRecipe'], ['id'], ['GET' => 0], null, false, true, null]],
        755 => [[['_route' => 'app_get_recipes_by_level', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeByLevel'], ['id'], ['GET' => 0], null, false, true, null]],
        783 => [[['_route' => 'app_restaurant_restaurant_getrestoorders', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestoOrders'], ['id'], null, null, false, true, null]],
        831 => [[['_route' => 'app_add_recipe_to_book_maker', '_controller' => 'App\\Controller\\Recipe\\RecipeController::addRecipeToBookMaker'], ['id'], ['GET' => 0], null, false, true, null]],
        857 => [[['_route' => 'app_subscription', '_controller' => 'App\\Controller\\SubscriptionController::subscribe'], ['subsFormulaId'], ['POST' => 0], null, false, true, null]],
        907 => [[['_route' => 'show_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        929 => [[['_route' => 'show_cooking_trick', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::show'], ['slug'], ['GET' => 0], null, false, true, null]],
        951 => [[['_route' => 'show_course', '_controller' => 'App\\Controller\\Course\\CourseController::showCourse'], ['slug'], ['GET' => 0], null, false, true, null]],
        981 => [[['_route' => 'show_dish_type', '_controller' => 'App\\Controller\\DishType\\DishTypeController::showRecipeType'], ['id'], ['GET' => 0], null, false, true, null]],
        998 => [[['_route' => 'app_get_dish_show', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::showDish'], ['id'], ['GET' => 0], null, false, true, null]],
        1021 => [[['_route' => 'app_show_level', '_controller' => 'App\\Controller\\Level\\LevelController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        1051 => [[['_route' => 'show_recipe', '_controller' => 'App\\Controller\\Recipe\\RecipeController::showRecipe'], ['slug'], ['GET' => 0], null, false, true, null]],
        1074 => [[['_route' => 'show_recipe_type', '_controller' => 'App\\Controller\\RecipeType\\RecipeTypeController::showRecipeType'], ['slug'], ['GET' => 0], null, false, true, null]],
        1101 => [[['_route' => 'show_restaurant', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::showRestaurant'], ['slug'], ['GET' => 0], null, false, true, null]],
        1157 => [[['_route' => 'search_cooking_tricks', '_controller' => 'App\\Controller\\CookingTrick\\CookingTricksController::getCookingTrickByName'], ['keyword'], ['GET' => 0], null, false, true, null]],
        1189 => [[['_route' => 'search_course_by_name', '_controller' => 'App\\Controller\\Course\\CourseController::searchCourseByName'], ['keyword'], ['GET' => 0], null, false, true, null]],
        1228 => [[['_route' => 'app_restaurant_restaurant_getrestaurantbyname', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getRestaurantByName'], ['keyword'], null, null, false, true, null]],
        1273 => [[['_route' => 'app_cooking_event', '_controller' => 'App\\Controller\\CookingEvents\\CookingEventsController::update'], ['id'], ['PATCH' => 0], null, false, true, null]],
        1311 => [[['_route' => 'update_subscription_formula', '_controller' => 'App\\Controller\\SubscriptionFormulaController::updateSubscriptionFormula'], ['id'], ['PUT' => 0], null, false, true, null]],
        1345 => [[['_route' => 'views-counter', '_controller' => 'App\\Controller\\FrontController::countView'], ['slug'], null, null, false, true, null]],
        1390 => [[['_route' => 'app_get_rate', '_controller' => 'App\\Controller\\RateController::getRecipeRate'], ['recipeID'], ['GET' => 0], null, false, true, null]],
        1415 => [[['_route' => 'app_get_rate_average', '_controller' => 'App\\Controller\\RateController::getRecipeAverage'], ['recipeID'], ['GET' => 0], null, false, true, null]],
        1449 => [[['_route' => 'app_get_rate_restaurant', '_controller' => 'App\\Controller\\RateRestaurantController::getRestaurantRate'], ['restaurantID'], ['GET' => 0], null, false, true, null]],
        1474 => [[['_route' => 'app_get_rate_restaurant_average', '_controller' => 'App\\Controller\\RateRestaurantController::getRestaurantAverage'], ['restaurantID'], ['GET' => 0], null, false, true, null]],
        1513 => [[['_route' => 'get_by_cat_recipes', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipesByType'], ['slug'], ['POST' => 0], null, false, true, null]],
        1541 => [[['_route' => 'app_get_recipes_by_name', '_controller' => 'App\\Controller\\Recipe\\RecipeController::getRecipeByName'], ['keyword'], ['GET' => 0], null, false, true, null]],
        1584 => [
            [['_route' => 'app_get_dishes_by_types', '_controller' => 'App\\Controller\\Restaurant\\RestaurantController::getDishByType'], ['id', 'resto_id'], ['GET' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
