<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* FrontOffice/partials/footer.html.twig */
class __TwigTemplate_2ee36d80002d0ace0578f84cf4ca7dcd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "FrontOffice/partials/footer.html.twig"));

        // line 1
        echo "<footer>
    <div class=\"mobile\">
        <div class=\"in\">
            <a href=\"https://www.abidjan.net/mobile\">
                <img src=\"https://abidjan.net/public/img/footer-mobile.png\" loading=\"lazy\">
                <p><em>Abidjan.net Mobile</em>Découvrez nos applications mobile pour iPhone ou Android et nos services SMS</p>
            </a>
            <a href=\"https://www.abidjan.net/mobile/\" class=\"cta sm\">En savoir plus</a>
        </div>
    </div>
    <div class=\"social\">
        <div class=\"in\">
            <div class=\"left\">
                <span>Abonnez-vous à la newsletter d'Abidjan.net !</span>
                <a href=\"https://www.abidjan.net/newsletter/\" class=\"cta sm\">Souscrire gratuitement</a>
            </div>
            <div class=\"right\">
                <div class=\"in\">
                    <span>Suivez-nous !</span>
                    <a href=\"https://www.facebook.com/abidjan.net\" target=\"_blank\" class=\"social-icon facebook-prm\"></a>
                    <a href=\"https://twitter.com/abidjan_net\" target=\"_blank\" class=\"social-icon twitter-prm\"></a>
                    <a href=\"https://www.abidjan.net/rss\" target=\"_blank\" class=\"social-icon rss-prm\"></a>
                    <a href=\"https://www.youtube.com/user/abidjannetTV\" target=\"_blank\" class=\"social-icon youtube-prm\"></a>
                </div>
            </div>
        </div>
    </div>
    <div class=\"network\">
        <div class=\"in\"><h4 class=\"section-title sub invert marginbottom\">sur le réseau afrique</h4></div>

        <!--<div class=\"grid3-3-2-1\">-->

        <!--<a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>

              <a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>

              <a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>

              <a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>

              <a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>

              <a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>-->


        <!--<script language=\"JavaScript\" src=\"https://agenda.abidjan.net/inc/inc_afrique_new_abnet.js\" async></script>-->

        <!--</div>-->

    </div>


    <div class=\"contact\">

        <div class=\"in\">

            <div class=\"left\">

                <span>Comment faire de la Publicité sur Abidjan.net ?</span>
                <a href=\"http://www.weblogymedia.com/sites-en-regie/abidjan-net/\" target=\"_blank\" class=\"cta sm invert\">Contactez-nous</a>

            </div>

            <div class=\"right\">

                <div class=\"in\">

                    <a href=\"http://www.weblogy.com/\"><img src=\"https://abidjan.net/public/img/weblogy-w.png\"></a>
                    <span>Copyright © 1998<script async=\"\">new Date().getFullYear()>1998&&document.write(\"-\"+new Date().getFullYear());</script>-2023 Weblogy Group Ltd. Tous droits réservés</span>

                </div>

            </div>

        </div>

    </div>

</footer>";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "FrontOffice/partials/footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<footer>
    <div class=\"mobile\">
        <div class=\"in\">
            <a href=\"https://www.abidjan.net/mobile\">
                <img src=\"https://abidjan.net/public/img/footer-mobile.png\" loading=\"lazy\">
                <p><em>Abidjan.net Mobile</em>Découvrez nos applications mobile pour iPhone ou Android et nos services SMS</p>
            </a>
            <a href=\"https://www.abidjan.net/mobile/\" class=\"cta sm\">En savoir plus</a>
        </div>
    </div>
    <div class=\"social\">
        <div class=\"in\">
            <div class=\"left\">
                <span>Abonnez-vous à la newsletter d'Abidjan.net !</span>
                <a href=\"https://www.abidjan.net/newsletter/\" class=\"cta sm\">Souscrire gratuitement</a>
            </div>
            <div class=\"right\">
                <div class=\"in\">
                    <span>Suivez-nous !</span>
                    <a href=\"https://www.facebook.com/abidjan.net\" target=\"_blank\" class=\"social-icon facebook-prm\"></a>
                    <a href=\"https://twitter.com/abidjan_net\" target=\"_blank\" class=\"social-icon twitter-prm\"></a>
                    <a href=\"https://www.abidjan.net/rss\" target=\"_blank\" class=\"social-icon rss-prm\"></a>
                    <a href=\"https://www.youtube.com/user/abidjannetTV\" target=\"_blank\" class=\"social-icon youtube-prm\"></a>
                </div>
            </div>
        </div>
    </div>
    <div class=\"network\">
        <div class=\"in\"><h4 class=\"section-title sub invert marginbottom\">sur le réseau afrique</h4></div>

        <!--<div class=\"grid3-3-2-1\">-->

        <!--<a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>

              <a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>

              <a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>

              <a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>

              <a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>

              <a href=\"\"class=\"grd-item ebloc-mini invert\">

                <picture><img src=\"/public/img/tmp/placeholder_16x10.png\" /></picture>
                <div class=\"txt\">
                  <span class=\"infos\">Ministères - 15/10/2018</span>
                  <span class=\"title\">Le Guest Speaker Mike Coffi DG Ecobank Asset Management face à la Presse</span>
                </div>

              </a>-->


        <!--<script language=\"JavaScript\" src=\"https://agenda.abidjan.net/inc/inc_afrique_new_abnet.js\" async></script>-->

        <!--</div>-->

    </div>


    <div class=\"contact\">

        <div class=\"in\">

            <div class=\"left\">

                <span>Comment faire de la Publicité sur Abidjan.net ?</span>
                <a href=\"http://www.weblogymedia.com/sites-en-regie/abidjan-net/\" target=\"_blank\" class=\"cta sm invert\">Contactez-nous</a>

            </div>

            <div class=\"right\">

                <div class=\"in\">

                    <a href=\"http://www.weblogy.com/\"><img src=\"https://abidjan.net/public/img/weblogy-w.png\"></a>
                    <span>Copyright © 1998<script async=\"\">new Date().getFullYear()>1998&&document.write(\"-\"+new Date().getFullYear());</script>-2023 Weblogy Group Ltd. Tous droits réservés</span>

                </div>

            </div>

        </div>

    </div>

</footer>", "FrontOffice/partials/footer.html.twig", "/Users/ouattarasidik/Documents/forDeploy/cuisine-abidjan-net/templates/FrontOffice/partials/footer.html.twig");
    }
}
