<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* FrontOffice/partials/navbar.html.twig */
class __TwigTemplate_410edfab11e1505f3ae32540b1115245 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "FrontOffice/partials/navbar.html.twig"));

        // line 1
        echo "<header id=\"menuHeader\">
\t<div class=\"menu-xs\">
\t\t<div class=\"in\">
\t\t\t<span></span>
\t\t\t<span></span>
\t\t\t<span></span>
\t\t\t<span></span>
\t\t</div>
\t</div>

\t<nav class=\"transverse\">
\t\t<span>
\t\t\tLe réseau
\t\t</span>
\t\t<a href=\"https://www.abidjan.net/\">
\t\t\tAbidjan.net
\t\t</a>
\t\t<a href=\"http://www.abamako.com/\">
\t\t\taBamako.com
\t\t</a>
\t\t<a href=\"http://www.aouaga.com/\">
\t\t\taOuaga.com
\t\t</a>
\t\t<a href=\"http://www.acotonou.com/\">
\t\t\taCotonou.com
\t\t</a>
\t\t<a href=\"http://www.alome.com/\">
\t\t\taLome.com
\t\t</a>
\t\t<a href=\"http://www.aniamey.com/\">
\t\t\taNiamey.com
\t\t</a>
\t\t<a href=\"http://www.adakar.com/\">
\t\t\taDakar.com
\t\t</a>
\t\t<a href=\"http://www.alibreville.com/\">
\t\t\taLibreville.com
\t\t</a>
\t\t<a href=\"http://www.abangui.com/\">
\t\t\taBangui.com
\t\t</a>
\t\t<a href=\"http://www.jobafrique.com/\" target=\"_blank\">
\t\t\tJobAfrique.com
\t\t</a>
\t\t<a href=\"http://www.nouchi.com/\" target=\"_blank\">
\t\t\tNouchi.com
\t\t</a>
\t</nav>

\t<div class=\"in\">
\t\t<a href=\"https://www.abidjan.net\" class=\"logo\"></a>
\t\t<nav class=\"menu\">
\t\t\t<span>
\t\t\t\t<a class=\"\">
\t\t\t\t\tAnnonces
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<!--<li><a href=\"https://annonces.abidjan.net/\">Petites Annonces</a></li>-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"http://www.immo.ci/\" target=\"_blank\">
\t\t\t\t\t\t\tImmobilier
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"http://www.automobile.ci/\" target=\"_blank\">
\t\t\t\t\t\t\tAutomobile
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://necrologie.abidjan.net\">
\t\t\t\t\t\t\tNécrologie
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://agenda.abidjan.net/\">
\t\t\t\t\t\t\tAgenda
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"http://www.jobafrique.com/\" target=\"_blank\">
\t\t\t\t\t\t\tEmplois
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<!--https://emploi.abidjan.net/-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/annonces-legales\">
\t\t\t\t\t\t\tAnnonces légales
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://monkiosk.com/home.php?cat=199\" target=\"_blank\">
\t\t\t\t\t\t\tMarchés Publics
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a href=\"https://business.abidjan.net\">
\t\t\t\t\tBusiness
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/pages-jaunes\">
\t\t\t\t\t\t\tPage jaunes
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/annonces-legales\">
\t\t\t\t\t\t\tAnnonces légales
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/qui\">
\t\t\t\t\t\t\tQui est Qui ?
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"http://www.jobafrique.com/\" target=\"_blank\">
\t\t\t\t\t\t\tEmplois
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://news.abidjan.net/chiffres-cles\">
\t\t\t\t\t\t\tChiffres clés
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<!--<li><a href=\"https://business.abidjan.net/AO/\">Appel d'offres</a></li>-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/bourse\">
\t\t\t\t\t\t\tBourse
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/panier-menagere\">
\t\t\t\t\t\t\tCoût de la vie
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a href=\"https://news.abidjan.net\">
\t\t\t\t\tNews
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<li><a href=\"https://www.monkiosk.com/\" target=\"_blank\">MonKiosk.com</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/titrologie\">Titrologie</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/depeches\">Dépêches</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/articles\">Articles</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/photos\">Galerie photos</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/videos\">Galerie vidéos</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/document\">Documents</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/dossier\">Dossiers</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/chronologie\">Chronologie</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/paroles\">Paroles fortes</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/caricatures\">Caricatures</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/cartoons\">Cartoons</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/flash-infos\">Flash Infos</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/fakenews\">Fake News</a></li>
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a href=\"https://sports.abidjan.net\">
\t\t\t\t\tSport
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<li class=\"xs\">
\t\t\t\t\t\t<a href=\"https://sports.abidjan.net\">
\t\t\t\t\t\t\tTout le sport
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<!--<li><a href=\"https://sports.abidjan.net/foot/ligue-1.asp\">Ligue 1</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"https://sports.abidjan.net/foot/can2019/\">CAN 2019</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"https://sports.abidjan.net/foot/can2017/\">CAN 2017</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"https://sports.abidjan.net/foot/can2015/\">CAN Orange 2015</a></li>-->
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a>
\t\t\t\t\tUtile
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/pharmacies-de-garde\">
\t\t\t\t\t\t\tPharmacie de garde
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://ticket.abidjan.net/\">
\t\t\t\t\t\t\tE-ticket
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://www.abidjan.net/cinema\">
\t\t\t\t\t\t\tCinéma
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https:/voyage.abidjan.net/\">
\t\t\t\t\t\t\tVoyage (billetterie)
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://agenda.abidjan.net/\">
\t\t\t\t\t\t\tAgenda
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://tv.abidjan.net/\">
\t\t\t\t\t\t\tProgramme TV
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<!--<li><a href=\"https://www.abidjan.net/voyage/\">Trafic Aérien</a></li>-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/navire\">
\t\t\t\t\t\t\tTrafic des navires
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://www.abidjan.net/trafic-routier\">
\t\t\t\t\t\t\tTrafic routier &amp; Météo
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://www.abidjan.net/mobile\">
\t\t\t\t\t\t\tMobile
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a href=\"https://civ.abidjan.net/\">
\t\t\t\t\tCôte d'Ivoire
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://civ.abidjan.net/index.php/connaitre\">
\t\t\t\t\t\t\tConnaître
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://civ.abidjan.net/index.php/visiter\">
\t\t\t\t\t\t\tVisiter
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://civ.abidjan.net/index.php/vivre\">
\t\t\t\t\t\t\tVivre
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<!--<li><a href=\"https://www.abidjan.net/elections/\">Elections</a></li>-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/gouvernement\">
\t\t\t\t\t\t\tLes gouvernements
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://news.abidjan.net/journal-officiel\">
\t\t\t\t\t\t\tJournal Officiel
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://monkiosk.com/home.php?cat=448\" target=\"_blank\">
\t\t\t\t\t\t\tCartographie
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a>
\t\t\t\t\tVous
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<!--<li><a href=\"https://forums.abidjan.net/\">Forums</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"https://chats.abidjan.net/\">Voice chat</a>-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://sondage.abidjan.net/poll_archive.asp\">
\t\t\t\t\t\t\tSondage
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://www.facebook.com/abidjan.net\" target=\"_blank\">
\t\t\t\t\t\t\tRéseaux Sociaux
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://carte.abidjan.net/commander-votre-carte/\" target=\"_blank\">
\t\t\t\t\t\t\tCarte Prépayée
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://www.apaym.com/app\" target=\"_blank\">
\t\t\t\t\t\t\tApplication Apaym
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://abidjan.net/livre/order\" target=\"_blank\">
\t\t\t\t\t\t\tLivre Abidjan.net
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://weblogy.com/nous-contacter\" target=\"_blank\">
\t\t\t\t\t\t\tContactez-Nous
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>

\t\t\t<span>
\t\t\t\t<a class=\"user-main-menu\"></a>
\t\t\t\t<ul>
\t\t\t\t\t<li id=\"seconnecter\">
\t\t\t\t\t\t<a href=\"#\" id=\"seconnecter_btn\">
\t\t\t\t\t\t\tSe connecter
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li id=\"creercompte\">
\t\t\t\t\t\t<a href=\"#\" id=\"creercompte_btn\">
\t\t\t\t\t\t\tCréer un compte
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li id=\"moncompte\" style=\"display: none;\">
\t\t\t\t\t\t<a href=\"#\" id=\"moncompte_btn\">
\t\t\t\t\t\t\tMon compte
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li id=\"deconnexion\" style=\"display: none;\">
\t\t\t\t\t\t<a href=\"#\" id=\"deconnexion_btn\">
\t\t\t\t\t\t\tDéconnexion
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>

\t\t\t<a href=\"\" class=\"search\" id=\"btnSearch\"></a>

\t\t\t<form action=\"https://news.abidjan.net/rechercher\" class=\"bloc-search\" id=\"blocSearch\">
\t\t\t\t<p>
\t\t\t\t\t<input type=\"hidden\" name=\"cx\" value=\"004748131455301746118:434f-ulwnp4\">
\t\t\t\t\t<input type=\"hidden\" name=\"ie\" value=\"UTF-8\">
\t\t\t\t\t<input type=\"text\" placeholder=\"Rechercher...\" value=\"\" name=\"q\" id=\"q\" autocomplete=\"off\">
\t\t\t\t\t<input type=\"submit\" value=\"valider\"></p>
\t\t\t</form>
\t\t</nav>
\t</div>
</header>

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "FrontOffice/partials/navbar.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header id=\"menuHeader\">
\t<div class=\"menu-xs\">
\t\t<div class=\"in\">
\t\t\t<span></span>
\t\t\t<span></span>
\t\t\t<span></span>
\t\t\t<span></span>
\t\t</div>
\t</div>

\t<nav class=\"transverse\">
\t\t<span>
\t\t\tLe réseau
\t\t</span>
\t\t<a href=\"https://www.abidjan.net/\">
\t\t\tAbidjan.net
\t\t</a>
\t\t<a href=\"http://www.abamako.com/\">
\t\t\taBamako.com
\t\t</a>
\t\t<a href=\"http://www.aouaga.com/\">
\t\t\taOuaga.com
\t\t</a>
\t\t<a href=\"http://www.acotonou.com/\">
\t\t\taCotonou.com
\t\t</a>
\t\t<a href=\"http://www.alome.com/\">
\t\t\taLome.com
\t\t</a>
\t\t<a href=\"http://www.aniamey.com/\">
\t\t\taNiamey.com
\t\t</a>
\t\t<a href=\"http://www.adakar.com/\">
\t\t\taDakar.com
\t\t</a>
\t\t<a href=\"http://www.alibreville.com/\">
\t\t\taLibreville.com
\t\t</a>
\t\t<a href=\"http://www.abangui.com/\">
\t\t\taBangui.com
\t\t</a>
\t\t<a href=\"http://www.jobafrique.com/\" target=\"_blank\">
\t\t\tJobAfrique.com
\t\t</a>
\t\t<a href=\"http://www.nouchi.com/\" target=\"_blank\">
\t\t\tNouchi.com
\t\t</a>
\t</nav>

\t<div class=\"in\">
\t\t<a href=\"https://www.abidjan.net\" class=\"logo\"></a>
\t\t<nav class=\"menu\">
\t\t\t<span>
\t\t\t\t<a class=\"\">
\t\t\t\t\tAnnonces
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<!--<li><a href=\"https://annonces.abidjan.net/\">Petites Annonces</a></li>-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"http://www.immo.ci/\" target=\"_blank\">
\t\t\t\t\t\t\tImmobilier
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"http://www.automobile.ci/\" target=\"_blank\">
\t\t\t\t\t\t\tAutomobile
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://necrologie.abidjan.net\">
\t\t\t\t\t\t\tNécrologie
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://agenda.abidjan.net/\">
\t\t\t\t\t\t\tAgenda
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"http://www.jobafrique.com/\" target=\"_blank\">
\t\t\t\t\t\t\tEmplois
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<!--https://emploi.abidjan.net/-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/annonces-legales\">
\t\t\t\t\t\t\tAnnonces légales
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://monkiosk.com/home.php?cat=199\" target=\"_blank\">
\t\t\t\t\t\t\tMarchés Publics
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a href=\"https://business.abidjan.net\">
\t\t\t\t\tBusiness
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/pages-jaunes\">
\t\t\t\t\t\t\tPage jaunes
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/annonces-legales\">
\t\t\t\t\t\t\tAnnonces légales
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/qui\">
\t\t\t\t\t\t\tQui est Qui ?
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"http://www.jobafrique.com/\" target=\"_blank\">
\t\t\t\t\t\t\tEmplois
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://news.abidjan.net/chiffres-cles\">
\t\t\t\t\t\t\tChiffres clés
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<!--<li><a href=\"https://business.abidjan.net/AO/\">Appel d'offres</a></li>-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/bourse\">
\t\t\t\t\t\t\tBourse
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/panier-menagere\">
\t\t\t\t\t\t\tCoût de la vie
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a href=\"https://news.abidjan.net\">
\t\t\t\t\tNews
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<li><a href=\"https://www.monkiosk.com/\" target=\"_blank\">MonKiosk.com</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/titrologie\">Titrologie</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/depeches\">Dépêches</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/articles\">Articles</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/photos\">Galerie photos</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/videos\">Galerie vidéos</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/document\">Documents</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/dossier\">Dossiers</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/chronologie\">Chronologie</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/paroles\">Paroles fortes</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/caricatures\">Caricatures</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/cartoons\">Cartoons</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/flash-infos\">Flash Infos</a></li>
\t\t\t\t\t<li><a href=\"https://news.abidjan.net/fakenews\">Fake News</a></li>
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a href=\"https://sports.abidjan.net\">
\t\t\t\t\tSport
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<li class=\"xs\">
\t\t\t\t\t\t<a href=\"https://sports.abidjan.net\">
\t\t\t\t\t\t\tTout le sport
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<!--<li><a href=\"https://sports.abidjan.net/foot/ligue-1.asp\">Ligue 1</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"https://sports.abidjan.net/foot/can2019/\">CAN 2019</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"https://sports.abidjan.net/foot/can2017/\">CAN 2017</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"https://sports.abidjan.net/foot/can2015/\">CAN Orange 2015</a></li>-->
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a>
\t\t\t\t\tUtile
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/pharmacies-de-garde\">
\t\t\t\t\t\t\tPharmacie de garde
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://ticket.abidjan.net/\">
\t\t\t\t\t\t\tE-ticket
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://www.abidjan.net/cinema\">
\t\t\t\t\t\t\tCinéma
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https:/voyage.abidjan.net/\">
\t\t\t\t\t\t\tVoyage (billetterie)
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://agenda.abidjan.net/\">
\t\t\t\t\t\t\tAgenda
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://tv.abidjan.net/\">
\t\t\t\t\t\t\tProgramme TV
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<!--<li><a href=\"https://www.abidjan.net/voyage/\">Trafic Aérien</a></li>-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/navire\">
\t\t\t\t\t\t\tTrafic des navires
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://www.abidjan.net/trafic-routier\">
\t\t\t\t\t\t\tTrafic routier &amp; Météo
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://www.abidjan.net/mobile\">
\t\t\t\t\t\t\tMobile
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a href=\"https://civ.abidjan.net/\">
\t\t\t\t\tCôte d'Ivoire
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://civ.abidjan.net/index.php/connaitre\">
\t\t\t\t\t\t\tConnaître
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://civ.abidjan.net/index.php/visiter\">
\t\t\t\t\t\t\tVisiter
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://civ.abidjan.net/index.php/vivre\">
\t\t\t\t\t\t\tVivre
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<!--<li><a href=\"https://www.abidjan.net/elections/\">Elections</a></li>-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://business.abidjan.net/gouvernement\">
\t\t\t\t\t\t\tLes gouvernements
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://news.abidjan.net/journal-officiel\">
\t\t\t\t\t\t\tJournal Officiel
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://monkiosk.com/home.php?cat=448\" target=\"_blank\">
\t\t\t\t\t\t\tCartographie
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>
\t\t\t<span>
\t\t\t\t<a>
\t\t\t\t\tVous
\t\t\t\t</a>
\t\t\t\t<ul>
\t\t\t\t\t<!--<li><a href=\"https://forums.abidjan.net/\">Forums</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"https://chats.abidjan.net/\">Voice chat</a>-->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://sondage.abidjan.net/poll_archive.asp\">
\t\t\t\t\t\t\tSondage
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://www.facebook.com/abidjan.net\" target=\"_blank\">
\t\t\t\t\t\t\tRéseaux Sociaux
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://carte.abidjan.net/commander-votre-carte/\" target=\"_blank\">
\t\t\t\t\t\t\tCarte Prépayée
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://www.apaym.com/app\" target=\"_blank\">
\t\t\t\t\t\t\tApplication Apaym
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://abidjan.net/livre/order\" target=\"_blank\">
\t\t\t\t\t\t\tLivre Abidjan.net
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"https://weblogy.com/nous-contacter\" target=\"_blank\">
\t\t\t\t\t\t\tContactez-Nous
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>

\t\t\t<span>
\t\t\t\t<a class=\"user-main-menu\"></a>
\t\t\t\t<ul>
\t\t\t\t\t<li id=\"seconnecter\">
\t\t\t\t\t\t<a href=\"#\" id=\"seconnecter_btn\">
\t\t\t\t\t\t\tSe connecter
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li id=\"creercompte\">
\t\t\t\t\t\t<a href=\"#\" id=\"creercompte_btn\">
\t\t\t\t\t\t\tCréer un compte
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li id=\"moncompte\" style=\"display: none;\">
\t\t\t\t\t\t<a href=\"#\" id=\"moncompte_btn\">
\t\t\t\t\t\t\tMon compte
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li id=\"deconnexion\" style=\"display: none;\">
\t\t\t\t\t\t<a href=\"#\" id=\"deconnexion_btn\">
\t\t\t\t\t\t\tDéconnexion
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</span>

\t\t\t<a href=\"\" class=\"search\" id=\"btnSearch\"></a>

\t\t\t<form action=\"https://news.abidjan.net/rechercher\" class=\"bloc-search\" id=\"blocSearch\">
\t\t\t\t<p>
\t\t\t\t\t<input type=\"hidden\" name=\"cx\" value=\"004748131455301746118:434f-ulwnp4\">
\t\t\t\t\t<input type=\"hidden\" name=\"ie\" value=\"UTF-8\">
\t\t\t\t\t<input type=\"text\" placeholder=\"Rechercher...\" value=\"\" name=\"q\" id=\"q\" autocomplete=\"off\">
\t\t\t\t\t<input type=\"submit\" value=\"valider\"></p>
\t\t\t</form>
\t\t</nav>
\t</div>
</header>

", "FrontOffice/partials/navbar.html.twig", "/Users/ouattarasidik/Documents/forDeploy/cuisine-abidjan-net/templates/FrontOffice/partials/navbar.html.twig");
    }
}
