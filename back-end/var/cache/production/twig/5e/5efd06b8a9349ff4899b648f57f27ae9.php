<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* FrontOffice/base-front.html.twig */
class __TwigTemplate_5d2b2b6775bae90216b1a34d0ad771b7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "FrontOffice/base-front.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
  <head>
    <title>
      ";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        // line 6
        echo "    </title>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0\" />
    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0\">
    <meta name=\"apple-mobile-web-app-capable\" content=\"yes\" />
    <meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\" />
    <link href=\"_themes/assets/css/theme.css\" rel=\"stylesheet\" />
    <link href=\"assets/css/custom.css\" rel=\"stylesheet\" />
    <link href=\"assets/css/font-awesome.min.css\" rel=\"stylesheet\" />
    <link rel=\"stylesheet\" href=\"assets/css/main.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/scd-main.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/main-devices.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/main-devices.css\">

    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css\" rel=\"stylesheet\" />
    <link rel=\"stylesheet\" href=\"https://pro.fontawesome.com/releases/v5.10.0/css/all.css\" integrity=\"sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p\" crossorigin=\"anonymous\" />
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css\" />
    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v6.2.0/css/all.css\" />
    <link src=\"http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" src=\"assets/font-awesome-4.7.0/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css\" integrity=\"sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==\" crossorigin=\"anonymous\" />
    
    ";
        // line 28
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 31
        echo "
    ";
        // line 32
        $this->displayBlock('javascripts', $context, $blocks);
        // line 337
        echo "
  </head>
  <body>
    ";
        // line 340
        echo twig_include($this->env, $context, "FrontOffice/partials/navbar.html.twig");
        echo "
        ";
        // line 341
        $this->displayBlock('body', $context, $blocks);
        // line 344
        echo "    ";
        echo twig_include($this->env, $context, "FrontOffice/partials/footer.html.twig");
        echo "
  </body>
</html>";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 28
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 29
        echo "        ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("app");
        echo "
    ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 32
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 33
        echo "    ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("app");
        echo "
        <script src=\"_themes/vendors/is/is.min.js\"></script>
        <script src=\"https://polyfill.io/v3/polyfill.min.js?features= window.scroll\"></script>
        <script src=\"_themes/vendors/fontawesome/all.min.js\"></script>
        <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\" integrity=\"sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj\" crossorigin=\"anonymous\"></script>
        ";
        // line 39
        echo "        ";
        // line 40
        echo "        ";
        // line 41
        echo "
        <!-- Google tag (gtag.js) -->
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-112758-1\"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-112758-1');
        </script>
        ";
        // line 52
        echo "        ";
        // line 53
        echo "
        <script>  
            document.addEventListener('DOMContentLoaded', function() {
            //Login Register
            const loginButton  = document.getElementById(\"seconnecter_btn\")
            const registerButton = document.getElementById(\"creercompte_btn\")

            loginButton.addEventListener(\"click\", (e)=>{                
            EventBus.\$emit(\"login_event\")
            })
        
            registerButton.addEventListener(\"click\", (e)=>{
            EventBus.\$emit(\"register_event\")
            })

            const myAccount = document.getElementById(\"moncompte_btn\")
            const deconnexion = document.getElementById(\"deconnexion_btn\")

            myAccount.addEventListener(\"click\", ()=>{
                EventBus.\$emit(\"my_account_event\")
            })

            deconnexion.addEventListener(\"click\", ()=>{
                EventBus.\$emit(\"deconnexion\")
            })

            var header = document.querySelector('header')
            var menu = document.querySelector('header nav.menu')

            EventBus.\$on(\"check_nav\", ()=>{
            checkNav()
            })

            checkNav()
            scroll()
            photos()
            search()
            menuxs()
        
            /*\$(\"a\").each(function () {
                if (this.href.indexOf('https://beta-business.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-business.abidjan.net', 'https://business.abidjan.net');
                } else if (this.href.indexOf('https://beta-news.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-news.abidjan.net', 'https://news.abidjan.net');
                }else if (this.href.indexOf('https://beta.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta.abidjan.net', 'https://www.abidjan.net');
                }else if (this.href.indexOf('https://beta-necrologie.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-necrologie.abidjan.net', 'https://necrologie.abidjan.net');
                }else if (this.href.indexOf('https://beta-sports.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-sports.abidjan.net', 'https://sports.abidjan.net');
                }
            });
            \$(\"link\").each(function () {
                if (this.href.indexOf('/public') != -1) {
                    this.href = this.href.replace('/public', 'https://beta-news.abidjan.net/public');
                }
            });*/
        
            // Search
            // _____________________________
        
            function search() {
        
            searchBtn = document.getElementById('btnSearch')
            searchBloc = document.getElementById('blocSearch')
        
            searchBtn.addEventListener('click', function(e) {
        
                e.preventDefault()
        
                if (searchBtn.classList.contains('on')) {
        
                searchBloc.classList.remove('show')
                searchBtn.classList.remove('on')
        
                } else {
        
                searchBloc.classList.add('show')
                searchBtn.classList.add('on')
        
                searchBloc.querySelector('input').focus()
        
                }
        
            })
        
            }

            function checkNav(){
            const moncompte = document.getElementById(\"moncompte\")
            const deconnexion = document.getElementById(\"deconnexion\")

            const loginLi  = document.getElementById(\"seconnecter\")
            const registerLi = document.getElementById(\"creercompte\")
            
            if(User.loggIn()){

                moncompte.style.display=\"block\";
                deconnexion.style.display=\"block\";

            
                loginLi.style.display=\"none\";
                registerLi.style.display=\"none\";

            }else{

                moncompte.style.display=\"none\";
                deconnexion.style.display=\"none\";

            }
            }

            // Menu Mobile
            // _____________________________
        
            function menuxs() {
        
            menuburger = document.querySelector('.menu-xs')
        
            menuburger.addEventListener('click', function() {
        
                if (menuburger.classList.contains('on')) {
        
                menu.classList.remove('show')
                menuburger.classList.remove('on')
        
                } else {
        
                menu.classList.add('show')
                menuburger.classList.add('on')
        
                }
        
            })
        
            if(isXS()||isSM()) {
        
                var links = document.querySelectorAll('header nav.menu span > a')
                Array.prototype.forEach.call(links, function(link, i) {
                // console.log(i)
                link.addEventListener('click', function(e){
                    e.preventDefault()
                    let parent = link.parentNode
                    if (parent.classList.contains('on')) {
                    parent.classList.remove('on')
                    } else {
                    parent.classList.add('on')
                    }
                })
                })
        
            }
        
            }

            // On affiche le mini-header au scroll
            // ________________________________________
        
            function scroll() {
            document.body.onscroll = function() {
        
                st = window.pageYOffset | document.body.scrollTop
        
                mini = (st>50) ? header.classList.add('mini') : header.classList.remove('mini')
        
            }
            }
        
            // Affichage de toutes les photos (page Article)
            // ____________________________________________________
        
            function photos() {
        
            if (document.getElementById('allPhotos')) {
        
                btn = document.getElementById('btnPhotos')
                all = document.getElementById('allPhotos')
        
                btn.addEventListener('click', function(e) {
        
                e.preventDefault()
                all.classList.add('show')
        
                })
        
                all.addEventListener('click', function(e) {
        
                all.classList.remove('show')
        
                })
        
            }
        
            }
        
            // Fonctions responsive
            // ____________________________________________________
        
            function isXS() {
        
                element = document.getElementById('chck-xs')

                if(typeof element != undefined && element != null){
                retour =  element.currentStyle ? element.currentStyle.display : getComputedStyle(element, null).display
                return ( retour == 'block' )
                }

            }
        
            function isSM() {
                if(typeof element != undefined && element != null){
                element = document.getElementById('chck-sm')
                retour =  element.currentStyle ? element.currentStyle.display : getComputedStyle(element, null).display
                return ( retour == 'block' )
                }
            }
            
            })

        </script>

        <script async src='https://securepubads.g.doubleclick.net/tag/js/gpt.js'></script>
        <script>
            window.googletag = window.googletag || {cmd: []};
            googletag.cmd.push(function() {
            var mapping1 = googletag.sizeMapping()
                                    .addSize([1024, 0], [[1000, 200], [970, 90], [728, 90]])
                                    .addSize([980, 0], [[1000, 200], [970, 90], [728, 90]])
                                    .addSize([640, 0], [[728, 90], [468, 60]])
                                    .addSize([0, 0], [[320, 50]])
                                    .build();
        
            var mapping2 = googletag.sizeMapping()
                                    .addSize([1024, 0], [[300, 600], [300, 250]])
                                    .addSize([980, 0], [[300, 600], [300, 250]])
                                    .addSize([640, 0], [[300, 250], [300, 600]])
                                    .addSize([0, 0], [[200, 200]])
                                    .build();
        
            var mapping3 = googletag.sizeMapping()
                                    .addSize([1024, 0], [[970, 90], [728, 90]])
                                    .addSize([980, 0], [[970, 90], [728, 90]])
                                    .addSize([640, 0], [[728, 90], [468, 60]])
                                    .addSize([0, 0], [[320, 50]])
                                    .build();
        
            var mapping4 = googletag.sizeMapping()
                                    .addSize([1024, 0], [[1, 1]])
                                    .addSize([980, 0], [[1, 1]])
                                    .addSize([640, 0], [[1, 1]])
                                    .addSize([0, 0], [])
                                    .build();
        
            googletag.defineSlot('/2760456/Abidjan_net_news_articles_ATF_top_970x90', [[1000,200],[970,90],[728,90],[468,60],[320,50]], 'div-gpt-ad-3238677-1')
                    .defineSizeMapping(mapping1)
                    .addService(googletag.pubads());
                    
            googletag.defineSlot('/2760456/Abidjan_net_news_articles_ATF_right_300x600', [[300,600],[300,250],[200,200]], 'div-gpt-ad-3238677-2')
                    .defineSizeMapping(mapping2)
                    .addService(googletag.pubads());
            googletag.defineSlot('/2760456/Abidjan_net_news_articles_BTF_Bottom_970x90', [[970,90],[728,90],[468,60],[320,50]], 'div-gpt-ad-3238677-3')
                    .defineSizeMapping(mapping3)
                    .addService(googletag.pubads());
            googletag.defineSlot('/2760456/Abidjan_net_news_habillage', [[1,1]], 'div-gpt-ad-3238677-4')
                    .defineSizeMapping(mapping4)
                    .addService(googletag.pubads());
        
            googletag.pubads().enableSingleRequest();
            googletag.pubads().collapseEmptyDivs();
            googletag.pubads().setCentering(true);
            googletag.enableServices();
            });
        </script>

        <!-- Google tag (gtag.js) -->
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-112758-1\"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-112758-1');
        </script>
    ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 341
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 342
        echo "
        ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "FrontOffice/base-front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  465 => 342,  458 => 341,  168 => 53,  166 => 52,  154 => 41,  152 => 40,  150 => 39,  141 => 33,  134 => 32,  124 => 29,  117 => 28,  105 => 5,  94 => 344,  92 => 341,  88 => 340,  83 => 337,  81 => 32,  78 => 31,  76 => 28,  52 => 6,  50 => 5,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
  <head>
    <title>
      {% block title %}{% endblock %}
    </title>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0\" />
    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0\">
    <meta name=\"apple-mobile-web-app-capable\" content=\"yes\" />
    <meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\" />
    <link href=\"_themes/assets/css/theme.css\" rel=\"stylesheet\" />
    <link href=\"assets/css/custom.css\" rel=\"stylesheet\" />
    <link href=\"assets/css/font-awesome.min.css\" rel=\"stylesheet\" />
    <link rel=\"stylesheet\" href=\"assets/css/main.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/scd-main.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/main-devices.css\">
    <link rel=\"stylesheet\" href=\"https://media-files.abidjan.net/public/css/main-devices.css\">

    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css\" rel=\"stylesheet\" />
    <link rel=\"stylesheet\" href=\"https://pro.fontawesome.com/releases/v5.10.0/css/all.css\" integrity=\"sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p\" crossorigin=\"anonymous\" />
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css\" />
    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v6.2.0/css/all.css\" />
    <link src=\"http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" src=\"assets/font-awesome-4.7.0/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css\" integrity=\"sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==\" crossorigin=\"anonymous\" />
    
    {% block stylesheets %}
        {{ encore_entry_link_tags('app') }}
    {% endblock %}

    {% block javascripts %}
    {{ encore_entry_script_tags('app') }}
        <script src=\"_themes/vendors/is/is.min.js\"></script>
        <script src=\"https://polyfill.io/v3/polyfill.min.js?features= window.scroll\"></script>
        <script src=\"_themes/vendors/fontawesome/all.min.js\"></script>
        <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\" integrity=\"sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj\" crossorigin=\"anonymous\"></script>
        {# <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js\" integrity=\"sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ\" crossorigin=\"anonymous\"></script> #}
        {# <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js\" integrity=\"sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN\" crossorigin=\"anonymous\"></script> #}
        {# <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\" integrity=\"sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV\" crossorigin=\"anonymous\"></script> #}

        <!-- Google tag (gtag.js) -->
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-112758-1\"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-112758-1');
        </script>
        {# <script src=\"_themes/assets/js/theme.js\"></script> #}
        {# <script src=\"https://media-files.abidjan.net/public/js/ui.js\"></script> #}

        <script>  
            document.addEventListener('DOMContentLoaded', function() {
            //Login Register
            const loginButton  = document.getElementById(\"seconnecter_btn\")
            const registerButton = document.getElementById(\"creercompte_btn\")

            loginButton.addEventListener(\"click\", (e)=>{                
            EventBus.\$emit(\"login_event\")
            })
        
            registerButton.addEventListener(\"click\", (e)=>{
            EventBus.\$emit(\"register_event\")
            })

            const myAccount = document.getElementById(\"moncompte_btn\")
            const deconnexion = document.getElementById(\"deconnexion_btn\")

            myAccount.addEventListener(\"click\", ()=>{
                EventBus.\$emit(\"my_account_event\")
            })

            deconnexion.addEventListener(\"click\", ()=>{
                EventBus.\$emit(\"deconnexion\")
            })

            var header = document.querySelector('header')
            var menu = document.querySelector('header nav.menu')

            EventBus.\$on(\"check_nav\", ()=>{
            checkNav()
            })

            checkNav()
            scroll()
            photos()
            search()
            menuxs()
        
            /*\$(\"a\").each(function () {
                if (this.href.indexOf('https://beta-business.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-business.abidjan.net', 'https://business.abidjan.net');
                } else if (this.href.indexOf('https://beta-news.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-news.abidjan.net', 'https://news.abidjan.net');
                }else if (this.href.indexOf('https://beta.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta.abidjan.net', 'https://www.abidjan.net');
                }else if (this.href.indexOf('https://beta-necrologie.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-necrologie.abidjan.net', 'https://necrologie.abidjan.net');
                }else if (this.href.indexOf('https://beta-sports.abidjan.net') != -1) {
                    this.href = this.href.replace('https://beta-sports.abidjan.net', 'https://sports.abidjan.net');
                }
            });
            \$(\"link\").each(function () {
                if (this.href.indexOf('/public') != -1) {
                    this.href = this.href.replace('/public', 'https://beta-news.abidjan.net/public');
                }
            });*/
        
            // Search
            // _____________________________
        
            function search() {
        
            searchBtn = document.getElementById('btnSearch')
            searchBloc = document.getElementById('blocSearch')
        
            searchBtn.addEventListener('click', function(e) {
        
                e.preventDefault()
        
                if (searchBtn.classList.contains('on')) {
        
                searchBloc.classList.remove('show')
                searchBtn.classList.remove('on')
        
                } else {
        
                searchBloc.classList.add('show')
                searchBtn.classList.add('on')
        
                searchBloc.querySelector('input').focus()
        
                }
        
            })
        
            }

            function checkNav(){
            const moncompte = document.getElementById(\"moncompte\")
            const deconnexion = document.getElementById(\"deconnexion\")

            const loginLi  = document.getElementById(\"seconnecter\")
            const registerLi = document.getElementById(\"creercompte\")
            
            if(User.loggIn()){

                moncompte.style.display=\"block\";
                deconnexion.style.display=\"block\";

            
                loginLi.style.display=\"none\";
                registerLi.style.display=\"none\";

            }else{

                moncompte.style.display=\"none\";
                deconnexion.style.display=\"none\";

            }
            }

            // Menu Mobile
            // _____________________________
        
            function menuxs() {
        
            menuburger = document.querySelector('.menu-xs')
        
            menuburger.addEventListener('click', function() {
        
                if (menuburger.classList.contains('on')) {
        
                menu.classList.remove('show')
                menuburger.classList.remove('on')
        
                } else {
        
                menu.classList.add('show')
                menuburger.classList.add('on')
        
                }
        
            })
        
            if(isXS()||isSM()) {
        
                var links = document.querySelectorAll('header nav.menu span > a')
                Array.prototype.forEach.call(links, function(link, i) {
                // console.log(i)
                link.addEventListener('click', function(e){
                    e.preventDefault()
                    let parent = link.parentNode
                    if (parent.classList.contains('on')) {
                    parent.classList.remove('on')
                    } else {
                    parent.classList.add('on')
                    }
                })
                })
        
            }
        
            }

            // On affiche le mini-header au scroll
            // ________________________________________
        
            function scroll() {
            document.body.onscroll = function() {
        
                st = window.pageYOffset | document.body.scrollTop
        
                mini = (st>50) ? header.classList.add('mini') : header.classList.remove('mini')
        
            }
            }
        
            // Affichage de toutes les photos (page Article)
            // ____________________________________________________
        
            function photos() {
        
            if (document.getElementById('allPhotos')) {
        
                btn = document.getElementById('btnPhotos')
                all = document.getElementById('allPhotos')
        
                btn.addEventListener('click', function(e) {
        
                e.preventDefault()
                all.classList.add('show')
        
                })
        
                all.addEventListener('click', function(e) {
        
                all.classList.remove('show')
        
                })
        
            }
        
            }
        
            // Fonctions responsive
            // ____________________________________________________
        
            function isXS() {
        
                element = document.getElementById('chck-xs')

                if(typeof element != undefined && element != null){
                retour =  element.currentStyle ? element.currentStyle.display : getComputedStyle(element, null).display
                return ( retour == 'block' )
                }

            }
        
            function isSM() {
                if(typeof element != undefined && element != null){
                element = document.getElementById('chck-sm')
                retour =  element.currentStyle ? element.currentStyle.display : getComputedStyle(element, null).display
                return ( retour == 'block' )
                }
            }
            
            })

        </script>

        <script async src='https://securepubads.g.doubleclick.net/tag/js/gpt.js'></script>
        <script>
            window.googletag = window.googletag || {cmd: []};
            googletag.cmd.push(function() {
            var mapping1 = googletag.sizeMapping()
                                    .addSize([1024, 0], [[1000, 200], [970, 90], [728, 90]])
                                    .addSize([980, 0], [[1000, 200], [970, 90], [728, 90]])
                                    .addSize([640, 0], [[728, 90], [468, 60]])
                                    .addSize([0, 0], [[320, 50]])
                                    .build();
        
            var mapping2 = googletag.sizeMapping()
                                    .addSize([1024, 0], [[300, 600], [300, 250]])
                                    .addSize([980, 0], [[300, 600], [300, 250]])
                                    .addSize([640, 0], [[300, 250], [300, 600]])
                                    .addSize([0, 0], [[200, 200]])
                                    .build();
        
            var mapping3 = googletag.sizeMapping()
                                    .addSize([1024, 0], [[970, 90], [728, 90]])
                                    .addSize([980, 0], [[970, 90], [728, 90]])
                                    .addSize([640, 0], [[728, 90], [468, 60]])
                                    .addSize([0, 0], [[320, 50]])
                                    .build();
        
            var mapping4 = googletag.sizeMapping()
                                    .addSize([1024, 0], [[1, 1]])
                                    .addSize([980, 0], [[1, 1]])
                                    .addSize([640, 0], [[1, 1]])
                                    .addSize([0, 0], [])
                                    .build();
        
            googletag.defineSlot('/2760456/Abidjan_net_news_articles_ATF_top_970x90', [[1000,200],[970,90],[728,90],[468,60],[320,50]], 'div-gpt-ad-3238677-1')
                    .defineSizeMapping(mapping1)
                    .addService(googletag.pubads());
                    
            googletag.defineSlot('/2760456/Abidjan_net_news_articles_ATF_right_300x600', [[300,600],[300,250],[200,200]], 'div-gpt-ad-3238677-2')
                    .defineSizeMapping(mapping2)
                    .addService(googletag.pubads());
            googletag.defineSlot('/2760456/Abidjan_net_news_articles_BTF_Bottom_970x90', [[970,90],[728,90],[468,60],[320,50]], 'div-gpt-ad-3238677-3')
                    .defineSizeMapping(mapping3)
                    .addService(googletag.pubads());
            googletag.defineSlot('/2760456/Abidjan_net_news_habillage', [[1,1]], 'div-gpt-ad-3238677-4')
                    .defineSizeMapping(mapping4)
                    .addService(googletag.pubads());
        
            googletag.pubads().enableSingleRequest();
            googletag.pubads().collapseEmptyDivs();
            googletag.pubads().setCentering(true);
            googletag.enableServices();
            });
        </script>

        <!-- Google tag (gtag.js) -->
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-112758-1\"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-112758-1');
        </script>
    {% endblock %}

  </head>
  <body>
    {{ include('FrontOffice/partials/navbar.html.twig') }}
        {% block body %}

        {% endblock %}
    {{ include('FrontOffice/partials/footer.html.twig') }}
  </body>
</html>", "FrontOffice/base-front.html.twig", "/Users/ouattarasidik/Documents/forDeploy/cuisine-abidjan-net/templates/FrontOffice/base-front.html.twig");
    }
}
