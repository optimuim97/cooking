<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerK3AHsxN\App_KernelProductionDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerK3AHsxN/App_KernelProductionDebugContainer.php') {
    touch(__DIR__.'/ContainerK3AHsxN.legacy');

    return;
}

if (!\class_exists(App_KernelProductionDebugContainer::class, false)) {
    \class_alias(\ContainerK3AHsxN\App_KernelProductionDebugContainer::class, App_KernelProductionDebugContainer::class, false);
}

return new \ContainerK3AHsxN\App_KernelProductionDebugContainer([
    'container.build_hash' => 'K3AHsxN',
    'container.build_id' => '629a1683',
    'container.build_time' => 1681202734,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerK3AHsxN');
