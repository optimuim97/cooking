

document.addEventListener('DOMContentLoaded', function() {

    var header = document.querySelector('header')
    var menu = document.querySelector('header nav.menu')

    menuxs()
    scroll()
    photos()
    search()

    // Search
    // _____________________________

    function search() {

      let searchBtn = document.getElementById('btnSearch')
      let searchBloc = document.getElementById('blocSearch')

      searchBtn.addEventListener('click', function(e) {

        e.preventDefault()

        if (searchBtn.classList.contains('on')) {

          searchBloc.classList.remove('show')
          searchBtn.classList.remove('on')

        } else {

          searchBloc.classList.add('show')
          searchBtn.classList.add('on')

          searchBloc.querySelector('input').focus()

        }

      })

    }

    // Menu Mobile
    // _____________________________

    function menuxs() {

      let menuburger = document.querySelector('.menu-xs')

      menuburger.addEventListener('click', function() {

        if (menuburger.classList.contains('on')) {

          menu.classList.remove('show')
          menuburger.classList.remove('on')

        } else {

          menu.classList.add('show')
          menuburger.classList.add('on')

        }

      })

      if(isXS()||isSM()) {

        var links = document.querySelectorAll('header nav.menu span > a')
        Array.prototype.forEach.call(links, function(link, i) {
          console.log(i)
          link.addEventListener('click', function(e){
            e.preventDefault()
            let parent = link.parentNode
            if (parent.classList.contains('on')) {
              parent.classList.remove('on')
            } else {
              parent.classList.add('on')
            }
          })
        })

      }

    }

    // On affiche le mini-header au scroll
    // ________________________________________

    function scroll() {

      document.body.onscroll = function() {

        st = window.pageYOffset | document.body.scrollTop

        mini = (st>50) ? header.classList.add('mini') : header.classList.remove('mini')

      }


    }


    // Affichage de toutes les photos (page Article)
    // ____________________________________________________

    function photos() {

      if (document.getElementById('allPhotos')) {

        btn = document.getElementById('btnPhotos')
        all = document.getElementById('allPhotos')

        btn.addEventListener('click', function(e) {

          e.preventDefault()
          all.classList.add('show')

        })

        all.addEventListener('click', function(e) {

          all.classList.remove('show')

        })

      }

    }



    // Fonctions responsive
    // ____________________________________________________

    function isXS() {

      element = document.getElementById('chck-xs')
      retour =  element.currentStyle ? element.currentStyle.display : getComputedStyle(element, null).display
      return ( retour == 'block' )

    }

    function isSM() {

      element = document.getElementById('chck-sm')
      retour =  element.currentStyle ? element.currentStyle.display : getComputedStyle(element, null).display
      return ( retour == 'block' )

    }

})
