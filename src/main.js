import Vue from "vue";
import App from "./App.vue";
import Routes from "./routes";
import Vuetify from "vuetify";
import User from "./utils/User.js";
import AppStorage from "./utils/AppStorage.js";
import Token from "./utils/Token.js";
import { Editor } from "@toast-ui/vue-editor";
import mdiVue from "mdi-vue/v2";
import * as mdijs from "@mdi/js";
import VueLoading from "vuejs-loading-plugin";
import VueMomentLocalePlugin from "vue-moment-locale";
import VueCountryCode from "vue-country-code";
import Toast from "vue-toastification";
import VModal from "vue-js-modal";
import VueMeta from "vue-meta";
import VueSweetalert2 from "vue-sweetalert2";
import vue2Dropzone from "vue2-dropzone";
import vueDebounce from "vue-debounce";
import CoolLightBox from "vue-cool-lightbox";
import LazyTube from "vue-lazytube";
import VuePageTransition from "vue-page-transition";
import shareIt from "vue-share-it";
import axios from "axios";
import VueAxios from "vue-axios";
import VuePusher from "vue-pusher";
import AdManager from 'google-ad-manager-vue';

let mappings = {
  banner:[
      { window :[0,0], sizes: [ [320,50] ] },
      { window :[980,0], sizes: [ [720,60],[728,90] ] }
  ],
  rectangle:[
      { window: [0, 0], sizes: [ [300, 250] ] },
      { window: [980, 0], sizes: [ [300, 250] ] }
  ]
}

let sizes = {
  banner: [ [720, 60],[728, 90],[320, 50] ],
  rectangle: [ [300, 250] ]
};

Vue.use(AdManager, {
  id: '2760456',
  mappings,
  sizes //optional
});

// import VS2 from "vue-script2";
// import { LMap, LTileLayer, LMarker } from "vue2-leaflet";
// import VueTinyLazyloadImg from "vue-tiny-lazyload-img";

import "leaflet/dist/leaflet.css";
import "leaflet-geosearch/dist/geosearch.css";
import "vue-js-modal/dist/styles.css";
import "sweetalert2/dist/sweetalert2.min.css";
import "vue2-dropzone/dist/vue2Dropzone.min.css";
// import "@/assets/css/main.css";

//CSS
import "leaflet/dist/leaflet.css";
import "vue-toastification/dist/index.css";
import "vue-multiselect/dist/vue-multiselect.min.css";
import "vue-cool-lightbox/dist/vue-cool-lightbox.min.css";
// import "../../node_modules/nprogress/nprogress.css";

// import Vue
const options = {
  // duration: 1000,
  // timeout: 2000,
  closeOnClick: true,
  draggable: true,
  draggablePercent: 0.33,
  hideProgressBar: false,
  closeButton: "button",
  icon: true,
  rtl: false,
  // position: "top",
  // pauseOnFocusLoss: false,
  // showCloseButtonOnHover: true,
  // pauseOnHover: true,
  // position: 'POSITION.TOP_RIGTH'
};

Vue.use(Toast, options);
Vue.use(VueAxios, axios);
const moment = require("moment");
require("moment/locale/fr");
Vue.use(VueMomentLocalePlugin, {
  lang: "fr",
});
// overwrite defaults
Vue.use(VueLoading, {
  text: "Chargement ....", // default 'Loading'
  background: "rgb(255,255,255)",
});
Vue.use(vueDebounce);
Vue.use(require("vue-moment"), {
  moment,
});
Vue.use(mdiVue, {
  icons: mdijs,
});
Vue.use(Vuetify);
Vue.use(Editor);
Vue.use(VueCountryCode);
Vue.use(VModal);
Vue.use(VueMeta);
Vue.use(vue2Dropzone);
Vue.use(CoolLightBox);
Vue.use(LazyTube);
Vue.use(shareIt);
Vue.use(VuePusher, {
  app_id: "1125560",
  key: "97dc542e7823683daa99",
  secret: "aa76c69f4db6bbd8a3ce",
  api_key: "1125560",
  options: {
    cluster: "eu",
    encrypted: false,
  },
});
Vue.use(VueSweetalert2, { confirmButtonColor: "#D21D3A" });
Vue.use(VuePageTransition);

// app_id = "1125560"
// key = "97dc542e7823683daa99"
// secret = "aa76c69f4db6bbd8a3ce"
// cluster = "eu"

Vue.prototype.User = User;
const Bus = new Vue();
Vue.prototype.EventBus = Bus;

const JWTToken = `Bearer ${localStorage.getItem("token")}`;
// const baseUrl = "http://localhost:8000/";
const baseUrl = "https://new-cuisine.abidjan.net/back-end";

// https://abjcuisine.weblogy.net/back-end/cuisine-abidjan/
// const baseUrl = "https://abjcuisine.weblogy.net/back-end";
// const baseUrl = "https://abjcuisine.weblogy.net/back-end/cuisine-abidjan/";
axios.defaults.baseURL = baseUrl;
axios.defaults.headers.common["Authorization"] = JWTToken;

Vue.prototype.$axios = axios;
Vue.prototype.Token = Token;
Vue.prototype.AppStorage = AppStorage;
Vue.prototype.User = User;
Vue.prototype.baseUrl = baseUrl;
Vue.prototype.abj = "https://abidjan.net"

// Declaration
Vue.filter("strLimit", function (value, size) {
  if (!value) return "";
  value = value.toString();

  if (value.length <= size) {
    return value;
  }
  return value.substr(0, size) + " ...";
});

Vue.filter("formatAmount", function (value) {
  if (!value) return "";
  let formatedValue = new Intl.NumberFormat().format(value);
  return formatedValue;
});

Vue.config.productionTip = false;

const app = new Vue({
  render: (h) => h(App),
  router: Routes,
  created() {
    /* this.EventBus.$on("login_event", () => {
      this.$router.push({ name: "Login" });
    });

    this.EventBus.$on("register_event", () => {
      this.$router.push({ name: "Register" });
    });

    this.EventBus.$on("my_account_event", () => {
      this.$router.push({ name: "Dashboard" });
    });

    this.EventBus.$on("deconnexion", () => {
      this.$router.push({ name: "Logout" });
    }); */
  },
}).$mount("#app");

export default app;
