import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from './Login.vue'
import Logout from './Logout.vue'
import Register from './Register.vue'
import Dashboard from './components/pages/user/Dashboard.vue'
import UserProfil from './components/pages/profil/UserProfilRecipe.vue'
import TestComponent from './components/test/TestComponent.vue'
import Category from './components/pages/category/Category.vue'
import Accueil from "./components/pages/accueil/Accueil.vue"
import CookingTricks from './components/pages/cookingTricks/CookingTricks.vue'
import CookingTrickDetails from './components/pages/cookingTricks/CookingTrickDetails.vue'
import PostCookingTrick from './components/pages/cookingTricks/PostCookingTricks.vue'
import EditCookingTrick from './components/pages/cookingTricks/EditCookingTrick.vue'
import CookingEvent from './components/pages/cookingEvent/CookingEvent.vue'
import PostCookingEvent from './components/pages/cookingEvent/PostCookingEvent.vue'
import CookingEventDetails from './components/pages/cookingEvent/CookingEventDetails.vue'
// import Home from './components/views/Home'
import RecipeDetails from './components/pages/recipe/Details.vue'
import PostRecipe from './components/pages/recipe/PostRecipe.vue'
import EditRecipe from './components/pages/recipe/EditRecipe.vue'
import Restaurants from "./components/pages/restaurant/Restaurants.vue"
import RestaurantDetails from "./components/pages/restaurant/RestaurantDetails.vue"
import PostRestoDish from "./components/pages/restaurant/dish/PostRestoDish.vue"
import EditRestoDish from "./components/pages/restaurant/dish/EditRestoDish.vue"
import CookingCourse from "./components/pages/course/Course.vue"
import EditCookingCourse from "./components/pages/course/EditCookingCourse.vue"
import CookingCourseDetails from "./components/pages/course/CourseDetails.vue"
import PostCourse from "./components/pages/course/PostCourse.vue"
import PasswordForget from "./PasswordForget.vue"
import addNewPassword from "./addNewPassword.vue"
import Lesson from './components/pages/course/_partials/Lesson.vue'
import NotFoundPage from './components/pages/errors/NotFoundPage.vue'

const router = new VueRouter({
    // hashbang: true,
    mode: 'history',
    // history: true,
    // mode: 'hash',
    beforeEach(toRoute, fromRoute, next) {
        window.document.title = toRoute.meta && toRoute.meta.title ? toRoute.meta.title : 'Home';
        next();
    },
    routes: [
        {
            path: '/accueil',
            name: 'Accueil',
            component: Accueil
        },
        {
            path: '/restaurants',
            name: 'Restaurants',
            component: Restaurants
        },
        {
            path: '/restaurant/:slug',
            name: 'RestaurantDetails',
            component: RestaurantDetails
        },
        {
            path: '/restaurant-ajouter-plat/:id',
            name: 'PostRestoDish',
            component: PostRestoDish
        },
        {
            path: '/restaurant-editer-plat/:id/:dish_id',
            name: 'EditRestoDish',
            component: EditRestoDish
        },
        {
            path: '/',
            redirect: { name: 'Accueil' }
        },
        {
            path: '/astuces-culinaire',
            name: 'CookingTricks',
            component: CookingTricks
        },
        {
            path: '/publier-astuces-culinaire',
            name: 'PostCookingTrick',
            component: PostCookingTrick,
        },
        {
            path: '/astuce-culinaire/:slug',
            name: 'CookingTrickDetails',
            component: CookingTrickDetails,
        },
        {
            path: '/editer-culinaire/:slug',
            name: 'EditCookingTrick',
            component: EditCookingTrick,
        },
        {
            path: '/evenements-culinaire',
            name: 'CookingEvent',
            component: CookingEvent,
        },
        {
            path: '/event-culinaire/:id',
            name: 'CookingEventDetails',
            component: CookingEventDetails,
        },
        {
            path: '/ajouter-culinaire',
            name: 'PostCookingEvent',
            component: PostCookingEvent,
        },
        {
            path: '/cours-de-cuisines',
            name: 'CookingCourse',
            component: CookingCourse,
        },
        {
            path: '/cours-de-cuisine/:slug',
            name: 'CookingCourseDetails',
            component: CookingCourseDetails,
        },
        {
            path: '/editer-cours-de-cuisine/:slug',
            name: 'EditCookingCourse',
            component: EditCookingCourse,
        },
        {
            path: '/ajouter-cours-cuisine',
            name: 'PostCourse',
            component: PostCourse,
        },
        {
            path: '/test',
            name: 'Test',
            component: TestComponent
        },
        {
            path: '/publier-recette',
            name: 'PostRecipe',
            component: PostRecipe,
        },
       {
            path: '/editer-recette/:slug',
            name: 'EditRecipe',
            component: EditRecipe,
        }, 
        {
            path: '/tableau-de-bord',
            name: 'Dashboard',
            component: Dashboard,
            alias : "/compte",
        },
        {
            path: '/se-connecter',
            name: 'Login',
            component: Login,
        },
        {
            path: '/s-inscrire',
            name: 'Register',
            component: Register,
        },
        {
            path: '/details-recette/:slug',
            name: 'RecipeDetails',
            component: RecipeDetails,
        },
        {
            path: '/categorie/:slug',
            name: 'Category',
            component: Category,
        },
        {
            path: '/logout',
            component: Logout,
            name : "Logout",
        },
        {
            path: '/profil-utilisateur/:email',
            component: UserProfil,
            name : "UserProfil",
        },
        {
            path: '/mot-de-passe-oublie',
            component: PasswordForget,
            name : "PasswordForget",
            alias : "passwordforget"
        },
        {
            path: '/creer-mot-de-passe/:id/:token',
            component: addNewPassword,
            name : "addNewPassword",
            alias : "addnewpassword"
        },
        {
            path: '/contenu-cours',
            component: Lesson,
            name : "contenuCours",
        } ,
        { path: "*", component: NotFoundPage }

    ],
    linkActiveClass: "active__link",
    scrollBehavior() {
        return { x: 0, y: 0 }
    }
})

Vue.use(VueRouter)

export default router