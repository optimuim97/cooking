class AppStorage{
    
    setOrderingDish(data){
        localStorage.setItem('orderingDish', data)
    }

    getOrderingDish(){
        return localStorage.getItem('orderingDish')
    }

    saveUserData(){
        //TODO GetUserDatas
    }

    getPreviewPageName(){
        return localStorage.getItem('previewPage')
    }

    setPreviewRoutePath(fullPath){
        return localStorage.setItem('previewRoutefullPath', fullPath)
    }

    setCurrentDash(currentDash){
        localStorage.setItem('currentDash', currentDash)
    }
 
    getCurrentDash(){
        return localStorage.getItem('currentDash')
    }

    getPreviewRoutePath(){
        return localStorage.getItem('previewRoutefullPath')
    }

    setPreviewPageName(previewPage){
         localStorage.setItem('previewPage', previewPage)
    }
    
    storeToken(token){
        localStorage.setItem('token',token);
    }

    storeUser(user){
        localStorage.setItem('user',user);
    }

    storeUserData(userData){
    
        localStorage.setItem('firstname',userData.firstname)
        localStorage.setItem('lastname',userData.lastname)
        localStorage.setItem('avatar_url', userData.avatar_url == "" ? "" : userData.avatarUrl)

    }

    getFullName(){
        const firstname = localStorage.getItem('firstname')
        const lastname = localStorage.getItem('lastname')

        if(typeof firstname == 'undefined'  || typeof lastname == 'undefined' || firstname == 'undefined' || lastname== 'undefined'){

            return '';

        }else{

            return `${firstname} ${lastname}`
            
        }

    }

    getAvatar(){
        const avatar = localStorage.getItem('avatar_url')
        return avatar;
    }

    storeData(user,token){
        this.storeUser(user);
        this.storeToken(token);
    }

    clear(){
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        localStorage.removeItem('firstname');
        localStorage.removeItem('lastname');
        localStorage.removeItem('avatar_url');

        localStorage.removeItem('previewRoutefullPath');
        localStorage.removeItem('previewPage');
        localStorage.removeItem('orderingDish');
        localStorage.removeItem('currentDash');
    }

    clearSoft(){
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        localStorage.removeItem('firstname');
        localStorage.removeItem('lastname');
        localStorage.removeItem('avatar_url');

        localStorage.removeItem('orderingDish');
        localStorage.removeItem('currentDash');

    }

    getDatas()
    {
        let user = localStorage.getItem('user');
        let token = localStorage.getItem('token')

        console.log(user, token)

        return {
            "user" : user,
            "token" : token
        }
    }

    getUser(){
        const user = localStorage.getItem('user')

        // alert(user)
        
        return user;
    }

    getToken(){
        const token = localStorage.getItem('token')
        return token;
    }

    setCookie(name,value,days, domain = '') {
        
        var expires = "";

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }

        document.cookie = name + "=" + (value || "")  + expires + ";path=/";
        document.cookie = name + "=" + (value || "")  + expires + `domain= ${domain == '.weblogy.net' }; path=/`;
        
    }

    getCookie(name) {

        var nameEQ = name + "=";
        var ca = document.cookie.split(';');

        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }

        return null;

    }

    deleteAllCookies() {

        var cookies = document.cookie.split(";");

        console.log(cookies)
    
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
        
    }

    deleteCookie( name, path, domain) {
        // if( get_cookie( name ) ) {
          document.cookie = name + "=" +
            ((path) ? ";path="+path:"")+
            ((domain)?";domain="+domain:"") +
            ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
        // }
    }

}

// eslint-disable-next-line no-class-assign
export default AppStorage = new AppStorage()