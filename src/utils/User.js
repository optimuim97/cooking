import AppStorage from "./AppStorage.js";
import Token from "./Token.js";

class User {

    hasToken() {
        const storedToken = AppStorage.getToken();
        const username  = AppStorage.getUser()

        if (storedToken) {
            return Token.isValid(storedToken, username) ? true : false;
        }

        return false;   
    }

    loggIn() {
        return this.hasToken();
    }

    redirectToHome(){
        window.location="/"
    }

    logout() {
        AppStorage.clear();
        window.location = "/";
    }

    name() {
        if (this.loggIn()) {
            return AppStorage.getUser();
        }
    }

    user(){
        if (this.loggIn()) {
            return AppStorage.getUser()
        } else {
            console.log("User is not loggin");
        }
    }

    id() {
        if (this.loggIn()) {

            const token = AppStorage.getToken();
            const payload = Token.payload(token);

            if (Token.isValid(token)) {
                return payload.sub;
            }

        } else {
            
            // alert("User is not loggin");
            console.log("User is not loggin");
            
        }
    }

    own(username){
        return this.user() == username
    }
    
}

// eslint-disable-next-line no-class-assign
export default User = new User();
